<apex:page controller="PendingApprovalforFinanceController" lightningStylesheets="true" title="Finance Invoice Overview" showHeader="false" sidebar="false" applyHtmlTag="True" docType="html-5.0">
    <html>

        <div class="slds-border_top slds-border_bottom slds-border_left slds-border_right" style="border-width: 2px;">
            <div class="slds-page-header">
              <div class="slds-page-header__row">
                <div class="slds-page-header__col-title">
                  <div class="slds-media">
                    <div class="slds-media__body">
                      <div class="slds-page-header__name">
                        <div class="slds-page-header__name-title">
                          <h1>
                            <span class="slds-page-header__title slds-truncate" title="Finance Invoice Overview">Finance Manager</span>
                          </h1>
                        </div>
                      </div>
                      <p class="slds-page-header__name-meta">Invoice Overview</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <head>
                <apex:slds />
                <script>
                    function showTab(tabName, element){
                         if(tabName=='first_tab') {
                            document.getElementById('second_tab_header').classList.remove('slds-is-active');
                            document.getElementById('third_tab_header').classList.remove('slds-is-active');
                            document.getElementById('first_tab_header').classList.add('slds-is-active');
                            document.getElementById('second_tab').classList.remove('slds-show');
                            document.getElementById('second_tab').classList.add('slds-hide');
                            document.getElementById('first_tab').classList.remove('slds-hide');
                            document.getElementById('first_tab').classList.add('slds-show');
                            document.getElementById('third_tab').classList.remove('slds-show');
                            document.getElementById('third_tab').classList.add('slds-hide');
                        }if(tabName=='second_tab'){
                            document.getElementById('first_tab_header').classList.remove('slds-is-active');
                            document.getElementById('third_tab_header').classList.remove('slds-is-active');
                            document.getElementById('second_tab_header').classList.add('slds-is-active'); 
                            document.getElementById('first_tab').classList.remove('slds-show');
                            document.getElementById('first_tab').classList.add('slds-hide');
                            document.getElementById('second_tab').classList.remove('slds-hide');
                            document.getElementById('second_tab').classList.add('slds-show');
                            document.getElementById('third_tab').classList.remove('slds-show');
                            document.getElementById('third_tab').classList.add('slds-hide');
                        }if(tabName=='third_tab'){
                            document.getElementById('first_tab_header').classList.remove('slds-is-active');
                            document.getElementById('second_tab_header').classList.remove('slds-is-active');
                            document.getElementById('third_tab_header').classList.add('slds-is-active'); 
                            document.getElementById('first_tab').classList.remove('slds-show');
                            document.getElementById('first_tab').classList.add('slds-hide');
                            document.getElementById('second_tab').classList.remove('slds-show');
                            document.getElementById('second_tab').classList.add('slds-hide');
                            document.getElementById('third_tab').classList.remove('slds-hide');
                            document.getElementById('third_tab').classList.add('slds-show');
                        }
                    }
                </script>
            </head> 
            <body>
                <div class="slds-tabs_card" >
                    <ul class="slds-tabs_scoped__nav" role="tablist" >
                        <li id="first_tab_header" class="slds-tabs_scoped__item slds-is-active" title="First Stage Question" role="presentation" style="width:50%;">
                            <a class="slds-tabs_scoped__link" href="javascript:;" role="tab" tabindex="-1" aria-selected="false" aria-controls="second_tab" id="second_tab__item" onclick="showTab('first_tab',this);">Pending Approval - ({!totalBillableHours})</a>
                        </li>
                        <li id="second_tab_header" class="slds-tabs_scoped__item" title="Second Stage Question" role="presentation" style="width:50%;">
                            <a class="slds-tabs_scoped__link" href="javascript:;" role="tab" tabindex="0" aria-selected="true" aria-controls="first_tab" id="second_tab__item" onclick="showTab('second_tab',this);">Pending Invoice - ({!totalPendingBillableHours})</a>
                        </li>
                        <li id="third_tab_header" class="slds-tabs_scoped__item" title="Second Stage Question" role="presentation" style="width:50%;">
                            <a class="slds-tabs_scoped__link" href="javascript:;" role="tab" tabindex="0" aria-selected="flase" aria-controls="third_tab" id="third_tab__item" onclick="showTab('third_tab',this);">Invoiced Time Cards</a>
                        </li>
                    </ul>
                    <div id="first_tab" class="slds-tabs_default__content slds-show" role="tabpanel" aria-labelledby="first_tab__item">
                        <apex:form >

                            <div class="slds-notify slds-notify_alert slds-theme_alert-texture slds-theme_warning" role="alert" style="{!IF((errorMessage!=null), 'display:block', 'display:none')}">
                                {!errorMessage}
                            </div>

                            <div class="slds-grid slds-gutters" style="padding-left: 5%;">
                                <div class="slds-form-element slds-col slds-size_1-of-6" id="filterDiv">
                                    <label class="slds-form-element__label" for="select-01">Project Name</label>
                                    <div class="slds-form-element__control">
                                        <div class="slds-select_container">
                                            <apex:selectList styleclass="slds-select" value="{!projectCriteraString}" size="1">
                                                <apex:selectOptions value="{!projectNameOptionsList}"/>
                                            </apex:selectList>  
                                        </div>
                                    </div>
                                </div>
                                <div class="slds-form-element slds-col slds-size_1-of-6" id="showMe">
                                    <label class="slds-form-element__label" for="select-02">Timesheet Status</label>
                                    <div class="slds-form-element__control">
                                        <div class="slds-select_container">
                                            <apex:selectList styleclass="slds-select" value="{!statusCriteraString}" size="1">
                                                <apex:selectOption itemValue="" itemLabel="Please select"/>
                                                <apex:selectOption itemValue="Assigned" itemLabel="Assigned"/>
                                                <apex:selectOption itemValue="Submitted" itemLabel="Submitted"/>
                                                <apex:selectOption itemValue="Draft" itemLabel="Draft"/>
                                                <apex:selectOption itemValue="Rejected" itemLabel="Rejected"/>
                                            </apex:selectList> 
                                        </div>
                                    </div>
                                </div>
                                <div class="slds-form-element slds-col slds-size_1-of-6">
                                    <label class="slds-form-element__label" for="select-03">Resource Name</label>
                                    <div class="slds-form-element__control">
                                        <div class="slds-select_container">
                                            <apex:selectList styleclass="slds-select" value="{!resourceCriteraString}" size="1">
                                                <apex:selectOptions value="{!resourceNameOptionsList}"/>
                                            </apex:selectList>  
                                        </div> 
                                    </div>
                                </div>
                                <div class="slds-form-element slds-col slds-size_1-of-6">
                                    <label class="slds-form-element__label" for="select-01">Start Date</label>
                                    <apex:input type="date" styleclass="slds-input" value="{!startDateFilter}"  />
                                </div>
                                <div class="slds-form-element slds-col slds-size_1-of-6">
                                    <label class="slds-form-element__label" for="select-01">End Date</label>
                                    <apex:input type="date" styleclass="slds-input" value="{!endDateFilter}" /> 
                                </div>
                                <div class="slds-form-element slds-col slds-size_1-of-6" style="padding-top: 22px;">
                                    <apex:commandLink styleClass="slds-button slds-button_outline-brand" value="Search" action="{!filterMethod}" />
                                    <apex:commandLink styleClass="slds-button slds-button_outline-brand" value="Reset" action="{!resetFiltersMethod}" />
                                </div>
                            </div>
                            <br/>
                            <apex:pageBlock rendered="{!timeCardWrapperList.size>0}" id = "bockDiv">
                                <div class="slds-scrollable">
                                <table id="table1" class="slds-scrollable slds-table slds-table_bordered slds-table_cell-buffer" >
                                    <thead>
                                        <tr class="slds-text-title_caps" style="height: 40px">
                                           <th scope="col">
                                                <div class="slds-truncate" title="Project" style="text-align:center;">Project</div>
                                            </th>
                                            <th scope="col">
                                                <div class="slds-truncate" title="Resource" style="text-align:center;">Resource</div>
                                            </th>
                                            <th scope="col">
                                                <div class="slds-truncate" title="Budgeted Hours" style="text-align:center;">Budgeted Hours</div>
                                            </th>
                                            <th scope="col">
                                                <div class="slds-truncate" title="Billable Hours" style="text-align:center;">Billable Hours</div>
                                            </th>
                                            <th scope="col">
                                                <div class="slds-truncate" title="Date" style="text-align:center;">Date</div>
                                            </th>
                                             <th scope="col">
                                                <div class="slds-truncate" title="Status" style="text-align:center;">Status</div>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <apex:repeat value="{!timeCardWrapperList}" var="timeCards">
                                            <tr class="slds-hint-parent" style="height: 30px">
                                                <td data-label="Project">
                                                    <div class="slds-truncate" title="{!timeCards.relatedAssignment.Project__r.Name}" style="text-align:center;">{!timeCards.relatedAssignment.Project__r.Name}</div>
                                                </td>
                                                <td data-label="Resource">
                                                    <div class="slds-truncate" title="{!timeCards.resourceNameString}" style="text-align:center;">{!timeCards.resourceNameString}</div>
                                                </td>
                                                <td data-label="Budgeted Hours">
                                                    <div class="slds-truncate" title="{!timeCards.relatedAssignment.Budgeted_Hours__c}" style="text-align:center;">{!timeCards.relatedAssignment.Budgeted_Hours__c}</div>
                                                </td>
                                                <td data-label="Billable Hours">
                                                    <div class="slds-truncate" style="text-align:center;">
                                                        <apex:repeat value="{!timeCards.assignmentWithTimeCardsMap}" var="temp">
                                                            <apex:repeat value="{!timeCards.assignmentWithTimeCardsMap[temp]}" var="temp1">
                                                                    <apex:outputText value="{!temp1.EP_Total_Number_of_Billable_Hours__c}">
                                                                    <apex:param value="{!temp1.EP_Total_Number_of_Billable_Hours__c}" />
                                                                    </apex:outputText>
                                                                <br/>
                                                            </apex:repeat>
                                                        </apex:repeat>
                                                    </div>
                                                </td>
                                                <td data-label="Date">
                                                    <div class="slds-truncate" style="text-align:center;">
                                                        <apex:repeat value="{!timeCards.assignmentWithTimeCardsMap}" var="temp">
                                                            <apex:repeat value="{!timeCards.assignmentWithTimeCardsMap[temp]}" var="temp1">
                                                                    <apex:outputText value="{0,date,MM/dd/yy} to {1,date,MM/dd/yy}">
                                                                        <apex:param value="{!temp1.Start_Date__c}" />
                                                                        <apex:param value="{!temp1.EP_End_Date__c}" />
                                                                    </apex:outputText>
                                                                <br/>
                                                            </apex:repeat>
                                                        </apex:repeat>
                                                    </div>
                                                </td>
                                                <td data-label="Status">
                                                    <div class="slds-truncate" style="text-align:center;">
                                                        <apex:repeat value="{!timeCards.assignmentWithTimeCardsMap}" var="temp">
                                                            <apex:repeat value="{!timeCards.assignmentWithTimeCardsMap[temp]}" var="temp1">
                                                                    <apex:outputText value="{!temp1.Status__c}">
                                                                    <apex:param value="{!temp1.Status__c}" />
                                                                    </apex:outputText>
                                                                <br/>
                                                            </apex:repeat>
                                                        </apex:repeat>
                                                    </div>
                                                </td>
                                            </tr>
                                        </apex:repeat>
                                    </tbody>
                                </table> 
                            </div>
                            </apex:pageBlock>
                        </apex:form>
                    </div>
                    <div id="second_tab" class="slds-tabs_default__content slds-hide" role="tabpanel" aria-labelledby="second_tab__item">
                       <apex:include PageName="PendingInvoiceforFinance" />
                    </div>
                    <div id="third_tab" class="slds-tabs_default__content slds-hide" role="tabpanel" aria-labelledby="third_tab__item">
                       <apex:include PageName="InvoicedCardsPage" />
                    </div>
                </div>
            </body>
        </div>
    </html>
</apex:page>