trigger assignmentTrigger on Assignment__c (before insert, after insert, before update, after update) {
    
    if(Trigger.isInsert && Trigger.isBefore){
        AssignmentTriggerHandler.beforeInsertHandler(Trigger.New);
    }
    if(Trigger.isUpdate && Trigger.isBefore){
        AssignmentTriggerHandler.beforeUpdatHandler(Trigger.New, Trigger.oldMap);
    }
    if(Trigger.isInsert && Trigger.isAfter){
        AssignmentTriggerHandler.afterInsertHandler(Trigger.New);
        calenderViewReportTriggerHandler.calenderView(trigger.new); 
    }
    
}