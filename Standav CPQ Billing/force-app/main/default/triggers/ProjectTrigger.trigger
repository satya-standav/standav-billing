/**
*  @Class Name: ProjectTrigger  
*  @Description: Trigger on Project Object
*  @Company: Standav
*  @CreatedDate: 25/02/2019
*  Change Log
*  -----------------------------------------------------------
*  Developer           Modification Date           Comments
*  -----------------------------------------------------------
*  Anand M             25/02/2019                 Original Version
*/
trigger ProjectTrigger on Project__c (after update, after insert, before insert, before update) {
    if(Trigger.isInsert && Trigger.isBefore){
        projectsTriggerHandler.beforeInsertHandler(Trigger.New);
    }
    if(Trigger.isUpdate && Trigger.isBefore){
        projectsTriggerHandler.beforeUpdatHandler(Trigger.New, Trigger.oldMap);
    }
    if(Trigger.isInsert && Trigger.isAfter){
        projectsTriggerHandler.afterInsertHandler(Trigger.New);
    }
    if(Trigger.isUpdate && Trigger.isAfter){
        projectsTriggerHandler.afterUpdateHandler(Trigger.New, Trigger.oldMap);
    }

}