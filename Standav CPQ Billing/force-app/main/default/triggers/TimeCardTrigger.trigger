trigger TimeCardTrigger on Time_Card__c (after update, after insert) {
    if(Trigger.isAfter && Trigger.isUpdate){
        TimeCardTriggerHandler.afterUpdateHandler(Trigger.New, Trigger.oldMap);
        UsageCreationTriggerHandler.CreateUsage(Trigger.new);
    }
    if(Trigger.isAfter && Trigger.isinsert){
        TimeCardTriggerHandler.afterInsertHandler(Trigger.New);

    }
}