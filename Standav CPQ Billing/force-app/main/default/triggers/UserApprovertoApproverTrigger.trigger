trigger UserApprovertoApproverTrigger on Approvers_Flow__c (before insert,before update){
     
    set<id> assignIds=new set<id>();
    set<id> conIds=new set<id>();
    Map<ID,ID> usercontactMap = new Map<ID,ID>();
    Map<ID,ID> contactuserMap = new Map<ID,ID>();
    for(Approvers_Flow__c app : Trigger.new){
        system.debug('----------app 0 -------------'+app);
        if(app.User_Approver__c != null){
            assignIds.add(app.User_Approver__c);
        }if(app.Client_Approver__c != null){
            conIds.add(app.Client_Approver__c);
        }
    }
    system.debug('----------assignIds-------------'+assignIds);
    system.debug('------------conIds-----------'+conIds);
    if(assignIds.size() > 0 || conIds.size() > 0){
        for(Contact con : [SELECT ID,User_ID__c,LastName FROM Contact WHERE (User_ID__c in:assignIds OR ID in: conIds)]){
            system.debug('-------------con----------'+con);
            if(con.User_ID__c != NULL && con.ID != NULL){
                contactuserMap.put(con.ID,con.User_ID__c);
                usercontactMap.put(con.User_ID__c,con.ID);
            }
        } 
    }
    system.debug('-----------contactuserMap------------'+contactuserMap);
    system.debug('-----------usercontactMap------------'+usercontactMap);
    if(Trigger.isInsert && Trigger.isBefore){
        for(Approvers_Flow__c app : Trigger.new){
            system.debug('--------app---------------'+app);
            if(app.User_Approver__c == NULL && app.Client_Approver__c != NULL && contactuserMap.get(app.Client_Approver__c) != null){
                app.User_Approver__c = contactuserMap.get(app.Client_Approver__c);
            }
            else if (app.User_Approver__c != NULL && app.Client_Approver__c == NULL && usercontactMap.get(app.User_Approver__c) != null){
                app.Client_Approver__c = usercontactMap.get(app.User_Approver__c);
            }
            system.debug('---------app 1--------------'+app);
        }
    }
    if(Trigger.isUpdate && Trigger.isBefore){
       for(Approvers_Flow__c app : Trigger.new){
           system.debug('---------app 2--------------'+app);
            Approvers_Flow__c AF = Trigger.oldMap.get(app.ID);
            if (app.User_Approver__c != AF.User_Approver__c){
                app.Client_Approver__c = usercontactMap.get(app.User_Approver__c);
            }
            else if (app.Client_Approver__c != AF.Client_Approver__c && contactuserMap.get(app.Client_Approver__c) != null){
                 app.User_Approver__c = contactuserMap.get(app.Client_Approver__c);
            }
            else if (app.Client_Approver__c != AF.Client_Approver__c && contactuserMap.get(app.Client_Approver__c) == null){
                app.User_Approver__c = NULL;
            }
            system.debug('---------app 3--------------'+app);
        }
    }       
}