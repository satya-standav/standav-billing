trigger ResourceTrigger on Resource__c (before insert, before delete) {
    if(Trigger.isInsert && Trigger.isBefore){
        ResourcesTriggerHandler.beforeInsertHandler(Trigger.New);
    }
    if(Trigger.isDelete && Trigger.isBefore){
        ResourcesTriggerHandler.beforeInsertHandler(Trigger.Old);
    }
    /*if(Trigger.isUpdate && Trigger.isAfter){
ResourcesTriggerHandler.afterUpdateHandler(Trigger.newMap, Trigger.oldMap);
}*/
}