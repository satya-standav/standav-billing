global class EP_Weekly_Scheduler implements Schedulable
{
    global void execute(SchedulableContext sc)
    {
    
        WeekEmailBatch ep = new WeekEmailBatch();
        database.executebatch(ep);
    }
   
}