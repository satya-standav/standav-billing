public class AssignmentTriggerHelper{
    public static void beforeInsertUpdateMethod(List<Assignment__c> relatedAssignmentList, Map<Id, Assignment__c> relatedAssignmentDataMap){
        Map<Id, String> recordIdWithNameMap = new Map<Id, String>();
        Map<String, String> duplicateTimeCardMap = new Map<String, String>();
        Map<String, Assignment__c> primaryAssignmentMap = new Map<String, Assignment__c>();
        Map<String, String> resourceDetailsMap = new Map<String,String>();
        set<Id> projectIdSet = new Set<Id>();
        Map<String, String> primaryallocationMap = new Map<String, String>();
        set<Id> shadowProjectIdSet = new Set<Id>();
        //set<Id> existingResourceIdSet = new Set<Id>();
        Map<Id, String> allocationTypeWithResourceMap = new Map<Id, String>();

        for(RecordType recordTypeName : [SELECT Id, Name, DeveloperName, SobjectType FROM RecordType WHERE SobjectType = 'Assignment__c']){
            recordIdWithNameMap.put(recordTypeName.Id, recordTypeName.Name);
        }

        for(Assignment__c newAssignment : relatedAssignmentList){
          if(newAssignment.Project__c != null){
            projectIdSet.add(newAssignment.Project__c);
          }
          if(Trigger.isInsert){
            if(recordIdWithNameMap.get(newAssignment.RecordTypeId) == 'Shadow' && 
              newAssignment.Project__c != null){
              shadowProjectIdSet.add(newAssignment.Project__c);
            }  
          }
          
        }
    
        for(Assignment__c existingAssignment : [SELECT Id, Project__c, Resource__c, Name, 
                                                Is_Primary_Resource_Contributing__c, 
                                                Start_Date__c, End_Date__c, RecordTypeId,
                                                EP_Shadow_Resource__c, RecordType.Name
                                                FROM Assignment__c
                                                WHERE Project__c IN: projectIdSet]){ 
            if(existingAssignment.RecordType.Name == 'Primary'){
              //existingResourceIdSet.add(existingAssignment.Resource__c);
              allocationTypeWithResourceMap.put(existingAssignment.Resource__c, 'Primary');
            }
            if(recordIdWithNameMap.get(existingAssignment.RecordTypeId) == 'Shadow' &&
              existingAssignment.EP_Shadow_Resource__c != null){
              //existingResourceIdSet.add(existingAssignment.EP_Shadow_Resource__c);
              allocationTypeWithResourceMap.put(existingAssignment.EP_Shadow_Resource__c, 'Shadow');
            }
            if(Trigger.isInsert && existingAssignment.RecordType.Name == 'Primary'){
              primaryallocationMap.put(existingAssignment.Resource__c+'-'+existingAssignment.Project__c, existingAssignment.Id);
            }
            system.debug('-----primaryallocationMap----------'+primaryallocationMap);
            if(recordIdWithNameMap.get(existingAssignment.RecordTypeId) == 'Primary'){
              resourceDetailsMap.put(existingAssignment.Project__c+'-'+existingAssignment.Resource__c+'-'+existingAssignment.Start_Date__c+'-'+existingAssignment.End_Date__c, existingAssignment.Name);
            }else if(recordIdWithNameMap.get(existingAssignment.RecordTypeId) == 'Shadow'){
              resourceDetailsMap.put(existingAssignment.Project__c+'-'+existingAssignment.Resource__c+'-'+existingAssignment.EP_Shadow_Resource__c+'-'+existingAssignment.Start_Date__c+'-'+existingAssignment.End_Date__c, existingAssignment.Name);
            }
            if(existingAssignment.Resource__c != null && existingAssignment.Project__c != null){
                if(recordIdWithNameMap.get(existingAssignment.RecordTypeId) == 'Primary'){
                    duplicateTimeCardMap.put(existingAssignment.Project__c+'-'+recordIdWithNameMap.get(existingAssignment.RecordTypeId)+'-'+existingAssignment.Resource__c+'-'+existingAssignment.Start_Date__c+'-'+existingAssignment.End_Date__c, existingAssignment.Resource__c);
                }else if(recordIdWithNameMap.get(existingAssignment.RecordTypeId) == 'Shadow'){
                    duplicateTimeCardMap.put(existingAssignment.Project__c+'-'+recordIdWithNameMap.get(existingAssignment.RecordTypeId)+'-'+existingAssignment.Resource__c+'-'+existingAssignment.EP_Shadow_Resource__c+'-'+existingAssignment.Start_Date__c+'-'+existingAssignment.End_Date__c, existingAssignment.EP_Shadow_Resource__c);
                }
            }
            if(recordIdWithNameMap.get(existingAssignment.RecordTypeId) == 'Primary'){
                primaryAssignmentMap.put(existingAssignment.Project__c+'-'+existingAssignment.Resource__c+'-'+existingAssignment.Start_Date__c+'-'+existingAssignment.End_Date__c, existingAssignment);
            }
        }
        for(Assignment__c assignment : relatedAssignmentList){
          /*if(recordIdWithNameMap.get(assignment.RecordTypeId) == 'Primary' && 
            !allocationTypeWithResourceMap.isEmpty() &&
            allocationTypeWithResourceMap.get(assignment.Resource__c) != null &&
            allocationTypeWithResourceMap.get(assignment.Resource__c) == 'Shadow'){
            assignment.addError('There is already a allocation for this shadow resource so you cant not create a allocation');
          }
          if(recordIdWithNameMap.get(assignment.RecordTypeId) == 'Shadow' && 
            !allocationTypeWithResourceMap.isEmpty() &&
            allocationTypeWithResourceMap.get(assignment.EP_Shadow_Resource__c) != null &&
            allocationTypeWithResourceMap.get(assignment.EP_Shadow_Resource__c) == 'Primary'){
            assignment.addError('There is already a allocation for this primary resource so you cant not create a allocation');
          }*/
          
            if(Trigger.isInsert && recordIdWithNameMap.get(assignment.RecordTypeId) == 'Shadow'){
                if(primaryAssignmentMap.containsKey(assignment.Project__c+'-'+assignment.Resource__c+'-'+assignment.Start_Date__c+'-'+assignment.End_Date__c) && 
                    assignment.Project__c != 
                        primaryAssignmentMap.get(assignment.Project__c+'-'+assignment.Resource__c+'-'+assignment.Start_Date__c+'-'+assignment.End_Date__c).Project__c &&
                    assignment.Resource__c != 
                        primaryAssignmentMap.get(assignment.Project__c+'-'+assignment.Resource__c+'-'+assignment.Start_Date__c+'-'+assignment.End_Date__c).Resource__c &&
                    (assignment.Start_Date__c <
                        primaryAssignmentMap.get(assignment.Project__c+'-'+assignment.Resource__c+'-'+assignment.Start_Date__c+'-'+assignment.End_Date__c).Start_Date__c ||
                    assignment.Start_Date__c > 
                        primaryAssignmentMap.get(assignment.Project__c+'-'+assignment.Resource__c+'-'+assignment.Start_Date__c+'-'+assignment.End_Date__c).End_Date__c ||
                    assignment.End_Date__c < 
                        primaryAssignmentMap.get(assignment.Project__c+'-'+assignment.Resource__c+'-'+assignment.Start_Date__c+'-'+assignment.End_Date__c).Start_Date__c ||
                    assignment.End_Date__c > 
                        primaryAssignmentMap.get(assignment.Project__c+'-'+assignment.Resource__c+'-'+assignment.Start_Date__c+'-'+assignment.End_Date__c).End_Date__c)){
                    assignment.addError('We can not create shadow resource allocation which are not matching with primary allocation paramerts such as date and project and resource');
                }if(!primaryAssignmentMap.containsKey(assignment.Project__c+'-'+assignment.Resource__c+'-'+assignment.Start_Date__c+'-'+assignment.End_Date__c)){
                    assignment.addError('We can not create shadow resource allocation without the primary resource allocation');    
                }
            }
            if(Trigger.isInsert || 
              (Trigger.isUpdate && (assignment.Project__c != relatedAssignmentDataMap.get(assignment.Id).Project__c || 
                assignment.Resource__c != relatedAssignmentDataMap.get(assignment.Id).Resource__c ||
                assignment.Start_Date__c != relatedAssignmentDataMap.get(assignment.Id).Start_Date__c ||
                assignment.End_Date__c != relatedAssignmentDataMap.get(assignment.Id).End_Date__c))){
                if((recordIdWithNameMap.get(assignment.RecordTypeId) == 'Primary' && 
                    duplicateTimeCardMap.containsKey(assignment.Project__c+'-'+recordIdWithNameMap.get(assignment.RecordTypeId)+'-'+assignment.Resource__c+'-'+assignment.Start_Date__c+'-'+assignment.End_Date__c) && 
                    assignment.Resource__c == duplicateTimeCardMap.get(assignment.Project__c+'-'+recordIdWithNameMap.get(assignment.RecordTypeId)+'-'+assignment.Resource__c+'-'+assignment.Start_Date__c+'-'+assignment.End_Date__c)) 
                ||
                    (recordIdWithNameMap.get(assignment.RecordTypeId) == 'Shadow' && 
                    duplicateTimeCardMap.containsKey(assignment.Project__c+'-'+recordIdWithNameMap.get(assignment.RecordTypeId)+'-'+assignment.Resource__c+'-'+assignment.EP_Shadow_Resource__c+'-'+assignment.Start_Date__c+'-'+assignment.End_Date__c) && 
                    assignment.EP_Shadow_Resource__c ==  duplicateTimeCardMap.get(assignment.Project__c+'-'+recordIdWithNameMap.get(assignment.RecordTypeId)+'-'+assignment.Resource__c+'-'+assignment.EP_Shadow_Resource__c+'-'+assignment.Start_Date__c+'-'+assignment.End_Date__c))){
                    assignment.addError('We can not create a duplicate assignment, we already have an existing assignment for the selected project and selected resource');
                }
            }

            if(!resourceDetailsMap.isEmpty()){
              //Check duplicate
              if(Trigger.isInsert || 
                (Trigger.isUpdate && (assignment.Project__c != relatedAssignmentDataMap.get(assignment.Id).Project__c || 
                assignment.Resource__c != relatedAssignmentDataMap.get(assignment.Id).Resource__c ||
                assignment.Start_Date__c != relatedAssignmentDataMap.get(assignment.Id).Start_Date__c ||
                assignment.End_Date__c != relatedAssignmentDataMap.get(assignment.Id).End_Date__c))){
                if(recordIdWithNameMap.get(assignment.RecordTypeId) == 'Primary'){
                  if(resourceDetailsMap.get(assignment.Project__c+'-'+assignment.Resource__c+'-'+assignment.Start_Date__c+'-'+assignment.End_Date__c) != null &&
                    resourceDetailsMap.containsKey(assignment.Project__c+'-'+assignment.Resource__c+'-'+assignment.Start_Date__c+'-'+assignment.End_Date__c)){
                    system.debug('---------Primary---------'+resourceDetailsMap.get(assignment.Project__c+'-'+assignment.Resource__c+'-'+assignment.Start_Date__c+'-'+assignment.End_Date__c));
                    assignment.addError('There is already a Primary assignment with this combination'+resourceDetailsMap.get(assignment.Project__c+'-'+assignment.Resource__c+'-'+assignment.Start_Date__c+'-'+assignment.End_Date__c));
                  } 
                }
                if(recordIdWithNameMap.get(assignment.RecordTypeId) == 'Shadow'){
                  if(resourceDetailsMap.get(assignment.Project__c+'-'+assignment.Resource__c+'-'+assignment.EP_Shadow_Resource__c+'-'+assignment.Start_Date__c+'-'+assignment.End_Date__c) != null &&
                    resourceDetailsMap.containsKey(assignment.Project__c+'-'+assignment.Resource__c+'-'+assignment.EP_Shadow_Resource__c+'-'+assignment.Start_Date__c+'-'+assignment.End_Date__c)){
                    system.debug('---------shadow---------'+resourceDetailsMap.get(assignment.Project__c+'-'+assignment.Resource__c+'-'+assignment.EP_Shadow_Resource__c+'-'+assignment.Start_Date__c+'-'+assignment.End_Date__c));
                    assignment.addError('There is already a Shadow assignment with this combination'+resourceDetailsMap.get(assignment.Project__c+'-'+assignment.Resource__c+'-'+assignment.EP_Shadow_Resource__c+'-'+assignment.Start_Date__c+'-'+assignment.End_Date__c));
                  } 
                }
              }
            }
        }
        if(Trigger.isInsert){
          if(!primaryallocationMap.isEmpty()){
            for(Assignment__c allocation : relatedAssignmentList){
              if(recordIdWithNameMap.get(allocation.RecordTypeId) == 'Shadow'){
                if(primaryallocationMap.get(allocation.Resource__c+'-'+allocation.Project__c) != null){
                  system.debug('-----primaryallocationMap-----1-----'+primaryallocationMap.get(allocation.Resource__c+'-'+allocation.Project__c));
                  allocation.Related_Resource_Allocation__c =  primaryallocationMap.get(allocation.Resource__c+'-'+allocation.Project__c);
                }else{
                  allocation.addError('We can not create shadow allocation without the primary allocation');    
                }
              }
            }
          }
      }
        
    }
    public static void afterInsertMethod(List<Assignment__c> relatedAssignmentList, Map<Id, Assignment__c> relatedAssignmentDataMap){
        if(trigger.isInsert && trigger.isAfter){
            List<Time_Card__c> timeCardsToInsertList = new List<Time_Card__c>();
            Map<Id, Id> assignmentWithProjectManagerMap = new Map<Id, Id>();
            Set<Id> relatedProjectIdSet = new Set<Id>();
            for(Assignment__c assignment : [SELECT Id, Project__c, Project__r.EP_Project_Manager__c, Resource__c, RecordType.Name, Role__c FROM Assignment__c WHERE Id IN: trigger.new]){
                if(assignment.Project__c != null && assignment.Project__r.EP_Project_Manager__c != null ){
                    assignmentWithProjectManagerMap.put(assignment.Id, assignment.Project__r.EP_Project_Manager__c);    
                }
            }
            for(Assignment__c assignment : relatedAssignmentList){
                Date startDate = assignment.Start_Date__c.toStartofWeek().addDays(1);
                Date endDate;
                if(startDate != Date.valueOf(assignment.createdDate).addDays(1)){
                    while(startDate <= Date.valueOf(assignment.createdDate).toStartofWeek().addDays(1) && startDate <= assignment.End_Date__c){
                        endDate = startDate.addDays(6);
                        Time_Card__c timeCard = new Time_Card__c();
                        timeCard.Assignment__c = assignment.Id;
                        timeCard.Start_Date__c = startDate;
                        timeCard.EP_End_Date__c = endDate;
                        timeCard.Project_Manager__c = assignmentWithProjectManagerMap.get(assignment.Id) != null ? assignmentWithProjectManagerMap.get(assignment.Id) : timeCard.Project_Manager__c;
                        timeCardsToInsertList.add(timeCard);
                        startDate= endDate.addDays(1); 
                    }
                }
            }
            if(timeCardsToInsertList.size() > 0){
                insert timeCardsToInsertList;
            }
        }
    } 

    //Link releated allocations to its associated allocation.
    public static void linkRelatedAllocation(List<Assignment__c> newAllocationList){
      Map<String, String> ShadowallocationMap = new Map<String, String>();
      //set<Id> relatedAllocationIdSet  = new Set<Id>();
      Map<Id, String> recordIdWithNameMap = new Map<Id, String>();
      List<Assignment__c> primaryAllocationList = new List<Assignment__c>();
      //Link resource contacts to manager
      set<Id> resourceAllocationIdSet = new Set<Id>();
      List<AccountShare> accShrs  = new List<AccountShare>();

      for(RecordType recordTypeName : [SELECT Id, Name, DeveloperName, SobjectType FROM RecordType WHERE SobjectType = 'Assignment__c']){
          recordIdWithNameMap.put(recordTypeName.Id, recordTypeName.Name);
      }
      for(Assignment__c allocation : newAllocationList){
        if(recordIdWithNameMap.get(allocation.RecordTypeId) == 'Shadow' && 
          allocation.Project__c != null){
          ShadowallocationMap.put(allocation.Related_Resource_Allocation__c, allocation.Id);
        }
        resourceAllocationIdSet.add(allocation.Id);
      }
      if(!ShadowallocationMap.isEmpty()){
        for(Assignment__c allocation : newAllocationList){
          if(recordIdWithNameMap.get(allocation.RecordTypeId) == 'Shadow'){
            Assignment__c primaryAssignment = new Assignment__c();
            primaryAssignment.Id = allocation.Related_Resource_Allocation__c;
            primaryAssignment.Related_Resource_Allocation__c = ShadowallocationMap.get(allocation.Related_Resource_Allocation__c);
            system.debug('-----primaryAssignment----------'+primaryAssignment);
            primaryAllocationList.add(primaryAssignment);
          }
        }
      }
      if(primaryAllocationList.size() > 0){
        update primaryAllocationList;
        system.debug('-----primaryAllocationList----------'+primaryAllocationList);
      }
    
    if(resourceAllocationIdSet.size() > 0){
      for(Assignment__c allocation : [SELECT Id, Project__r.Project_Manager__c,
                                      Resource__r.User__r.Contact.AccountId
                                      FROM Assignment__c 
                                      WHERE Id IN : resourceAllocationIdSet]){
        if(allocation.Resource__r.User__r.Contact.AccountId != null && 
          allocation.Project__r.Project_Manager__c != null){
          AccountShare accShare = new AccountShare();
          accShare.accountid = allocation.Resource__r.User__r.Contact.AccountId;
          accShare.UserOrGroupId = allocation.Project__r.Project_Manager__c;
          accShare.accountaccesslevel = 'Edit';
          accShare.OpportunityAccessLevel = 'None';
          accShare.CaseAccessLevel = 'None';
          accShrs.add(accShare);
        }
      }
    }
    if(accShrs.size() > 0){
        Database.insert(accShrs,false);
    }
  }
}