public class InvoicedCardsPageController {

    public String oppIdToWin {get; set;}
    public string projectCriteraString{get; set;}
    public string resourceCriteraString{get; set;}
    public String errorMessage{get;set;}
    public Date startDateFilter{get;set;}
    public Date endDateFilter{get;set;}
    public Boolean ishide{get;set;}
    public List<SelectOption> resourceNameOptionsList{get; set;} 
    public List<SelectOption> projectNameOptionsList{get; set;}
    Map<Id, String> userIdWithNameMap;
    public List < timeCardWrapper > timeCardWrapperList {
        get {
            if (timeCardWrapperList != null) {
                return timeCardWrapperList;
            }
            return fetchTimeCards();
        }
        public set;
    }
    
    //constructor = start
    public InvoicedCardsPageController(){
        ishide = true;
        userIdWithNameMap = new Map<Id, String>();
        resourceNameOptionsList = new List<SelectOption>();
        Set<SelectOption> resourceNameSet = new Set<SelectOption>();
        resourceNameSet.add(new SelectOption('', 'Please Select'));

        for(User UserRec : UserDataCache.getMap().values()){
            userIdWithNameMap.put(UserRec.Id, UserRec.FirstName+' '+UserRec.LastName);
            resourceNameSet.add(new SelectOption(UserRec.FirstName+' '+UserRec.LastName, UserRec.FirstName+' '+UserRec.LastName)); 
        } 
        resourceNameOptionsList.addAll(resourceNameSet);
    }

    public PageReference resetFiltersMethod(){
        fetchTimeCards();
        errorMessage = null;
        projectCriteraString = null;
        resourceCriteraString = null;
        startDateFilter = null;
        endDateFilter = null;
        return null;
    }
    public PageReference filterMethod(){

        Date filterStartDate = startDateFilter;
        Date filterEndDate = endDateFilter;
        string queryString;

        if(startDateFilter != null){
            Date dToday1 = filterStartDate;
            Datetime dt1 = datetime.newInstance(dToday1.year(), dToday1.month(),dToday1.day());
            String dayOfCurrentDate = dt1.format('E');
            filterStartDate = (dayOfCurrentDate == 'Sun' ? filterStartDate.addDays(1):
                             dayOfCurrentDate == 'Mon' ? filterStartDate.addDays(0):
                             dayOfCurrentDate == 'Tue' ? filterStartDate.addDays(-1):
                             dayOfCurrentDate == 'Wed' ? filterStartDate.addDays(-2):
                             dayOfCurrentDate == 'Thu' ? filterStartDate.addDays(-3):
                             dayOfCurrentDate == 'Fri' ? filterStartDate.addDays(-4):
                             dayOfCurrentDate == 'Sat' ? filterStartDate.addDays(-5): 
                             filterStartDate);     
        }
        if(endDateFilter != null){
            Date dToday2 = filterEndDate;
            Datetime dt2 = datetime.newInstance(dToday2.year(), dToday2.month(),dToday2.day());
            String dayOfCurrentDate = dt2.format('E');
            filterEndDate = (dayOfCurrentDate == 'Sun' ? filterEndDate.addDays(0):
                             dayOfCurrentDate == 'Mon' ? filterEndDate.addDays(6):
                             dayOfCurrentDate == 'Tue' ? filterEndDate.addDays(5):
                             dayOfCurrentDate == 'Wed' ? filterEndDate.addDays(4):
                             dayOfCurrentDate == 'Thu' ? filterEndDate.addDays(3):
                             dayOfCurrentDate == 'Fri' ? filterEndDate.addDays(2):
                             dayOfCurrentDate == 'Sat' ? filterEndDate.addDays(1): 
                             filterEndDate);     
        }
        if(resourceCriteraString != null)
            queryString = 'SELECT Id, Name, Start_Date__c, End_Date__c,Budgeted_Hours__c,Project__r.Name,EP_Shadow_Resource__r.User__r.Name, Resource__r.User__r.Name,'+
                '(SELECT Id, Name,Assignment__r.Resource__r.User__c, Approved_Hours_Weekly__c, Assignment__c, EP_Total_Number_of_Billable_Hours__c, Start_Date__c, EP_End_Date__c, Status__c,Invoiced__c FROM Time_Cards__r WHERE Invoiced__c = True)'+
                ' FROM Assignment__c WHERE Resource__r.User__r.Name =: resourceCriteraString';
        if(projectCriteraString != null)
            queryString = 'SELECT Id, Name, Start_Date__c, End_Date__c,Budgeted_Hours__c,Project__r.Name,EP_Shadow_Resource__r.User__r.Name, Resource__r.User__r.Name,'+
                '(SELECT Id, Name,Assignment__r.Resource__r.User__c, Assignment__c, Approved_Hours_Weekly__c, EP_Total_Number_of_Billable_Hours__c, Start_Date__c, EP_End_Date__c, Status__c,Invoiced__c FROM Time_Cards__r WHERE Invoiced__c = True)'+
                ' FROM Assignment__c WHERE Project__r.Name =: projectCriteraString';
        if(filterStartDate != null && filterEndDate != null)
            queryString = 'SELECT Id, Name, Start_Date__c, End_Date__c,Budgeted_Hours__c,Project__r.Name,EP_Shadow_Resource__r.User__r.Name, Resource__r.User__r.Name,'+
                '(SELECT Id, Name,Assignment__r.Resource__r.User__c, Assignment__c, Approved_Hours_Weekly__c, EP_Total_Number_of_Billable_Hours__c, Start_Date__c, EP_End_Date__c, Status__c,Invoiced__c FROM Time_Cards__r WHERE Invoiced__c = True AND (Start_Date__c >=: filterStartDate AND EP_End_Date__c <=: filterEndDate))'+
                ' FROM Assignment__c';
        if(resourceCriteraString != null && projectCriteraString != null)
            queryString = 'SELECT Id, Name, Start_Date__c, End_Date__c,Budgeted_Hours__c,Project__r.Name,EP_Shadow_Resource__r.User__r.Name, Resource__r.User__r.Name,'+
                '(SELECT Id, Name,Assignment__r.Resource__r.User__c, Assignment__c, Approved_Hours_Weekly__c, EP_Total_Number_of_Billable_Hours__c, Start_Date__c, EP_End_Date__c, Status__c,Invoiced__c FROM Time_Cards__r WHERE Invoiced__c = True)'+
                ' FROM Assignment__c WHERE Resource__r.User__r.Name =: resourceCriteraString AND Project__r.Name =: projectCriteraString';
        if(filterStartDate != null && filterEndDate != null && projectCriteraString != null)
            queryString = 'SELECT Id, Name, Start_Date__c, End_Date__c,Budgeted_Hours__c,Project__r.Name,EP_Shadow_Resource__r.User__r.Name, Resource__r.User__r.Name,'+
                '(SELECT Id, Name, Assignment__r.Resource__r.User__c,Assignment__c,Approved_Hours_Weekly__c,  EP_Total_Number_of_Billable_Hours__c, Start_Date__c, EP_End_Date__c, Status__c,Invoiced__c FROM Time_Cards__r WHERE Invoiced__c = True AND (Start_Date__c >=: filterStartDate AND EP_End_Date__c <=: filterEndDate))'+
                ' FROM Assignment__c WHERE Project__r.Name =: projectCriteraString';
        if(filterStartDate != null && filterEndDate != null&& resourceCriteraString != null)
            queryString = 'SELECT Id, Name, Start_Date__c, End_Date__c,Budgeted_Hours__c,Project__r.Name,EP_Shadow_Resource__r.User__r.Name, Resource__r.User__r.Name,'+
                '(SELECT Id, Name, Assignment__r.Resource__r.User__c,Assignment__c, Approved_Hours_Weekly__c, EP_Total_Number_of_Billable_Hours__c, Start_Date__c, EP_End_Date__c, Status__c,Invoiced__c FROM Time_Cards__r WHERE Invoiced__c = True AND (Start_Date__c >=: filterStartDate AND EP_End_Date__c <=: filterEndDate))'+
                ' FROM Assignment__c WHERE Resource__r.User__r.Name =: resourceCriteraString';
        if(resourceCriteraString != null && projectCriteraString != null && filterStartDate != null && filterEndDate != null)
            queryString = 'SELECT Id, Name, Start_Date__c, End_Date__c,Budgeted_Hours__c,Project__r.Name,EP_Shadow_Resource__r.User__r.Name, Resource__r.User__r.Name,'+
                '(SELECT Id, Name, Assignment__r.Resource__r.User__c,Assignment__c,Approved_Hours_Weekly__c,  EP_Total_Number_of_Billable_Hours__c, Start_Date__c, EP_End_Date__c, Status__c,Invoiced__c FROM Time_Cards__r WHERE Invoiced__c = True AND (Start_Date__c >=: filterStartDate AND EP_End_Date__c <=: filterEndDate))'+
                ' FROM Assignment__c WHERE Resource__r.User__r.Name =: resourceCriteraString AND Project__r.Name =: projectCriteraString';
        system.debug('--1----queryString--------'+queryString);
        if(queryString == null){
            timeCardWrapperList = new List < timeCardWrapper > ();
            timeCardWrapperList = fetchTimeCards();
            if(timeCardWrapperList.size() == 0){
                errorMessage = 'No Pending Timesheets for the selected filter';
            }
        }
        if(queryString != null){
            timeCardWrapperList = new List < timeCardWrapper > ();
            system.debug('--2----queryString--------'+queryString);
            system.debug('--2----resourceCriteraString--------'+resourceCriteraString);
            List<Assignment__c> assignmentList = database.query(queryString);
            system.debug('---assignmentList-------'+assignmentList);
            system.debug('---assignmentList--.size()-----'+assignmentList.size());
            if(assignmentList.size() == 0){
                errorMessage = 'No Pending Timesheets for the selected filter';   
            }else{
                for (Assignment__c relatedAssignment : assignmentList){
                    system.debug('--1----relatedAssignment--------'+relatedAssignment);
                    timeCardWrapper newTimeCardWrpper = new timeCardWrapper();
                    newTimeCardWrpper.relatedAssignment = relatedAssignment;
                    for (Time_Card__c relatedTimeCard: relatedAssignment.Time_Cards__r) {
                        newTimeCardWrpper.resourceNameString = userIdWithNameMap.get(relatedTimeCard.Assignment__r.Resource__r.User__c);
                    
                        if (newTimeCardWrpper.assignmentWithTimeCardsMap.get(relatedTimeCard.Assignment__c) != null &&
                            !newTimeCardWrpper.assignmentWithTimeCardsMap.get(relatedTimeCard.Assignment__c).contains(relatedTimeCard)) {
                            newTimeCardWrpper.assignmentWithTimeCardsMap.get(relatedTimeCard.Assignment__c).add(relatedTimeCard);
                        } else {
                            List < Time_Card__c > releatedTimeCardsList = new List < Time_Card__c > ();
                            releatedTimeCardsList.add(relatedTimeCard);
                            newTimeCardWrpper.assignmentWithTimeCardsMap.put(relatedTimeCard.Assignment__c, releatedTimeCardsList);
                        }
                    }
                    if(!newTimeCardWrpper.assignmentWithTimeCardsMap.isEmpty()){
                        timeCardWrapperList.add(newTimeCardWrpper);
                    }
                }    
            }
            
            if(timeCardWrapperList.size() == 0){
                errorMessage = 'No Pending Timesheets for the selected filter';
            }else{
                errorMessage = null;
            }
        }
        return null;
    }
    
    public List < timeCardWrapper > fetchTimeCards() {
        
        timeCardWrapperList = new List < timeCardWrapper > ();
        projectNameOptionsList = new List<SelectOption>();
        Set<SelectOption> projectNameSet = new Set<SelectOption>();
        projectNameSet.add(new SelectOption('', 'Please Select'));
        
        //Collect the assignments which are active on day the page loads
        for (Assignment__c relatedAssignment: [SELECT Id, Name, Start_Date__c, End_Date__c,Budgeted_Hours__c,
                Project__r.Name,EP_Shadow_Resource__r.User__r.Name, Resource__r.User__r.Name,
                (SELECT Id, Name,Assignment__r.Resource__r.User__c, Approved_Hours_Weekly__c, Assignment__c, 
                EP_Total_Number_of_Billable_Hours__c, Start_Date__c, EP_End_Date__c, Status__c,Invoiced__c 
                FROM Time_Cards__r 
                WHERE Invoiced__c = True)
                FROM Assignment__c]){
            
            timeCardWrapper newTimeCardWrpper = new timeCardWrapper();
            newTimeCardWrpper.relatedAssignment = relatedAssignment;
            for (Time_Card__c relatedTimeCard: relatedAssignment.Time_Cards__r) {
                projectNameSet.add(new SelectOption(relatedAssignment.Project__r.Name, relatedAssignment.Project__r.Name));  
                newTimeCardWrpper.resourceNameString = userIdWithNameMap.get(relatedTimeCard.Assignment__r.Resource__r.User__c);
                if (newTimeCardWrpper.assignmentWithTimeCardsMap.get(relatedTimeCard.Assignment__c) != null &&
                    !newTimeCardWrpper.assignmentWithTimeCardsMap.get(relatedTimeCard.Assignment__c).contains(relatedTimeCard)) {
                    newTimeCardWrpper.assignmentWithTimeCardsMap.get(relatedTimeCard.Assignment__c).add(relatedTimeCard);
                } else {
                    List < Time_Card__c > releatedTimeCardsList = new List < Time_Card__c > ();
                    releatedTimeCardsList.add(relatedTimeCard);
                    newTimeCardWrpper.assignmentWithTimeCardsMap.put(relatedTimeCard.Assignment__c, releatedTimeCardsList);
                }
            }
            if(!newTimeCardWrpper.assignmentWithTimeCardsMap.isEmpty()){
                timeCardWrapperList.add(newTimeCardWrpper);
            }
        }
        projectNameOptionsList.addAll(projectNameSet);
        if(timeCardWrapperList.size() == 0){
            errorMessage = 'No Pending Timesheets';
        }
        return timeCardWrapperList; 
    }
    //Fetch the related data method -end
        
    //Wrapper class - start
    public class timeCardWrapper {
        
        public Assignment__c relatedAssignment {
            get;
            set;
        }
        public Map < Id, List < Time_Card__c >> assignmentWithTimeCardsMap {
            get;
            set;
        }
        public String resourceNameString{get;set;}
        public timeCardWrapper() {
            this.relatedAssignment = new Assignment__c();
            this.assignmentWithTimeCardsMap = new Map < Id, List < Time_Card__c >> ();
        }
    }
    //Wrapper class -End

    public pageReference reDirect(){
        PageReference pageref = new PageReference('/apex/PDFCSSPage');
        pageref.getParameters().put('id',oppIdToWin);
        pageref.setRedirect(false);
        return pageref;
    }
    
     public pageReference reZip(){
       Time_Card__c timecardtoreview = [Select Id,Name,Assignment__r.Resource__r.Name,Start_Date__c From Time_Card__C Where Id=:oppIdToWin];
      
    //String objectId = oppIdToWin; // your object's Id
    String docName = timecardtoreview.Assignment__r.Resource__r.Name+' Attachments '+timecardtoreview.Start_Date__c+'.zip';
    List<Attachment> attachments = [SELECT Id,Name,ParentId, Body FROM Attachment WHERE ParentId = :oppIdToWin];
    
    System.debug('>>> attachments ' + attachments.size());
    Zippex sampleZip = new Zippex();
    for(Attachment file : attachments) {
      sampleZip.addFile( file.Name, file.Body, null);
    }
    try{
      Document doc = new Document();
      doc.FolderId = UserInfo.getUserId();
      doc.Name = docName;
      doc.Body = sampleZip.getZipArchive();
      insert doc;
      System.debug('>>> doc ' + doc.Id);
      return new PageReference('https://qa-standavglobal.cs92.force.com/StandavPortal/servlet/servlet.FileDownload?file=' + doc.Id);
    } catch ( Exception ex ) {
      System.debug('>>> ERROR ' + ex);
    }
    return null;
        
        
    }
}