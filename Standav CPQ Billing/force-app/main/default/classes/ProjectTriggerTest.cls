@isTest
public class ProjectTriggerTest {
    public static testmethod void updateAccount(){
            User user = new User();
            user.LastName = 'Test User';
            user.Alias = 'tu';
            user.Email = 'test@gmail.com';
            user.Username = 'test.dev@stand.com';
            user.CommunityNickname = 'test.t';
            user.User_Time_Zone__c = 'Pst';
            user.ProfileId = '00e54000000HSHG';           
            user.TimeZoneSidKey    = 'America/Denver';
            user.LocaleSidKey      = 'en_US';
            user.EmailEncodingKey  = 'UTF-8';
            user.LanguageLocaleKey = 'en_US';
            insert user;
        
            User user1 = new User();
            user1.LastName = 'Test User1';
            user1.Alias = 'tu1';
            user1.Email = 'test1@gmail.com';
            user1.Username = 'test1.dev@stand.com';
            user1.CommunityNickname = 'test1.t';
            user1.User_Time_Zone__c = 'Pst';
            user1.ProfileId = '00e54000000HSHG';           
            user1.TimeZoneSidKey    = 'America/Denver';
            user1.LocaleSidKey      = 'en_US';
            user1.EmailEncodingKey  = 'UTF-8';
            user1.LanguageLocaleKey = 'en_US';
            insert user1;
        
            
            Account acc = new Account();
            acc.Name = 'Test Account';
            insert acc;
            
        Contact con = new Contact();
            con.FirstName = 'test';
            con.LastName = 'contact';
            con.AccountId = acc.id;
            insert con;
        
            Opportunity opp1 = new Opportunity();
            opp1.AccountId = acc.id;
            opp1.Name = 'TestOpp1';
            opp1.OwnerId = user.id;
            opp1.CloseDate = Date.newInstance(2021, 31, 3);
            opp1.StageName = 'Closed Won';
            opp1.Type = 'New Project';
            //opp1.Technology__c = 'Salesforce';
            //opp1.Apttus_Sales__c = '1';
                //insert opp1;
            

        
        
            Project__c project = new Project__c();
            project.Name = 'Test Project';
            project.EP_Project_Manager__c = user.Id;
            project.Contact_Person__c = con.Id;
            //project.OpportuityCommunity__c = opp.id;
            project.Start_Date__c = Date.newInstance(2020, 31, 3);
            project.Account__c = acc.id;
            project.Number__c = 2;
            insert project;        
            update project;
        
            Resource__c resource = new Resource__c();
            resource.User__c = user.Id;
            resource.Contact__c = con.Id;
            resource.Active__c = true;
            insert resource;        
            
        
            Assignment__c assignment = new Assignment__c();
            assignment.Project__c = project.id;
            assignment.Resource__c = resource.id;
            assignment.Status__c = 'Approved';
            assignment.Start_Date__c =  Date.newInstance(2020, 31, 3);
            assignment.End_Date__c =  Date.newInstance(2021, 31, 3);
            assignment.Role__c = 'Developer';
            assignment.Billing_Type__c = 'Billable';
            assignment.Assignment_Type__c = 'Long Term';  
            assignment.RecordTypeId ='012540000018VJv';
        assignment.Budgeted_Hours__c=80;
            //assignment.Actual_Hours__c = 20;
            insert assignment;
        
            
            Time_Card__c card = new Time_Card__c();
            card.Assignment__c = assignment.id;
            card.EP_Primary_Resource_Email_Id__c = user.Email;
            card.Start_Date__c = Date.newInstance(2020, 31, 3);
            card.EP_End_Date__c = Date.newInstance(2020, 6, 4);
            card.Billable_Hours_Fri__c = 4;
            card.Billable_Hours_Mon__c =4;
            card.Billable_Hours_Sat__c = 0;
            card.Billable_Hours_Sun__c = 0;
            card.Billable_Hours_Thu__c = 4;
            card.Billable_Hours_Tue__c = 4;
            card.Billable_Hours_Wed__c =4;
            card.Status__c = 'Draft';            
            insert card;
            project.Number__c = 3;
            update project;
       
        System.runAs ( new User(Id = UserInfo.getUserId()) ) {
            UserLicense licence = [SELECT Id FROM UserLicense where name ='Customer Community'];
            Profile p = [SELECT Id FROM Profile WHERE UserLicenseId = : licence.Id Limit 1]; 
            User comUser = new User(alias = 'test123', email='test123@noemail.com',
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = p.Id, country='United States',IsActive =true,
                                    contactId=con.Id,
                                    timezonesidkey='America/Los_Angeles', username='newone@noemail.com');
            insert comuser;
        }
        User cuser = [select id from user where username ='newone@noemail.com' limit 1];
        resource.User__c = cuser.id;
        update resource;
        
        System.RunAs(cuser)
    {
            system.Test.startTest();
             /* ProjectDetailBatch ua = new ProjectDetailBatch();
              Database.executebatch(ua);*/
            system.Test.stopTest();
    }
    }
}