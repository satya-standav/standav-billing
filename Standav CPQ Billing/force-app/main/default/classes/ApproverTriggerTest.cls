@isTest
private class ApproverTriggerTest{
   // @testSetUp 
   @isTest
    static void createData(){
      
        
        User user = new User();
            user.LastName = 'Test';
            user.Alias = 'tester';
            user.Email = 'test@gmail.com';
            user.Username = 'test@stand.com';
            user.CommunityNickname = 'te';
            user.User_Time_Zone__c = 'Pst';
            user.ProfileId = '00e54000000HSHG';           
            user.TimeZoneSidKey    = 'America/Denver';
            user.LocaleSidKey      = 'en_US';
            user.EmailEncodingKey  = 'UTF-8';
            user.LanguageLocaleKey = 'en_US';
            insert user;
        
        User user1 = new User();
            user1.LastName = 'Test1';
            user1.Alias = 'tester1';
            user1.Email = 'test1@gmail.com';
            user1.Username = 'test1@stand.com';
            user1.CommunityNickname = 'te1';
            user1.User_Time_Zone__c = 'Pst';
            user1.ProfileId = '00e54000000HSHG';           
            user1.TimeZoneSidKey    = 'America/Denver';
            user1.LocaleSidKey      = 'en_US';
            user1.EmailEncodingKey  = 'UTF-8';
            user1.LanguageLocaleKey = 'en_US';
            insert user1;
        
        Account Acc = new Account();
            Acc.Name = 'TestBoi';
            insert Acc;
        
        Contact cont = new Contact();
            cont.LastName = 'Konikkara';
        	cont.AccountId = Acc.id;
        	cont.User_ID__c = user.id;
            insert cont;
        
        Contact cont1 = new Contact();
            cont1.LastName = 'Konikkara';
        	cont1.AccountId = Acc.id;
        	cont1.User_ID__c = user1.id;
            insert cont1;
        
        
        Project__c project = new Project__c();
            project.Name = 'TestProject';
            project.EP_Project_Manager__c = user.ID;
            project.Start_Date__c = Date.newInstance(2018, 12, 9);
            project.Contact_Person__c = cont.Id;
            //project.Opportunity__c = NULL;
            project.Number__c = 2;
            insert project;
        

       
        
      Resource__c resource = new Resource__c();
            resource.User__c = user.ID;
            insert resource;  
        
      Resource__c resource1 = new Resource__c();
            resource.User__c = user1.ID;
            insert resource1;
        
      Assignment__c assignment = new Assignment__c();
            assignment.Project__c = project.id;
            assignment.Resource__c = resource.id;
            assignment.Status__c = 'Approved';
            assignment.Start_Date__c = Date.newInstance(2018, 12, 9);
            assignment.End_Date__c = Date.newInstance(2019, 12, 9);
            assignment.Role__c = 'Developer';
            assignment.Billing_Type__c = 'Billable';
            assignment.Assignment_Type__c = 'Long Term';  
            assignment.RecordTypeId ='012540000018VJv';
        assignment.Budgeted_Hours__c=50;
            //assignment.Actual_Hours__c = 20;
            insert assignment;
        
       Assignment__c assignment1 = new Assignment__c();
            assignment1.Project__c = project.id;
            assignment1.Resource__c = resource1.id;
            assignment1.Status__c = 'Approved';
            assignment1.EP_Shadow_Resource__c = resource.id;
            assignment1.Start_Date__c = Date.newInstance(2018, 12, 9);
            assignment1.End_Date__c = Date.newInstance(2019, 12, 9);
            assignment1.Role__c = 'Developer';
            assignment1.Billing_Type__c = 'Non Billable';
            assignment1.Assignment_Type__c = 'Long Term';  
            assignment1.RecordTypeId ='012540000018VLmAAM';
        assignment1.Budgeted_Hours__c=50;
            //assignment1.Actual_Hours__c = 20;
          //  insert assignment1;
         
    
    project.Name = 'RetestProject';
    assignment.Role__c = 'Consultant';
        update assignment;
        update project;
        
    Approvers_Flow__c AP = new Approvers_Flow__c();
        	//AP.Client_Approver__c = cont.ID;
        	AP.Name = 'Test Approver';
        	AP.User_Approver__c = user1.ID;
        	AP.Project__c = project.ID;
        	insert AP;
        
    Approvers_Flow__c AP1 = new Approvers_Flow__c();
        	AP1.Name = 'Test Approver 1';
        	AP1.Client_Approver__c = cont.ID;
        	AP1.Project__c = project.ID;
        	insert AP1;
        
system.Test.startTest();

        	AP.User_Approver__c = user.ID;
        	update AP;
        	AP.Client_Approver__c = cont1.Id;
        	update AP;
        
system.Test.stopTest();        
}
}