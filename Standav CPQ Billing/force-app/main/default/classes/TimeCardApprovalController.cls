public class TimeCardApprovalController {
    public static void approveOrReject(List<Time_Card__c> timeCardToUpdateList, String action){
        List<Time_Card__c> updateList = new List<Time_Card__c>();
        
        system.debug('------timeCardToUpdateList-------'+timeCardToUpdateList);
        system.debug('------action-------'+action);
        for(Time_Card__c timeCardToReview : timeCardToUpdateList){
            timeCardToReview.Manager_Approval_Comments__c = timeCardToReview.Manager_Approval_Comments__c;
            if(action == 'Approved'){
                if(timeCardToReview.Approval_Status__c == null && 
                    timeCardToReview.X1st_Approver__c == timeCardToReview.Project_Manager__c){
                    timeCardToReview.Approval_Status__c = 'Approver 1 Approved';
                    timeCardToReview.Previous_Approver__c = timeCardToReview.X1st_Approver__c;
                    if(timeCardToReview.X2nd_Approver__c == null || 
                        (timeCardToReview.X2nd_Approver__c != null && 
                        timeCardToReview.X2nd_Approver__c.substring(0,3) != '005')){
                        timeCardToReview.Status__c = 'Approved';
                        timeCardToReview.Finally_Approved__c = timeCardToReview.X2nd_Approver__c == null ? True : False;
                    }else if(timeCardToReview.X2nd_Approver__c != null){
                        timeCardToReview.Project_Manager__c = timeCardToReview.X2nd_Approver__c;
                    }
                }

                else if(timeCardToReview.X2nd_Approver__c != null && 
                    timeCardToReview.X2nd_Approver__c.substring(0,3) == '005'  &&
                    timeCardToReview.Approval_Status__c == 'Approver 1 Approved' && 
                    timeCardToReview.Project_Manager__c == timeCardToReview.X2nd_Approver__c){
                    timeCardToReview.Approval_Status__c = 'Approver 2 Approved';
                    timeCardToReview.Previous_Approver__c = timeCardToReview.X2nd_Approver__c;
                    if(timeCardToReview.X3rd_Approver__c == null || 
                        timeCardToReview.X3rd_Approver__c.substring(0,3) != '005'){
                        timeCardToReview.Status__c = 'Approved';
                        timeCardToReview.Finally_Approved__c = timeCardToReview.X3rd_Approver__c== null ? True : False;
                    }else if(timeCardToReview.X3rd_Approver__c != null){
                        timeCardToReview.Project_Manager__c = timeCardToReview.X3rd_Approver__c;
                    }
                }

                else if(timeCardToReview.X3rd_Approver__c != null && 
                    timeCardToReview.X3rd_Approver__c.substring(0,3) == '005' &&
                    timeCardToReview.Approval_Status__c == 'Approver 2 Approved' && 
                    timeCardToReview.Project_Manager__c == timeCardToReview.X3rd_Approver__c){
                    timeCardToReview.Approval_Status__c = 'Approver 3 Approved';
                    timeCardToReview.Previous_Approver__c = timeCardToReview.X3rd_Approver__c;
                    if(timeCardToReview.X4th_Approver__c == null ||
                        timeCardToReview.X4th_Approver__c.substring(0,3) != '005'){
                        timeCardToReview.Status__c = 'Approved';
                        timeCardToReview.Finally_Approved__c = timeCardToReview.X4th_Approver__c == null ? True : False;
                    }else if(timeCardToReview.X4th_Approver__c != null){
                        timeCardToReview.Project_Manager__c = timeCardToReview.X4th_Approver__c;
                    }
                }

                else if(timeCardToReview.X4th_Approver__c != null && 
                    timeCardToReview.X4th_Approver__c.substring(0,3) == '005' &&
                    timeCardToReview.Approval_Status__c == 'Approver 3 Approved' && 
                    timeCardToReview.Project_Manager__c == timeCardToReview.X4th_Approver__c){
                    timeCardToReview.Approval_Status__c = 'Approver 4 Approved';
                    timeCardToReview.Previous_Approver__c = timeCardToReview.X4th_Approver__c;
                    if(timeCardToReview.X5th_Approver__c == null ||
                        timeCardToReview.X5th_Approver__c.substring(0,3) != '005'){
                        timeCardToReview.Status__c = 'Approved';
                        timeCardToReview.Finally_Approved__c = timeCardToReview.X5th_Approver__c == null ? True : False;
                    }else if(timeCardToReview.X5th_Approver__c != null){
                        timeCardToReview.Project_Manager__c = timeCardToReview.X5th_Approver__c;
                    }
                }

                else if(timeCardToReview.X5th_Approver__c != null && 
                    timeCardToReview.X5th_Approver__c.substring(0,3) == '005' &&
                    timeCardToReview.Approval_Status__c == 'Approver 4 Approved' && 
                    timeCardToReview.Project_Manager__c == timeCardToReview.X5th_Approver__c){
                    timeCardToReview.Approval_Status__c = 'Approver 5 Approved';
                    timeCardToReview.Previous_Approver__c = timeCardToReview.X5th_Approver__c;
                    timeCardToReview.Status__c = 'Approved';
                    timeCardToReview.Finally_Approved__c = True;
                } 
                timeCardToReview.Rejected_By_Salesforce_Approvers__c = False;   
            }
            else if(action == 'Rejected'){
                timeCardToReview.Status__c = 'Draft';
                timeCardToReview.Previous_Approver__c = timeCardToReview.Project_Manager__c ;
                timeCardToReview.Approval_Status__c = null;
                timeCardToReview.Project_Manager__c = null;
                timeCardToReview.Rejected_By_Salesforce_Approvers__c = True;
            }
            system.debug('------timeCardToReview-------'+timeCardToReview);
            updateList.add(timeCardToReview);
            
        }
        system.debug('------updateList size-------'+updateList.size());
        system.debug('------updateList -------'+updateList);
        
        if(updateList.size() > 0){
            update updateList;
        }
    }

}