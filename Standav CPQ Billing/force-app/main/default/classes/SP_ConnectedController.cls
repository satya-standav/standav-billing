public class SP_ConnectedController {
    public static String companyInfo {
        get { return companyInfo; }
        set { companyInfo = value; }
    }
    public static String statusMessage {
        get { return statusMessage; }
        set { statusMessage = value; }
    }
    
    public static String statusMessageAccount {
        get { return statusMessageAccount; }
        set { statusMessageAccount = value; }
    }
    
    public static String accountInfo {
        get { return accountInfo; }
        set { accountInfo = value; }
    }
    
    public static String accountNameInput {
        get { return accountNameInput; }
        set { accountNameInput = value; }
    }
    
     public static String accountIdInput {
        get { return accountIdInput; }
        set { accountIdInput = value; }
    }
    
    public static void getCompanyInfo() {
        try{
            API_Token__c apiToken = [Select Access_Token__c, Realm_ID__c from API_Token__c ORDER BY LastModifiedDate DESC limit 1];
            String accessToken = apiToken.Access_Token__c ;
            String realmId = apiToken.Realm_ID__c;
            String endPoint = '/companyinfo/'+realmId;
            HttpResponse httpResponse = new HttpResponse();
            httpResponse = SP_LoginQuickBooksController.makeHttpGetRequest(endPoint);
            statusMessage = 'API Callout Status : ' + httpResponse.getStatus();
            if(httpResponse.getStatusCode() == 200){
                companyInfo = httpResponse.getBody();
            }
            companyInfo = httpResponse.getBody(); //TODO - handle error status codes
        }
        catch(System.Exception e){
            System.debug('Exception found in class SP_ConnectedController method getCompanyInfo ' + e.getMessage());
            System.debug('Exception found in class SP_ConnectedController method getCompanyInfo ' + e.getLineNumber());
            statusMessage = 'Exception: ' + e.getMessage();
        }
    } 
    
    public static void createAccount () {
        try {
            AccountEntity obj = new AccountEntity();
            obj.Name = accountNameInput;
            obj.AccountType = 'Accounts Receivable';
            String endPoint = '/account';
            String reqBody = JSON.serialize(obj);
            HttpResponse httpResponse = new HttpResponse();
            httpResponse = SP_LoginQuickBooksController.makeHttpPostRequest(endPoint, reqBody);
            statusMessageAccount = 'API Callout Status : ' + httpResponse.getStatusCode() + ' : ' + httpResponse.getStatus();
            if(httpResponse.getStatusCode() == 200){
                accountInfo = httpResponse.getBody();
            }
            accountInfo = httpResponse.getBody(); //TODO - handle error status codes
        }
        catch(System.Exception e){
            System.debug('Exception found in class SP_ConnectedController method createAccount ' + e.getMessage());
            System.debug('Exception found in class SP_ConnectedController method createAccount ' + e.getLineNumber());
            statusMessageAccount = 'Exception: ' + e.getMessage();
        } 
    }
    
    public static void createCustomer () {
        try {
            CustomerEntity obj = new CustomerEntity();
            obj.DisplayName  = accountNameInput;
            String endPoint = '/customer';
            String reqBody = JSON.serialize(obj);
            HttpResponse httpResponse = new HttpResponse();
            httpResponse = SP_LoginQuickBooksController.makeHttpPostRequest(endPoint, reqBody);
            statusMessageAccount = 'API Callout Status : ' + httpResponse.getStatusCode() + ' : ' + httpResponse.getStatus();
            if(httpResponse.getStatusCode() == 200){
                accountInfo = httpResponse.getBody();
            }
            accountInfo = httpResponse.getBody(); //TODO - handle error status codes
        }
        catch(System.Exception e){
            System.debug('Exception found in class SP_ConnectedController method createAccount ' + e.getMessage());
            System.debug('Exception found in class SP_ConnectedController method createAccount ' + e.getLineNumber());
            statusMessageAccount = 'Exception: ' + e.getMessage();
        } 
    }
    
    public static void createProduct () {
        try {
            CustomerEntity obj = new CustomerEntity();
            obj.DisplayName  = accountNameInput;
            String endPoint = '/Customer';
            String reqBody = JSON.serialize(obj);
            HttpResponse httpResponse = new HttpResponse();
            httpResponse = SP_LoginQuickBooksController.makeHttpPostRequest(endPoint, reqBody);
            statusMessageAccount = 'API Callout Status : ' + httpResponse.getStatusCode() + ' : ' + httpResponse.getStatus();
            if(httpResponse.getStatusCode() == 200){
                accountInfo = httpResponse.getBody();
            }
            accountInfo = httpResponse.getBody(); //TODO - handle error status codes
        }
        catch(System.Exception e){
            System.debug('Exception found in class SP_ConnectedController method createAccount ' + e.getMessage());
            System.debug('Exception found in class SP_ConnectedController method createAccount ' + e.getLineNumber());
            statusMessageAccount = 'Exception: ' + e.getMessage();
        } 
    }
    
    private static List<blng__InvoiceLine__c> getInvoiceLines(Set<Id> invoiceIds) {
        blng__InvoiceLine__c[] invoiceLines = [SELECT Id, name,blng__UnitPrice__c,blng__CalculatedQuantity__c,blng__TotalAmount__c,blng__Subtotal__c FROM blng__InvoiceLine__c  WHERE blng__Invoice__c IN :invoiceIds];
        return invoiceLines;
    }
    
    public static void createInvoice () {
        try {       
            
            InvoiceEntity obj = new InvoiceEntity();
            
            blng__Invoice__c[] invoices = [SELECT Id,name, blng__InvoiceStatus__c FROM blng__Invoice__c WHERE Name = 'INV-0007'];
            blng__InvoiceLine__c[] invoiceLines ;
            if (!invoices.isEmpty()) {
                Set<Id> invoiceIds = new Set<Id>();
                for (blng__Invoice__c invoice : invoices) {
                    invoiceIds.add(invoice.Id);                
                }              
                invoiceLines = getInvoiceLines(invoiceIds); 
            }       
            
            InvoiceEntity.TaxCodeRef CustReffer = new InvoiceEntity.TaxCodeRef() ;
            CustReffer.value = '1';     
                        
            InvoiceEntity.TaxCodeRef  TaxCodeRefs  = new InvoiceEntity.TaxCodeRef() ;
            TaxCodeRefs.value = '26';   
                        
            List<InvoiceEntity.Line> LineList = new List<InvoiceEntity.Line>();
            for (Integer i=0;i<invoiceLines.size();i++)
            {
                InvoiceEntity.ItemRef ItemRefs = new InvoiceEntity.ItemRef() ;
                ItemRefs.value = '1';       //Product/service details
                ItemRefs.name = 'Hours';
            
                InvoiceEntity.SalesItemLineDetail saleslines = new InvoiceEntity.SalesItemLineDetail();
                saleslines.ItemRef = ItemRefs;
                //saleslines.UnitPrice = double.ValueOf(invoiceLines[i].blng__Subtotal__c);
                //saleslines.Qty = 1;//integer.ValueOf(invoiceLines[i].blng__CalculatedQuantity__c);        
                saleslines.TaxCodeRef = TaxCodeRefs;    
            
                InvoiceEntity.Line LineRec = new InvoiceEntity.Line();
                //if (i==(invoiceLines.size()-1 )) {
                //  LineRec.Amount = 4000.00;
                //  LineRec.DetailType = 'SubTotalLineDetail';
                //} else {
                    LineRec.Description = invoiceLines[i].Name;
                    LineRec.Amount = invoiceLines[i].blng__Subtotal__c;
                    LineRec.DetailType = 'SalesItemLineDetail';
                    LineRec.SalesItemLineDetail = saleslines;
                    LineRec.LineNum = i+1;
                //}
                LineList.add(LineRec);
            }
            
            obj.CustomerRef = CustReffer;
            obj.Line = LineList;
            obj.DocNumber = 'INV-0016'; 
            
            /*InvoiceEntity.TaxCodeRef CustReffer = new InvoiceEntity.TaxCodeRef() ;
            CustReffer.value = '1';     
            
            InvoiceEntity.ItemRef ItemRefs = new InvoiceEntity.ItemRef() ;
            ItemRefs.value = '1';       
            ItemRefs.name = 'Hours';
            
            InvoiceEntity.TaxCodeRef  TaxCodeRefs  = new InvoiceEntity.TaxCodeRef() ;
            TaxCodeRefs.value = '26';   
            
            InvoiceEntity.SalesItemLineDetail saleslines = new InvoiceEntity.SalesItemLineDetail();
            saleslines.ItemRef = ItemRefs;
            saleslines.UnitPrice = 2000;
            saleslines.Qty = 1;     
            saleslines.TaxCodeRef = TaxCodeRefs;                
            
            List<InvoiceEntity.Line> LineList = new List<InvoiceEntity.Line>();
            for (Integer i=0;i<3;i++)
            {
                InvoiceEntity.Line LineRec = new InvoiceEntity.Line();
                if (i==2 ) {
                    LineRec.Amount = 4000.00;
                    LineRec.DetailType = 'SubTotalLineDetail';
                } else {
                    LineRec.Description = 'test by API';
                    LineRec.Amount = 2000.00;
                    LineRec.DetailType = 'SalesItemLineDetail';
                    LineRec.SalesItemLineDetail = saleslines;
                    LineRec.LineNum = i+1;
                }
                LineList.add(LineRec);
            }
            
            obj.CustomerRef = CustReffer;
            obj.Line = LineList;
            obj.DocNumber = 'INV-0015'; */  

            String endPoint = '/invoice';
            String reqBody = JSON.serialize(obj);
            HttpResponse httpResponse = new HttpResponse();
            httpResponse = SP_LoginQuickBooksController.makeHttpPostRequest(endPoint, reqBody);
            statusMessageAccount = 'API Callout Status : ' + httpResponse.getStatusCode() + ' : ' + httpResponse.getStatus();
            if(httpResponse.getStatusCode() == 200){
                accountInfo = httpResponse.getBody();
            }
            accountInfo = httpResponse.getBody(); //TODO - handle error status codes
        }
        catch(System.Exception e){
            System.debug('Exception found in class SP_ConnectedController method createAccount ' + e.getMessage());
            System.debug('Exception found in class SP_ConnectedController method createAccount ' + e.getLineNumber());
            statusMessageAccount = 'Exception: ' + e.getMessage();
        } 
    }
    
    @future(callout=true)
    public static void createQBInvoice (String nme,String cstref) {
        try {       
            System.debug('Start SP_ConnectedController method createQBInvoice ' );
            InvoiceEntity obj = new InvoiceEntity();
            
            blng__Invoice__c[] invoices = [SELECT Id,name, blng__InvoiceStatus__c FROM blng__Invoice__c WHERE Name =: nme];
            blng__InvoiceLine__c[] invoiceLines ;
            if (!invoices.isEmpty()) {
                Set<Id> invoiceIds = new Set<Id>();
                for (blng__Invoice__c invoice : invoices) {
                    invoiceIds.add(invoice.Id);                
                }              
                invoiceLines = getInvoiceLines(invoiceIds); 
            }       
            
            InvoiceEntity.TaxCodeRef CustReffer = new InvoiceEntity.TaxCodeRef() ;
            CustReffer.value = cstref;     
                        
            InvoiceEntity.TaxCodeRef  TaxCodeRefs  = new InvoiceEntity.TaxCodeRef() ;
            TaxCodeRefs.value = '26';   
                        
            List<InvoiceEntity.Line> LineList = new List<InvoiceEntity.Line>();
            for (Integer i=0;i<invoiceLines.size();i++)
            {
                InvoiceEntity.ItemRef ItemRefs = new InvoiceEntity.ItemRef() ;
                //invoiceLines[i].blng__Product__r.QBPrdExternal__c;
                ItemRefs.value = '14';       //Product/service details
                ItemRefs.name = 'Developer';
            
                InvoiceEntity.SalesItemLineDetail saleslines = new InvoiceEntity.SalesItemLineDetail();
                saleslines.ItemRef = ItemRefs;
                saleslines.TaxCodeRef = TaxCodeRefs;    
            
                InvoiceEntity.Line LineRec = new InvoiceEntity.Line();
                     LineRec.Description = invoiceLines[i].Name;
                    LineRec.Amount = invoiceLines[i].blng__Subtotal__c;
                    LineRec.DetailType = 'SalesItemLineDetail';
                    LineRec.SalesItemLineDetail = saleslines;
                    LineRec.LineNum = i+1;
              
                LineList.add(LineRec);
            }
            
            obj.CustomerRef = CustReffer;
            obj.Line = LineList;
            obj.DocNumber = nme; 
            
            String endPoint = '/invoice';
            String reqBody = JSON.serialize(obj);
            
            System.debug('SP SP_ConnectedController method reqBody ' +reqBody );
            HttpResponse httpResponse = new HttpResponse();
            httpResponse = SP_LoginQuickBooksController.makeHttpPostRequest(endPoint, reqBody);
            statusMessageAccount = 'API Callout Status : ' + httpResponse.getStatusCode() + ' : ' + httpResponse.getStatus();
            if(httpResponse.getStatusCode() == 200){
                accountInfo = httpResponse.getBody();
            }
            accountInfo = httpResponse.getBody(); //TODO - handle error status codes
        }
        catch(System.Exception e){
            System.debug('Exception found in class SP_ConnectedController method createAccount ' + e.getMessage());
            System.debug('Exception found in class SP_ConnectedController method createAccount ' + e.getLineNumber());
            statusMessageAccount = 'Exception: ' + e.getMessage();
        } 
    }
    
    public static void getAccountById() {
        /*try{
            API_Token__c apiToken = [Select Access_Token__c, Realm_ID__c from API_Token__c ORDER BY LastModifiedDate DESC limit 1];
            String accessToken = apiToken.Access_Token__c ;
            String endPoint = '/account/'+accountIdInput;
            HttpResponse httpResponse = new HttpResponse();
            httpResponse = SP_LoginQuickBooksController.makeHttpGetRequest(endPoint);
            statusMessageAccount = 'API Callout Status : ' + httpResponse.getStatus();
            if(httpResponse.getStatusCode() == 200){
                accountInfo = httpResponse.getBody();
            }
            //TODO - handle error status codes
        }
        catch(System.Exception e){
            System.debug('Exception found in class SP_ConnectedController method getCompanyInfo ' + e.getMessage());
            System.debug('Exception found in class SP_ConnectedController method getCompanyInfo ' + e.getLineNumber());
            statusMessageAccount = 'Exception: ' + e.getMessage();
        }*/
        getRecords('', 'Account', 'Name = \'SatyaAPI\'') ;
        //getRecords('account', '', '133') ;
    } 
    
    public static void getRecords(String ObjectName, String QueryObj, String WhereClause) {
        try{
            API_Token__c apiToken = [Select Access_Token__c, Realm_ID__c from API_Token__c ORDER BY LastModifiedDate DESC limit 1];
            String accessToken = apiToken.Access_Token__c ;
            String Query = '';
            String endPoint ='';
            if (ObjectName == '') {
                If (WhereClause == '')
                    Query = 'select * from '+ QueryObj ;
                else    
                    Query = 'select * from '+ QueryObj + ' where '+WhereClause;
                
                endPoint = '/query?query='+Query;
                endPoint ='select * from Account where Name = \'SatyaAPI\'';//select * from Account where Name = 'SatyaAPI'
            } else {
                endPoint = '/'+ObjectName+'/'+WhereClause;
            }
            System.debug('query found in class SP_ConnectedController method getRecords ' + endPoint);
            
            //endPoint = '/query?query='+Query;
            HttpResponse httpResponse = new HttpResponse();
            httpResponse = SP_LoginQuickBooksController.makeHttpGetRequest(endPoint);
            statusMessageAccount = 'API Callout Status : ' + httpResponse.getStatus();
            if(httpResponse.getStatusCode() == 200){
                accountInfo = httpResponse.getBody();
            }
            //TODO - handle error status codes
        }
        catch(System.Exception e){
            System.debug('Exception found in class SP_ConnectedController method getRecords ' + e.getMessage());
            System.debug('Exception found in class SP_ConnectedController method getRecords ' + e.getLineNumber());
            statusMessageAccount = 'Exception: ' + e.getMessage();
        }
    } 
}