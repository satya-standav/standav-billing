Public class ResourcesTriggerHandler{
    public static void beforeInsertHandler(List<Resource__c> relatedResourcesList){
        if(Trigger.isInsert && Trigger.isBefore){
            ResourceTriggerHelper.beforeInsertMethod(relatedResourcesList);
        }
        if(Trigger.isDelete && Trigger.isBefore){
            ResourceTriggerHelper.beforeDeleteMethod(relatedResourcesList);
        }
        
    }
    
    /*public static void afterUpdateHandler(Map<Id, Resource__c> newResourceDataMap, Map<Id, Resource__c> oldResourceDataMap){
if(Trigger.isUpdate && Trigger.isAfter){
//ResourceTriggerHelper.updateBudgetedAllocation(newResourceDataMap);    
}
}*/
    /* public static void afterInsertHandler(List<Resource__c> relatedResourcesList){
if(Trigger.isInsert && Trigger.isAfter){
ResourceTriggerHelper.beforeInsertMethod(relatedResourcesList);    
}
}
public static void beforeUpdatHandler(List<Resource__c> relatedResourcesList, Map<Id, Resource__c> oldResourceDataMap){
if(Trigger.isUpdate && Trigger.isBefore){

}
}
*/
    /*   public static void beforeDeleteHandler(){
if(Trigger.isDelete && Trigger.isBefore){

}
}
public static void afterDeleteHandler(){
if(Trigger.isDelete && Trigger.isAfter){

}
}
public static void afterUndeleteHandler(){
if(Trigger.isUndelete && Trigger.isAfter){

}
}*/
}