global class EP_Update_Total_No_Approved_Hours implements Database.Batchable<sObject>
{         
    global Database.QueryLocator start(Database.BatchableContext BC)
    { 
        //String approvedStatus = 'Approved';        
        return Database.getQueryLocator([Select Project__r.Name,Resource__r.User__r.Name,Name,(Select Status__c,
        EP_Total_Number_of_Billable_Hours__c, EP_Total_Number_of_Unbillable_Hours__c from
        Time_Cards__r Where Status__c = 'Approved' ) from Assignment__c]);
    }
    global void execute(Database.BatchableContext BC, List<Assignment__c> assignmentList)
    {
        for(Assignment__c assignment : assignmentList){
            Double totalHoursapproved = 0;                   
            for(Time_Card__c tc : assignment.Time_Cards__r){               
                totalHoursapproved+=tc.EP_Total_Number_of_Billable_Hours__c;
            }
            assignment.Total_Number_of_Approved_Hours__c = totalHoursapproved;
          
        }
              
        database.update(assignmentList,false);
    }
    global void finish(Database.BatchableContext BC) {
    }
    
  
}