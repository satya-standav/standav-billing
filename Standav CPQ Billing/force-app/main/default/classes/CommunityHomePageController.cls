public class CommunityHomePageController {

    public String errorMessage{get;set;}

    public CommunityHomePageController(ApexPages.StandardController controller) {

    }

    public Id ResourceId {get;set;}  
  
    public CommunityHomePageController() { 
        try{
            Resource__c resourceList= [SELECT ID, User__c FROM Resource__c WHERE User__c =: UserInfo.getUserId() LIMIT 1];
            system.debug('resourceList'+resourceList);
            if(resourceList!=Null){
                ResourceId = resourceList.ID;
            }else{
               errorMessage = 'No Resource created for the logged in user, create a resource to view the home page';
            }    
        }catch(Exception ex){
            errorMessage = 'No Resource created for the logged in user, create a resource to view the home page';
        } 
        
    }  
}