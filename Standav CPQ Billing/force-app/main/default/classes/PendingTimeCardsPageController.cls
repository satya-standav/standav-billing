/**
*  @Class Name: PendingTimeCardsPageController
*  @Description: Handles all functionality required for "PendingTimeCardsPage" visualforce page.
*  @Company: Standav
*  @CreatedDate: 11/21/2019
*  Change Log
*  -----------------------------------------------------------
*  Developer: Shruti MP           
   Modification Date : 11/27/2019          
   Comments
*  -----------------------------------------------------------
*/
public class PendingTimeCardsPageController {

    //Class variable declaration section - Strart
    public String currentResourceName {
        get;
        set;
    }
    public List < timeCardWrapper > timeCardWrapperList {
        get {
            if (timeCardWrapperList != null) {
                return timeCardWrapperList;
            }
            return fetchTimeCards();
        }
        public set;
    }
    public String errorMessage{get;set;}
    //Class variable declaration section - End
    
    //Fecth the related data method - strat
    /**
     *  Method Name         : fetchTimeCards
     *  param               : N/A
     *  Description         : method to fetch the timecard based on resource and selected week
     */
    public List < timeCardWrapper > fetchTimeCards() {
        
        timeCardWrapperList = new List < timeCardWrapper > ();
        currentResourceName = UserInfo.getName();
        
        //Collect the assignments which are active on day the page loads
        for (Assignment__c relatedAssignment: [SELECT Id, Name, RecordTypeId, Billing_Rate__c, Approval_Type__c, 
            Start_Date__c, End_Date__c,
                Billing_Type__c, EP_Shadow_Resource__c, Is_Primary_Resource_Contributing__c, RecordType.Name, 
                Resource__r.User__c, EP_Shadow_Resource__r.User__c ,
                Resource_Name__c, Role__c, Resource__c, Status__c, Assignment_Type__c, Project__c, Project__r.Name, 
                EP_Shadow_Resource__r.User__r.Name, Resource__r.User__r.Name,
                (SELECT Id, Name, Assignment__r.EP_Shadow_Resource__c, Assignment__r.Resource__c,
                    Type__c, EP_End_Date__c, Billable_Hours_Fri__c, EP_Notes__c, EP_Approval_Date__c,
                    EP_Total_Number_of_Billable_Hours__c, EP_Total_Number_of_Unbillable_Hours__c,
                    EP_Rejected_Date__c, Submitted_On__c, Project_Manager__c, Billable_Hours_Mon__c,
                    Billable_Hours_Sat__c, Assignment__c, Status__c, Start_Date__c, Billable_Hours_Sun__c,
                    Billable_Hours_Thu__c, Billable_Hours_Tue__c, Billable_Hours_Wed__c, UnBillable_Hours_Fri__c,
                    UnBillable_Hours_Mon__c, UnBillable_Hours_Sat__c, UnBillable_Hours_Sun__c, UnBillable_Hours_Thu__c, 
                    Assignment__r.RecordType.Name,
                    UnBillable_Hours_Tue__c, UnBillable_Hours_Wed__c, Assignment__r.Project__c, Assignment__r.Project__r.Name,
                    X1st_Approver__c, X2nd_Approver__c, X3rd_Approver__c, X4th_Approver__c, X5th_Approver__c, 
                    Assignment__r.Is_Primary_Resource_Contributing__c,
                    Assignment__r.Resource__r.User__c
                    FROM Time_Cards__r 
                    WHERE Status__c != 'Approved' AND Status__c != 'Client Approved')
                FROM Assignment__c
                WHERE EP_Shadow_Resource__r.User__c =: UserInfo.getUserId() OR Resource__r.User__c =: UserInfo.getUserId()]) {
                system.debug('----------1-----------'+relatedAssignment.EP_Shadow_Resource__r.User__r.Name);
                system.debug('-----------1----------'+relatedAssignment.Resource__r.User__r.Name);
            system.debug('--------relatedAssignment----------'+relatedAssignment);
            if((relatedAssignment.RecordType.Name == 'Primary' && relatedAssignment.Resource__r.User__c == UserInfo.getUserId()) ||
                (relatedAssignment.RecordType.Name == 'Shadow' && relatedAssignment.EP_Shadow_Resource__r.User__c == UserInfo.getUserId()) &&
                (relatedAssignment.Time_Cards__r.size() > 0 && relatedAssignment.Time_Cards__r != null )) {
                
                timeCardWrapper newTimeCardWrpper = new timeCardWrapper();
                newTimeCardWrpper.relatedAssignment = relatedAssignment;
                for (Time_Card__c relatedTimeCard: relatedAssignment.Time_Cards__r) {
                    if (relatedTimeCard.Status__c != 'Submitted') {
                        if (newTimeCardWrpper.assignmentWithTimeCardsMap.get(relatedTimeCard.Assignment__c) != null &&
                            !newTimeCardWrpper.assignmentWithTimeCardsMap.get(relatedTimeCard.Assignment__c).contains(relatedTimeCard)) {
                            newTimeCardWrpper.assignmentWithTimeCardsMap.get(relatedTimeCard.Assignment__c).add(relatedTimeCard);
                        } else {
                            List < Time_Card__c > releatedTimeCardsList = new List < Time_Card__c > ();
                            releatedTimeCardsList.add(relatedTimeCard);
                            newTimeCardWrpper.assignmentWithTimeCardsMap.put(relatedTimeCard.Assignment__c, releatedTimeCardsList);
                        }
                    }
                }
                if(!newTimeCardWrpper.assignmentWithTimeCardsMap.isEmpty()){
                    timeCardWrapperList.add(newTimeCardWrpper);
                }
            }
        }
        if(timeCardWrapperList.size() == 0 ){
        //    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'No Time cards are assigned to you for approval'));
            //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'No  pending Time cards for submission');
            //ApexPages.addMessage(myMsg);
            errorMessage = 'No Pending Timesheets';
        }

        system.debug('--------timeCardWrapperList----------'+timeCardWrapperList);
        system.debug('--------timeCardWrapperList----------'+timeCardWrapperList.size());
        return timeCardWrapperList; 
    }
    //Fetch the related data method -end

    //Wrapper class - start
    public class timeCardWrapper {
        
        public Assignment__c relatedAssignment {
            get;
            set;
        }
        public Map < Id, List < Time_Card__c >> assignmentWithTimeCardsMap {
            get;
            set;
        }
        
        public timeCardWrapper() {
            this.relatedAssignment = new Assignment__c();
            this.assignmentWithTimeCardsMap = new Map < Id, List < Time_Card__c >> ();
        }
    }
    //Wrapper class -End
}