@isTest
private class EmailtoContactsTest{
   // @testSetUp 
   @isTest
    static void createData(){
		
         User user = new User();
            user.LastName = 'Test User';
            user.Alias = 'tu';
            user.Email = 'test@gmail.com';
            user.Username = 'test.dev@stand.com';
            user.CommunityNickname = 'test.t';
            //user.UserRoleId = '00ei0000000rTfp';
            user.User_Time_Zone__c = 'Pst';
            user.ProfileId = '00e54000000HSHG';           
            user.TimeZoneSidKey    = 'America/Denver';
            user.LocaleSidKey      = 'en_US';
            user.EmailEncodingKey  = 'UTF-8';
            user.LanguageLocaleKey = 'en_US';
            insert user;
        	
        	Contact con = new Contact();
        	con.firstname = 'Test';
        	con.LastName = 'LAST';
        	insert con;
        
        	Account acc = new Account();
        	acc.Name = 'Test Account';
        	insert acc;
            
            Project__c project = new Project__c();
            project.Name = 'Test Project';
            project.EP_Project_Manager__c = user.Id;
        	project.Number__c = 2;
            project.Contact_Person__c = con.Id;
            project.Start_Date__c = Date.newInstance(2016, 12, 9);
        	project.Account__c = acc.id;
            insert project;        
            update project;
        
            Resource__c resource = new Resource__c();
            resource.User__c = user.id;
            resource.Contact__c = con.Id;
            insert resource;        
            update resource;
        
            Assignment__c assignment = new Assignment__c();
            assignment.Project__c = project.id;
            assignment.Resource__c = resource.id;
            assignment.Status__c = 'Approved';
        	assignment.Budgeted_Hours__c = 100;
            assignment.Start_Date__c = Date.newInstance(2016, 12, 9);
            assignment.End_Date__c = Date.newInstance(2018, 12, 9);
            assignment.Role__c = 'Developer';
            assignment.Billing_Type__c = 'Billable';
            assignment.Assignment_Type__c = 'Long Term';   
            //assignment.Actual_Hours__c = 20;
            insert assignment;        
           // update assignment;
            
            Time_Card__c card = new Time_Card__c();
            card.Billable_Hours_Fri__c = 4;
            card.Billable_Hours_Mon__c =4;
            card.Billable_Hours_Sat__c = 0;
            card.Billable_Hours_Sun__c = 0;
            card.Billable_Hours_Thu__c = 4;
            card.Billable_Hours_Tue__c = 4;
            card.Billable_Hours_Wed__c =4;
            card.Status__c = 'Approved';  
        	card.Assignment__c = assignment.id;
            insert card;        
        
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
       

        email.subject ='Test' ;
        email.fromName = 'test test';
        email.plainTextBody = 'Hello, this a test email body. for testing purposes only.Phone:123456 Bye';
        Messaging.InboundEmail.BinaryAttachment[] binaryAttachments = new Messaging.InboundEmail.BinaryAttachment[1]; 
        Messaging.InboundEmail.BinaryAttachment binaryAttachment = new Messaging.InboundEmail.BinaryAttachment();
        binaryAttachment.Filename = 'test.csv';
        String algorithmName = 'HMacSHA1';
        
        binaryAttachment.Body = blob.valueOf('my attachment text');
        binaryAttachments[0] =  binaryAttachment ;
        email.binaryAttachments = binaryAttachments ;
        envelope.fromAddress = 'user@acme.com';
        
        test.startTest();
        EmailtoContacts testInbound=new EmailtoContacts();
      	testInbound.handleInboundEmail(email, envelope);
        test.stopTest();
}
}