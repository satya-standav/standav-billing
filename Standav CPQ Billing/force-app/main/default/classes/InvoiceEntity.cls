public class InvoiceEntity {
    
    public List<Line> Line;
    public TaxCodeRef CustomerRef;
    public string DocNumber;
    
    public class Line {
        public String Description;
        public Double Amount;
        public String DetailType;
        public SalesItemLineDetail SalesItemLineDetail;
        public String Id;
        public Integer LineNum;
        public SubTotalLineDetail SubTotalLineDetail;
    }


    public class TaxCodeRef {
        public String value;
    }

    public class SubTotalLineDetail {
    }

    public class SalesItemLineDetail {
        public ItemRef ItemRef;
        public Integer UnitPrice;
        public Integer Qty;
        public TaxCodeRef TaxCodeRef;
    }

    public class ItemRef {
        public String value;
        public String name;
    }

}