global class EP_Total_Approved_Hours_Weekly implements Database.Batchable<sObject>
{    
    private List<Assignment__c> allAssignmentList = new List<Assignment__c>();      
    global Database.QueryLocator start(Database.BatchableContext BC)
    { 
        //String approvedStatus = 'Approved';        
        return Database.getQueryLocator([Select Project__r.Name,Resource__r.User__r.Name,Name,(Select Status__c,Start_Date__c,
        EP_Total_Number_of_Billable_Hours__c, EP_Total_Number_of_Unbillable_Hours__c from
        Time_Cards__r where Status__c != 'Rejected' AND Start_Date__c = LAST_WEEK) from Assignment__c]);
    }
    global void execute(Database.BatchableContext BC, List<Assignment__c> assignmentList)
    {
        for(Assignment__c assignment : assignmentList){
            Double totalHours = 0;
           // double burneddHours = 0;            
            for(Time_Card__c tc : assignment.Time_Cards__r){
                totalHours+=tc.EP_Total_Number_of_Billable_Hours__c;
            }
           // assignment.Actual_Hours_assignment__c = totalHours;
           // assignment.Burned_Hours_assignment__c = burneddHours;
        }
        
        System.debug(assignmentList);
        allAssignmentList.addAll(assignmentList);        
        database.update(assignmentList,false);
    }
    global void finish(Database.BatchableContext BC) {
        
            List<Assignment__c> asslist = [Select Id,Project__r.Name,Resource__r.User__r.Name,Name,(Select Status__c,Start_Date__c,
            EP_Total_Number_of_Billable_Hours__c, EP_Total_Number_of_Unbillable_Hours__c from
            Time_Cards__r where Status__c != 'Rejected' AND Start_Date__c = LAST_WEEK) from Assignment__c];            
            String csvContent = '';
            String csvHeader = 'Sr.No.,Project Name,Resource Name,Week Starting,Hours Submitted,Status\n';
            String csvBody = '';
            Integer srNo = 1;            
            for(Assignment__c assign : asslist){   
               List<Time_Card__c> timeCardList = assign.Time_Cards__r;
                if(timeCardList!=null && !timeCardList.isEmpty()) {                
                    for(Time_Card__c tc : timeCardList){
                    csvBody+= srNo + ',' + assign.Project__r.Name + ',' + assign.Resource__r.User__r.Name + ',' + tc.Start_Date__c.format()+','+ tc.EP_Total_Number_of_Billable_Hours__c +','+ tc.Status__c + '\n';    
                    srNo++;
                }
                }
            }
        
            //List<User> userList = new List<User>();
            //userList = [SELECT Id,Email,IsActive FROM User WHERE Profile.Name = 'Finance Manager' AND IsActive = True] ;
            //List<String> toAddresses = new List<String>();
            //for(User u : userList){           
               //toAddresses.add(u.Email);
            //}  

            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {'kevin.s@standav.com'};
            mail.setToAddresses(toAddresses);
            mail.setSubject('Weekly Finance Report');
            mail.setPlainTextBody('Please find below the attached Report for Approved Hours from the Last Week');        
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
  
}