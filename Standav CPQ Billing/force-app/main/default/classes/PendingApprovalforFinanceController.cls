public class PendingApprovalforFinanceController {

    public string projectCriteraString{get; set;}
    public string statusCriteraString{get; set;}
    public string resourceCriteraString{get; set;}
    public String errorMessage{get;set;}
    public Date startDateFilter{get;set;}
    public Date endDateFilter{get;set;}
    public List<SelectOption> resourceNameOptionsList{get; set;} 
    public List<SelectOption> projectNameOptionsList{get; set;}  
    public Decimal totalBillableHours{get; set;} 
    public Decimal totalPendingBillableHours{get;set;}  
    public List < timeCardWrapper > timeCardWrapperList {
        get {
            if (timeCardWrapperList != null) {
                return timeCardWrapperList;
            }
            return fetchTimeCards();
        }
        public set;
    }
    Map<Id, String> userIdWithNameMap;

    //Constructor -start
    public PendingApprovalforFinanceController(){
        userIdWithNameMap = new Map<Id, String>();
        resourceNameOptionsList = new List<SelectOption>();
        Set<SelectOption> resourceNameSet = new Set<SelectOption>();
        resourceNameSet.add(new SelectOption('', 'Please Select'));

        for(User UserRec : UserDataCache.getMap().values()){
            userIdWithNameMap.put(UserRec.Id, UserRec.FirstName+' '+UserRec.LastName);
            resourceNameSet.add(new SelectOption(UserRec.FirstName+' '+UserRec.LastName, UserRec.FirstName+' '+UserRec.LastName)); 
        } 
        resourceNameOptionsList.addAll(resourceNameSet);  
    }
    //Constructor - End

    public PageReference resetFiltersMethod(){
        fetchTimeCards();
        errorMessage = null;
        projectCriteraString = null;
        statusCriteraString = null;
        resourceCriteraString = null;
        startDateFilter = null;
        endDateFilter = null;
        return null;
    }
    public PageReference filterMethod(){
        totalBillableHours = 0.0;
        Date filterStartDate = startDateFilter;
        Date filterEndDate = endDateFilter;
        string queryString;
        system.debug('--1----resourceCriteraString--------'+resourceCriteraString);
        if(startDateFilter != null){
            Date dToday1 = filterStartDate;
            Datetime dt1 = datetime.newInstance(dToday1.year(), dToday1.month(),dToday1.day());
            String dayOfCurrentDate = dt1.format('E');
            filterStartDate = (dayOfCurrentDate == 'Sun' ? filterStartDate.addDays(1):
                             dayOfCurrentDate == 'Mon' ? filterStartDate.addDays(0):
                             dayOfCurrentDate == 'Tue' ? filterStartDate.addDays(-1):
                             dayOfCurrentDate == 'Wed' ? filterStartDate.addDays(-2):
                             dayOfCurrentDate == 'Thu' ? filterStartDate.addDays(-3):
                             dayOfCurrentDate == 'Fri' ? filterStartDate.addDays(-4):
                             dayOfCurrentDate == 'Sat' ? filterStartDate.addDays(-5): 
                             filterStartDate);     
        }
        if(endDateFilter != null){
            Date dToday2 = filterEndDate;
            Datetime dt2 = datetime.newInstance(dToday2.year(), dToday2.month(),dToday2.day());
            String dayOfCurrentDate = dt2.format('E');
            filterEndDate = (dayOfCurrentDate == 'Sun' ? filterEndDate.addDays(0):
                             dayOfCurrentDate == 'Mon' ? filterEndDate.addDays(6):
                             dayOfCurrentDate == 'Tue' ? filterEndDate.addDays(5):
                             dayOfCurrentDate == 'Wed' ? filterEndDate.addDays(4):
                             dayOfCurrentDate == 'Thu' ? filterEndDate.addDays(3):
                             dayOfCurrentDate == 'Fri' ? filterEndDate.addDays(2):
                             dayOfCurrentDate == 'Sat' ? filterEndDate.addDays(1): 
                             filterEndDate);     
        }
        if(projectCriteraString != null){
            queryString = 'SELECT Id, Name, Start_Date__c, End_Date__c,Budgeted_Hours__c,Project__r.Name,EP_Shadow_Resource__r.User__r.Name, Resource__r.User__r.Name,RecordType.Name,Billing_Type__c,'+
                '(SELECT Id, Name,Assignment__r.Resource__r.User__c, Assignment__c, EP_Total_Number_of_Billable_Hours__c, Start_Date__c, EP_End_Date__c, Status__c,Invoiced__c FROM Time_Cards__r WHERE Invoiced__c = False)'+
                ' FROM Assignment__c WHERE Project__r.Name =: projectCriteraString';
        
        
            if(projectCriteraString != null && statusCriteraString != null)
                queryString = 'SELECT Id, Name, Start_Date__c, End_Date__c,Budgeted_Hours__c,Project__r.Name,EP_Shadow_Resource__r.User__r.Name, Resource__r.User__r.Name,RecordType.Name,Billing_Type__c,'+
                    '(SELECT Id, Name,Assignment__r.Resource__r.User__c, Assignment__c, EP_Total_Number_of_Billable_Hours__c, Start_Date__c, EP_End_Date__c, Status__c,Invoiced__c FROM Time_Cards__r WHERE Invoiced__c = False AND (Status__c =: statusCriteraString))'+
                    ' FROM Assignment__c WHERE Project__r.Name =: projectCriteraString';
            
            if(projectCriteraString != null && resourceCriteraString != null)
                queryString = 'SELECT Id, Name, Start_Date__c, End_Date__c,Budgeted_Hours__c,Project__r.Name,EP_Shadow_Resource__r.User__r.Name, Resource__r.User__r.Name,RecordType.Name,Billing_Type__c,'+
                    '(SELECT Id, Name,Assignment__r.Resource__r.User__c, Assignment__c, EP_Total_Number_of_Billable_Hours__c, Start_Date__c, EP_End_Date__c, Status__c,Invoiced__c FROM Time_Cards__r WHERE Invoiced__c = False)'+
                    ' FROM Assignment__c WHERE Resource__r.User__r.Name =: resourceCriteraString AND Project__r.Name =: projectCriteraString';

            if(projectCriteraString != null && statusCriteraString != null && filterStartDate != null && filterEndDate != null && resourceCriteraString == null)
                queryString = 'SELECT Id, Name, Start_Date__c, End_Date__c,Budgeted_Hours__c,Project__r.Name,EP_Shadow_Resource__r.User__r.Name, Resource__r.User__r.Name,RecordType.Name,Billing_Type__c,'+
                    '(SELECT Id, Name, Assignment__r.Resource__r.User__c,Assignment__c, EP_Total_Number_of_Billable_Hours__c, Start_Date__c, EP_End_Date__c, Status__c,Invoiced__c FROM Time_Cards__r WHERE Invoiced__c = False AND (Status__c =: statusCriteraString) AND (Start_Date__c >=: filterStartDate AND EP_End_Date__c <=: filterEndDate))'+
                    ' FROM Assignment__c WHERE Project__r.Name =: projectCriteraString';

            if(filterStartDate != null && filterEndDate != null && projectCriteraString != null && statusCriteraString == null && resourceCriteraString == null)
                queryString = 'SELECT Id, Name, Start_Date__c, End_Date__c,Budgeted_Hours__c,Project__r.Name,EP_Shadow_Resource__r.User__r.Name, Resource__r.User__r.Name,RecordType.Name,Billing_Type__c,'+
                    '(SELECT Id, Name,Assignment__r.Resource__r.User__c, Assignment__c, EP_Total_Number_of_Billable_Hours__c, Start_Date__c, EP_End_Date__c, Status__c,Invoiced__c FROM Time_Cards__r WHERE Invoiced__c = False AND (Start_Date__c >=: filterStartDate AND EP_End_Date__c <=: filterEndDate))'+
                    ' FROM Assignment__c WHERE Project__r.Name =: projectCriteraString';

        }
        else if(statusCriteraString != null){
            queryString = 'SELECT Id, Name, Start_Date__c, End_Date__c,Budgeted_Hours__c,Project__r.Name,EP_Shadow_Resource__r.User__r.Name, Resource__r.User__r.Name,RecordType.Name,Billing_Type__c,'+
                '(SELECT Id, Name,Assignment__r.Resource__r.User__c, Assignment__c, EP_Total_Number_of_Billable_Hours__c, Start_Date__c, EP_End_Date__c, Status__c,Invoiced__c FROM Time_Cards__r WHERE Invoiced__c = False AND (Status__c =: statusCriteraString))'+
                ' FROM Assignment__c';
       
        if(statusCriteraString != null && filterStartDate != null && filterEndDate != null && projectCriteraString == null)
            queryString = 'SELECT Id, Name, Start_Date__c, End_Date__c,Budgeted_Hours__c,Project__r.Name,EP_Shadow_Resource__r.User__r.Name, Resource__r.User__r.Name,RecordType.Name,Billing_Type__c,'+
                '(SELECT Id, Name, Assignment__r.Resource__r.User__c,Assignment__c, EP_Total_Number_of_Billable_Hours__c, Start_Date__c, EP_End_Date__c, Status__c,Invoiced__c FROM Time_Cards__r WHERE Invoiced__c = False AND Status__c =: statusCriteraString AND (Start_Date__c >=: filterStartDate AND EP_End_Date__c <=: filterEndDate))'+
                ' FROM Assignment__c';        

        if(statusCriteraString != null && resourceCriteraString != null)
            queryString = 'SELECT Id, Name, Start_Date__c, End_Date__c,Budgeted_Hours__c,Project__r.Name,EP_Shadow_Resource__r.User__r.Name, Resource__r.User__r.Name,RecordType.Name,Billing_Type__c,'+
                '(SELECT Id, Name,Assignment__r.Resource__r.User__c, Assignment__c, EP_Total_Number_of_Billable_Hours__c, Start_Date__c, EP_End_Date__c, Status__c,Invoiced__c FROM Time_Cards__r WHERE Invoiced__c = False AND (Status__c =: statusCriteraString))'+
                ' FROM Assignment__c WHERE Resource__r.User__r.Name =: resourceCriteraString';

        }
        else if(resourceCriteraString != null){
            queryString = 'SELECT Id, Name, Start_Date__c, End_Date__c,Budgeted_Hours__c,Project__r.Name,EP_Shadow_Resource__r.User__r.Name, Resource__r.User__r.Name,RecordType.Name,Billing_Type__c,'+
                '(SELECT Id, Name,Assignment__r.Resource__r.User__c, Assignment__c, EP_Total_Number_of_Billable_Hours__c, Start_Date__c, EP_End_Date__c, Status__c,Invoiced__c FROM Time_Cards__r WHERE Invoiced__c = False)'+
                ' FROM Assignment__c WHERE Resource__r.User__r.Name =: resourceCriteraString';

            
            if(filterStartDate != null && filterEndDate != null && resourceCriteraString != null)
            queryString = 'SELECT Id, Name, Start_Date__c, End_Date__c,Budgeted_Hours__c,Project__r.Name,EP_Shadow_Resource__r.User__r.Name, Resource__r.User__r.Name,'+
                '(SELECT Id, Name,Assignment__r.Resource__r.User__c, Assignment__c, EP_Total_Number_of_Billable_Hours__c, Start_Date__c, EP_End_Date__c, Status__c,Invoiced__c FROM Time_Cards__r WHERE Invoiced__c = False AND (Start_Date__c >=: filterStartDate AND EP_End_Date__c <=: filterEndDate))'+
                ' FROM Assignment__c WHERE Resource__r.User__r.Name =: resourceCriteraString';

        }

        else if(filterStartDate != null && filterEndDate != null && projectCriteraString == null && statusCriteraString == null && resourceCriteraString == null){
            queryString = 'SELECT Id, Name, Start_Date__c, End_Date__c,Budgeted_Hours__c,Project__r.Name,EP_Shadow_Resource__r.User__r.Name, Resource__r.User__r.Name,RecordType.Name,Billing_Type__c,'+
                '(SELECT Id, Name,Assignment__r.Resource__r.User__c, Assignment__c, EP_Total_Number_of_Billable_Hours__c, Start_Date__c, EP_End_Date__c, Status__c,Invoiced__c FROM Time_Cards__r WHERE Invoiced__c = False AND (Start_Date__c >=: filterStartDate AND EP_End_Date__c <=: filterEndDate))'+
                ' FROM Assignment__c';
        }

                
        if(statusCriteraString != null && resourceCriteraString != null && projectCriteraString != null && (filterStartDate == null && filterEndDate == null))
            queryString = 'SELECT Id, Name, Start_Date__c, End_Date__c,Budgeted_Hours__c,Project__r.Name,EP_Shadow_Resource__r.User__r.Name, Resource__r.User__r.Name,RecordType.Name,Billing_Type__c,'+
                '(SELECT Id, Name,Assignment__r.Resource__r.User__c, Assignment__c, EP_Total_Number_of_Billable_Hours__c, Start_Date__c, EP_End_Date__c, Status__c FROM Time_Cards__r WHERE Invoiced__c = False AND Status__c =: statusCriteraString)'+
                ' FROM Assignment__c WHERE Resource__r.User__r.Name =: resourceCriteraString AND Project__r.Name =: projectCriteraString';

        if(statusCriteraString != null && resourceCriteraString != null && projectCriteraString != null && filterStartDate != null && filterEndDate != null)
            queryString = 'SELECT Id, Name, Start_Date__c, End_Date__c,Budgeted_Hours__c,Project__r.Name,EP_Shadow_Resource__r.User__r.Name, Resource__r.User__r.Name,RecordType.Name,Billing_Type__c,'+
                '(SELECT Id, Name,Assignment__r.Resource__r.User__c, Assignment__c, EP_Total_Number_of_Billable_Hours__c, Start_Date__c, EP_End_Date__c, Status__c FROM Time_Cards__r WHERE Invoiced__c = False AND Status__c =: statusCriteraString AND (Start_Date__c >=: filterStartDate AND EP_End_Date__c <=: filterEndDate))'+
                ' FROM Assignment__c WHERE Resource__r.User__r.Name =: resourceCriteraString AND Project__r.Name =: projectCriteraString';
        
        system.debug('--1----queryString--------'+queryString);
        if(queryString == null){
            timeCardWrapperList = new List < timeCardWrapper > ();
            timeCardWrapperList = fetchTimeCards();
            if(timeCardWrapperList.size() == 0){
                errorMessage = 'No Pending Timesheets for the selected filter';
            }
        }
        if(queryString != null){
            timeCardWrapperList = new List < timeCardWrapper > ();
            for (Assignment__c relatedAssignment : database.query(queryString)){
                if(relatedAssignment.RecordType.Name == 'Primary' && relatedAssignment.Billing_Type__c == 'Billable'){
                    timeCardWrapper newTimeCardWrpper = new timeCardWrapper();
                    newTimeCardWrpper.relatedAssignment = relatedAssignment;
                    for (Time_Card__c relatedTimeCard: relatedAssignment.Time_Cards__r) {
                        newTimeCardWrpper.resourceNameString = userIdWithNameMap.get(relatedTimeCard.Assignment__r.Resource__r.User__c);
                    
                        totalBillableHours = totalBillableHours+relatedTimeCard.EP_Total_Number_of_Billable_Hours__c;
                        if (newTimeCardWrpper.assignmentWithTimeCardsMap.get(relatedTimeCard.Assignment__c) != null &&
                            !newTimeCardWrpper.assignmentWithTimeCardsMap.get(relatedTimeCard.Assignment__c).contains(relatedTimeCard)) {
                            newTimeCardWrpper.assignmentWithTimeCardsMap.get(relatedTimeCard.Assignment__c).add(relatedTimeCard);
                        } else {
                            List < Time_Card__c > releatedTimeCardsList = new List < Time_Card__c > ();
                            releatedTimeCardsList.add(relatedTimeCard);
                            newTimeCardWrpper.assignmentWithTimeCardsMap.put(relatedTimeCard.Assignment__c, releatedTimeCardsList);
                        }
                    }
                    if(!newTimeCardWrpper.assignmentWithTimeCardsMap.isEmpty()){
                        timeCardWrapperList.add(newTimeCardWrpper);
                    }    
                }
                
            }
            if(timeCardWrapperList.size() == 0){
                errorMessage = 'No Pending Timesheets for the selected filter';
            }else{
                errorMessage = null;
            }
        }
        return null;
    }
    
    public List < timeCardWrapper > fetchTimeCards() {
        totalBillableHours = 0.0;
        totalPendingBillableHours = FinanceInvoicePageController.totalBillableHours;
        timeCardWrapperList = new List < timeCardWrapper > ();
        projectNameOptionsList = new List<SelectOption>();
        Set<SelectOption> projectNameSet = new Set<SelectOption>();
        projectNameSet.add(new SelectOption('', 'Please Select'));
        
        //Collect the assignments which are active on day the page loads
        for (Assignment__c relatedAssignment: [SELECT Id, Name, Start_Date__c, End_Date__c,RecordType.Name,Billing_Type__c,
                Budgeted_Hours__c,Project__r.Name,EP_Shadow_Resource__r.User__r.Name, Resource__r.User__r.Name,
                (SELECT Id, Name,Assignment__r.Resource__r.User__c, Assignment__c, 
                    EP_Total_Number_of_Billable_Hours__c, Start_Date__c, EP_End_Date__c, 
                    Status__c,Invoiced__c 
                    FROM Time_Cards__r 
                    WHERE Invoiced__c = False AND 
                    (Status__c ='Assigned' OR Status__c ='Rejected' OR Status__c ='Draft' OR Status__c ='Submitted'))
                FROM Assignment__c WHERE RecordType.Name = 'Primary' AND Billing_Type__c = 'Billable']){
            system.debug('--1----name--------'+relatedAssignment.Resource__r.User__r.Name);
            timeCardWrapper newTimeCardWrpper = new timeCardWrapper();
            newTimeCardWrpper.relatedAssignment = relatedAssignment;
            for (Time_Card__c relatedTimeCard: relatedAssignment.Time_Cards__r) {
                newTimeCardWrpper.resourceNameString = userIdWithNameMap.get(relatedTimeCard.Assignment__r.Resource__r.User__c);
                    
                totalBillableHours = totalBillableHours+relatedTimeCard.EP_Total_Number_of_Billable_Hours__c;
                if (newTimeCardWrpper.assignmentWithTimeCardsMap.get(relatedTimeCard.Assignment__c) != null &&
                    !newTimeCardWrpper.assignmentWithTimeCardsMap.get(relatedTimeCard.Assignment__c).contains(relatedTimeCard)) {
                    newTimeCardWrpper.assignmentWithTimeCardsMap.get(relatedTimeCard.Assignment__c).add(relatedTimeCard);
                } else {
                    List < Time_Card__c > releatedTimeCardsList = new List < Time_Card__c > ();
                    releatedTimeCardsList.add(relatedTimeCard);
                    newTimeCardWrpper.assignmentWithTimeCardsMap.put(relatedTimeCard.Assignment__c, releatedTimeCardsList);
                }
                projectNameSet.add(new SelectOption(relatedAssignment.Project__r.Name, relatedAssignment.Project__r.Name));                
            }
            if(!newTimeCardWrpper.assignmentWithTimeCardsMap.isEmpty()){
                timeCardWrapperList.add(newTimeCardWrpper);
            }
        }
        projectNameOptionsList.addAll(projectNameSet);
        if(timeCardWrapperList.size() == 0){
            errorMessage = 'No Pending Timesheets for approvals';
        }
        FinanceInvoicePageController PC2 = new FinanceInvoicePageController();
        PC2.fetchTimeCards();
        system.debug('----FinanceInvoicePageController.totalBillableHours---1---'+FinanceInvoicePageController.totalBillableHours);
        totalPendingBillableHours = FinanceInvoicePageController.totalBillableHours;
        system.debug('----totalPendingBillableHours---1---'+totalPendingBillableHours);
        return timeCardWrapperList; 
    }
    //Fetch the related data method -end
    
    //Wrapper class - start
    public class timeCardWrapper {
        
        public Assignment__c relatedAssignment {
            get;
            set;
        }
        public Map < Id, List < Time_Card__c >> assignmentWithTimeCardsMap {
            get;
            set;
        }
        public String resourceNameString{get;set;}
        public timeCardWrapper() {
            this.relatedAssignment = new Assignment__c();
            this.assignmentWithTimeCardsMap = new Map < Id, List < Time_Card__c >> ();
        }
    
    }
    //Wrapper class -End
}