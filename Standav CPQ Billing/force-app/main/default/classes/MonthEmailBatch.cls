global class MonthEmailBatch implements Database.Batchable<sObject>
{    
    global Database.QueryLocator start(Database.BatchableContext BC)
    { 
        //String approvedStatus = 'Approved';        
        return Database.getQueryLocator([Select Project__r.Name,Resource__r.User__r.Name,Name,(Select Status__c,
        Start_Date__c, EP_Total_Number_of_Billable_Hours__c, EP_Total_Number_of_Unbillable_Hours__c,Approved_Hours_Weekly__c from
        Time_Cards__r WHERE Status__c != 'Rejected' AND Start_Date__c = LAST_N_WEEKS:4),Approved_Hours__c from Assignment__c]);
    }
    global void execute(Database.BatchableContext BC, List<Assignment__c> assignmentList)
    {
        for(Assignment__c assignment : assignmentList){
            Double totalHoursapproved = 0;                   
            for(Time_Card__c tc : assignment.Time_Cards__r){  
               if(tc.Status__c == 'Approved'){
                totalHoursapproved+=tc.EP_Total_Number_of_Billable_Hours__c;
                }
            }
            assignment.Monthly_Approved_Hours__c = totalHoursapproved;
          
        }
              
        database.update(assignmentList,false);
    }
    global void finish(Database.BatchableContext BC) {        
            List<Assignment__c> asslist = [Select Id,Approved_Hours__c,Monthly_Approved_Hours__c,Project__r.Name,Resource__r.User__r.Name,Name,(Select Status__c,
             Start_Date__c, EP_Total_Number_of_Billable_Hours__c,Approved_Hours_Weekly__c, EP_Total_Number_of_Unbillable_Hours__c from
            Time_Cards__r Where Status__c != 'Rejected' AND Start_Date__c = LAST_N_WEEKS:4) from Assignment__c];                  
            for(Assignment__c assign : asslist){
                List<Time_Card__c> timeCardList = assign.Time_Cards__r;   
            }            
                    
      
            List<User> userList = new List<User>();
            userList = [SELECT Id,Email,Name,IsActive FROM User WHERE Profile.Name = 'Operation Team Profile - Community' AND IsActive = True] ;
            List<String> toAddresses = new List<String>();
           
        	for(User u : userList)
            {           
               toAddresses.add(u.Email);
            }  

            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            //String[] toAddresses = new String[] {'kevin.s@standav.com'};
            mail.setToAddresses(toAddresses);
            mail.setSubject('Monthly Finance Report'); 
        	mail.setHtmlBody('Hi Finance Team,<br/><br/> Here is the link for the Finance Report for the Previous Month, <a href="https://devemp-standavglobal.cs40.force.com/FinancePortal/s/?tabset-d52af=2aefd">Click Here To View.</a><br/><br/> Regards,<br/>Standav Community Portal');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
  
}