public class calenderViewReportTriggerHandler {

    public static void calenderView(List<Assignment__c> newAssignmentList){
        Set<Id> resourceSet = new Set<Id>();
        Set<Id> assignmentIdSet = new Set<Id>();
        List<Resource__c> resourceList = new List<Resource__c>();
        Map<Id, Resource__c> resourceMapToUpdate = new Map<Id, Resource__c>();
        Map<Integer, String> mapOfMonthNameByMonthNumber = new Map<Integer, String>();
        
        mapOfMonthNameByMonthNumber.put(1, 'January');
        mapOfMonthNameByMonthNumber.put(2, 'February');
        mapOfMonthNameByMonthNumber.put(3, 'March');
        mapOfMonthNameByMonthNumber.put(4, 'April');
        mapOfMonthNameByMonthNumber.put(5, 'May');
        mapOfMonthNameByMonthNumber.put(6, 'June');
        mapOfMonthNameByMonthNumber.put(7, 'July');
        mapOfMonthNameByMonthNumber.put(8, 'August');
        mapOfMonthNameByMonthNumber.put(9, 'September');
        mapOfMonthNameByMonthNumber.put(10, 'October');
        mapOfMonthNameByMonthNumber.put(11, 'November');
        mapOfMonthNameByMonthNumber.put(12, 'December');
        try{
        for(Assignment__c newAssign : newAssignmentList){
            system.debug('newAssign.Resource__c------>'+newAssign.Resource__c);
            system.debug('newAssign.EP_Shadow_Resource__c------>'+newAssign.EP_Shadow_Resource__c);
            assignmentIdSet.add(newAssign.Id);
            if(newAssign.Resource__c != null)
                resourceSet.add(newAssign.Resource__c);
            if(newAssign.EP_Shadow_Resource__c != null)
                resourceSet.add(newAssign.EP_Shadow_Resource__c);
        }
        Map<Id, Resource__c> resourceMap = new Map<Id, Resource__c>([SELECT Id,January__c, 
                                            February__c, March__c, April__c, May__c,
                                            June__c, July__c, August__c, September__c, October__c,
                                            November__c, December__c, Budgeted_Project_Allocation__c
                                            FROM Resource__c 
                                            WHERE Id IN:resourceSet]);
        system.debug('----resourceMap----->'+resourceMap);
        system.debug('----resourceSet----->'+resourceSet);
        for(Assignment__c assign : [Select Id, Name, Project__r.Name, Start_Date__c, Resource__c, Is_Primary_Resource_Contributing__c,
                                    End_Date__c, EP_Shadow_Resource__c, RecordType.Name, Project__c, Budgeted_Hours__c
                                    from Assignment__c 
                                    WHERE Id IN: assignmentIdSet]){
            system.debug('---------assign.Project__r.Name---------'+assign.Project__r.Name);
            system.debug('---------assign.RecordType.Name---------'+assign.RecordType.Name);
            system.debug('-----Resource__r.User__r.Name-------'+assign.Resource__c);
            system.debug('-------EP_Shadow_Resource__r.User__r.Name------'+assign.EP_Shadow_Resource__c);
            system.debug('---------assign--------'+assign);
            string month;
            Integer numberOfMonths = assign.Start_Date__c.monthsBetween(assign.End_Date__c) + 1 ;
            if(assign.RecordType.Name == 'Primary'){
                //Resource__c res = new Resource__c();
                Resource__c resPrimary = resourceMap.get(assign.Resource__c);        
                decimal Days=assign.Start_Date__c.daysBetween(assign.End_Date__c) + 1 ;
                decimal numberofDays = Days.setScale(2);
                system.debug('numberofDays'+numberofDays);
                decimal numberofWeeks=(numberofDays)/7.0;
                //Decimal numberofWeeks = weeks.setScale(2);
                system.debug('numberofWeeks'+numberofWeeks);
                decimal budget = assign.Budgeted_Hours__c/numberofWeeks; 
                resPrimary.Budgeted_Project_Allocation__c =  budget.round(system.roundingMode.FLOOR);
                system.debug('res.Budgeted_Project_Allocation__c'+resPrimary.Budgeted_Project_Allocation__c);
                
                system.debug('-------res Primary-------'+resPrimary);
                for(Integer i = 0; i < numberOfMonths; i++){
                
                    Date closeDateToUse = assign.Start_Date__c.addMonths(i+1).toStartofMonth().addDays(-1); // Start date of 12/16/2017 becomes 1/1/2018 - 1 day = 12/31/2017
                    
                    month = mapOfMonthNameByMonthNumber.get(closeDateToUse.month());
                    system.debug('month----->'+month);
                    system.debug('numberOfMonths------->'+numberOfMonths);
                    system.debug('closeDateToUse------->'+closeDateToUse);
                    system.debug('assign------->'+assign);
                    system.debug('assign---Project__r.Name---->'+assign.Project__r.Name);
                    if(month == 'January'){
                        system.debug('January---1--->'+resourceMap.get(resPrimary.Id).January__c);
                        resPrimary.January__c = resourceMap.get(resPrimary.Id).January__c != null && !resourceMap.get(resPrimary.Id).January__c.contains(assign.Project__r.Name)  ? resourceMap.get(resPrimary.Id).January__c+','+assign.Project__r.Name+'('+resPrimary.Budgeted_Project_Allocation__c+')' : assign.Project__r.Name+'('+resPrimary.Budgeted_Project_Allocation__c+')';
                        system.debug('January------>'+resPrimary.January__c);
                    }
                    if(month == 'February'){
                        resPrimary.February__c = resourceMap.get(resPrimary.Id).February__c != null && !resourceMap.get(resPrimary.Id).February__c.contains(assign.Project__r.Name) ? resourceMap.get(resPrimary.Id).February__c+','+assign.Project__r.Name+'('+resPrimary.Budgeted_Project_Allocation__c+')' : assign.Project__r.Name+'('+resPrimary.Budgeted_Project_Allocation__c+')';
                        system.debug('February------>'+resPrimary.February__c);
                    }
                    if(month == 'March'){
                        resPrimary.March__c = resourceMap.get(resPrimary.Id).March__c != null  && !resourceMap.get(resPrimary.Id).March__c.contains(assign.Project__r.Name)  ? resourceMap.get(resPrimary.Id).March__c+','+assign.Project__r.Name+'('+resPrimary.Budgeted_Project_Allocation__c+')' : assign.Project__r.Name+'('+resPrimary.Budgeted_Project_Allocation__c+')';
                        system.debug('March__c------>'+resPrimary.March__c);
                    }
                    if(month == 'April'){
                        resPrimary.April__c = resourceMap.get(resPrimary.Id).April__c != null  && !resourceMap.get(resPrimary.Id).April__c.contains(assign.Project__r.Name)  ? resourceMap.get(resPrimary.Id).April__c+','+assign.Project__r.Name+'('+resPrimary.Budgeted_Project_Allocation__c+')' : assign.Project__r.Name+'('+resPrimary.Budgeted_Project_Allocation__c+')';
                        system.debug('April------>'+resPrimary.April__c);
                    }
                    if(month == 'May'){
                        resPrimary.May__c = resourceMap.get(resPrimary.Id).May__c != null && !resourceMap.get(resPrimary.Id).May__c.contains(assign.Project__r.Name) ? resourceMap.get(resPrimary.Id).May__c+','+assign.Project__r.Name+'('+resPrimary.Budgeted_Project_Allocation__c+')' : assign.Project__r.Name+'('+resPrimary.Budgeted_Project_Allocation__c+')';
                        system.debug('May------>'+resPrimary.May__c);
                    }
                    if(month == 'June'){
                        resPrimary.June__c = resourceMap.get(resPrimary.Id).June__c != null && !resourceMap.get(resPrimary.Id).June__c.contains(assign.Project__r.Name) ? resourceMap.get(resPrimary.Id).June__c+','+assign.Project__r.Name+'('+resPrimary.Budgeted_Project_Allocation__c+')' : assign.Project__r.Name+'('+resPrimary.Budgeted_Project_Allocation__c+')';
                        system.debug('June------>'+resPrimary.June__c);
                    }
                    if(month == 'July'){
                        resPrimary.July__c = resourceMap.get(resPrimary.Id).July__c != null  && !resourceMap.get(resPrimary.Id).July__c.contains(assign.Project__r.Name) ? resourceMap.get(resPrimary.Id).July__c+','+assign.Project__r.Name+'('+resPrimary.Budgeted_Project_Allocation__c+')' : assign.Project__r.Name+'('+resPrimary.Budgeted_Project_Allocation__c+')';
                        system.debug('July------>'+resPrimary.July__c);
                    }
                    if(month == 'August'){
                        resPrimary.August__c = resourceMap.get(resPrimary.Id).August__c != null  && !resourceMap.get(resPrimary.Id).August__c.contains(assign.Project__r.Name) ? resourceMap.get(resPrimary.Id).August__c+','+assign.Project__r.Name+'('+resPrimary.Budgeted_Project_Allocation__c+')' : assign.Project__r.Name+'('+resPrimary.Budgeted_Project_Allocation__c+')';
                        system.debug('July------>'+resPrimary.August__c);
                    }
                    if(month == 'September'){
                        resPrimary.September__c = resourceMap.get(resPrimary.Id).September__c != null  && !resourceMap.get(resPrimary.Id).September__c.contains(assign.Project__r.Name) ? resourceMap.get(resPrimary.Id).September__c+','+assign.Project__r.Name+'('+resPrimary.Budgeted_Project_Allocation__c+')' : assign.Project__r.Name+'('+resPrimary.Budgeted_Project_Allocation__c+')';
                        system.debug('September------>'+resPrimary.September__c);
                    }
                    if(month == 'October'){
                        resPrimary.October__c = resourceMap.get(resPrimary.Id).October__c != null  && !resourceMap.get(resPrimary.Id).October__c.contains(assign.Project__r.Name) ? resourceMap.get(resPrimary.Id).October__c+','+assign.Project__r.Name+'('+resPrimary.Budgeted_Project_Allocation__c+')' : assign.Project__r.Name+'('+resPrimary.Budgeted_Project_Allocation__c+')';
                        system.debug('October------>'+resPrimary.October__c);
                    }
                    if(month == 'November'){
                        resPrimary.November__c = resourceMap.get(resPrimary.Id).November__c != null  && !resourceMap.get(resPrimary.Id).November__c.contains(assign.Project__r.Name) ? resourceMap.get(resPrimary.Id).November__c+','+assign.Project__r.Name+'('+resPrimary.Budgeted_Project_Allocation__c+')' : assign.Project__r.Name+'('+resPrimary.Budgeted_Project_Allocation__c+')';
                        system.debug('November------>'+resPrimary.November__c);
                    }
                    if(month == 'December'){
                        resPrimary.December__c = resourceMap.get(resPrimary.Id).December__c != null  && !resourceMap.get(resPrimary.Id).December__c.contains(assign.Project__r.Name) ? resourceMap.get(resPrimary.Id).December__c+','+assign.Project__r.Name+'('+resPrimary.Budgeted_Project_Allocation__c+')' : assign.Project__r.Name+'('+resPrimary.Budgeted_Project_Allocation__c+')';
                        system.debug('December------>'+resPrimary.December__c);
                    }
                }
                resourceMapToUpdate.put(resPrimary.Id, resPrimary);
            }
                                        
            if(assign.RecordType.Name == 'Shadow'){
                Resource__c res = resourceMap.get(assign.EP_Shadow_Resource__c);
                if(assign.Is_Primary_Resource_Contributing__c == True){
                
                    decimal Days=assign.Start_Date__c.daysBetween(assign.End_Date__c) + 1 ;
                    decimal numberofDays = Days.setScale(2);
                    system.debug('numberofDays'+numberofDays);
                    decimal numberofWeeks=(numberofDays)/7.0;
                    //Decimal numberofWeeks = weeks.setScale(2);
                    system.debug('numberofWeeks'+numberofWeeks);
                    decimal budget = assign.Budgeted_Hours__c/numberofWeeks; 
                    res.Budgeted_Project_Allocation__c =  budget.round(system.roundingMode.FLOOR);
                    system.debug('res.Budgeted_Project_Allocation__c'+res.Budgeted_Project_Allocation__c);

                        system.debug('-------res shadow-------'+res);
                    for(Integer i = 0; i < numberOfMonths; i++){
                    
                        Date closeDateToUse = assign.Start_Date__c.addMonths(i+1).toStartofMonth().addDays(-1); // Start date of 12/16/2017 becomes 1/1/2018 - 1 day = 12/31/2017
                        
                        month = mapOfMonthNameByMonthNumber.get(closeDateToUse.month());
                        system.debug('month----->'+month);
                        system.debug('numberOfMonths------->'+numberOfMonths);
                        system.debug('closeDateToUse------->'+closeDateToUse);
                        if(month == 'January'){
                            system.debug('January-1----->'+resourceMap.get(res.Id).January__c);
                            res.January__c = resourceMap.get(res.Id).January__c != null && !resourceMap.get(res.Id).January__c.contains(assign.Project__r.Name)  ? resourceMap.get(res.Id).January__c+','+assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')' : assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')';
                            system.debug('January------>'+res.January__c);
                        }
                        if(month == 'February'){
                            res.February__c = resourceMap.get(res.Id).February__c != null  && !resourceMap.get(res.Id).February__c.contains(assign.Project__r.Name) ? resourceMap.get(res.Id).February__c+','+assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')' : assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')';
                            system.debug('February------>'+res.February__c);
                        }
                        if(month == 'March'){
                            res.March__c = resourceMap.get(res.Id).March__c != null  && !resourceMap.get(res.Id).March__c.contains(assign.Project__r.Name) ? resourceMap.get(res.Id).March__c+','+assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')' : assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')';
                            system.debug('March__c------>'+res.March__c);
                        }
                        if(month == 'April'){
                            res.April__c = resourceMap.get(res.Id).April__c != null  && !resourceMap.get(res.Id).April__c.contains(assign.Project__r.Name) ? resourceMap.get(res.Id).April__c+','+assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')' : assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')';
                            system.debug('April------>'+res.April__c);
                        }
                        if(month == 'May'){
                            res.May__c = resourceMap.get(res.Id).May__c != null && !resourceMap.get(res.Id).May__c.contains(assign.Project__r.Name) ? resourceMap.get(res.Id).May__c+','+assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')' : assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')';
                            system.debug('May------>'+res.May__c);
                        }
                        if(month == 'June'){
                            res.June__c = resourceMap.get(res.Id).June__c != null && !resourceMap.get(res.Id).June__c.contains(assign.Project__r.Name) ? resourceMap.get(res.Id).June__c+','+assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')' : assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')';
                            system.debug('June------>'+res.June__c);
                        }
                        if(month == 'July'){
                            res.July__c = resourceMap.get(res.Id).July__c != null  && !resourceMap.get(res.Id).July__c.contains(assign.Project__r.Name) ? resourceMap.get(res.Id).July__c+','+assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')' : assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')';
                            system.debug('July------>'+res.July__c);
                        }
                        if(month == 'August'){
                            res.August__c = resourceMap.get(res.Id).August__c != null  && !resourceMap.get(res.Id).August__c.contains(assign.Project__r.Name) ? resourceMap.get(res.Id).August__c+','+assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')' : assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')';
                            system.debug('July------>'+res.August__c);
                        }
                        if(month == 'September'){
                            res.September__c = resourceMap.get(res.Id).September__c != null  && !resourceMap.get(res.Id).September__c.contains(assign.Project__r.Name) ? resourceMap.get(res.Id).September__c+','+assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')' : assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')';
                            system.debug('September------>'+res.September__c);
                        }
                        if(month == 'October'){
                            res.October__c = resourceMap.get(res.Id).October__c != null  && !resourceMap.get(res.Id).October__c.contains(assign.Project__r.Name) ? resourceMap.get(res.Id).October__c+','+assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')' : assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')';
                            system.debug('October------>'+res.October__c);
                        }
                        if(month == 'November'){
                            res.November__c = resourceMap.get(res.Id).November__c != null  && !resourceMap.get(res.Id).November__c.contains(assign.Project__r.Name) ? resourceMap.get(res.Id).November__c+','+assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')' : assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')';
                            system.debug('November------>'+res.November__c);
                        }
                        if(month == 'December'){
                            res.December__c = resourceMap.get(res.Id).December__c != null  && !resourceMap.get(res.Id).December__c.contains(assign.Project__r.Name) ? resourceMap.get(res.Id).December__c+','+assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')' : assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')';
                            system.debug('December------>'+res.December__c);
                        }
                    }

                    resourceMapToUpdate.put(res.Id, res);
                    system.debug('res---->'+res);
                }
                else if(assign.Is_Primary_Resource_Contributing__c == False){
                    Resource__c resPrimary = resourceMap.get(assign.Resource__c); 
                    decimal Days=assign.Start_Date__c.daysBetween(assign.End_Date__c) + 1 ;
                    decimal numberofDays = Days.setScale(2);
                    system.debug('numberofDays'+numberofDays);
                    decimal numberofWeeks=(numberofDays)/7.0;
                    //Decimal numberofWeeks = weeks.setScale(2);
                    system.debug('numberofWeeks'+numberofWeeks);
                    decimal budget = assign.Budgeted_Hours__c/numberofWeeks; 
                    res.Budgeted_Project_Allocation__c =  budget.round(system.roundingMode.FLOOR);
                    system.debug('res.Budgeted_Project_Allocation__c'+res.Budgeted_Project_Allocation__c);

                    //res.Budgeted_Project_Allocation__c = resourceMap.get(res.Id).Budgeted_Project_Allocation__c + assign.Budgeted_Hours__c;  
                    system.debug('-------res shadow-------'+res);
                    for(Integer i = 0; i < numberOfMonths; i++){
                    
                        Date closeDateToUse = assign.Start_Date__c.addMonths(i+1).toStartofMonth().addDays(-1); // Start date of 12/16/2017 becomes 1/1/2018 - 1 day = 12/31/2017
                        
                        month = mapOfMonthNameByMonthNumber.get(closeDateToUse.month());
                        system.debug('month----->'+month);
                        system.debug('numberOfMonths------->'+numberOfMonths);
                        system.debug('closeDateToUse------->'+closeDateToUse);
                        if(month == 'January'){
                            system.debug('January-1----->'+resourceMap.get(res.Id).January__c);
                            res.January__c = resourceMap.get(res.Id).January__c != null && !resourceMap.get(res.Id).January__c.contains(assign.Project__r.Name)  ? resourceMap.get(res.Id).January__c+','+assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')' : assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')';
                            system.debug('January------>'+res.January__c);
                            Decimal bugHrs = resPrimary.Budgeted_Project_Allocation__c - res.Budgeted_Project_Allocation__c;
                            resourceMap.get(resPrimary.Id).January__c  = resourceMap.get(resPrimary.Id).January__c != null && resourceMap.get(resPrimary.Id).January__c.contains(assign.Project__r.Name)  ? resourceMap.get(resPrimary.Id).January__c.remove(assign.Project__r.Name) : resourceMap.get(resPrimary.Id).January__c;
                            resourceMap.get(resPrimary.Id).January__c  = resourceMap.get(resPrimary.Id).January__c.remove('('+resPrimary.Budgeted_Project_Allocation__c+')');
                            resourceMap.get(resPrimary.Id).January__c  = resourceMap.get(resPrimary.Id).January__c.removeEnd(',');
                        }
                        if(month == 'February'){
                            res.February__c = resourceMap.get(res.Id).February__c != null  && !resourceMap.get(res.Id).February__c.contains(assign.Project__r.Name) ? resourceMap.get(res.Id).February__c+','+assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')' : assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')';
                            system.debug('February------>'+res.February__c);
                            Decimal bugHrs = resPrimary.Budgeted_Project_Allocation__c - res.Budgeted_Project_Allocation__c;
                            resourceMap.get(resPrimary.Id).February__c = resourceMap.get(resPrimary.Id).February__c != null && resourceMap.get(resPrimary.Id).February__c.contains(assign.Project__r.Name)  ? resourceMap.get(resPrimary.Id).February__c.remove(assign.Project__r.Name) : resourceMap.get(resPrimary.Id).February__c;
                            resourceMap.get(resPrimary.Id).February__c  = resourceMap.get(resPrimary.Id).February__c.remove('('+resPrimary.Budgeted_Project_Allocation__c+')');
                            resourceMap.get(resPrimary.Id).February__c  = resourceMap.get(resPrimary.Id).February__c.removeEnd(',');
                        }
                        if(month == 'March'){
                            res.March__c = resourceMap.get(res.Id).March__c != null  && !resourceMap.get(res.Id).March__c.contains(assign.Project__r.Name) ? resourceMap.get(res.Id).March__c+','+assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')' : assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')';
                            Decimal bugHrs = resPrimary.Budgeted_Project_Allocation__c - res.Budgeted_Project_Allocation__c;
                            resourceMap.get(resPrimary.Id).March__c = resourceMap.get(resPrimary.Id).March__c != null && resourceMap.get(resPrimary.Id).March__c.contains(assign.Project__r.Name)  ? resourceMap.get(resPrimary.Id).March__c.remove(assign.Project__r.Name) : resourceMap.get(resPrimary.Id).March__c;
                            resourceMap.get(resPrimary.Id).March__c  = resourceMap.get(resPrimary.Id).March__c.remove('('+resPrimary.Budgeted_Project_Allocation__c+')');
                            resourceMap.get(resPrimary.Id).March__c  = resourceMap.get(resPrimary.Id).March__c.removeEnd(',');
                            system.debug('March__c------>'+res.March__c);
                        }
                        if(month == 'April'){
                            res.April__c = resourceMap.get(res.Id).April__c != null  && !resourceMap.get(res.Id).April__c.contains(assign.Project__r.Name) ? resourceMap.get(res.Id).April__c+','+assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')' : assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')';
                            Decimal bugHrs = resPrimary.Budgeted_Project_Allocation__c - res.Budgeted_Project_Allocation__c;
                            resourceMap.get(resPrimary.Id).April__c = resourceMap.get(resPrimary.Id).April__c != null && resourceMap.get(resPrimary.Id).April__c.contains(assign.Project__r.Name)  ? resourceMap.get(resPrimary.Id).April__c.remove(assign.Project__r.Name) : resourceMap.get(resPrimary.Id).April__c;
                            system.debug('April------>'+res.April__c);
                            resourceMap.get(resPrimary.Id).April__c  = resourceMap.get(resPrimary.Id).April__c.remove('('+resPrimary.Budgeted_Project_Allocation__c+')');
                            resourceMap.get(resPrimary.Id).April__c  = resourceMap.get(resPrimary.Id).April__c.removeEnd(',');
                        }
                        if(month == 'May'){
                            res.May__c = resourceMap.get(res.Id).May__c != null && !resourceMap.get(res.Id).May__c.contains(assign.Project__r.Name) ? resourceMap.get(res.Id).May__c+','+assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')' : assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')';
                            Decimal bugHrs = resPrimary.Budgeted_Project_Allocation__c - res.Budgeted_Project_Allocation__c;
                            resourceMap.get(resPrimary.Id).May__c = resourceMap.get(resPrimary.Id).May__c != null && resourceMap.get(resPrimary.Id).May__c.contains(assign.Project__r.Name)  ? resourceMap.get(resPrimary.Id).May__c.remove(assign.Project__r.Name) : resourceMap.get(resPrimary.Id).May__c;
                            system.debug('May------>'+res.May__c);
                            resourceMap.get(resPrimary.Id).May__c  = resourceMap.get(resPrimary.Id).May__c.remove('('+resPrimary.Budgeted_Project_Allocation__c+')');
                            resourceMap.get(resPrimary.Id).May__c  = resourceMap.get(resPrimary.Id).May__c.removeEnd(',');
                        }
                        if(month == 'June'){
                            res.June__c = resourceMap.get(res.Id).June__c != null && !resourceMap.get(res.Id).June__c.contains(assign.Project__r.Name) ? resourceMap.get(res.Id).June__c+','+assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')' : assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')';
                            Decimal bugHrs = resPrimary.Budgeted_Project_Allocation__c - res.Budgeted_Project_Allocation__c;
                            resourceMap.get(resPrimary.Id).June__c = resourceMap.get(resPrimary.Id).June__c != null && resourceMap.get(resPrimary.Id).June__c.contains(assign.Project__r.Name)  ? resourceMap.get(resPrimary.Id).June__c.remove(assign.Project__r.Name) : resourceMap.get(resPrimary.Id).June__c;
                            system.debug('June------>'+res.June__c);
                            if(resourceMap.get(resPrimary.Id).June__c != null){
                                resourceMap.get(resPrimary.Id).June__c  = resourceMap.get(resPrimary.Id).June__c.remove('('+resPrimary.Budgeted_Project_Allocation__c+')');
                            resourceMap.get(resPrimary.Id).June__c  = resourceMap.get(resPrimary.Id).June__c.removeEnd(',');    
                            }
                            
                        }
                        if(month == 'July'){
                            res.July__c = resourceMap.get(res.Id).July__c != null  && !resourceMap.get(res.Id).July__c.contains(assign.Project__r.Name) ? resourceMap.get(res.Id).July__c+','+assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')' : assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')';
                            Decimal bugHrs = resPrimary.Budgeted_Project_Allocation__c - res.Budgeted_Project_Allocation__c;
                            resourceMap.get(resPrimary.Id).July__c = resourceMap.get(resPrimary.Id).July__c != null && resourceMap.get(resPrimary.Id).July__c.contains(assign.Project__r.Name)  ? resourceMap.get(resPrimary.Id).July__c.remove(assign.Project__r.Name) : resourceMap.get(resPrimary.Id).July__c;
                            system.debug('July------>'+res.July__c);
                            resourceMap.get(resPrimary.Id).July__c  = resourceMap.get(resPrimary.Id).July__c.remove('('+resPrimary.Budgeted_Project_Allocation__c+')');
                            resourceMap.get(resPrimary.Id).July__c  = resourceMap.get(resPrimary.Id).July__c.removeEnd(',');
                        }
                        if(month == 'August'){
                            res.August__c = resourceMap.get(res.Id).August__c != null  && !resourceMap.get(res.Id).August__c.contains(assign.Project__r.Name) ? resourceMap.get(res.Id).August__c+','+assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')' : assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')';
                            Decimal bugHrs = resPrimary.Budgeted_Project_Allocation__c - res.Budgeted_Project_Allocation__c;
                            resourceMap.get(resPrimary.Id).August__c = resourceMap.get(resPrimary.Id).August__c != null && resourceMap.get(resPrimary.Id).August__c.contains(assign.Project__r.Name)  ? resourceMap.get(resPrimary.Id).August__c.remove(assign.Project__r.Name) : resourceMap.get(resPrimary.Id).August__c;
                            system.debug('July------>'+res.August__c);
                            resourceMap.get(resPrimary.Id).August__c  = resourceMap.get(resPrimary.Id).August__c.remove('('+resPrimary.Budgeted_Project_Allocation__c+')');
                            resourceMap.get(resPrimary.Id).August__c  = resourceMap.get(resPrimary.Id).August__c.removeEnd(',');
                        }
                        if(month == 'September'){
                            res.September__c = resourceMap.get(res.Id).September__c != null  && !resourceMap.get(res.Id).September__c.contains(assign.Project__r.Name) ? resourceMap.get(res.Id).September__c+','+assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')' : assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')';
                            Decimal bugHrs = resPrimary.Budgeted_Project_Allocation__c - res.Budgeted_Project_Allocation__c;
                            resourceMap.get(resPrimary.Id).September__c = resourceMap.get(resPrimary.Id).September__c != null && resourceMap.get(resPrimary.Id).September__c.contains(assign.Project__r.Name)  ? resourceMap.get(resPrimary.Id).September__c.remove(assign.Project__r.Name) : resourceMap.get(resPrimary.Id).September__c;
                            system.debug('September------>'+res.September__c);
                            resourceMap.get(resPrimary.Id).September__c  = resourceMap.get(resPrimary.Id).September__c.remove('('+resPrimary.Budgeted_Project_Allocation__c+')');
                            resourceMap.get(resPrimary.Id).September__c  = resourceMap.get(resPrimary.Id).September__c.removeEnd(',');
                        }
                        if(month == 'October'){
                            res.October__c = resourceMap.get(res.Id).October__c != null  && !resourceMap.get(res.Id).October__c.contains(assign.Project__r.Name) ? resourceMap.get(res.Id).October__c+','+assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')' : assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')';
                            Decimal bugHrs = resPrimary.Budgeted_Project_Allocation__c - res.Budgeted_Project_Allocation__c;
                            resourceMap.get(resPrimary.Id).October__c = resourceMap.get(resPrimary.Id).October__c != null && resourceMap.get(resPrimary.Id).October__c.contains(assign.Project__r.Name)  ? resourceMap.get(resPrimary.Id).October__c.remove(assign.Project__r.Name) : resourceMap.get(resPrimary.Id).October__c;
                            system.debug('October------>'+res.October__c);
                            resourceMap.get(resPrimary.Id).October__c  = resourceMap.get(resPrimary.Id).October__c.remove('('+resPrimary.Budgeted_Project_Allocation__c+')');
                            resourceMap.get(resPrimary.Id).October__c  = resourceMap.get(resPrimary.Id).October__c.removeEnd(',');
                        }
                        if(month == 'November'){
                            res.November__c = resourceMap.get(res.Id).November__c != null  && !resourceMap.get(res.Id).November__c.contains(assign.Project__r.Name) ? resourceMap.get(res.Id).November__c+','+assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')' : assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')';
                            Decimal bugHrs = resPrimary.Budgeted_Project_Allocation__c - res.Budgeted_Project_Allocation__c;
                            resourceMap.get(resPrimary.Id).November__c = resourceMap.get(resPrimary.Id).November__c != null && resourceMap.get(resPrimary.Id).November__c.contains(assign.Project__r.Name)  ? resourceMap.get(resPrimary.Id).November__c.remove(assign.Project__r.Name) : resourceMap.get(resPrimary.Id).November__c;
                            system.debug('November------>'+res.November__c);
                            resourceMap.get(resPrimary.Id).November__c  = resourceMap.get(resPrimary.Id).November__c.remove('('+resPrimary.Budgeted_Project_Allocation__c+')');
                            resourceMap.get(resPrimary.Id).November__c  = resourceMap.get(resPrimary.Id).November__c.removeEnd(',');
                        }
                        if(month == 'December'){
                            res.December__c = resourceMap.get(res.Id).December__c != null  && !resourceMap.get(res.Id).December__c.contains(assign.Project__r.Name) ? resourceMap.get(res.Id).December__c+','+assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')' : assign.Project__r.Name+'('+res.Budgeted_Project_Allocation__c+')';
                            Decimal bugHrs = resPrimary.Budgeted_Project_Allocation__c - res.Budgeted_Project_Allocation__c;
                            resourceMap.get(resPrimary.Id).December__c = resourceMap.get(resPrimary.Id).December__c != null && resourceMap.get(resPrimary.Id).December__c.contains(assign.Project__r.Name)  ? resourceMap.get(resPrimary.Id).December__c.remove(assign.Project__r.Name) : resourceMap.get(resPrimary.Id).December__c;
                            system.debug('December------>'+res.December__c);
                            resourceMap.get(resPrimary.Id).December__c  = resourceMap.get(resPrimary.Id).December__c.remove('('+resPrimary.Budgeted_Project_Allocation__c+')');
                            resourceMap.get(resPrimary.Id).December__c  = resourceMap.get(resPrimary.Id).December__c.removeEnd(',');
                        }
                    }
                    if(resPrimary != null){
                        resourceMapToUpdate.put(resPrimary.Id, resPrimary);
                    }
                    resourceMapToUpdate.put(res.Id, res);
                    system.debug('res---->'+res);
                }
            }
        }
        system.debug('resourceList---->'+resourceMapToUpdate.values());
        system.debug('resourceList-.size()--->'+resourceMapToUpdate.values().size());
        if(resourceMapToUpdate.values().size() > 0){
            update resourceMapToUpdate.values();    
        }
        }catch(EXception ex){
            
        }
    }
}