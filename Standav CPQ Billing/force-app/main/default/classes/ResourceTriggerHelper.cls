public class ResourceTriggerHelper{
    public static void beforeInsertMethod(List<Resource__c> relatedResourcesList){
        List<String> existingResourceUserList = new List<String>();
        for(Resource__c existingResource : [Select ID, Name,User__c,User__r.Name FROM Resource__c]){
            if(existingResource.User__c != null){
                existingResourceUserList.add(existingResource.User__c);
            }
        }
        
        for(Resource__c newResource : relatedResourcesList){
            if(existingResourceUserList.contains(newResource.User__c)){
                newResource.addError('We cant have duplicate resource');
            }
        }
        
    }
    public static void beforeDeleteMethod(List<Resource__c> relatedResourcesList){
        system.debug('relatedResourcesList'+relatedResourcesList);
        /*List<String> existingResourceUserList = new List<String>();
        List<Resource__c> existingResource = [Select ID, Name,(SELECT ID, Resource_Name__c FROM Assignments__r) FROM Resource__c];
        for(Resource__c res: existingResource){
            for(Assignment__c Assignment: res.Assignments__r){
                if(Assignment.Resource_Name__c != null){
                    existingResourceUserList.add(Assignment.Resource_Name__c);
                    
                }
                system.debug('Assignment.Resource_Name__c'+Assignment.Resource_Name__c);
            }
        }
        system.debug('existingResourceUserList'+existingResourceUserList);

        for(Resource__c deleteResource : relatedResourcesList){
            system.debug('deleteResource'+deleteResource);
            for(Assignment__c Assignment: deleteResource.Assignments__r){
                 system.debug('Assignment'+Assignment);
                if(existingResourceUserList.contains(Assignment.Resource_Name__c)){
                   Assignment.addError('Cannot delete Resource without deleting Resource Allocation');
                }
                  
            }
            
        }*/
        Set<Id> resourceIdSet = new Set<Id>();
        for(Resource__c res: relatedResourcesList){
            resourceIdSet.add(res.Id);
        }
        try{
            Map<Id, Resource__c> resourceWithassignmentMap = new Map<Id, Resource__c>([SELECT Id, (SELECT ID, Resource__c, EP_Shadow_Resource__c 
                                                 FROM Assignments__r 
                                                 WHERE EP_Shadow_Resource__c IN: resourceIdSet OR Resource__c IN: resourceIdSet)
                                                 FROM Resource__c WHERE Id IN: resourceIdSet]);
            for(Resource__c objResource : relatedResourcesList){
                if(resourceWithassignmentMap.containsKey(objResource.Id) && 
                    !resourceWithassignmentMap.get(objResource.Id).Assignments__r.isEmpty()) 
                    //continue;
                objResource.adderror('Cannot Delete Resource Because It Has Related Allocations');
            }
        }catch(Exception ex){
        
        }
    }
}