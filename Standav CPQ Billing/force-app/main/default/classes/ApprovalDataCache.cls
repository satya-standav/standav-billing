public class ApprovalDataCache{
    
    public static Map<Id, Time_Card__c> getMap(Id projectManagerId, String assignmentType){
        return loadMap(projectManagerId, assignmentType);
    }

    private static Map<Id, Time_Card__c> loadMap(Id projectManagerId, String assignmentType){
        
        Map<Id, Time_Card__c> timeCardMap = new Map<Id, Time_Card__c>();
        string selectString = 'SELECT Id, Name, Project_Manager__c,Project_Name__c,Assignment__r.Project__r.Name,'+
                            'Status__c, Start_Date__c,Client_Details__c,Manager_Approval_Comments__c,Client_Rejected__c,'+
                            'Invoiced__c,EP_Primary_Resource_Email_Id__c,Send_Reminder_on_Recall__c,Send_Reminder__c,'+
                            'Reminder_Sent_Date__c,Send_to_Client_Approval__c,EP_Shadow_Resource_Email_Id__c,'+
                            'Send_reminder_to_Finance_team__c,Total_Hours__c,Show_Comments__c,Burned_Hours__c,'+
                            'EP_End_Date__c,RecordTypeId,EP_Notes__c,Resource_Name__c, EP_Approval_Date__c,Approval_Status__c,'+
                            'EP_Total_Number_of_Billable_Hours__c,Approved_Hours__c,Approved_Hours_Weekly__c,'+
                            'EP_Total_Number_of_Unbillable_Hours__c,EP_Rejected_Date__c, Submitted_On__c,'+
                            'Billable_Hours_Mon__c,Billable_Hours_Tue__c,Billable_Hours_Wed__c,Billable_Hours_Thu__c,'+
                            'Billable_Hours_Fri__c,Billable_Hours_Sat__c,  Billable_Hours_Sun__c,'+
                            'UnBillable_Hours_Mon__c,UnBillable_Hours_Tue__c,UnBillable_Hours_Wed__c,UnBillable_Hours_Thu__c,'+
                            'UnBillable_Hours_Fri__c,UnBillable_Hours_Sat__c, UnBillable_Hours_Sun__c, '+
                            'Assignment__c,Assignment__r.RecordType.Name,Assignment__r.Resource__r.User__r.Name,'+
                            'Assignment__r.Role__c,Assignment__r.Project__r.Contact_Person__r.Email, '+
                            'Assignment__r.Project__r.Contact_Person__c,Assignment__r.EP_Shadow_Resource__r.User__r.Name,'+
                            'Assignment__r.Project__c,Assignment__r.Project__r.Contact_Person__r.Single_Reminder__c,'+ 
                            'Monday_Work_Details__c,Tuesday_Work_Details__c,Wednesday_Work_Details__c,Thursday_Work_Details__c,'+
                            'Friday_Work_Details__c,    Saturday_Work_Details__c,   Sunday_Work_Details__c,'+ 
                            'Assignment__r.Resource__c,Assignment__r.EP_Shadow_Resource__c,'+
                            'Assignment__r.Is_Primary_Resource_Contributing__c,Assignment__r.Project__r.Contact_Person__r.Name,'+
                            'X1st_Approver__c, X2nd_Approver__c, X3rd_Approver__c, X4th_Approver__c, X5th_Approver__c,'+
                            '(SELECT Id, Name FROM attachments)';
        string fromString = ' FROM Time_Card__c';
        //string whereString = ' WHERE Status__c IN: statuslist AND Project_Manager__c =: projectManagerId AND Assignment__r.RecordType.Name =: assignmentType';
        string whereString = ' WHERE Project_Manager__c =: projectManagerId AND Assignment__r.RecordType.Name =: assignmentType';
        string queryString = selectString + fromString + whereString;
        system.debug('-------queryString---------'+queryString);
        for(Time_Card__c timecard : Database.query(queryString)){
            timecardMap.put(timecard.Id, timeCard);
        }
        system.debug('-------Time card size---------'+timecardMap.values().size());
        return timecardMap;
    }
}