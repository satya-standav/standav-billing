/*Added Comments By Satya*/
public class OrderProductTriggerHandler {
    public static void createProject(List<OrderItem> OrdProdList){
        
        OrderItem ordPrd = OrdProdList[0];
        Order ord = [SELECT Id,AccountId,EffectiveDate,Project__c,OrderNumber,Type,CreatedById FROM Order WHERE id=:ordPrd.OrderId];
        Id acc = ord.AccountId;
        List<Contact> cntList = [SELECT id FROM Contact WHERE AccountId =: acc];
        Contact cnt = cntList[0];
        String prjname = ord.OrderNumber;
        If(ord.Project__c == NULL && ord.Type == 'New'){
            //create Project on the order
            Project__c prj = new Project__c();
            prj.Name = 'Project '+ prjname;
            prj.Contact_Person__c = cnt.id;
            prj.Number__c =0;
            prj.EP_Project_Manager__c = UserInfo.getUserId();
            //prj.EP_Project_Manager__c= '0052x000001guJF';
            //prj.EP_Project_Manager__c = ord.CreatedById;
            prj.Contractual_Start_Date__c = ord.EffectiveDate;
            prj.Contractual_End_Date__c = ord.EffectiveDate.addDays(2);
            prj.Account__c = acc;   
            prj.created_by_Trigger__c = True;
            //prj.RecordTypeId = '0121y000000NoME';
           
            Insert prj;
            
            
            ord.Project__c = prj.id;
            update ord;
        }
        Order neword = [SELECT id,Project__c FROM Order WHERE Id=:ord.id];
        Id prjt = neword.Project__c;
        
        Product2 prd = [SELECT id,Name FROM Product2 WHERE id=:ordPrd.Product2Id];
        String prdname = prd.Name;
            
        //create Resource Allocation 
        //List<usrid> usr = [SELECT id FROM USER WHERE id=:'0052x000001guJF'];
        //Id usrid = usr[0].id
        Assignment__c ResAlc = new Assignment__c();
        //ResAlc.RecordTypeId = '0121y000000NoMBAA0';
        ResAlc.Project__c = prjt;
        ResAlc.Resource__c = 'a2f1y000000736tAAA';
        ResAlc.Start_Date__c = ordPrd.ServiceDate;
        ResAlc.End_Date__c = ordPrd.ServiceDate.addDays(2);
        ResAlc.Role__c = prdname;
        ResAlc.Billing_Type__c = 'Billable';
        ResAlc.Budgeted_Hours__c = ordPrd.Quantity;
        Insert ResAlc;
        
    }

    public static void checkProject(List<OrderItem> OrdProdList){
       
        system.debug('SizeofList='+OrdProdList.size());
        Order ord; 
        Id acc ;        
        String prjname ;        
        list<Product2> prd = new list<product2>();
        List<Contact> cntList = new list<contact>();
        list<Id> successQuoteList = new list<Id>();
        
        if (OrdProdList.size() > 0) {
            ord = [SELECT Id,AccountId,EffectiveDate,Project__c,OrderNumber,Type,CreatedById FROM Order WHERE id=:OrdProdList[0].OrderId];
            acc = ord.AccountId;    
            prjname = ord.OrderNumber;
            cntList = [SELECT id FROM Contact WHERE AccountId =: acc];
            prd = [SELECT id,Name FROM Product2 WHERE id=:OrdProdList[0].Product2Id];
        }
        
        integer x=0;        
        Id prjt ;
        list<Assignment__c> AssignList = new list<Assignment__c>();
        
        For(OrderItem ordPrd:OrdProdList){  
            If(ord.Type == 'New'){                
                if (x ==0 ) {
                    system.debug('SP First Order Product= '+x );
                    //create Project on the order
                    Project__c prj = new Project__c();
                    prj.Name = 'Project '+ prjname;
                    if (cntList.size() > 0)
                        prj.Contact_Person__c = cntList[0].id;
                    prj.Number__c =0;
                    prj.EP_Project_Manager__c = UserInfo.getUserId();
                    //prj.EP_Project_Manager__c= '0052x000001guJF';
                    //prj.EP_Project_Manager__c = ord.CreatedById;
                    prj.Contractual_Start_Date__c = ord.EffectiveDate;
                    prj.Contractual_End_Date__c = ord.EffectiveDate.addDays(2);
                    prj.Account__c = acc;   
                    prj.created_by_Trigger__c = True;
                    //prj.RecordTypeId = '0121y000000NoME';
                   
                    //Insert prj;
                    Database.SaveResult srprj = Database.insert(prj, false);
                    if (srprj.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                        System.debug('Successfully inserted Project.  ID: ' + srprj.getId());
                        ord.Project__c = prj.id;
                        update ord;
                        
                        system.debug('First Assignment started');    
                        prjt = srprj.getId();
                        Assignment__c ResAlc = new Assignment__c();
                        //ResAlc.RecordTypeId = '0121y000000NoMBAA0';
                        ResAlc.Project__c = prjt;
                        ResAlc.Resource__c = 'a2f1y000000736tAAA';
                        ResAlc.Start_Date__c = ordPrd.ServiceDate;
                        ResAlc.End_Date__c = ordPrd.ServiceDate.addDays(2);
                        ResAlc.Role__c = ordPrd.product2.name;
                        ResAlc.Billing_Type__c = 'Billable';
                        ResAlc.Budgeted_Hours__c = ordPrd.Quantity;
                        AssignList.add(ResAlc);
                        x++;
                    }
                    else {
                        // Operation failed, so get all errors                
                        for(Database.Error err : srprj.getErrors()) {
                            System.debug('The following error has occurred.');                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('Project fields that affected this error: ' + err.getFields());
                        }
                    }
                }
                else {
                    String prdname = ordPrd.product2.name;
                    system.debug('SP Second Order Product= '+x );
                    //create Resource Allocation 
                    //List<usrid> usr = [SELECT id FROM USER WHERE id=:'0052x000001guJF'];
                    //Id usrid = usr[0].id
                    Assignment__c ResAlc = new Assignment__c();
                    //ResAlc.RecordTypeId = '0121y000000NoMBAA0';
                    ResAlc.Project__c = prjt;
                    ResAlc.Resource__c = 'a2f1y000000736tAAA';
                    ResAlc.Start_Date__c = ordPrd.ServiceDate;
                    ResAlc.End_Date__c = ordPrd.ServiceDate.addDays(2);
                    ResAlc.Role__c = prdname;
                    ResAlc.Billing_Type__c = 'Billable';
                    ResAlc.Budgeted_Hours__c = ordPrd.Quantity;
                    AssignList.add(ResAlc);
                    x++;
                    //Insert ResAlc;            
                }
            }         
        }
        
        Database.SaveResult[] srra; 
        
        system.debug('Satya Inserting Assignments'+AssignList);    
        srra = Database.insert(AssignList, false);
        if(srra != Null && srra.size() > 0){  
            for(Integer i = 0; i < srra.size(); i++){
                if(srra.get(i).isSuccess()){
                    successQuoteList.add(srra.get(i).getId());
                }
                else if(!srra.get(i).isSuccess()){
                    Database.Error error = srra.get(i).getErrors().get(0);
                    String failedDML = error.getMessage();
        
                    for(string errorField : error.getfields()){
                        system.debug(' errors '+failedDML + ' ' +errorField) ;            
                    }
                }
            }
        } 
        //Order neword = [SELECT id,Project__c FROM Order WHERE Id=:ord.id];    
        system.debug('Assignment inserted');
    }
    
}