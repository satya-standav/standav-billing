public class SubmittedTimeSheet {
    public List<Time_Card__c> submittedTimeCardsList{
        get {
            if (submittedTimeCardsList != null) {
                return submittedTimeCardsList;
            }
            return fetchTimeCards1();
        }
        public set;
    }
    public String timeCardToUpdate{get;set;}
    
    public String errorMessage{get;set;}
    
    //Fetch time card method
    public List<Time_Card__c> fetchTimeCards1(){
    
        submittedTimeCardsList = [SELECT Id, Assignment__r.Project__c, Name, Assignment__r.EP_Shadow_Resource__c, Assignment__r.Resource__c,
                                    Type__c, EP_End_Date__c, Billable_Hours_Fri__c, EP_Notes__c, EP_Approval_Date__c, Assignment__r.Project__r.Contact_Person__c,
                                    EP_Total_Number_of_Billable_Hours__c, EP_Total_Number_of_Unbillable_Hours__c, Assignment__r.Name,
                                    EP_Rejected_Date__c, Submitted_On__c, Project_Manager__c, Billable_Hours_Mon__c, Assignment__r.Start_Date__c,
                                    Billable_Hours_Sat__c, Assignment__c, Status__c, Start_Date__c, Billable_Hours_Sun__c, Assignment__r.End_Date__c,
                                    Billable_Hours_Thu__c, Billable_Hours_Tue__c, Billable_Hours_Wed__c, UnBillable_Hours_Fri__c, 
                                    UnBillable_Hours_Mon__c, UnBillable_Hours_Sat__c, UnBillable_Hours_Sun__c, UnBillable_Hours_Thu__c, 
                                    UnBillable_Hours_Tue__c, UnBillable_Hours_Wed__c, Assignment__r.Project__r.Name,Can_not_Recall__c,
                                    Assignment__r.Resource__r.User__r.Name , Assignment__r.RecordType.Name, Assignment__r.Is_Primary_Resource_Contributing__c,
                                    X1st_Approver__c, X2nd_Approver__c, X3rd_Approver__c, X4th_Approver__c, X5th_Approver__c, Approval_Status__c,
                                    Assignment__r.EP_Shadow_Resource__r.User__r.Name,  Assignment__r.Role__c,(SELECT Id, Name FROM attachments)
                                    FROM Time_Card__c 
                                    WHERE (Status__c = 'Submitted' OR (Status__c = 'Approved' AND Client_Details__c = true)) AND 
                                    ((Assignment__r.RecordType.Name = 'Primary'AND Assignment__r.Resource__r.User__c =: UserInfo.getUserId()) OR
                (Assignment__r.RecordType.Name = 'Shadow' AND Assignment__r.EP_Shadow_Resource__r.User__c =: UserInfo.getUserId()))];
        system.debug('--------submittedTimeCardsList-------'+submittedTimeCardsList);
        return submittedTimeCardsList;
    }
    
    //Recall time card which are already submitted in cas u need to change any thing - Method
    public void sortTimeCard(){
        if(restringPrimaryRecall(timeCardToUpdate) == null){
            Time_Card__c timeCardToUpdate = new Time_Card__c(Id = timeCardToUpdate,
                                            Status__c = 'Draft',Send_Reminder_on_Recall__c = True);
            update timeCardToUpdate;
            try{
                Time_Card__c initialTimeCard = [SELECT Id, Assignment__r.Related_Resource_Allocation__c,
                                            Start_Date__c, EP_End_Date__c, Assignment__r.RecordType.Name
                                            FROM Time_Card__c 
                                            WHERE Assignment__r.Related_Resource_Allocation__c != null AND
                                            Id =: timeCardToUpdate.Id LIMIT 1];
                if(initialTimeCard != null){

                    Time_Card__c dependentTimeCard = [SELECT Id, Assignment__r.Related_Resource_Allocation__c,
                                            Start_Date__c, EP_End_Date__c
                                            FROM Time_Card__c 
                                            WHERE Assignment__r.Related_Resource_Allocation__c != null AND
                                            Assignment__c =: initialTimeCard.Assignment__r.Related_Resource_Allocation__c AND
                                            Start_Date__c =: initialTimeCard.Start_Date__c AND
                                            EP_End_Date__c =: initialTimeCard.EP_End_Date__c LIMIT 1];
                    if(dependentTimeCard != null){
                        dependentTimeCard.Status__c = 'Draft';
                        dependentTimeCard.Send_Reminder_on_Recall__c = True;
                        update dependentTimeCard;    
                    }
                    
                }
            }catch(Exception ex){

            }
        }else{
            errorMessage = restringPrimaryRecall(timeCardToUpdate);   
        }
        system.debug('----errorMessage--------'+errorMessage);
    }

    //Restrict primary from recalling if there is a shadow.
    public static String restringPrimaryRecall(Id timeCardId){
        String errorMessage = null;
        try{
            Time_Card__c initialTimeCard = [SELECT Id, Related_Time_Card__c, Assignment_Type__c,
                                        Related_Time_Card__r.Status__c
                                        FROM Time_Card__c 
                                        WHERE Assignment_Type__c = 'Primary' AND
                                        Id =: timeCardId AND 
                                        Related_Time_Card__c != null
                                        LIMIT 1];
            if(initialTimeCard != null && initialTimeCard.Related_Time_Card__r.Status__c != 'Draft'){
                errorMessage = 'Primary resource cannot recall the record until the Shadow resource reacal';    
            }
        }catch(Exception ex){
            //errorMessage = ex.getMessage(); 
        }
        return errorMessage;
    }

}