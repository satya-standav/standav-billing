public class ClentApprovalProcessPageController {
    //Variable declaration
    public List<timeCardWrapperForApprovals> timeCardWrapperForApprovalsList {
        get {
            if(timeCardWrapperForApprovalsList != null){
                return timeCardWrapperForApprovalsList;
            }
            return fetchApprovedTimeCards();
        }
        public set;
    }
    public String timeCardId {get; set;}
    public string projectCriteraString{get; set;}
    public string resourceCriteraString{get; set;}
    public String errorMessage{get;set;}
    public Date startDateFilter{get;set;}
    public Date endDateFilter{get;set;}
    public List<SelectOption> resourceNameOptionsList{get; set;} 
    public List<SelectOption> projectNameOptionsList{get; set;}  
    public Decimal totalBillableHours{get; set;}  
    public Integer divNumber{get; set;}
    
    public class timeCardWrapperForApprovals{
        public Boolean selected {get; set;}
        public Time_Card__c relatedTimeCard {get; set;}
        public Id conAttId {get; set;}
        
        public timeCardWrapperForApprovals(){
            selected = false;
            relatedTimeCard = new Time_Card__c();
        }
    }
    //Constructor
    public ClentApprovalProcessPageController(){
        divNumber = 0;
        reload();
    }

    public PageReference resetFiltersMethod(){
        fetchApprovedTimeCards();
        errorMessage = null;
        projectCriteraString = null;
        resourceCriteraString = null;
        startDateFilter = null;
        endDateFilter = null;
        return null;
    }
    public PageReference filterMethod(){
        totalBillableHours = 0.0;
        Date filterStartDate = startDateFilter;
        Date filterEndDate = endDateFilter;
        string queryString;
        String approvedStatus = 'Approved';
        String clientApprovedStatus = 'Client Approved';
        String primaryString = 'Primary';
        String clientId = System.currentPageReference().getParameters().get('clientId');
        system.debug('--1----resourceCriteraString--------'+resourceCriteraString);
        if(startDateFilter != null){
            Date dToday1 = filterStartDate;
            Datetime dt1 = datetime.newInstance(dToday1.year(), dToday1.month(),dToday1.day());
            String dayOfCurrentDate = dt1.format('E');
            filterStartDate = (dayOfCurrentDate == 'Sun' ? filterStartDate.addDays(1):
                             dayOfCurrentDate == 'Mon' ? filterStartDate.addDays(0):
                             dayOfCurrentDate == 'Tue' ? filterStartDate.addDays(-1):
                             dayOfCurrentDate == 'Wed' ? filterStartDate.addDays(-2):
                             dayOfCurrentDate == 'Thu' ? filterStartDate.addDays(-3):
                             dayOfCurrentDate == 'Fri' ? filterStartDate.addDays(-4):
                             dayOfCurrentDate == 'Sat' ? filterStartDate.addDays(-5): 
                             filterStartDate);     
        }
        if(endDateFilter != null){
            Date dToday2 = filterEndDate;
            Datetime dt2 = datetime.newInstance(dToday2.year(), dToday2.month(),dToday2.day());
            String dayOfCurrentDate = dt2.format('E');
            filterEndDate = (dayOfCurrentDate == 'Sun' ? filterEndDate.addDays(0):
                             dayOfCurrentDate == 'Mon' ? filterEndDate.addDays(6):
                             dayOfCurrentDate == 'Tue' ? filterEndDate.addDays(5):
                             dayOfCurrentDate == 'Wed' ? filterEndDate.addDays(4):
                             dayOfCurrentDate == 'Thu' ? filterEndDate.addDays(3):
                             dayOfCurrentDate == 'Fri' ? filterEndDate.addDays(2):
                             dayOfCurrentDate == 'Sat' ? filterEndDate.addDays(1): 
                             filterEndDate);     
        }
        String selectString = 'SELECT Id, Assignment__r.Project__c, Name, Assignment__r.EP_Shadow_Resource__c, Assignment__r.Resource__c,'+
                            'Type__c, EP_End_Date__c, Billable_Hours_Fri__c, EP_Notes__c, EP_Approval_Date__c, Assignment__r.Project__r.Contact_Person__c,'+
                            'EP_Total_Number_of_Billable_Hours__c, EP_Total_Number_of_Unbillable_Hours__c, '+
                            'EP_Rejected_Date__c, Submitted_On__c, Project_Manager__c, Billable_Hours_Mon__c,'+ 
                            'Billable_Hours_Sat__c, Assignment__c, Status__c, Start_Date__c, Billable_Hours_Sun__c, '+
                            'Billable_Hours_Thu__c, Billable_Hours_Tue__c, Billable_Hours_Wed__c, UnBillable_Hours_Fri__c, '+
                            'UnBillable_Hours_Mon__c, UnBillable_Hours_Sat__c, UnBillable_Hours_Sun__c, UnBillable_Hours_Thu__c,'+ 
                            'UnBillable_Hours_Tue__c, UnBillable_Hours_Wed__c, Assignment__r.Project__r.Name,'+
                            'Assignment__r.Resource__r.User__r.Name , Assignment__r.RecordType.Name, Assignment__r.Is_Primary_Resource_Contributing__c,'+
                            'X1st_Approver__c, X2nd_Approver__c, X3rd_Approver__c, X4th_Approver__c, X5th_Approver__c, Approval_Status__c,'+
                            'Assignment__r.EP_Shadow_Resource__r.User__r.Name,  Assignment__r.Role__c,(SELECT Id, Name FROM attachments)';
        String formString = ' FROM Time_Card__c';
        String whereString = ' WHERE Status__c =: approvedStatus AND Status__c !=: clientApprovedStatus AND Assignment__r.RecordType.Name =: primaryString AND'+ 
                            ' Assignment__r.Project__r.Contact_Person__c =: clientId';
        system.debug('------selectString----------'+selectString);
        system.debug('-------formString---------'+formString);
        system.debug('-------whereString---------'+whereString);
        if(projectCriteraString != null && resourceCriteraString == null && filterStartDate == null && filterEndDate == null){
            queryString = selectString + formString + whereString + ' AND Assignment__r.Project__r.Name =: projectCriteraString';
            system.debug('------queryString----------'+queryString);                
        }
        if(resourceCriteraString != null && filterStartDate == null && filterEndDate == null && projectCriteraString == null ){
            queryString = selectString + formString + whereString + ' AND Assignment__r.Resource__r.User__r.Name =: resourceCriteraString';
            system.debug('------queryString--1--------'+queryString); 
        }
        if(filterStartDate != null && filterEndDate != null && resourceCriteraString == null && projectCriteraString == null){
            queryString = selectString + formString + whereString + ' AND Start_Date__c >=: filterStartDate AND EP_End_Date__c <=: filterEndDate';
            system.debug('------queryString--2--------'+queryString); 
        }
        if(projectCriteraString != null && resourceCriteraString != null && filterStartDate != null && filterEndDate != null){
            queryString = selectString + formString + whereString + ' AND Assignment__r.Project__r.Name =: projectCriteraString AND Assignment__r.Resource__r.User__r.Name =: resourceCriteraString AND Start_Date__c >=: filterStartDate AND EP_End_Date__c <=: filterEndDate';
            system.debug('------queryString--3--------'+queryString); 
        }
        if(projectCriteraString != null && resourceCriteraString == null && filterStartDate != null && filterEndDate != null){
            queryString = selectString + formString + whereString + ' AND Assignment__r.Project__r.Name =: projectCriteraString AND Start_Date__c >=: filterStartDate AND EP_End_Date__c <=: filterEndDate';
            system.debug('------queryString--4--------'+queryString); 
        }
        if(projectCriteraString != null && resourceCriteraString != null && filterStartDate == null && filterEndDate == null){
            queryString = selectString + formString + whereString + ' AND Assignment__r.Project__r.Name =: projectCriteraString AND Assignment__r.Resource__r.User__r.Name =: resourceCriteraString';
            system.debug('------queryString--5--------'+queryString); 
        }
        if(projectCriteraString == null && resourceCriteraString != null && filterStartDate != null && filterEndDate != null){
            queryString = selectString + formString + whereString + ' AND Assignment__r.Resource__r.User__r.Name =: resourceCriteraString AND Start_Date__c >=: filterStartDate AND EP_End_Date__c <=: filterEndDate';
            system.debug('------queryString--6--------'+queryString); 
        }
        if(queryString == null){
            timeCardWrapperForApprovalsList = new List < timeCardWrapperForApprovals > ();
            timeCardWrapperForApprovalsList = fetchApprovedTimeCards();
            if(timeCardWrapperForApprovalsList.size() == 0){
                errorMessage = 'No Pending Timesheets for the selected filter';
            }
            system.debug('------timeCardWrapperForApprovalsList 1----------'+timeCardWrapperForApprovalsList); 
        }
        if(queryString != null){
            timeCardWrapperForApprovalsList = new List<timeCardWrapperForApprovals>();
            for(Time_Card__c timeCardApproved : Database.query(queryString)){
                system.debug('------timeCardApproved 1----------'+timeCardApproved); 
                timeCardWrapperForApprovals approvalWrapper = new timeCardWrapperForApprovals();
                approvalWrapper.relatedTimeCard = timeCardApproved;  
                divNumber++; 
                timeCardWrapperForApprovalsList.add(approvalWrapper);  
            }
            if(timeCardWrapperForApprovalsList == null || timeCardWrapperForApprovalsList.size() == 0){
                errorMessage = 'No Pending Timesheets for Approvals';
            }
            system.debug('------timeCardWrapperForApprovalsList 2----------'+timeCardWrapperForApprovalsList); 
        }
        return null;
    }

    public PageReference reload(){
        PageReference pr = ApexPages.currentPage();
        fetchApprovedTimeCards();
        //pr.getParameters().put('clientId',System.currentPageReference().getParameters().get('clientId'));
        pr.setRedirect(true); // If you want a redirect. Do not set anything if you want a forward.
        return pr;
    }
    //Fetch time card method
    public List<timeCardWrapperForApprovals> fetchApprovedTimeCards(){
        system.debug('--entered fetchApprovedTimeCards--------');
        timeCardWrapperForApprovalsList = new List<timeCardWrapperForApprovals>();
        projectNameOptionsList = new List<SelectOption>();
        resourceNameOptionsList = new List<SelectOption>();
        Set<SelectOption> resourceNameSet = new Set<SelectOption>();
        resourceNameSet.add(new SelectOption('', 'Please Select'));
        Set<SelectOption> projectNameSet = new Set<SelectOption>();
        projectNameSet.add(new SelectOption('', 'Please Select'));
        //List<Attachment> conAttList = new List<Attachment>();
        //Map<String, Id> attNameWithConIdMap = new Map<String, Id>();

        for(Time_Card__c timeCardApproved : [SELECT Id, Assignment__r.Project__c, Name, 
                                                Assignment__r.EP_Shadow_Resource__c, 
                                                Assignment__r.Resource__c,
                                                Type__c, EP_End_Date__c, Billable_Hours_Fri__c, EP_Notes__c, 
                                                EP_Approval_Date__c, Assignment__r.Project__r.Contact_Person__c,
                                                EP_Total_Number_of_Billable_Hours__c, 
                                                EP_Total_Number_of_Unbillable_Hours__c, 
                                                Finally_Approved__c,
                                                EP_Rejected_Date__c, Submitted_On__c, Project_Manager__c, 
                                                Billable_Hours_Mon__c, 
                                                Assignment__r.Billing_Type__c,
                                                Billable_Hours_Sat__c, Assignment__c, Status__c, Start_Date__c, 
                                                Billable_Hours_Sun__c, 
                                                Billable_Hours_Thu__c, Billable_Hours_Tue__c, Billable_Hours_Wed__c, 
                                                UnBillable_Hours_Fri__c, 
                                                UnBillable_Hours_Mon__c, UnBillable_Hours_Sat__c, UnBillable_Hours_Sun__c, 
                                                UnBillable_Hours_Thu__c, 
                                                UnBillable_Hours_Tue__c, UnBillable_Hours_Wed__c, 
                                                Assignment__r.Project__r.Name,
                                                Assignment__r.Resource__r.User__r.Name , Assignment__r.RecordType.Name, 
                                                Assignment__r.Is_Primary_Resource_Contributing__c,
                                                X1st_Approver__c, X2nd_Approver__c, X3rd_Approver__c, X4th_Approver__c, 
                                                X5th_Approver__c, Approval_Status__c,
                                                Assignment__r.EP_Shadow_Resource__r.User__r.Name,  
                                                Assignment__r.Role__c, 
                                                (SELECT Id, Name FROM attachments)
                                                FROM Time_Card__c 
                                                WHERE Status__c = 'Approved' AND Status__c != 'Client Approved' AND 
                                                Assignment__r.RecordType.Name = 'Primary' AND 
                                                Assignment__r.Billing_Type__c = 'Billable' AND 
                                                Assignment__r.Project__r.Contact_Person__c =: System.currentPageReference().getParameters().get('clientId')]){
            timeCardWrapperForApprovals approvalWrapper = new timeCardWrapperForApprovals();
            approvalWrapper.relatedTimeCard = timeCardApproved;  
            divNumber++; 
            timeCardWrapperForApprovalsList.add(approvalWrapper); 
            resourceNameSet.add(new SelectOption(timeCardApproved.Assignment__r.Resource__r.User__r.Name, timeCardApproved.Assignment__r.Resource__r.User__r.Name)); 
            projectNameSet.add(new SelectOption(timeCardApproved.Assignment__r.Project__r.Name, timeCardApproved.Assignment__r.Project__r.Name)); 
            /*if(timeCardApproved.attachments.size() > 0){
                for(Attachment att : timeCardApproved.attachments){
                    attNameWithConIdMap.put(att.Name, timeCardApproved.Assignment__r.Project__r.Contact_Person__c);    
                }
            }*/
        }
        /*if(conAttList.size() > 0){
            insert conAttList;
        }*/
        projectNameOptionsList.addAll(projectNameSet);
        resourceNameOptionsList.addAll(resourceNameSet);
        if(timeCardWrapperForApprovalsList == null || timeCardWrapperForApprovalsList.size() == 0){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'No Pending Timesheets for Approvals'));
            errorMessage = 'No Pending Timesheets for Approvals';
            return null;
        }else{
            return timeCardWrapperForApprovalsList;
        }
    }
    //Approve method
    public void approveTimeCard(){
        Time_Card__c timeCardApproved = [SELECT Id, Assignment__r.Project__c, Name, Assignment__r.EP_Shadow_Resource__c, Assignment__r.Resource__c,
                                        Type__c, EP_End_Date__c, Billable_Hours_Fri__c, EP_Notes__c, EP_Approval_Date__c, Finally_Approved__c,
                                        EP_Total_Number_of_Billable_Hours__c, EP_Total_Number_of_Unbillable_Hours__c, 
                                        EP_Rejected_Date__c, Submitted_On__c, Project_Manager__c, Billable_Hours_Mon__c, 
                                        Billable_Hours_Sat__c, Assignment__c, Status__c, Start_Date__c, Billable_Hours_Sun__c, 
                                        Billable_Hours_Thu__c, Billable_Hours_Tue__c, Billable_Hours_Wed__c, UnBillable_Hours_Fri__c, 
                                        UnBillable_Hours_Mon__c, UnBillable_Hours_Sat__c, UnBillable_Hours_Sun__c, UnBillable_Hours_Thu__c, 
                                        UnBillable_Hours_Tue__c, UnBillable_Hours_Wed__c, Assignment__r.Project__r.Name,
                                        Assignment__r.Resource__r.User__r.Name , Assignment__r.RecordType.Name, Assignment__r.Is_Primary_Resource_Contributing__c,
                                        X1st_Approver__c, X2nd_Approver__c, X3rd_Approver__c, X4th_Approver__c, X5th_Approver__c, Approval_Status__c,
                                        Assignment__r.EP_Shadow_Resource__r.User__r.Name,  Assignment__r.Role__c
                                        FROM Time_Card__c 
                                        WHERE Status__c = 'Approved' AND Assignment__r.RecordType.Name = 'Primary' AND 
                                        (Id =: System.currentPageReference().getParameters().get('id') OR Id =: timeCardId)  Limit 1];
        timeCardApproved.Status__c = 'Client Approved'; 
        timeCardApproved.Finally_Approved__c = True;  
        update timeCardApproved; 
        fetchApprovedTimeCards();
        updateAwaitingTimeCardCount(System.currentPageReference().getParameters().get('clientId'), 1);
        
    }
    //Reject method
    public void rejectRecord(){
        Time_Card__c timeCardApproved = [SELECT Id, Assignment__r.Project__c, Name, Assignment__r.EP_Shadow_Resource__c, Assignment__r.Resource__c,
                                        Type__c, EP_End_Date__c, Billable_Hours_Fri__c, EP_Notes__c, EP_Approval_Date__c, 
                                        EP_Total_Number_of_Billable_Hours__c, EP_Total_Number_of_Unbillable_Hours__c, 
                                        EP_Rejected_Date__c, Submitted_On__c, Project_Manager__c, Billable_Hours_Mon__c, 
                                        Billable_Hours_Sat__c, Assignment__c, Status__c, Start_Date__c, Billable_Hours_Sun__c, 
                                        Billable_Hours_Thu__c, Billable_Hours_Tue__c, Billable_Hours_Wed__c, UnBillable_Hours_Fri__c, 
                                        UnBillable_Hours_Mon__c, UnBillable_Hours_Sat__c, UnBillable_Hours_Sun__c, UnBillable_Hours_Thu__c,                                                 UnBillable_Hours_Tue__c, UnBillable_Hours_Wed__c, Assignment__r.Project__r.Name,
                                        Assignment__r.Resource__r.User__r.Name , Assignment__r.RecordType.Name, Assignment__r.Is_Primary_Resource_Contributing__c,
                                        X1st_Approver__c, X2nd_Approver__c, X3rd_Approver__c, X4th_Approver__c, X5th_Approver__c, Approval_Status__c,
                                        Assignment__r.EP_Shadow_Resource__r.User__r.Name,  Assignment__r.Role__c
                                        FROM Time_Card__c 
                                        WHERE Status__c = 'Approved' AND Assignment__r.RecordType.Name = 'Primary' AND 
                                        (Id =: System.currentPageReference().getParameters().get('id') OR Id =: timeCardId) Limit 1];
        timeCardApproved.Status__c = 'Draft';  
        timeCardApproved.Client_Rejected__c = True;
        timeCardApproved.Approval_Status__c = ''; 
        update timeCardApproved; 
        fetchApprovedTimeCards();
    }
    
    public pageReference reDirect(){
        //PageReference pr = new PageReference('https://devemp-standav.cs40.force.com/clientpage/ClientTimeCardReviewPage');
        String pageURL = System.Label.Client_Page_URL+'ClientTimeCardReviewPage';
        PageReference pr = new PageReference(pageURL);
        pr.getParameters().put('id',timeCardId);
        //pr.setRedirect(true); // If you want a redirect. Do not set anything if you want a forward.
        //fetchApprovedTimeCards();
        return pr;
    }

    //Mass Approve
    public void massApprove(){
        List<Time_Card__c> timeCardsToApproveIdList = new List<Time_Card__c>();
        List<Time_Card__c> timeCardsToApproveToUpdateList = new List<Time_Card__c>();

        system.debug('--------timeCardWrapperForApprovalsList---------'+timeCardWrapperForApprovalsList);
        for(timeCardWrapperForApprovals timeCardToReject : timeCardWrapperForApprovalsList){
            if(timeCardToReject.selected){
                timeCardsToApproveIdList.add(timeCardToReject.relatedTimeCard);
            }
        } 
        system.debug('--------timeCardsToApproveIdList---------'+timeCardsToApproveIdList);  
        for(Time_Card__C timeCardToApprove : timeCardsToApproveIdList){
            timeCardToApprove.Status__c = 'Client Approved'; 
            timeCardToApprove.Finally_Approved__c = True;
            timeCardsToApproveToUpdateList.add(timeCardToApprove);
        }
        if(timeCardsToApproveToUpdateList.size() > 0){
            update timeCardsToApproveToUpdateList;
        }
        fetchApprovedTimeCards();
        updateAwaitingTimeCardCount(System.currentPageReference().getParameters().get('clientId'), timeCardsToApproveToUpdateList.size());
        
    }
    
    //Mass Reject
    public void massReject(){
        List<Time_Card__c> timeCardsToRejectIdList = new List<Time_Card__c>();
        List<Time_Card__c> timeCardsToApproveToUpdateList = new List<Time_Card__c>();

        system.debug('--------timeCardWrapperForApprovalsList---------'+timeCardWrapperForApprovalsList);
        for(timeCardWrapperForApprovals timeCardToReject : timeCardWrapperForApprovalsList){
            if(timeCardToReject.selected){
                timeCardsToRejectIdList.add(timeCardToReject.relatedTimeCard);
            }
        } 
        system.debug('--------timeCardsToRejectIdList---------'+timeCardsToRejectIdList);  
        for(Time_Card__C timeCardToReject : timeCardsToRejectIdList){
            timeCardToReject.Status__c = 'Draft'; 
            timeCardToReject.Client_Rejected__c = True;
            timeCardToReject.Approval_Status__c = '';
            timeCardsToApproveToUpdateList.add(timeCardToReject);
        }
        if(timeCardsToApproveToUpdateList.size() > 0){
            update timeCardsToApproveToUpdateList;
        }
        fetchApprovedTimeCards();
    }
    //Update Number of Awaiting Approval TimeCards counf on cantact.
    public void updateAwaitingTimeCardCount(Id contactId, Integer noOfApprovedTimeCards){
        system.debug('-------contactId--------'+contactId);
        system.debug('-------noOfApprovedTimeCards--------'+noOfApprovedTimeCards);
        Contact conToUpdate = [SELECT Id, Number_of_Awaiting_Approval_TimeCards__c 
                                FROM Contact
                                WHERE Id =: contactId];
        /*if(conToUpdate.Number_of_Awaiting_Approval_TimeCards__c == null || 
            conToUpdate.Number_of_Awaiting_Approval_TimeCards__c == 0)
            conToUpdate.Number_of_Awaiting_Approval_TimeCards__c = noOfApprovedTimeCards > 1 ?
                conToUpdate.Number_of_Awaiting_Approval_TimeCards__c - noOfApprovedTimeCards : 0;
        else if(conToUpdate.Number_of_Awaiting_Approval_TimeCards__c > 0 &&
            noOfApprovedTimeCards > 0)
            conToUpdate.Number_of_Awaiting_Approval_TimeCards__c = 
            conToUpdate.Number_of_Awaiting_Approval_TimeCards__c - noOfApprovedTimeCards;*/
        conToUpdate.Number_of_Awaiting_Approval_TimeCards__c = timeCardWrapperForApprovalsList.size();
        
        update conToUpdate;
        system.debug('-------conToUpdate--------'+conToUpdate);
    }

    public pageReference reZip(){
        Time_Card__c timecardtoreview = [Select Id,Name,Assignment__r.Resource__r.Name,Start_Date__c From Time_Card__C Where Id=:timeCardId LIMIT 1];
        String docName = timecardtoreview.Assignment__r.Resource__r.Name+' Attachments '+timecardtoreview.Start_Date__c+'.zip';
        Attachment atta = [SELECT Id,Name,ParentId, Body FROM Attachment WHERE ParentId = :timeCardId LIMIT 1];
        try{
            System.debug('>>> attachments ' + atta);
            Zippex sampleZip = new Zippex();
            //for(Attachment file : attachments) {
            sampleZip.addFile( atta.Name, atta.Body, null);
            //}
            try{
              Document doc = new Document();
              doc.FolderId = [SELECT Id From Folder WHERE Name='AttachmentsZipFolder' LIMIT 1].id;
              doc.Name = docName;
              doc.Body = sampleZip.getZipArchive();
              insert doc;
              System.debug('>>> doc ' + doc.Id);
              return new PageReference('https://qa-standavglobal.cs92.force.com/StandavPortal/s/servlet/servlet.FileDownload?file=' + doc.Id);
            }catch ( Exception ex ) {
              System.debug('>>> ERROR ' + ex);
            }
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()+'ERROR'));
        }
        return null;  
    }
}