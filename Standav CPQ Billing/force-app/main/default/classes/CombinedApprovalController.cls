/**
*  @Class Name: CombinedApprovalController
*  @Description: Handles all functionality required for "ApprovalProcessPage" visualforce page.
*  @Company: Standav
*  @CreatedDate: 12/01/2019
*  Change Log
*  -----------------------------------------------------------
*  Developer: Shruti MP           
   Modification Date : 12/01/2019          
   Comments
*  -----------------------------------------------------------
*
*/
public class CombinedApprovalController{
    
    //Variable Declaration
    public Id timeCardToUpdate{get;set;}
    public String currentResourceName {get; set;}
    public String oppIdToWin {get; set;}
    public String relatedClientEmail {get; set;}
    public String errorMessage{get;set;}
    public String errorMessage1{get;set;}
    public String errorMessage2{get;set;}
    public string projectCriteraString{get; set;}
    public string resourceCriteraString{get; set;}
    public Date startDateFilter{get;set;}
    public String parentId {get; set;}
    public Date endDateFilter{get;set;}
    public Attachment attach {get;set;}
    public Boolean relatedContactReminder{get;set;}
    public String clientId{get; set;}
    public List<SelectOption> resourceNameOptionsList{get; set;} 
    public List<SelectOption> projectNameOptionsList{get; set;}  
    Map<Id, String> userIdWithNameMap;
    public List<Time_Card__c> relatedTimeCardAwaitingSubmission {get; set;}
    public List<Time_Card__c> relatedTimeCardAwaitingClientApproval {get; set;}
    public List<pendingapprovalTimeCardsWrapper> timeCardWrapperForApprovalsList {
        get {
            if(timeCardWrapperForApprovalsList != null){
                return timeCardWrapperForApprovalsList;
            }
            return fetchTimeCardsForApproval();
        }
        public set;
    }

    //Constructor
    public CombinedApprovalController(){
        timeCardToUpdate = this.timeCardToUpdate;
        oppIdToWin = this.oppIdToWin;
        attach = new Attachment();
        relatedClientEmail = this.relatedClientEmail;
        relatedTimeCardAwaitingSubmission = new List<Time_Card__c>();
        relatedTimeCardAwaitingClientApproval = new List<Time_Card__c>();
        userIdWithNameMap = new Map<Id, String>();
        resourceNameOptionsList = new List<SelectOption>();
        Set<SelectOption> resourceNameSet = new Set<SelectOption>();
        resourceNameSet.add(new SelectOption('', 'Please Select'));

        for(User UserRec : UserDataCache.getMap().values()){
            userIdWithNameMap.put(UserRec.Id, UserRec.FirstName+' '+UserRec.LastName);
            resourceNameSet.add(new SelectOption(UserRec.FirstName+' '+UserRec.LastName, UserRec.FirstName+' '+UserRec.LastName)); 
        } 
        system.debug('-------User size---------'+userIdWithNameMap.values().size());
        system.debug('-------User size---1---------'+resourceNameSet.size());
        resourceNameOptionsList.addAll(resourceNameSet); 
        fetchTimeCardsForApproval();
    }

    //Method to fetch the data n construct the structure to display it in VF Page
    public List<pendingapprovalTimeCardsWrapper> fetchTimeCardsForApproval(){

        timeCardWrapperForApprovalsList = new List<pendingapprovalTimeCardsWrapper>();
        relatedTimeCardAwaitingSubmission = new List<Time_Card__c>();
        relatedTimeCardAwaitingClientApproval = new List<Time_Card__c>();

        projectNameOptionsList = new List<SelectOption>();
        Set<SelectOption> projectNameSet = new Set<SelectOption>();
        projectNameSet.add(new SelectOption('', 'Please Select'));
        system.debug('--3----projectCriteraString --------'+projectCriteraString );
        system.debug('--3----resourceCriteraString --------'+resourceCriteraString );
        system.debug('--3----startDateFilter --------'+startDateFilter );
        system.debug('--3---endDateFilter ---------'+endDateFilter );
        system.debug('--3----errorMessage --------'+errorMessage );
        /*List<String> statuslist = new List<String>();
        statuslist.add('Submitted');
        statuslist.add('Assigned');
        statuslist.add('Rejected');
        statuslist.add('Draft');
        statuslist.add('Client Approved');
        statuslist.add('Approved');*/
        
        //Id projectManagerId = UserInfo.getUserId();
        Id projectManagerId = '00554000004V4gK';
        String assignmentType = 'Primary';

        try{
            currentResourceName = UserInfo.getName();
            for(Time_Card__c timeCardSubmitted : ApprovalDataCache.getMap(projectManagerId, assignmentType).values()){
                system.debug('-------timeCardSubmitted ---------'+timeCardSubmitted );
                if(timeCardSubmitted.Status__c == 'Assigned' || 
                    timeCardSubmitted.Status__c == 'Rejected' || 
                    timeCardSubmitted.Status__c == 'Draft'){
                    system.debug('--------timeCardSubmitted 1-----------'+timeCardSubmitted);
                    if(relatedTimeCardAwaitingSubmission.size() == 0){
                        system.debug('--------timeCardSubmitted 1-2-----------'+timeCardSubmitted);
                        relatedTimeCardAwaitingSubmission = new List<Time_Card__c>();
                        relatedTimeCardAwaitingSubmission.add(timeCardSubmitted);   
                    }
                    else{
                        system.debug('--------timeCardSubmitted 1-3-----------'+timeCardSubmitted);
                        relatedTimeCardAwaitingSubmission.add(timeCardSubmitted);
                    }
                }
                if(timeCardSubmitted.Status__c == 'Submitted'){
                    pendingapprovalTimeCardsWrapper approvalWrapper = new pendingapprovalTimeCardsWrapper();
                    approvalWrapper.clientEmailId = timeCardSubmitted.Assignment__r.Project__r.Contact_Person__r.Email;
                    approvalWrapper.clientId = timeCardSubmitted.Assignment__r.Project__r.Contact_Person__c;
                    approvalWrapper.timeCardMap.put(timeCardSubmitted.Id, timeCardSubmitted);
                    system.debug('--------approvalWrapper.relatedTimeCard 2-----------'+approvalWrapper.relatedTimeCard);
                    system.debug('--------approvalWrapper.relatedTimeCard 2-1----------'+timeCardSubmitted.Project_Name__c);
                    approvalWrapper.relatedTimeCard = timeCardSubmitted;
                    timeCardWrapperForApprovalsList.add(approvalWrapper);
                }
                if(timeCardSubmitted.Status__c == 'Approved' &&
                    timeCardSubmitted.Status__c != 'Client Approved' &&
                    timeCardSubmitted.Client_Details__c == True){
                    if(relatedTimeCardAwaitingClientApproval.size() == 0){
                        relatedTimeCardAwaitingClientApproval = new List<Time_Card__c>();
                        relatedTimeCardAwaitingClientApproval.add(timeCardSubmitted);   
                    }else{
                        relatedTimeCardAwaitingClientApproval.add(timeCardSubmitted);
                    }
                }                                 
                projectNameSet.add(new SelectOption(timeCardSubmitted.Project_Name__c, timeCardSubmitted.Project_Name__c)); 
            }
        projectNameOptionsList.addAll(projectNameSet);
        system.debug('----timeCardWrapperForApprovalsList--------'+timeCardWrapperForApprovalsList);
        if(relatedTimeCardAwaitingSubmission.size() == 0){
            errorMessage = 'No Pending Timesheets for submission';
        }if(timeCardWrapperForApprovalsList.size() == 0){
            errorMessage1 = 'No Pending Timesheets for Approvals';
        }if(relatedTimeCardAwaitingClientApproval.size() == 0){
            errorMessage2 = 'No Pending Timesheets for Approvals';
        }
        }catch(Exception ex){
            errorMessage = ex.getMessage();
            system.debug('--------exception------'+ex);
        }
        system.debug('--------relatedTimeCardAwaitingSubmission size()--------'+relatedTimeCardAwaitingSubmission.size());
        system.debug('--------timeCardWrapperForApprovalsList size()--------'+timeCardWrapperForApprovalsList.size());
        system.debug('--------relatedTimeCardAwaitingClientApproval.size()--------'+relatedTimeCardAwaitingClientApproval.size());
        system.debug('--2----projectCriteraString --------'+projectCriteraString );
        system.debug('--2----resourceCriteraString --------'+resourceCriteraString );
        system.debug('--2----startDateFilter --------'+startDateFilter );
        system.debug('--2---endDateFilter ---------'+endDateFilter );
        system.debug('--2----errorMessage --------'+errorMessage );
        return timeCardWrapperForApprovalsList;
    }

    //Wrapper class for Pending Approval
    public class pendingapprovalTimeCardsWrapper{
        public Boolean selected {get; set;}
        public String clientEmailId {get; set;}
        public String clientId {get; set;}
        public Map<Id, Time_Card__c> timeCardMap {get; set;}
        public Time_Card__c relatedTimeCard {get; set;}
        public pendingapprovalTimeCardsWrapper(){
            selected = false;
            timeCardMap = new Map<Id, Time_Card__c>();
            relatedTimeCard = new Time_Card__c();
        }   
    }

    //REdirect method
    public PageReference cancel(){
        PageReference pr = new PageReference('/ApprovalProcessPage');
        pr.setRedirect(true);
        return pr;
    }

    //approve Method
    public void approveTimeCard(){
        List<Time_Card__c> approvedTimeCardList = new List<Time_Card__c>();
        for(pendingapprovalTimeCardsWrapper timeCardToReject : timeCardWrapperForApprovalsList){
            approvedTimeCardList.add(timeCardToReject.timeCardMap.get(timeCardToReject.relatedTimeCard.Id));
        }
        TimeCardApprovalController.approveOrReject(approvedTimeCardList, 'Approved');
        fetchTimeCardsForApproval();
    }
    
    //Reject approval
    public void rejectRecord(){
        List<Time_Card__c> approvedTimeCardList = new List<Time_Card__c>();
        for(pendingapprovalTimeCardsWrapper timeCardToReject : timeCardWrapperForApprovalsList){
            approvedTimeCardList.add(timeCardToReject.timeCardMap.get(timeCardToReject.relatedTimeCard.Id));
        }
        TimeCardApprovalController.approveOrReject(approvedTimeCardList, 'Rejected');
        fetchTimeCardsForApproval();
    }
    
    //Mass Approve
    public void massApprove(){
        List<Time_Card__c> timeCardsToApproveIdList = new List<Time_Card__c>();
        for(pendingapprovalTimeCardsWrapper timeCardToReject : timeCardWrapperForApprovalsList){
            if(timeCardToReject.selected){
                timeCardsToApproveIdList.add(timeCardToReject.timeCardMap.get(timeCardToReject.relatedTimeCard.Id));
            }
        }
        system.debug('--------timeCardWrapperForApprovalsList---------'+timeCardWrapperForApprovalsList);
        
        system.debug('--------timeCardsToApproveIdList---------'+timeCardsToApproveIdList);  
        TimeCardApprovalController.approveOrReject(timeCardsToApproveIdList, 'Approved');
        fetchTimeCardsForApproval();
    }
    
    //Mass Reject
    public void massReject(){
        List<Time_Card__c> timeCardsToApproveIdList = new List<Time_Card__c>();
        
        system.debug('--------timeCardWrapperForApprovalsList---------'+timeCardWrapperForApprovalsList);
        for(pendingapprovalTimeCardsWrapper timeCardToReject : timeCardWrapperForApprovalsList){
            if(timeCardToReject.selected){
                timeCardsToApproveIdList.add(timeCardToReject.timeCardMap.get(timeCardToReject.relatedTimeCard.Id));
            }
        } 
        system.debug('--------timeCardsToApproveIdList---------'+timeCardsToApproveIdList);  
        TimeCardApprovalController.approveOrReject(timeCardsToApproveIdList, 'Rejected');
        fetchTimeCardsForApproval();
    }
    
    //Redirect to Review page
    public pageReference reDirect(){
        PageReference pr = new PageReference('/apex/Approval_Review_Page');
        pr.getParameters().put('id',oppIdToWin);
        pr.setRedirect(true); // If you want a redirect. Do not set anything if you want a forward.
        fetchTimeCardsForApproval();
        return pr;
    }

    //attach files
    public PageReference attachFile(){
        Attachment a = new Attachment(parentId = parentId, name = attach.Name, body=attach.body) ;
        Insert a;
        PageReference p= new PageReference('/apex/AwaitingClientApprovalPage');
        p.setRedirect(true);
        return p;
    }

    //Send reminder Method
    public void sendReminder(){
        Time_Card__c timeCardToUpdate = new Time_Card__c(Id = timeCardToUpdate,
                                        Send_Reminder__c = True);
        update timeCardToUpdate;
        fetchTimeCardsForApproval();
    }
    
    //approve MEthod
    public PageReference approveAndSendToClient(){    
        
        List<Time_Card__c> approvedTimeCardList = new List<Time_Card__c>();
        for(pendingapprovalTimeCardsWrapper timeCardToReject : timeCardWrapperForApprovalsList){
            approvedTimeCardList.add(timeCardToReject.timeCardMap.get(timeCardToReject.relatedTimeCard.Id));
        }
        TimeCardApprovalController.approveOrReject(approvedTimeCardList, 'Approved');

        //Sending the approval request to client.
        if(relatedContactReminder == False){
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage mail =  new Messaging.SingleEmailMessage();
            List<String> sendTo = new List<String>{relatedClientEmail};
            mail.setToAddresses(sendTo);
            
             mail.setSubject('Time Card for Approval');
             String body = '<html lang="ja"><body>'+
               '<br><br>'+'Dear Approver'+
               '<br><br>'+'There are timesheets submitted by the team members pending your approval. Please review and take necessary action.'+
               '<br><br>'+'You can find the details in the link given below.'+
               '<br><br> <a href=https://devemp-standav.cs40.force.com/clientpage/ClientApprovalPage?clientId='+clientId+'&id='+oppIdToWin+'>Take Action Now</a>'+
                                '<br><br>'+'Kind Regards,'+
                                '<br>'+'Standav';
            mail.setHtmlBody(body);
            mails.add(mail);
            Messaging.sendEmail(mails);
            Contact con = new Contact(Id = clientId, Single_Reminder__c = True);
            system.debug('--------con------'+con);
            update con;
        }
        fetchTimeCardsForApproval();
        return null; 
    }

    //Reset Method
    public PageReference resetFiltersMethod(){
        system.debug('------projectCriteraString --------'+projectCriteraString );
        system.debug('------resourceCriteraString --------'+resourceCriteraString );
        system.debug('------startDateFilter --------'+startDateFilter );
        system.debug('-----endDateFilter ---------'+endDateFilter );
        system.debug('------errorMessage --------'+errorMessage );
        
        errorMessage = null;
        projectCriteraString = null;
        resourceCriteraString = null;
        startDateFilter = null;
        endDateFilter = null;
        system.debug('-------projectCriteraString 1-------'+projectCriteraString );
        system.debug('-------resourceCriteraString 1-------'+resourceCriteraString );
        system.debug('------startDateFilter 1--------'+startDateFilter );
        system.debug('------endDateFilter 1--------'+endDateFilter );
        
        fetchTimeCardsForApproval();
        return null;
    }

    //Filter Method
    public PageReference filterMethod(){
        Date filterStartDate = startDateFilter;
        Date filterEndDate = endDateFilter;
        string queryString;
        system.debug('--1----resourceCriteraString--------'+resourceCriteraString);
        if(startDateFilter != null){
            Date dToday1 = filterStartDate;
            Datetime dt1 = datetime.newInstance(dToday1.year(), dToday1.month(),dToday1.day());
            String dayOfCurrentDate = dt1.format('E');
            filterStartDate = (dayOfCurrentDate == 'Sun' ? filterStartDate.addDays(1):
                             dayOfCurrentDate == 'Mon' ? filterStartDate.addDays(0):
                             dayOfCurrentDate == 'Tue' ? filterStartDate.addDays(-1):
                             dayOfCurrentDate == 'Wed' ? filterStartDate.addDays(-2):
                             dayOfCurrentDate == 'Thu' ? filterStartDate.addDays(-3):
                             dayOfCurrentDate == 'Fri' ? filterStartDate.addDays(-4):
                             dayOfCurrentDate == 'Sat' ? filterStartDate.addDays(-5): 
                             filterStartDate);     
        }
        if(endDateFilter != null){
            Date dToday2 = filterEndDate;
            Datetime dt2 = datetime.newInstance(dToday2.year(), dToday2.month(),dToday2.day());
            String dayOfCurrentDate = dt2.format('E');
            filterEndDate = (dayOfCurrentDate == 'Sun' ? filterEndDate.addDays(0):
                             dayOfCurrentDate == 'Mon' ? filterEndDate.addDays(6):
                             dayOfCurrentDate == 'Tue' ? filterEndDate.addDays(5):
                             dayOfCurrentDate == 'Wed' ? filterEndDate.addDays(4):
                             dayOfCurrentDate == 'Thu' ? filterEndDate.addDays(3):
                             dayOfCurrentDate == 'Fri' ? filterEndDate.addDays(2):
                             dayOfCurrentDate == 'Sat' ? filterEndDate.addDays(1): 
                             filterEndDate);     
        }

        String primaryString = 'Primary';
        //Id loggedInUserId = UserInfo.getUserId();
        Id loggedInUserId = '00554000004V4gK';
        String selectString = 'SELECT Id, Name, Project_Manager__c,Project_Name__c,Assignment__r.Project__r.Name,'+
                            'Status__c, Start_Date__c,Client_Details__c,Manager_Approval_Comments__c,Client_Rejected__c,'+
                            'Invoiced__c,EP_Primary_Resource_Email_Id__c,Send_Reminder_on_Recall__c,Send_Reminder__c,'+
                            'Reminder_Sent_Date__c,Send_to_Client_Approval__c,EP_Shadow_Resource_Email_Id__c,'+
                            'Send_reminder_to_Finance_team__c,Total_Hours__c,Show_Comments__c,Burned_Hours__c,'+
                            'EP_End_Date__c,RecordTypeId,EP_Notes__c,Resource_Name__c, EP_Approval_Date__c,Approval_Status__c,'+
                            'EP_Total_Number_of_Billable_Hours__c,Approved_Hours__c,Approved_Hours_Weekly__c,'+
                            'EP_Total_Number_of_Unbillable_Hours__c,EP_Rejected_Date__c, Submitted_On__c,'+
                            'Billable_Hours_Mon__c,Billable_Hours_Tue__c,Billable_Hours_Wed__c,Billable_Hours_Thu__c,'+
                            'Billable_Hours_Fri__c,Billable_Hours_Sat__c,  Billable_Hours_Sun__c,'+
                            'UnBillable_Hours_Mon__c,UnBillable_Hours_Tue__c,UnBillable_Hours_Wed__c,UnBillable_Hours_Thu__c,'+
                            'UnBillable_Hours_Fri__c,UnBillable_Hours_Sat__c, UnBillable_Hours_Sun__c, '+
                            'Assignment__c,Assignment__r.RecordType.Name,Assignment__r.Resource__r.User__r.Name,'+
                            'Assignment__r.Role__c,Assignment__r.Project__r.Contact_Person__r.Email, '+
                            'Assignment__r.Project__r.Contact_Person__c,Assignment__r.EP_Shadow_Resource__r.User__r.Name,'+
                            'Assignment__r.Project__c,Assignment__r.Project__r.Contact_Person__r.Single_Reminder__c,'+ 
                            'Monday_Work_Details__c,Tuesday_Work_Details__c,Wednesday_Work_Details__c,Thursday_Work_Details__c,'+
                            'Friday_Work_Details__c,    Saturday_Work_Details__c,   Sunday_Work_Details__c,'+ 
                            'Assignment__r.Resource__c,Assignment__r.EP_Shadow_Resource__c,'+
                            'Assignment__r.Is_Primary_Resource_Contributing__c,Assignment__r.Project__r.Contact_Person__r.Name,'+
                            'X1st_Approver__c, X2nd_Approver__c, X3rd_Approver__c, X4th_Approver__c, X5th_Approver__c,'+
                            '(SELECT Id, Name FROM attachments)';
        String formString = ' FROM Time_Card__c';
        String whereString = ' WHERE Project_Manager__c =: loggedInUserId AND Assignment__r.RecordType.Name =: primaryString';
        system.debug('------selectString----------'+selectString);
        system.debug('-------formString---------'+formString);
        system.debug('-------whereString---------'+whereString);
        if(projectCriteraString != null && resourceCriteraString == null && filterStartDate == null && filterEndDate == null){
            queryString = selectString + formString + whereString + ' AND Assignment__r.Project__r.Name =: projectCriteraString';
            system.debug('------queryString----------'+queryString);                
        }
        if(resourceCriteraString != null && filterStartDate == null && filterEndDate == null && projectCriteraString == null ){
            queryString = selectString + formString + whereString + ' AND Assignment__r.Resource__r.User__r.Name =: resourceCriteraString';
            system.debug('------queryString--1--------'+queryString); 
        }
        if(filterStartDate != null && filterEndDate != null && resourceCriteraString == null && projectCriteraString == null){
            queryString = selectString + formString + whereString + ' AND Start_Date__c >=: filterStartDate AND EP_End_Date__c <=: filterEndDate';
            system.debug('------queryString--2--------'+queryString); 
        }
        if(projectCriteraString != null && resourceCriteraString != null && filterStartDate != null && filterEndDate != null){
            queryString = selectString + formString + whereString + ' AND Assignment__r.Project__r.Name =: projectCriteraString AND Assignment__r.Resource__r.User__r.Name =: resourceCriteraString AND Start_Date__c >=: filterStartDate AND EP_End_Date__c <=: filterEndDate';
            system.debug('------queryString--3--------'+queryString); 
        }
        if(projectCriteraString != null && resourceCriteraString == null && filterStartDate != null && filterEndDate != null){
            queryString = selectString + formString + whereString + ' AND Assignment__r.Project__r.Name =: projectCriteraString AND Start_Date__c >=: filterStartDate AND EP_End_Date__c <=: filterEndDate';
            system.debug('------queryString--4--------'+queryString); 
        }
        if(projectCriteraString != null && resourceCriteraString != null && filterStartDate == null && filterEndDate == null){
            queryString = selectString + formString + whereString + ' AND Assignment__r.Project__r.Name =: projectCriteraString AND Assignment__r.Resource__r.User__r.Name =: resourceCriteraString';
            system.debug('------queryString--5--------'+queryString); 
        }
        if(projectCriteraString == null && resourceCriteraString != null && filterStartDate != null && filterEndDate != null){
            queryString = selectString + formString + whereString + ' AND Assignment__r.Resource__r.User__r.Name =: resourceCriteraString AND Start_Date__c >=: filterStartDate AND EP_End_Date__c <=: filterEndDate';
            system.debug('------queryString--6--------'+queryString); 
        }
        system.debug('--1----queryString--------'+queryString);
        if(queryString == null){
            timeCardWrapperForApprovalsList = new List < pendingapprovalTimeCardsWrapper > ();
            timeCardWrapperForApprovalsList = fetchTimeCardsForApproval();
            if(timeCardWrapperForApprovalsList.size() == 0){
                errorMessage = 'No Pending Timesheets for the selected filter';
            }
        }
        if(queryString != null){
            system.debug('----relatedTimeCardAwaitingSubmission---1-----'+relatedTimeCardAwaitingSubmission);
            system.debug('----timeCardWrapperForApprovalsList-----1---'+timeCardWrapperForApprovalsList);
            system.debug('----relatedTimeCardAwaitingClientApproval---1-----'+relatedTimeCardAwaitingClientApproval);

            system.debug('----relatedTimeCardAwaitingSubmission---size--1---'+relatedTimeCardAwaitingSubmission.size());
            system.debug('----timeCardWrapperForApprovalsList---size--1---'+timeCardWrapperForApprovalsList.size());
            system.debug('----relatedTimeCardAwaitingClientApproval--size---1---'+relatedTimeCardAwaitingClientApproval.size());
            timeCardWrapperForApprovalsList = new List < pendingapprovalTimeCardsWrapper > ();
            relatedTimeCardAwaitingSubmission = new List<Time_Card__c>();
            relatedTimeCardAwaitingClientApproval = new List<Time_Card__c>();
            for (Time_Card__c timeCardSubmitted : database.query(queryString)) {
                system.debug('--------timeCardSubmitted 0-----------'+timeCardSubmitted);
                if(timeCardSubmitted.Status__c == 'Assigned' || 
                    timeCardSubmitted.Status__c == 'Rejected' || 
                    timeCardSubmitted.Status__c == 'Draft'){
                    system.debug('--------timeCardSubmitted 1-----------'+timeCardSubmitted);
                    if(relatedTimeCardAwaitingSubmission.size() == 0){
                        system.debug('--------timeCardSubmitted 1-2-----------'+timeCardSubmitted);
                        relatedTimeCardAwaitingSubmission = new List<Time_Card__c>();
                        relatedTimeCardAwaitingSubmission.add(timeCardSubmitted);   
                    }
                    else{
                        system.debug('--------timeCardSubmitted 1-3-----------'+timeCardSubmitted);
                        relatedTimeCardAwaitingSubmission.add(timeCardSubmitted);
                    }
                }
                if(timeCardSubmitted.Status__c == 'Submitted'){
                    pendingapprovalTimeCardsWrapper approvalWrapper = new pendingapprovalTimeCardsWrapper();
                    approvalWrapper.clientEmailId = timeCardSubmitted.Assignment__r.Project__r.Contact_Person__r.Email;
                    approvalWrapper.clientId = timeCardSubmitted.Assignment__r.Project__r.Contact_Person__c;
                    approvalWrapper.timeCardMap.put(timeCardSubmitted.Id, timeCardSubmitted);
                    system.debug('--------approvalWrapper.relatedTimeCard 2-----------'+approvalWrapper.relatedTimeCard);
                    system.debug('--------approvalWrapper.relatedTimeCard 2-1----------'+timeCardSubmitted.Project_Name__c);
                    approvalWrapper.relatedTimeCard = timeCardSubmitted;
                    timeCardWrapperForApprovalsList.add(approvalWrapper);
                }
                if(timeCardSubmitted.Status__c == 'Approved' &&
                    timeCardSubmitted.Status__c != 'Client Approved' &&
                    timeCardSubmitted.Client_Details__c == True){
                    if(relatedTimeCardAwaitingClientApproval.size() == 0){
                        relatedTimeCardAwaitingClientApproval = new List<Time_Card__c>();
                        relatedTimeCardAwaitingClientApproval.add(timeCardSubmitted);   
                    }else{
                        relatedTimeCardAwaitingClientApproval.add(timeCardSubmitted);
                    }
                }
            }
            system.debug('----relatedTimeCardAwaitingSubmission--------'+relatedTimeCardAwaitingSubmission);
            system.debug('----timeCardWrapperForApprovalsList--------'+timeCardWrapperForApprovalsList);
            system.debug('----relatedTimeCardAwaitingClientApproval--------'+relatedTimeCardAwaitingClientApproval);

            system.debug('----relatedTimeCardAwaitingSubmission---size-----'+relatedTimeCardAwaitingSubmission.size());
            system.debug('----timeCardWrapperForApprovalsList---size-----'+timeCardWrapperForApprovalsList.size());
            system.debug('----relatedTimeCardAwaitingClientApproval--size------'+relatedTimeCardAwaitingClientApproval.size());
            if(relatedTimeCardAwaitingSubmission.size() == 0){
                errorMessage = 'No Pending Timesheets for submission';
            }if(timeCardWrapperForApprovalsList.size() == 0){
                errorMessage1 = 'No Pending Timesheets for Approvals';
            }if(relatedTimeCardAwaitingClientApproval.size() == 0){
                errorMessage2 = 'No Pending Timesheets for Approvals';
            }
            else if(timeCardWrapperForApprovalsList.size() == 0 &&
                relatedTimeCardAwaitingSubmission.size() == 0 && 
                relatedTimeCardAwaitingClientApproval.size() == 0){
                errorMessage = 'No Pending Timesheets for the selected filter';
                errorMessage1 = 'No Pending Timesheets for the selected filter';
                errorMessage2 = 'No Pending Timesheets for the selected filter';
            }else{
                errorMessage = null;
                errorMessage1 = null;
                errorMessage2 = null;
            }

            
        }
        return null;
    }
}