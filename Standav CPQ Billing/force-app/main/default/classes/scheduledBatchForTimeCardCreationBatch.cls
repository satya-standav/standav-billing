global class scheduledBatchForTimeCardCreationBatch implements Schedulable {
   global void execute(SchedulableContext sc) {
      TimeCardCreationBatchController b = new TimeCardCreationBatchController(); 
      database.executebatch(b);
   }
}