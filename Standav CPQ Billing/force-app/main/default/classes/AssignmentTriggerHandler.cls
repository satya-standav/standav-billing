Public class AssignmentTriggerHandler{
    public static void beforeInsertHandler(List<Assignment__c> relatedAssignmentList){
        if(Trigger.isInsert && Trigger.isBefore){
            AssignmentTriggerHelper.beforeInsertUpdateMethod(relatedAssignmentList, null);
            
        }
    }
    public static void afterInsertHandler(List<Assignment__c> relatedAssignmentList){
        if(Trigger.isInsert && Trigger.isAfter){
            AssignmentTriggerHelper.afterInsertMethod(relatedAssignmentList, null);
            AssignmentTriggerHelper.linkRelatedAllocation(relatedAssignmentList);
        }
    }
    public static void beforeUpdatHandler(List<Assignment__c> relatedAssignmentList, Map<Id, Assignment__c> oldAssignmentDataMap){
        if(Trigger.isUpdate && Trigger.isBefore){
            AssignmentTriggerHelper.beforeInsertUpdateMethod(relatedAssignmentList, oldAssignmentDataMap);
        }
    }
    
   /* public static void afterUpdateHandler(List<Assignment__c> relatedAssignmentList, Map<Id, Assignment__c> oldAssignmentDataMap){
        if(Trigger.isUpdate && Trigger.isAfter){
            //AssignmentTriggerHelper.afterInsertUpdatewMethod(relatedAssignmentList, oldAssignmentDataMap);
        }
    }
    public static void beforeDeleteHandler(){
        if(Trigger.isDelete && Trigger.isBefore){
        
        }
    }
    public static void afterDeleteHandler(){
        if(Trigger.isDelete && Trigger.isAfter){
        
        }
    }
    public static void afterUndeleteHandler(){
        if(Trigger.isUndelete && Trigger.isAfter){
        
        }*/
    
}