global class EmailtoContacts implements Messaging.InboundEmailHandler{
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.inboundEmail email, Messaging.InboundEnvelope envelope)
    {   
        
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();        
        Messaging.InboundEmail.TextAttachment[] tAttachments = email.textAttachments;        
        Messaging.InboundEmail.BinaryAttachment[] bAttachments = email.BinaryAttachments;
        
        String csvbody='';
   
        if(bAttachments !=null){
            for(Messaging.InboundEmail.BinaryAttachment btt : bAttachments){
                System.debug( 'this looks like a binary attachment'); 
                if(btt.filename.endsWith('.csv')){
                    csvbody = btt.body.toString();
                    
                }
                     system.debug('csvbody----'+csvbody);
		}
        }
        else
        {
            if(tAttachments !=null){
                
                for(Messaging.InboundEmail.TextAttachment ttt : tAttachments){
                    System.debug('this looks like a text attachment');
                    
                    if(ttt.filename.endsWith('.csv')){ csvbody = ttt.body; 
                                                     }
                    
                }
            } 
        }
            return result;
           
    }
                global void noReport()
                {
                    Date CurrentDate = system.today();
                    CurrentDate = CurrentDate.toStartofWeek();
                    List<String> EmailList = new List<String>();
                    // List<Assignment__c > acclist = [Select id,name,Project__r.Name,Resource__r.Name from Assignment__c where Status__c = 'Approved'];
                    List<Time_Card__c> TCList = [Select id,name,Assignment__r.Project__r.Contact_Person__r.Email,Assignment__r.Name,Assignment__r.Project__r.Name from Time_Card__c where Status__c = 'Approved'];
                    for(Time_Card__c tc : TCList)
                    {
                        if(tc.Assignment__r.Project__r.Contact_Person__r.Email!=null)
                            EmailList.add(tc.Assignment__r.Project__r.Contact_Person__r.Email); 
                    }
                    
                    system.debug('TCList----'+TCList);
                    string header = 'Record Id, Name , Assignment , Project \n';
                    string finalstr = header ;
                    for(Time_Card__c a: TCList)
                    {
                        
                        string recordString = a.id+','+a.Name+','+a.Assignment__r.Project__r.Name+','+a.Assignment__r.Name+'\n';
                        
                        finalstr = finalstr +recordString;
                        
                    }
                    system.debug('Email list ----'+EmailList);
                    Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
                    blob csvBlob = Blob.valueOf(finalstr);
                    string csvname= 'Time Card.csv';
                    csvAttc.setFileName(csvname);
                    csvAttc.setBody(csvBlob);
                    Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
                    String[] toAddresses = new list<string> {};
                        toAddresses.addAll(EmailList);
                    String subject ='Account CSV';
                    email.setSubject(subject);
                    email.setToAddresses( toAddresses );
                    email.setPlainTextBody('Account CSV ');
                    email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
                    Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
                }
            }