@isTest
private class PendingTimeCardsPageControllerTest{
    private static testMethod void test() {

User user = new User();
            user.LastName = 'Test User';
            user.Alias = 'tu';
            user.Email = 'test@gmail.com';
            user.Username = 'test.dev@stand.com';
            user.CommunityNickname = 'test.t';
            //user.UserRoleId = '00ei0000000rTfp';
            user.User_Time_Zone__c = 'Pst';
            user.ProfileId = '00e54000000HSHG';           
            user.TimeZoneSidKey    = 'America/Denver';
            user.LocaleSidKey      = 'en_US';
            user.EmailEncodingKey  = 'UTF-8';
            user.LanguageLocaleKey = 'en_US';
            insert user;
        
                	Account acc = new Account();
        	acc.Name = 'Test Account';
        	insert acc;
         
      Contact con = new Contact();
        	con.FirstName = 'test';
        	con.LastName = 'contact';
        	insert con;
     
     
            Project__c project = new Project__c();
            project.Name = 'Test Project';
            project.EP_Project_Manager__c = user.Id;
            project.Contact_Person__c = con.Id;
            project.Start_Date__c = Date.newInstance(2016, 12, 9);
        	project.Account__c = acc.id;
        	project.Number__c = 2;
            insert project;        
            update project;
      
       Resource__c resource = new Resource__c();
            resource.User__c = user.id;
            insert resource;        
            update resource; 
    
        
            Assignment__c assignment = new Assignment__c();
            assignment.Project__c = project.id;
            //assignment.RecordType.Name = 'Primary';
            assignment.Resource__c = resource.id;
            assignment.Status__c = 'Approved';
            assignment.Start_Date__c = Date.newInstance(2016, 12, 9);
            assignment.End_Date__c = Date.newInstance(2018, 12, 9);
            assignment.Role__c = 'Developer';
            assignment.Billing_Type__c = 'Billable';
            assignment.Assignment_Type__c = 'Long Term';   
            assignment.Budgeted_Hours__c=60;
            insert assignment;        
        
                 Time_Card__c card = new Time_Card__c();
        	card.Assignment__c = assignment.id;
            card.Billable_Hours_Fri__c = 4;
            card.Billable_Hours_Mon__c =4;
            card.Billable_Hours_Sat__c = 0;
            card.Billable_Hours_Sun__c = 0;
            card.Billable_Hours_Thu__c = 4;
            card.Billable_Hours_Tue__c = 4;
            card.Billable_Hours_Wed__c =4;
            card.Status__c = 'Approved';            
            insert card;
 
        System.runAs(user){
          system.Test.startTest();
          PageReference myVfPage = Page.SubmittedTimeCardsPage;
          myVfPage.getParameters().put('id',card.id);
          Test.setCurrentPage(myVfPage); 
          String id = ApexPages.currentPage().getParameters().get('id');
        
          List<PendingTimeCardsPageController.timeCardWrapper> w;
          PendingTimeCardsPageController.timeCardWrapper testWrap=new PendingTimeCardsPageController.timeCardWrapper();
          PendingTimeCardsPageController testPlan1 = new PendingTimeCardsPageController();
            testPlan1.fetchTimeCards();
          
          /*List<SubmittedTimeSheet.timeCardWrapper> newTimeCardWrpper;
          SubmittedTimeSheet.timeCardWrapper testWrap1=new SubmittedTimeSheet.timeCardWrapper();       
          SubmittedTimeSheet f1= new SubmittedTimeSheet();
          f1.fetchTimeCards1(); 
          f1.fetchTimeCards();
          //f1.sortTimeCard();
          testWrap1.relatedAssignment= assignment;
          //newTimeCardWrpper.add(testWrap1);*/
          
      system.Test.stopTest();

  }
  
    }
private static testMethod void test1() {
User user = new User();
            user.LastName = 'Test User';
            user.Alias = 'tu';
            user.Email = 'test@gmail.com';
            user.Username = 'test.dev@stand.com';
            user.CommunityNickname = 'test.t';
            //user.UserRoleId = '00ei0000000rTfp';
            user.User_Time_Zone__c = 'Pst';
            user.ProfileId = '00e54000000HSHG';           
            user.TimeZoneSidKey    = 'America/Denver';
            user.LocaleSidKey      = 'en_US';
            user.EmailEncodingKey  = 'UTF-8';
            user.LanguageLocaleKey = 'en_US';
            insert user;
        
                	Account acc = new Account();
        	acc.Name = 'Test Account';
        	insert acc;
         
      Contact con = new Contact();
        	con.FirstName = 'test';
        	con.LastName = 'contact';
        	insert con;
     
     
            Project__c project = new Project__c();
            project.Name = 'Test Project';
            project.EP_Project_Manager__c = user.Id;
            project.Contact_Person__c = con.Id;
            project.Start_Date__c = Date.newInstance(2016, 12, 9);
        	project.Account__c = acc.id;
        	project.Number__c = 2;
            insert project;        
            update project;
      
       Resource__c resource = new Resource__c();
            resource.User__c = user.id;
            insert resource;        
            update resource; 
    
        
            Assignment__c assignment = new Assignment__c();
            assignment.Project__c = project.id;
            //assignment.RecordType.Name = 'Primary';
            assignment.Resource__c = resource.id;
            assignment.Status__c = 'Approved';
            assignment.Start_Date__c = Date.newInstance(2016, 12, 9);
            assignment.End_Date__c = Date.newInstance(2018, 12, 9);
            assignment.Role__c = 'Developer';
            assignment.Billing_Type__c = 'Billable';
            assignment.Assignment_Type__c = 'Long Term';   
            assignment.RecordType = new RecordType();
            assignment.RecordType.Name = 'Primary';
    assignment.Budgeted_Hours__c=60;
            insert assignment;        
        
            Time_Card__c card = new Time_Card__c();
        	card.Assignment__c = assignment.id;
            card.Billable_Hours_Fri__c = 4;
            card.Billable_Hours_Mon__c =4;
            card.Billable_Hours_Sat__c = 0;
            card.Billable_Hours_Sun__c = 0;
            card.Billable_Hours_Thu__c = 4;
            card.Billable_Hours_Tue__c = 4;
            card.Billable_Hours_Wed__c =4;
            card.Status__c = 'Submitted';            
            insert card;
            update card;
            Time_Card__c card1 = new Time_Card__c();
        	card1.Assignment__c = assignment.id;
            card1.Billable_Hours_Fri__c = 4;
            card1.Billable_Hours_Mon__c =4;
            card1.Billable_Hours_Sat__c = 0;
            card1.Billable_Hours_Sun__c = 0;
            card1.Billable_Hours_Thu__c = 4;
            card1.Billable_Hours_Tue__c = 4;
            card1.Billable_Hours_Wed__c =4;
            card1.Status__c = 'Rejected';            
            insert card1;
            update card1;
            Time_Card__c card2 = new Time_Card__c();
        	card2.Assignment__c = assignment.id;
            card2.Billable_Hours_Fri__c = 4;
            card2.Billable_Hours_Mon__c =4;
            card2.Billable_Hours_Sat__c = 0;
            card2.Billable_Hours_Sun__c = 0;
            card2.Billable_Hours_Thu__c = 4;
            card2.Billable_Hours_Tue__c = 4;
            card2.Billable_Hours_Wed__c =4;
            card2.Status__c = 'Draft';            
            insert card2;
            update card2;
    
       System.runAs(user){
          system.Test.startTest();
          PageReference myVfPage = Page.SubmittedTimeCardsPage;
          myVfPage.getParameters().put('id',card.id);
          Test.setCurrentPage(myVfPage); 
          String id = ApexPages.currentPage().getParameters().get('id');
          
          //List<SubmittedTimeSheet.timeCardWrapper> newTimeCardWrpper;
          //SubmittedTimeSheet.timeCardWrapper testWrap1=new SubmittedTimeSheet.timeCardWrapper();       
          SubmittedTimeSheet f1= new SubmittedTimeSheet();
          //f1.fetchTimeCards1(); 
          //f1.fetchTimeCards();
          //f1.sortTimeCard();
          //testWrap1.relatedAssignment= assignment;
          //newTimeCardWrpper.add(testWrap1);*/
          
      system.Test.stopTest();

  }
  
    }
private static testMethod void test2() {
User user = new User();
            user.LastName = 'Test User';
            user.Alias = 'tu';
            user.Email = 'test@gmail.com';
            user.Username = 'test.dev@stand.com';
            user.CommunityNickname = 'test.t';
            //user.UserRoleId = '00ei0000000rTfp';
            user.User_Time_Zone__c = 'Pst';
            user.ProfileId = '00e54000000HSHG';           
            user.TimeZoneSidKey    = 'America/Denver';
            user.LocaleSidKey      = 'en_US';
            user.EmailEncodingKey  = 'UTF-8';
            user.LanguageLocaleKey = 'en_US';
            insert user;
        
                	Account acc = new Account();
        	acc.Name = 'Test Account';
        	insert acc;
         
      Contact con = new Contact();
        	con.FirstName = 'test';
        	con.LastName = 'contact';
        	insert con;
     
     
            Project__c project = new Project__c();
            project.Name = 'Test Project';
            project.EP_Project_Manager__c = user.Id;
            project.Contact_Person__c = con.Id;
            project.Start_Date__c = Date.newInstance(2016, 12, 9);
        	project.Account__c = acc.id;
        	project.Number__c = 2;
            insert project;        
            update project;
      
       Resource__c resource = new Resource__c();
            resource.User__c = user.id;
            insert resource;        
            update resource; 
           Resource__c resource1 = new Resource__c();
            resource.User__c = '0051Q00000GoFQm';
            insert resource1;        
            update resource1;
        
            Assignment__c assignment = new Assignment__c();
            assignment.Project__c = project.id;
            //assignment.RecordType.Name = 'Primary';
            assignment.Resource__c = resource.id;
            assignment.Status__c = 'Approved';
            assignment.Start_Date__c = Date.newInstance(2016, 12, 9);
            assignment.End_Date__c = Date.newInstance(2018, 12, 9);
            assignment.Role__c = 'Developer';
            assignment.Billing_Type__c = 'Billable';
            assignment.Assignment_Type__c = 'Long Term';   
                        assignment.RecordType = new RecordType();
            assignment.RecordType.Name = 'Primary';
    assignment.Budgeted_Hours__c=60;
            insert assignment;
        
       Assignment__c assignment1 = new Assignment__c();
            assignment1.Project__c = project.id;
            assignment1.Resource__c = resource1.id;
            assignment1.Status__c = 'Approved';
            assignment1.Start_Date__c = Date.newInstance(2016, 12, 9);
            assignment1.End_Date__c = Date.newInstance(2018, 12, 9);
            assignment1.Role__c = 'Developer';
            assignment1.Billing_Type__c = 'Billable';
            assignment1.Assignment_Type__c = 'Long Term';
            assignment1.RecordType = new RecordType();
            assignment1.RecordType.Name = 'Shadow';
    assignment1.Budgeted_Hours__c=60;
            //insert assignment1;     
        
            Time_Card__c card = new Time_Card__c();
        	card.Assignment__c = assignment.id;
            card.Billable_Hours_Fri__c = 4;
            card.Billable_Hours_Mon__c =4;
            card.Billable_Hours_Sat__c = 0;
            card.Billable_Hours_Sun__c = 0;
            card.Billable_Hours_Thu__c = 4;
            card.Billable_Hours_Tue__c = 4;
            card.Billable_Hours_Wed__c =4;
            card.Status__c = 'Submitted';            
            insert card;
            update card;
            Time_Card__c card1 = new Time_Card__c();
        	card1.Assignment__c = assignment.id;
            card1.Billable_Hours_Fri__c = 4;
            card1.Billable_Hours_Mon__c =4;
            card1.Billable_Hours_Sat__c = 0;
            card1.Billable_Hours_Sun__c = 0;
            card1.Billable_Hours_Thu__c = 4;
            card1.Billable_Hours_Tue__c = 4;
            card1.Billable_Hours_Wed__c =4;
            card1.Status__c = 'Rejected';            
            insert card1;
            update card1;
            Time_Card__c card2 = new Time_Card__c();
        	card2.Assignment__c = assignment.id;
            card2.Billable_Hours_Fri__c = 4;
            card2.Billable_Hours_Mon__c =4;
            card2.Billable_Hours_Sat__c = 0;
            card2.Billable_Hours_Sun__c = 0;
            card2.Billable_Hours_Thu__c = 4;
            card2.Billable_Hours_Tue__c = 4;
            card2.Billable_Hours_Wed__c =4;
            card2.Status__c = 'Draft';            
            insert card2;
            update card2;

       System.runAs(user){
          system.Test.startTest();
          PageReference myVfPage = Page.SubmittedTimeCardsPage;
          myVfPage.getParameters().put('id',card.id);
          Test.setCurrentPage(myVfPage); 
          String id = ApexPages.currentPage().getParameters().get('id');
          
          //List<SubmittedTimeSheet.timeCardWrapper> newTimeCardWrpper;
          //SubmittedTimeSheet.timeCardWrapper testWrap1=new SubmittedTimeSheet.timeCardWrapper();       
          SubmittedTimeSheet f1= new SubmittedTimeSheet();
          //f1.timeCardToUpdate= card.id;
          //f1.fetchTimeCards1(); 
          //f1.fetchTimeCards();
          Time_Card__c timeCardToUpdate = new Time_Card__c(Id = card.id,
                                        Status__c = 'Draft', Send_Reminder_on_Recall__c = True);
          update timeCardToUpdate;
          //f1.sortTimeCard();
          //testWrap1.relatedAssignment= assignment;
          //newTimeCardWrpper.add(testWrap1);*/
          
      system.Test.stopTest();

  }
  
    }
    }