/**
*  @Class Name: TimeCardController
*  @Description: Handles all functionality required for TimeSheetEntryPage visualforce page.
*  @Company: Standav
*  @CreatedDate: 11/15/2019
*  Change Log
*  -----------------------------------------------------------
*  Developer: Shruti MP           
   Modification Date : 11/15/2019          
   Comments
*  -----------------------------------------------------------
*
*/
public class TimeCardController{

    public String currentResourceName { get; set; }

    //Variable Declaration Section
     public Date selectedDate;
     public Date startDate                                            {get;set;}
     public Date endDate                                              {get;set;}
     public List<Date> weekDatesList                                  {get;set;}
     public Assignment__c assignment                                  {get;set;}
     public List<assignmentTimeCardWrap> assignmentTimeCardWrapList   {get;set;}
     public String currentUserName {get; set;}
     
    /*Start Constructor */
    public TimeCardController(){
        assignment = new Assignment__c();
        List<Resource__c> resourcesList = [SELECT Id, User__c, User__r.Name  
                        FROM Resource__c 
                        WHERE User__c =: UserInfo.getUserId() AND Active__c = true]; 
        System.debug('-------resourcesList ------------'+resourcesList );
        
        if(resourcesList.size() > 0){
            currentUserName = resourcesList[0].User__r.Name;
            assignment.Resource__c = resourcesList[0].Id; 
            system.debug('---------assignment.Resource__c-------'+assignment.Resource__c);
            selectedDate = Date.today();
            fetchTimeCards();
        }
    } 
    
    /*
        Method Name         : fetchTimeCards
        param               : N/A
        Description         : method to fetch the timecard based on resource and selected week
    */
    public void fetchTimeCards(){
        
        assignmentTimeCardWrapList = new List<assignmentTimeCardWrap>();
        weekDatesList = new list<Date>();
        Set<Id> assignmentsIdSet = new Set<Id>();
        Map<Id, Time_Card__c> timeCardWithAsignment = new Map<Id, Time_Card__c>();
        assignmentTimeCardWrapList = new List<assignmentTimeCardWrap>();
        startDate = selectedDate.toStartofWeek();
        endDate = startDate.addDays(6);
        system.debug('---------startDate -------------'+startDate );
        system.debug('---------endDate -------------'+endDate );
        List<Time_Card__c> allTimeCardsList = new List<Time_Card__c>();
        Date dtvaries = startDate;
        while(dtvaries <= endDate)
        {
            weekDatesList.add(dtvaries);
            dtvaries = dtvaries.addDays(1);
        } 
        List<Time_Card__c> relatedTimeCardsList = [SELECT Id, Name, Assignment__r.EP_Shadow_Resource__c, Assignment__r.Resource__c,
                                            Type__c, EP_End_Date__c, Billable_Hours_Fri__c, EP_Notes__c, EP_Approval_Date__c, 
                                            EP_Total_Number_of_Billable_Hours__c, EP_Total_Number_of_Unbillable_Hours__c, 
                                            EP_Rejected_Date__c, Submitted_On__c, Project_Manager__c, Billable_Hours_Mon__c, 
                                            Billable_Hours_Sat__c, Assignment__c, Status__c, Start_Date__c, Billable_Hours_Sun__c, 
                                            Billable_Hours_Thu__c, Billable_Hours_Tue__c, Billable_Hours_Wed__c, UnBillable_Hours_Fri__c, 
                                            UnBillable_Hours_Mon__c, UnBillable_Hours_Sat__c, UnBillable_Hours_Sun__c, UnBillable_Hours_Thu__c, 
                                            UnBillable_Hours_Tue__c, UnBillable_Hours_Wed__c,Assignment__r.Project__c 
                                            FROM Time_Card__c
                                            WHERE Start_Date__c =: startDate AND  EP_End_Date__c =: endDate AND Assignment__c != null
                                            AND (Assignment__r.EP_Shadow_Resource__c =: assignment.Resource__c OR 
                                            Assignment__r.Resource__c =: assignment.Resource__c)];
        System.debug('--------timeCardsList -----------'+relatedTimeCardsList );
        System.debug('--------timeCardsList -Size----------'+relatedTimeCardsList.size() );
        for(Time_Card__c timeCard : relatedTimeCardsList){
            if(timeCard.Assignment__c != null){
                assignmentsIdSet.add(timeCard.Assignment__c);
                timeCardWithAsignment.put(timeCard.Assignment__c, timeCard);
            }
        }
       
        List<Assignment__c> assignmentsList = [SELECT Id, Name, RecordTypeId, Billing_Rate__c, Approval_Type__c, Start_Date__c,End_Date__c, 
                                               Billing_Type__c, EP_Shadow_Resource__c, Is_Primary_Resource_Contributing__c, RecordType.Name,
                                               Resource_Name__c, Role__c, Resource__c, Status__c, Assignment_Type__c, Project__c, Project__r.Name
                                               FROM Assignment__c
                                               WHERE (Id IN: assignmentsIdSet OR (Start_Date__c <=: startDate AND End_Date__c >=: endDate))
                                               AND (EP_Shadow_Resource__c =: assignment.Resource__c OR 
                                               Resource__c =: assignment.Resource__c) ];
        System.debug('--------assignmentsList -----------'+assignmentsList );
        System.debug('--------assignmentsList -size----------'+assignmentsList.size() );
        for(Assignment__c assignment : assignmentsList){
            system.debug('------assignment.Start_Date__c -----------'+assignment.Start_Date__c ); 
            system.debug('------assignment.End_Date__c -----------'+assignment.End_Date__c );
            System.debug('----------assignment.RecordType.Name-------------'+assignment.RecordType.Name);
            assignmentTimeCardWrap assignmentTimeCardWrapObj = new assignmentTimeCardWrap();
            
            assignmentTimeCardWrapObj.timeCardAssginment.Assignment__c = assignment.Id;
            assignmentTimeCardWrapObj.projectName = assignment.Project__r.Name;
            if(timeCardWithAsignment.get(assignment.Id) != null)
            assignmentTimeCardWrapObj.assignmentWithTimeCardMap.put(assignment.Id, timeCardWithAsignment.get(assignment.Id));
            assignmentTimeCardWrapObj.resourceType = assignment.RecordType.Name;
            /*if(assignment.RecordType.Name == 'Shadow' && assignment.EP_Shadow_Resource__c != null && assignment.Resource_Name__c != null){
                assignmentTimeCardWrapObj.shadowWithPrimaryResourceMap.put(assignment.EP_Shadow_Resource__c, assignment.Resource_Name__c);
            }*/
            system.debug('------------assignmentTimeCardWrapObj.shadowWithPrimaryResourceMap---------------'+assignmentTimeCardWrapObj.shadowWithPrimaryResourceMap);
                for(Date dtWeek: weekDatesList){
                    system.debug('============dtWeek==='+dtWeek);
                    Datetime dt = DateTime.newInstance(dtWeek, Time.newInstance(0, 0, 0, 0));
                    system.debug('-------myDateTime------'+dt);
                    String dayOfWeek=dt.format('E');
                    System.debug('Day : ' + dayOfWeek);
                    System.debug('------------Primary-------------'+assignment.RecordType.Name);
                        if(timeCardWithAsignment.get(assignment.Id) != null){
                            assignmentTimeCardWrapObj.timeCardAssginment = timeCardWithAsignment.get(assignment.Id);
                            assignmentTimeCardWrapObj.timeCardAssginment.Id = timeCardWithAsignment.get(assignment.Id).Id;
                            assignmentTimeCardWrapObj.timeCardAssginment.Type__c = timeCardWithAsignment.get(assignment.Id).Type__c != null ?
                                timeCardWithAsignment.get(assignment.Id).Type__c : '';
                            if(dayOfWeek == 'Sun'){
                                timeCardWithAsignment.get(assignment.Id).Billable_Hours_Sun__c = timeCardWithAsignment.get(assignment.Id).Billable_Hours_Sun__c != null ?
                                    timeCardWithAsignment.get(assignment.Id).Billable_Hours_Sun__c : 0;
                                timeCardWithAsignment.get(assignment.Id).UnBillable_Hours_Sun__c = timeCardWithAsignment.get(assignment.Id).UnBillable_Hours_Sun__c != null ?
                                    timeCardWithAsignment.get(assignment.Id).UnBillable_Hours_Sun__c : 0;
                                assignmentTimeCardWrapObj.shadowTimeCardWithDateMap.put(dtWeek, timeCardWithAsignment.get(assignment.Id).UnBillable_Hours_Sun__c);
                                assignmentTimeCardWrapObj.primaryTimeCardWithDateMap.put(dtWeek, timeCardWithAsignment.get(assignment.Id).Billable_Hours_Sun__c);
                            }
                            else if(dayOfWeek == 'Mon'){
                                timeCardWithAsignment.get(assignment.Id).Billable_Hours_Mon__c = timeCardWithAsignment.get(assignment.Id).Billable_Hours_Mon__c != null ?
                                    timeCardWithAsignment.get(assignment.Id).Billable_Hours_Mon__c : 0;
                                timeCardWithAsignment.get(assignment.Id).UnBillable_Hours_Mon__c = timeCardWithAsignment.get(assignment.Id).UnBillable_Hours_Mon__c != null ?
                                    timeCardWithAsignment.get(assignment.Id).UnBillable_Hours_Mon__c : 0;
                                assignmentTimeCardWrapObj.shadowTimeCardWithDateMap.put(dtWeek, timeCardWithAsignment.get(assignment.Id).UnBillable_Hours_Mon__c);
                                assignmentTimeCardWrapObj.primaryTimeCardWithDateMap.put(dtWeek, timeCardWithAsignment.get(assignment.Id).Billable_Hours_Mon__c);
                            }
                            else if(dayOfWeek == 'Tue'){
                                timeCardWithAsignment.get(assignment.Id).Billable_Hours_Tue__c = timeCardWithAsignment.get(assignment.Id).Billable_Hours_Tue__c != null ?
                                    timeCardWithAsignment.get(assignment.Id).Billable_Hours_Tue__c : 0;
                                assignmentTimeCardWrapObj.primaryTimeCardWithDateMap.put(dtWeek, timeCardWithAsignment.get(assignment.Id).Billable_Hours_Tue__c);
                                timeCardWithAsignment.get(assignment.Id).UnBillable_Hours_Tue__c = timeCardWithAsignment.get(assignment.Id).UnBillable_Hours_Tue__c != null ?
                                    timeCardWithAsignment.get(assignment.Id).UnBillable_Hours_Tue__c : 0;
                                assignmentTimeCardWrapObj.shadowTimeCardWithDateMap.put(dtWeek, timeCardWithAsignment.get(assignment.Id).UnBillable_Hours_Tue__c);
                            }
                            else if(dayOfWeek == 'Wed'){
                                timeCardWithAsignment.get(assignment.Id).Billable_Hours_Wed__c = timeCardWithAsignment.get(assignment.Id).Billable_Hours_Wed__c != null ?
                                    timeCardWithAsignment.get(assignment.Id).Billable_Hours_Wed__c : 0;
                                assignmentTimeCardWrapObj.primaryTimeCardWithDateMap.put(dtWeek, timeCardWithAsignment.get(assignment.Id).Billable_Hours_Wed__c);
                                timeCardWithAsignment.get(assignment.Id).UnBillable_Hours_Wed__c = timeCardWithAsignment.get(assignment.Id).UnBillable_Hours_Wed__c != null ?
                                    timeCardWithAsignment.get(assignment.Id).UnBillable_Hours_Wed__c : 0;
                                assignmentTimeCardWrapObj.shadowTimeCardWithDateMap.put(dtWeek, timeCardWithAsignment.get(assignment.Id).UnBillable_Hours_Wed__c);
                            }
                            else if(dayOfWeek == 'Thu'){
                                timeCardWithAsignment.get(assignment.Id).Billable_Hours_Thu__c = timeCardWithAsignment.get(assignment.Id).Billable_Hours_Thu__c != null ?
                                    timeCardWithAsignment.get(assignment.Id).Billable_Hours_Thu__c : 0;
                                assignmentTimeCardWrapObj.primaryTimeCardWithDateMap.put(dtWeek, timeCardWithAsignment.get(assignment.Id).Billable_Hours_Thu__c);
                                timeCardWithAsignment.get(assignment.Id).UnBillable_Hours_Thu__c = timeCardWithAsignment.get(assignment.Id).UnBillable_Hours_Thu__c != null ?
                                    timeCardWithAsignment.get(assignment.Id).UnBillable_Hours_Thu__c : 0;
                                assignmentTimeCardWrapObj.shadowTimeCardWithDateMap.put(dtWeek, timeCardWithAsignment.get(assignment.Id).UnBillable_Hours_Thu__c);
                            }
                            else if(dayOfWeek == 'Fri'){
                                timeCardWithAsignment.get(assignment.Id).Billable_Hours_Fri__c = timeCardWithAsignment.get(assignment.Id).Billable_Hours_Fri__c != null ?
                                    timeCardWithAsignment.get(assignment.Id).Billable_Hours_Fri__c : 0;
                                assignmentTimeCardWrapObj.primaryTimeCardWithDateMap.put(dtWeek, timeCardWithAsignment.get(assignment.Id).Billable_Hours_Fri__c);
                                timeCardWithAsignment.get(assignment.Id).UnBillable_Hours_Fri__c = timeCardWithAsignment.get(assignment.Id).UnBillable_Hours_Fri__c != null ?
                                    timeCardWithAsignment.get(assignment.Id).UnBillable_Hours_Fri__c : 0;
                                assignmentTimeCardWrapObj.shadowTimeCardWithDateMap.put(dtWeek, timeCardWithAsignment.get(assignment.Id).UnBillable_Hours_Fri__c);
                            }
                            else if(dayOfWeek == 'Sat'){
                                timeCardWithAsignment.get(assignment.Id).Billable_Hours_Sat__c = timeCardWithAsignment.get(assignment.Id).Billable_Hours_Sat__c != null ?
                                    timeCardWithAsignment.get(assignment.Id).Billable_Hours_Sat__c : 0;
                                system.debug('----------------'+timeCardWithAsignment.get(assignment.Id).Billable_Hours_Sat__c);
                                assignmentTimeCardWrapObj.primaryTimeCardWithDateMap.put(dtWeek, timeCardWithAsignment.get(assignment.Id).Billable_Hours_Sat__c);
                                timeCardWithAsignment.get(assignment.Id).UnBillable_Hours_Sat__c = timeCardWithAsignment.get(assignment.Id).UnBillable_Hours_Sat__c != null ?
                                    timeCardWithAsignment.get(assignment.Id).UnBillable_Hours_Sat__c : 0;
                                system.debug('----------------'+timeCardWithAsignment.get(assignment.Id).UnBillable_Hours_Sat__c);
                                assignmentTimeCardWrapObj.shadowTimeCardWithDateMap.put(dtWeek, timeCardWithAsignment.get(assignment.Id).UnBillable_Hours_Sat__c);
                            }
                            assignmentTimeCardWrapObj.dateWithTimeCardHoursMap.put(assignment.Id+'-'+assignment.RecordType.Name, timeCardWithAsignment.get(assignment.Id));
                        }    
                        else if(timeCardWithAsignment.get(assignment.Id) == null){
                            Time_Card__c newTimeCard = new Time_Card__c();
                            newTimeCard.Assignment__c = assignment.Id;
                            System.debug('------Sun 1---------'+newTimeCard.Billable_Hours_Sun__c);
                            System.debug('------Mon 1---------'+newTimeCard.Billable_Hours_Mon__c);
                            System.debug('------Tue 1---------'+newTimeCard.Billable_Hours_Tue__c);
                            System.debug('------Wed 1---------'+newTimeCard.Billable_Hours_Wed__c);
                            System.debug('------Thu 1---------'+newTimeCard.Billable_Hours_Thu__c);
                            System.debug('------FRi 1---------'+newTimeCard.Billable_Hours_Fri__c);
                            System.debug('------Sat 1---------'+newTimeCard.Billable_Hours_Sat__c);
                            newTimeCard.Billable_Hours_Sun__c = 0;
                            newTimeCard.Billable_Hours_Mon__c = 0;
                            newTimeCard.Billable_Hours_Tue__c = 0;
                            newTimeCard.Billable_Hours_Wed__c = 0; 
                            newTimeCard.Billable_Hours_Thu__c = 0;
                            newTimeCard.Billable_Hours_Fri__c = 0;
                            newTimeCard.Billable_Hours_Sat__c = 0;
                            newTimeCard.UnBillable_Hours_Sun__c = 0;
                            newTimeCard.UnBillable_Hours_Mon__c = 0;
                            newTimeCard.UnBillable_Hours_Tue__c = 0;
                            newTimeCard.UnBillable_Hours_Wed__c = 0; 
                            newTimeCard.UnBillable_Hours_Thu__c = 0;
                            newTimeCard.UnBillable_Hours_Fri__c = 0;
                            newTimeCard.UnBillable_Hours_Sat__c = 0;
                            System.debug('------Sun 1--2-------'+newTimeCard.Billable_Hours_Sun__c);
                            System.debug('------Mon 1--2-------'+newTimeCard.Billable_Hours_Mon__c);
                            System.debug('------Tue 1--2-------'+newTimeCard.Billable_Hours_Tue__c);
                            System.debug('------Wed 1--2-------'+newTimeCard.Billable_Hours_Wed__c);
                            System.debug('------Thu 1--2-------'+newTimeCard.Billable_Hours_Thu__c);
                            System.debug('------FRi 1--2-------'+newTimeCard.Billable_Hours_Fri__c);
                            System.debug('------Sat 1--2-------'+newTimeCard.Billable_Hours_Sat__c);
                            if(dayOfWeek == 'Sun'){
                                assignmentTimeCardWrapObj.primaryTimeCardWithDateMap.put(dtWeek, newTimeCard.Billable_Hours_Sun__c);
                                assignmentTimeCardWrapObj.shadowTimeCardWithDateMap.put(dtWeek, newTimeCard.UnBillable_Hours_Sun__c);
                            }
                            else if(dayOfWeek == 'Mon'){
                                assignmentTimeCardWrapObj.primaryTimeCardWithDateMap.put(dtWeek, newTimeCard.Billable_Hours_Mon__c);
                                assignmentTimeCardWrapObj.shadowTimeCardWithDateMap.put(dtWeek, newTimeCard.UnBillable_Hours_Mon__c);
                            }
                            else if(dayOfWeek == 'Tue'){
                                assignmentTimeCardWrapObj.primaryTimeCardWithDateMap.put(dtWeek, newTimeCard.Billable_Hours_Tue__c);
                                assignmentTimeCardWrapObj.shadowTimeCardWithDateMap.put(dtWeek, newTimeCard.UnBillable_Hours_Tue__c);
                            }
                            else if(dayOfWeek == 'Wed'){
                                assignmentTimeCardWrapObj.primaryTimeCardWithDateMap.put(dtWeek, newTimeCard.Billable_Hours_Wed__c);
                                assignmentTimeCardWrapObj.shadowTimeCardWithDateMap.put(dtWeek, newTimeCard.UnBillable_Hours_Wed__c);
                            }
                            else if(dayOfWeek == 'Thu'){
                                assignmentTimeCardWrapObj.primaryTimeCardWithDateMap.put(dtWeek, newTimeCard.Billable_Hours_Thu__c);
                                assignmentTimeCardWrapObj.shadowTimeCardWithDateMap.put(dtWeek, newTimeCard.UnBillable_Hours_Thu__c);
                            }
                            else if(dayOfWeek == 'Fri'){
                                assignmentTimeCardWrapObj.primaryTimeCardWithDateMap.put(dtWeek, newTimeCard.Billable_Hours_Fri__c);
                                assignmentTimeCardWrapObj.shadowTimeCardWithDateMap.put(dtWeek, newTimeCard.UnBillable_Hours_Fri__c);
                            }
                            else if(dayOfWeek == 'Sat'){
                                assignmentTimeCardWrapObj.primaryTimeCardWithDateMap.put(dtWeek, newTimeCard.Billable_Hours_Sat__c);
                                assignmentTimeCardWrapObj.shadowTimeCardWithDateMap.put(dtWeek, newTimeCard.UnBillable_Hours_Sat__c);
                            }
                            assignmentTimeCardWrapObj.timeCardAssginment = newTimeCard;
                            assignmentTimeCardWrapObj.dateWithTimeCardHoursMap.put(assignment.Id+'-'+assignment.RecordType.Name, newTimeCard);
                        
                    
                }
                
                System.debug('--------assignmentTimeCardWrapObj.shadowTimeCardWithDateMap-----------'+assignmentTimeCardWrapObj.shadowTimeCardWithDateMap);
                System.debug('--------assignmentTimeCardWrapObj.shadowTimeCardWithDateMap------size-----'+assignmentTimeCardWrapObj.shadowTimeCardWithDateMap.size());
                System.debug('--------assignmentTimeCardWrapObj.primaryTimeCardWithDateMap-----------'+assignmentTimeCardWrapObj.primaryTimeCardWithDateMap);
                System.debug('--------assignmentTimeCardWrapObj.primaryTimeCardWithDateMap------size-----'+assignmentTimeCardWrapObj.primaryTimeCardWithDateMap.size());
                System.debug('----------assignmentTimeCardWrapObj.dateWithTimeCardHoursMap-----------'+assignmentTimeCardWrapObj.dateWithTimeCardHoursMap);
                System.debug('----------assignmentTimeCardWrapObj.dateWithTimeCardHoursMap---size--------'+assignmentTimeCardWrapObj.dateWithTimeCardHoursMap.size());
                
                system.debug('------------assignmentTimeCardWrapObj.timeCardAssginment---------------'+assignmentTimeCardWrapObj.timeCardAssginment);
                system.debug('------------assignmentTimeCardWrapObj.projectName---------------'+assignmentTimeCardWrapObj.projectName);
                system.debug('------------assignmentTimeCardWrapObj.assignmentWithTimeCardMap---------------'+assignmentTimeCardWrapObj.assignmentWithTimeCardMap);
                system.debug('-----------assignmentTimeCardWrapObj----------------'+assignmentTimeCardWrapObj);
                
            }
            assignmentTimeCardWrapList.add(assignmentTimeCardWrapObj);
            system.debug('---------assignmentTimeCardWrapList------------------'+assignmentTimeCardWrapList);
            system.debug('---------assignmentTimeCardWrapList-----size-------------'+assignmentTimeCardWrapList.size());
        }
         
    }
    
    /*
        Method Name         : prevWeekTimeEntry
        param               : N/A
        Description         : method to fetch previous week Time Entries
    */
    public void prevWeekTimeEntry()
    {
        selectedDate = startDate.addDays(-1);
        fetchTimeCards();
    }
    
     /*
        Method Name         : saveTimeEntry
        param               : N/A
        Description         : method to save the  Time Entries
    */
    public void saveTimeEntry(){
        /*List<Time_Card__c> timeCardsToInsertList = new List<Time_Card__c>();
       
        for(assignmentTimeCardWrap wrapValue : assignmentTimeCardWrapList){
            system.debug('---------wrapValue ----------'+wrapValue );
            system.debug('---------wrapValue.resourceType ----------'+wrapValue.resourceType );
            system.debug('---------timeCardAssginment--------------'+wrapValue.timeCardAssginment);
            //if(wrapValue.assignmentWithTimeCardMap.get(timeCardAssginment.Assignment__c 
            //for( Time_Card__c timeCardValue : wrapValue.assignmentWithTimeCardMap.values()){
           Time_Card__c timeCard = new Time_Card__c();
            //timeCard.Id = wrapValue.dateWithTimeCardHoursMap.get(wrapValue.timeCardAssginment.Assignment__c+'-'+wrapValue.resourceType).Id;
            timeCard.Assignment__c = wrapValue.timeCardAssginment.Assignment__c;
            timeCard.Start_Date__c = startDate;
            timeCard.EP_End_Date__c = endDate;
            timeCard.Type__c = wrapValue.timeCardAssginment.Type__c;
            for(Date dtWeek: weekDatesList){
                system.debug('====1========dtWeek==='+dtWeek);
                Datetime dt = DateTime.newInstance(dtWeek, Time.newInstance(0, 0, 0, 0));
                system.debug('--1-----myDateTime------'+dt);
                String dayOfWeek=dt.format('E');
                System.debug('1--------Day : ' + dayOfWeek);
                if(dayOfWeek == 'Sun'){
                    timeCard.Billable_Hours_Sun__c = wrapValue.primaryTimeCardWithDateMap.get(dtWeek);  
                }
                else if(dayOfWeek == 'Mon'){
                    timeCard.Billable_Hours_Mon__c = wrapValue.primaryTimeCardWithDateMap.get(dtWeek);  
                }
                else if(dayOfWeek == 'Tue'){
                    timeCard.Billable_Hours_Tue__c = wrapValue.primaryTimeCardWithDateMap.get(dtWeek);  
                }
                else if(dayOfWeek == 'Wed'){
                    timeCard.Billable_Hours_Wed__c = wrapValue.primaryTimeCardWithDateMap.get(dtWeek);  
                }
                else if(dayOfWeek == 'Thu'){
                    timeCard.Billable_Hours_Thu__c = wrapValue.primaryTimeCardWithDateMap.get(dtWeek);  
                }
                else if(dayOfWeek == 'Fri'){
                    timeCard.Billable_Hours_Fri__c = wrapValue.primaryTimeCardWithDateMap.get(dtWeek);  
                }
                else if(dayOfWeek == 'Sat'){
                    timeCard.Billable_Hours_Sat__c = wrapValue.primaryTimeCardWithDateMap.get(dtWeek);  
                }
            }
            timeCardsToInsertList.add(timeCard);
        }
        if(timeCardsToInsertList.size() > 0){
            upsert timeCardsToInsertList;
        }
        system.debug('-----------timeCardsToInsertList----------------'+timeCardsToInsertList);*/
    //}
    
    }
    
     /*
        Method Name         : nextWeekTimeEntry
        param               : N/A
        Description         : method to fetch next week Time Entries
    */
    public void nextWeekTimeEntry()
    {
        selectedDate = endDate.addDays(1);
        fetchTimeCards();
    }

    /*
        Method Name         : assignmentTimeCardWrap
        param               : N/A
        Description         : method to build the wrapper to fetch the required details to display in the page.
    */
    public class assignmentTimeCardWrap{
        public Time_Card__c timeCardAssginment {get; set;}
        public Map<Id, Time_Card__c> assignmentWithTimeCardMap {get;set;}
        public String projectName {get; set;}
        public String resourceType {get; set;}
        public Map<Date, Decimal> primaryTimeCardWithDateMap  {get;set;}
        public Map<Date, Decimal> shadowTimeCardWithDateMap  {get;set;}
        public Map<String, Time_Card__c> dateWithTimeCardHoursMap {get;set;}
        public Map<Id, Id> shadowWithPrimaryResourceMap {get; set;}
        
        public assignmentTimeCardWrap(){
            this.timeCardAssginment = new Time_Card__c();
            this.assignmentWithTimeCardMap = new Map<Id, Time_Card__c>();
            this.primaryTimeCardWithDateMap = new Map<Date, Decimal>();
            this.shadowTimeCardWithDateMap = new Map<Date, Decimal>();
            this.dateWithTimeCardHoursMap = new Map<String, Time_Card__c>();
            this.shadowWithPrimaryResourceMap = new Map<Id, Id>();
        }
    }
}