Public class AllocationForResourceController{

    public List<Assignment__c> relatedOpenAllocationsList{
        get{
            if(relatedOpenAllocationsList != null){
                return relatedOpenAllocationsList;
            }
            return [select Start_Date__c, End_Date__c, Project__c,Project__r.Name,
                                          Project__r.Contractual_Start_Date__c, Project__r.Contractual_End_Date__c, 
                                          Project__r.Contact_Person__c, Project__r.EP_Project_Manager__c, Project__r.EP_Project_Manager__r.Name,
                                          Project__r.Contact_Person__r.Name, RecordType.Name, Budgeted_Hours__c, Burned_Hours__c,Approved_Hours__c,
                                          Billing_Type__c, Resource__r.User__c,EP_Shadow_Resource__r.User__c
                                          from Assignment__c 
                                          WHERE Resource__r.User__c =: UserInfo.getUserId() OR EP_Shadow_Resource__r.User__c =: UserInfo.getUserId()];
           }public set;   
        
    }
}