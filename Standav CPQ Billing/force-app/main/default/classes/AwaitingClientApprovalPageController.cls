/**
*  @Class Name: AwaitingClientApprovalPageController
*  @Description: Handles all functionality required for "AwaitingClientApprovalPage" visualforce page.
*  @Company: Standav
*  @CreatedDate: 26/02/2019
*  Change Log
*  -----------------------------------------------------------
*  Developer: Shruti MP           
   Modification Date : 12/01/2019          
   Comments
*  -----------------------------------------------------------
*
*/
public class AwaitingClientApprovalPageController {
    public String timeCardToUpdate{get;set;}
    Public string selectedField {get;set;}
    public Attachment attach {get;set;}
    public String errorMessage{get;set;}
    public String parentId {get; set;}
    public Boolean relatedContactReminder{get;set;}
    public string projectCriteraString{get; set;}
    public string resourceCriteraString{get; set;}
    public Date startDateFilter{get;set;}
    public Date endDateFilter{get;set;}
    public List<SelectOption> resourceNameOptionsList{get; set;} 
    public List<SelectOption> projectNameOptionsList{get; set;}  
    Map<Id, String> userIdWithNameMap;
    
    public List<Time_Card__c> relatedTimeCardsList{
        get {
            if(relatedTimeCardsList != null){
                return relatedTimeCardsList;
            }
            return [SELECT Id, Assignment__r.Project__c, Name, Assignment__r.EP_Shadow_Resource__c, Assignment__r.Resource__c,
                    Type__c, EP_End_Date__c, Billable_Hours_Fri__c, EP_Notes__c, EP_Approval_Date__c, Assignment__r.Resource__r.User__c,
                    EP_Total_Number_of_Billable_Hours__c, EP_Total_Number_of_Unbillable_Hours__c,Send_Reminder__c,
                    EP_Rejected_Date__c, Submitted_On__c, Project_Manager__c, Billable_Hours_Mon__c, 
                    Billable_Hours_Sat__c, Assignment__c, Status__c, Start_Date__c, Billable_Hours_Sun__c, 
                    Billable_Hours_Thu__c, Billable_Hours_Tue__c, Billable_Hours_Wed__c, UnBillable_Hours_Fri__c, 
                    UnBillable_Hours_Mon__c, UnBillable_Hours_Sat__c, UnBillable_Hours_Sun__c, UnBillable_Hours_Thu__c, 
                    UnBillable_Hours_Tue__c, UnBillable_Hours_Wed__c, Assignment__r.Project__r.Name, Assignment__r.Project__r.Contact_Person__c,
                    Assignment__r.Resource__r.User__r.Name , Assignment__r.RecordType.Name, Assignment__r.Is_Primary_Resource_Contributing__c,
                    X1st_Approver__c, X2nd_Approver__c, X3rd_Approver__c, X4th_Approver__c, X5th_Approver__c, Approval_Status__c,
                    Assignment__r.EP_Shadow_Resource__r.User__r.Name,  Assignment__r.Role__c, Assignment__r.Project__r.Contact_Person__r.Email,
                    Assignment__r.Project__r.Contact_Person__r.Name, (SELECT Id, Name FROM attachments)
                    FROM Time_Card__c 
                    WHERE Client_Details__c = True AND Status__c = 'Approved' 
                    AND Status__c != 'Client Approved' AND Project_Manager__c =: UserInfo.getUserId() AND 
                        Assignment__r.RecordType.Name = 'Primary' AND 
                    Assignment__r.Billing_Type__c = 'Billable'];
        }
        public set;
    }
    //Constructor
    public AwaitingClientApprovalPageController(){
        userIdWithNameMap = new Map<Id, String>();
        for(User UserRec : UserDataCache.getMap().values()){
            userIdWithNameMap.put(UserRec.Id, UserRec.FirstName+' '+UserRec.LastName);
        } 
        timeCardToUpdate = this.timeCardToUpdate;
        projectNameOptionsList = new List<SelectOption>();
        Set<SelectOption> projectNameSet = new Set<SelectOption>();
        projectNameSet.add(new SelectOption('', 'Please Select'));
        resourceNameOptionsList = new List<SelectOption>();
        Set<SelectOption> resourceNameSet = new Set<SelectOption>();
        resourceNameSet.add(new SelectOption('', 'Please Select'));
        for(Time_Card__c timeCardSubmitted : relatedTimeCardsList){
            projectNameSet.add(new SelectOption(timeCardSubmitted.Assignment__r.Project__r.Name, timeCardSubmitted.Assignment__r.Project__r.Name)); 
            resourceNameSet.add(new SelectOption(userIdWithNameMap.get(timeCardSubmitted.Assignment__r.Resource__r.User__c), userIdWithNameMap.get(timeCardSubmitted.Assignment__r.Resource__r.User__c))); 
                
        }
        projectNameOptionsList.addAll(projectNameSet);
        resourceNameOptionsList.addAll(resourceNameSet);
        attach = new Attachment();
        system.debug('--------relatedTimeCardsList----------'+relatedTimeCardsList);
        if(relatedTimeCardsList.size() == 0){
            errorMessage = 'No Pending Timesheets for Approvals';
        }
    }

    public PageReference resetFiltersMethod(){
        //fetchTimeCard();
        //relatedTimeCardsList;
        errorMessage = null;
        projectCriteraString = null;
        resourceCriteraString = null;
        startDateFilter = null;
        endDateFilter = null;
        return null;
    }

    public PageReference filterMethod(){
        Date filterStartDate = startDateFilter;
        Date filterEndDate = endDateFilter;
        string queryString;
        system.debug('--1----resourceCriteraString--------'+resourceCriteraString);
        if(startDateFilter != null){
            Date dToday1 = filterStartDate;
            Datetime dt1 = datetime.newInstance(dToday1.year(), dToday1.month(),dToday1.day());
            String dayOfCurrentDate = dt1.format('E');
            filterStartDate = (dayOfCurrentDate == 'Sun' ? filterStartDate.addDays(1):
                             dayOfCurrentDate == 'Mon' ? filterStartDate.addDays(0):
                             dayOfCurrentDate == 'Tue' ? filterStartDate.addDays(-1):
                             dayOfCurrentDate == 'Wed' ? filterStartDate.addDays(-2):
                             dayOfCurrentDate == 'Thu' ? filterStartDate.addDays(-3):
                             dayOfCurrentDate == 'Fri' ? filterStartDate.addDays(-4):
                             dayOfCurrentDate == 'Sat' ? filterStartDate.addDays(-5): 
                             filterStartDate);     
        }
        if(endDateFilter != null){
            Date dToday2 = filterEndDate;
            Datetime dt2 = datetime.newInstance(dToday2.year(), dToday2.month(),dToday2.day());
            String dayOfCurrentDate = dt2.format('E');
            filterEndDate = (dayOfCurrentDate == 'Sun' ? filterEndDate.addDays(0):
                             dayOfCurrentDate == 'Mon' ? filterEndDate.addDays(6):
                             dayOfCurrentDate == 'Tue' ? filterEndDate.addDays(5):
                             dayOfCurrentDate == 'Wed' ? filterEndDate.addDays(4):
                             dayOfCurrentDate == 'Thu' ? filterEndDate.addDays(3):
                             dayOfCurrentDate == 'Fri' ? filterEndDate.addDays(2):
                             dayOfCurrentDate == 'Sat' ? filterEndDate.addDays(1): 
                             filterEndDate);     
        }
        String approvedString = 'Approved';
        String clientapprovedString = 'Client Approved';

        Id loggedInUserId = UserInfo.getUserId();
        String selectString = 'SELECT Id, Assignment__r.Project__c, Name, Assignment__r.EP_Shadow_Resource__c, Assignment__r.Resource__c,'+
                            'Type__c, EP_End_Date__c, Billable_Hours_Fri__c, EP_Notes__c, EP_Approval_Date__c, Assignment__r.Project__r.Contact_Person__c,'+
                            'EP_Total_Number_of_Billable_Hours__c, EP_Total_Number_of_Unbillable_Hours__c, Assignment__r.Project__r.Contact_Person__r.Single_Reminder__c,'+
                            'EP_Rejected_Date__c, Submitted_On__c, Project_Manager__c, Billable_Hours_Mon__c,Assignment__r.Project__r.Contact_Person__r.Email,'+ 
                            'Billable_Hours_Sat__c, Assignment__c, Status__c, Start_Date__c, Billable_Hours_Sun__c, Send_to_Client_Approval__c,'+
                            'Billable_Hours_Thu__c, Billable_Hours_Tue__c, Billable_Hours_Wed__c, UnBillable_Hours_Fri__c, Client_Details__c,'+
                            'UnBillable_Hours_Mon__c, UnBillable_Hours_Sat__c, UnBillable_Hours_Sun__c, UnBillable_Hours_Thu__c,'+ 
                            'UnBillable_Hours_Tue__c, UnBillable_Hours_Wed__c, Assignment__r.Project__r.Name,Send_Reminder__c,'+
                            'Assignment__r.Resource__r.User__r.Name , Assignment__r.RecordType.Name, Assignment__r.Is_Primary_Resource_Contributing__c,'+
                            'X1st_Approver__c, X2nd_Approver__c, X3rd_Approver__c, X4th_Approver__c, X5th_Approver__c, Approval_Status__c,'+
                            'Assignment__r.EP_Shadow_Resource__r.User__r.Name,  Assignment__r.Role__c,(SELECT Id, Name FROM attachments)';
        String formString = ' FROM Time_Card__c';
        String whereString = ' WHERE Client_Details__c = True AND Status__c =: approvedString AND Status__c !=: clientapproed AND Project_Manager__c =: loggedInUserId AND Assignment__r.RecordType.Name =: primaryString'; 
                  
        system.debug('------selectString----------'+selectString);
        system.debug('-------formString---------'+formString);
        system.debug('-------whereString---------'+whereString);
        if(projectCriteraString != null && resourceCriteraString == null && filterStartDate == null && filterEndDate == null){
            queryString = selectString + formString + whereString + ' AND Assignment__r.Project__r.Name =: projectCriteraString';
            system.debug('------queryString----------'+queryString);                
        }
        if(resourceCriteraString != null && filterStartDate == null && filterEndDate == null && projectCriteraString == null ){
            queryString = selectString + formString + whereString + ' AND Assignment__r.Resource__r.User__r.Name =: resourceCriteraString';
            system.debug('------queryString--1--------'+queryString); 
        }
        if(filterStartDate != null && filterEndDate != null && resourceCriteraString == null && projectCriteraString == null){
            queryString = selectString + formString + whereString + ' AND Start_Date__c >=: filterStartDate AND EP_End_Date__c <=: filterEndDate';
            system.debug('------queryString--2--------'+queryString); 
        }
        if(projectCriteraString != null && resourceCriteraString != null && filterStartDate != null && filterEndDate != null){
            queryString = selectString + formString + whereString + ' AND Assignment__r.Project__r.Name =: projectCriteraString AND Assignment__r.Resource__r.User__r.Name =: resourceCriteraString AND Start_Date__c >=: filterStartDate AND EP_End_Date__c <=: filterEndDate';
            system.debug('------queryString--3--------'+queryString); 
        }
        if(projectCriteraString != null && resourceCriteraString == null && filterStartDate != null && filterEndDate != null){
            queryString = selectString + formString + whereString + ' AND Assignment__r.Project__r.Name =: projectCriteraString AND Start_Date__c >=: filterStartDate AND EP_End_Date__c <=: filterEndDate';
            system.debug('------queryString--4--------'+queryString); 
        }
        if(projectCriteraString != null && resourceCriteraString != null && filterStartDate == null && filterEndDate == null){
            queryString = selectString + formString + whereString + ' AND Assignment__r.Project__r.Name =: projectCriteraString AND Assignment__r.Resource__r.User__r.Name =: resourceCriteraString';
            system.debug('------queryString--5--------'+queryString); 
        }
        if(projectCriteraString == null && resourceCriteraString != null && filterStartDate != null && filterEndDate != null){
            queryString = selectString + formString + whereString + ' AND Assignment__r.Resource__r.User__r.Name =: resourceCriteraString AND Start_Date__c >=: filterStartDate AND EP_End_Date__c <=: filterEndDate';
            system.debug('------queryString--6--------'+queryString); 
        }
        system.debug('--1----queryString--------'+queryString);
        if(queryString == null){
            //relatedTimeCardsList();
            if(relatedTimeCardsList.size() == 0){
                errorMessage = 'No Pending Timesheets for the selected filter';
            }
        }
        if(queryString != null){
          system.debug('---0-relatedTimeCardsList--------'+relatedTimeCardsList);
          relatedTimeCardsList = new List<Time_Card__c>();
          system.debug('---1-relatedTimeCardsList--------'+relatedTimeCardsList);
          for (Time_Card__c relatedTimeCard : database.query(queryString)) {
              relatedTimeCardsList.add(relatedTimeCard); 
          }
          system.debug('----relatedTimeCardsList--------'+relatedTimeCardsList);
          if(relatedTimeCardsList.size() == 0){
              errorMessage = 'No Pending Timesheets for the selected filter';
          }else{
              errorMessage = null;
          }
        }
        return null;
    }
    
    //attach files
    public PageReference attachFile(){
        //attach = new Attachment();
        system.debug('-----------------'+UserInfo.getUserId());
        system.debug('-----------------'+parentId);
        /*if(parentId != null && fileName != null && bl != null){
            attach.OwnerId = UserInfo.getUserId();
            attach.ParentId = parentId;
            attach.Name = fileName;
            attach.Body = bl;
            attach.ContentType = contentType;
            insert attach;
        }else{*/
          Attachment a = new Attachment(parentId = parentId, name = attach.Name, body=attach.body) ;
          Insert a;
        //}
        
        PageReference p= new PageReference('/apex/AwaitingClientApprovalPage');
        p.setRedirect(true);
        
        return p;
    }
}