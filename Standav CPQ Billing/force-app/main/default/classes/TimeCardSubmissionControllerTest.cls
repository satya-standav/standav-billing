@isTest
private class TimeCardSubmissionControllerTest{
      private testMethod static void test(){
      
        User user = new User();
            user.LastName = 'Test User';
            user.Alias = 'tu';
            user.Email = 'test@gmail.com';
            user.Username = 'test.dev@stand.com';
            user.CommunityNickname = 'test.t';
            user.User_Time_Zone__c = 'Pst';
            user.ProfileId = '00e54000000HSHG';           
            user.TimeZoneSidKey    = 'America/Denver';
            user.LocaleSidKey      = 'en_US';
            user.EmailEncodingKey  = 'UTF-8';
            user.LanguageLocaleKey = 'en_US';
            insert user;
        
        Account acc = new Account();
        	acc.Name = 'Test Account';
        	insert acc;
         
        Contact con = new Contact();
        	con.FirstName = 'test';
        	con.LastName = 'contact';
        	insert con;
         
        Project__c project = new Project__c();
            project.Name = 'Test Project';
            project.EP_Project_Manager__c = user.Id;
            project.Contact_Person__c = con.Id;
            project.Start_Date__c = Date.newInstance(2016, 12, 9);
        	project.Account__c = acc.id;
        	project.Number__c = 2;
            insert project;        
            update project;
               
       Resource__c resource = new Resource__c();
            resource.User__c = '0051Q00000GoFQn';
            insert resource;        
            update resource;
        
       Assignment__c assignment = new Assignment__c();
            assignment.Project__c = project.id;
            assignment.Resource__c = resource.id;
            assignment.Status__c = 'Approved';
            assignment.Start_Date__c = Date.newInstance(2016, 12, 9);
            assignment.End_Date__c = Date.newInstance(2018, 12, 9);
            assignment.Role__c = 'Developer';
            assignment.Billing_Type__c = 'Billable';
            assignment.Assignment_Type__c = 'Long Term';
          	assignment.Budgeted_Hours__c = 50;
            insert assignment;        
        
       Time_Card__c card = new Time_Card__c();
        	card.Assignment__c = assignment.id;
            card.Billable_Hours_Fri__c = 4;
            card.Billable_Hours_Mon__c =4;
            card.Billable_Hours_Sat__c = 0;
            card.Billable_Hours_Sun__c = 0;
            card.Billable_Hours_Thu__c = 4;
            card.Billable_Hours_Tue__c = 4;
            card.Billable_Hours_Wed__c =4;
            card.Status__c = 'Draft';            
            insert card;        
          
      Test.startTest();
         PageReference myVfPage = Page.Approval_Review_Page;
         myVfPage.getParameters().put('id',card.id);
         Test.setCurrentPage(myVfPage); 
         String id = ApexPages.currentPage().getParameters().get('id');
          
        TimeCardSubmissionController f1 = new TimeCardSubmissionController(); 
        f1.calculateTimeCardSubmission();
        f1.saveTimeEntry();
        f1.currentUser='test.dev@stand.com';
        f1.cancel();
        f1.calculateHours();
        f1.showBillableHoursCheck= True;
        f1.bl=Blob.valueOf('Unit Test Attachment Body');
        f1.parentId=con.id;
        f1.fileName='abc';
        f1.contentType='abc';
        f1.relatedTimeCard=card;
        f1.relatedTimeCard.Status__c = 'Draft';
          
     Test.stopTest();

  }
  
private testMethod static void test1(){
      
        User user = new User();
            user.LastName = 'Test User';
            user.Alias = 'tu';
            user.Email = 'test@gmail.com';
            user.Username = 'test.dev@stand.com';
            user.CommunityNickname = 'test.t';
            user.User_Time_Zone__c = 'Pst';
            user.ProfileId = '00e54000000HSHG';           
            user.TimeZoneSidKey    = 'America/Denver';
            user.LocaleSidKey      = 'en_US';
            user.EmailEncodingKey  = 'UTF-8';
            user.LanguageLocaleKey = 'en_US';
            insert user;
        
    	User user1 = new User();
            user1.LastName = 'Test User1';
            user1.Alias = 'tu1';
            user1.Email = 'test1@gmail.com';
            user1.Username = 'test1.dev@stand.com';
            user1.CommunityNickname = 'test1.t';
            user1.User_Time_Zone__c = 'Pst';
            user1.ProfileId = '00e54000000HSHG';           
            user1.TimeZoneSidKey    = 'America/Denver';
            user1.LocaleSidKey      = 'en_US';
            user1.EmailEncodingKey  = 'UTF-8';
            user1.LanguageLocaleKey = 'en_US';
            insert user1;
        
        Account acc = new Account();
        	acc.Name = 'Test Account';
        	insert acc;
         
        Contact con = new Contact();
        	con.FirstName = 'test';
        	con.LastName = 'contact';
        	insert con;
         
    	Contact con1 = new Contact();
        	con1.FirstName = 'test1';
        	con1.LastName = 'contact1';
        	insert con1;
    
        Project__c project = new Project__c();
            project.Name = 'Test Project';
            project.EP_Project_Manager__c = user.Id;
            project.Contact_Person__c = con.Id;
            project.Start_Date__c = Date.newInstance(2016, 12, 9);
        	project.Account__c = acc.id;
        	project.Number__c = 2;
            insert project;        
            update project;
               
       Resource__c resource = new Resource__c();
            resource.User__c = '0051Q00000GoFQn';
    		resource.Contact__c = con.Id;
            insert resource;        
            update resource;
    
       Resource__c resource1 = new Resource__c();
            resource1.User__c = user1.id;
    		resource1.Contact__c = con1.Id;
            insert resource1;        
            update resource1;
    
           Assignment__c assignment = new Assignment__c();
            assignment.Project__c = project.id;
            assignment.Resource__c = resource.id;
            assignment.Status__c = 'Approved';
            assignment.Start_Date__c = Date.newInstance(2016, 12, 9);
            assignment.End_Date__c = Date.newInstance(2018, 12, 9);
            assignment.Role__c = 'Developer';
            assignment.Billing_Type__c = 'Billable';
            assignment.Assignment_Type__c = 'Long Term';
    		assignment.Budgeted_Hours__c = 50;
            assignment.RecordType = new RecordType();
            assignment.RecordType.Name = 'Primary';
            insert assignment;
        
       Assignment__c assignment1 = new Assignment__c();
            assignment1.Project__c = project.id;
            assignment1.Resource__c = resource1.id;
            assignment1.Status__c = 'Approved';
            assignment1.Start_Date__c = Date.newInstance(2016, 12, 9);
            assignment1.End_Date__c = Date.newInstance(2018, 12, 9);
            assignment1.Role__c = 'Developer';
            assignment1.Billing_Type__c = 'Billable';
            assignment1.Assignment_Type__c = 'Long Term';
    		assignment1.Budgeted_Hours__c = 50;
            assignment1.RecordType = new RecordType();
            assignment1.RecordType.Name = 'Shadow';
            insert assignment1;
    
        /*Assignment__c assignment2 = new Assignment__c();
            assignment2.Project__c = project.id;
            assignment2.Resource__c = resource1.id;
            assignment2.Status__c = 'Approved';
            assignment2.Start_Date__c = Date.newInstance(2016, 12, 9);
            assignment2.End_Date__c = Date.newInstance(2018, 12, 9);
            assignment2.Role__c = 'Developer';
            assignment2.Billing_Type__c = 'Billable';
            assignment2.Assignment_Type__c = 'Long Term';
            assignment2.RecordType = new RecordType();
            assignment2.RecordType.Name = 'Shadow';
            update assignment; */
        
       Time_Card__c card = new Time_Card__c();
        	card.Assignment__c = assignment.id;
            card.Billable_Hours_Fri__c = 4;
            card.Billable_Hours_Mon__c =4;
            card.Billable_Hours_Sat__c = 0;
            card.Billable_Hours_Sun__c = 0;
            card.Billable_Hours_Thu__c = 4;
            card.Billable_Hours_Tue__c = 4;
            card.Billable_Hours_Wed__c =4;
            card.Status__c = 'Draft'; 
            card.Start_Date__c = Date.newInstance(2016, 12, 9);
            card.EP_End_Date__c = Date.newInstance(2018, 12, 9);
            insert card;        
          
      Test.startTest();
         PageReference myVfPage = Page.Approval_Review_Page;
         myVfPage.getParameters().put('id',card.id);
         Test.setCurrentPage(myVfPage); 
         String id = ApexPages.currentPage().getParameters().get('id');
          
        TimeCardSubmissionController f1 = new TimeCardSubmissionController(); 
        f1.submitTimeEntry();

          
     Test.stopTest();

  }
  
}