/**
*  @Class Name: TimeCardCreationBatchController
*  @Description: Batch class to create a time cards for the upcoming week if the assignment is still active.
*  @Company: Standav
*  @CreatedDate: 11/25/2019
*  Change Log
*  -----------------------------------------------------------
*  Developer: Shruti MP           
   Modification Date : 11/25/2019          
   Comments
*  -----------------------------------------------------------
*
*/
global class TimeCardCreationBatchController implements Database.Batchable<sObject> {
    
    //Class variables
     DateTime currentDate = System.Today();
     String dayOfCurrentDate = currentDate.format('E'); 
     Date startDateForNewTimeCard = (dayOfCurrentDate == 'Sun' ? currentDate.addDays(1):
                                     dayOfCurrentDate == 'Mon' ? currentDate.addDays(0):
                                     dayOfCurrentDate == 'Tue' ? currentDate.addDays(-1):
                                     dayOfCurrentDate == 'Wed' ? currentDate.addDays(-2):
                                     dayOfCurrentDate == 'Thu' ? currentDate.addDays(-3):
                                     dayOfCurrentDate == 'Fri' ? currentDate.addDays(-4):
                                     dayOfCurrentDate == 'Sat' ? currentDate.addDays(-5): 
                                     currentDate).date();         
      Date endDateforNewTimeCard = startDateForNewTimeCard.addDays(6);
      
       
    //Start Method - start
    global Database.QueryLocator start(Database.BatchableContext BC){ 
        system.debug('----org time------'+System.Today());
        system.debug('----org time----1--'+System.Now());
        string dayOfCurrentDate = currentDate.format('E');
        system.debug('----dayOfCurrentDate-----'+dayOfCurrentDate);
        system.debug('----startDateForNewTimeCard -----'+startDateForNewTimeCard );
        system.debug('----endDateforNewTimeCard -----'+endDateforNewTimeCard );
        String query = 'SELECT Id,Name,Project__r.Name,Project__r.Start_Date__c,Project__r.End_Date__c,Start_Date__c,Project__r.EP_Project_Manager__c, Project__r.EP_Project_Manager__r.Name, Resource__c, End_Date__c,(select Id,Name,Assignment__r.Name,Start_Date__c,EP_End_Date__c from Time_Cards__r WHERE Start_Date__c =: startDateForNewTimeCard AND EP_End_Date__c =: endDateforNewTimeCard) FROM Assignment__c where (Project__r.Start_Date__c<=TODAY OR Project__r.End_Date__c>=TODAY) and (Start_Date__c<=TODAY AND End_Date__c>=TODAY)';
        return Database.getQueryLocator(query); 
   } 
   //Start Method -End
   
   //Execute Method - start
   global void execute(Database.BatchableContext BC, List<Assignment__c> scope){
       system.debug('----scope------'+scope);
      List<Time_Card__C> timeCardsToInsertList = new List<Time_Card__c>();
      for(Assignment__c assignment : scope){
          if(assignment.Time_Cards__r.size() == 0){
              Time_Card__c newTimeCard = new Time_Card__c();
               newTimeCard.Start_Date__c = startDateForNewTimeCard;
               newTimeCard.EP_End_Date__c = endDateforNewTimeCard;
               newTimeCard.Assignment__c = assignment.Id;
               newTimeCard.Project_Manager__c = assignment.Project__r.EP_Project_Manager__c;
               system.debug('----newTimeCard------'+newTimeCard);
               timeCardsToInsertList.add(newTimeCard);
          }
          system.debug('----timeCardsToInsertList------'+timeCardsToInsertList);
      }
      system.debug('----timeCardsToInsertList--size----'+timeCardsToInsertList.size());
      if(timeCardsToInsertList.size() > 0){
          insert timeCardsToInsertList;
      }
   }  
   
   global void finish(Database.BatchableContext info){     
   }  
}