public class ResourcePageController {
    public List<wrapAssignment> ref {get; set;}
    public id currentRecord {get;set;}
    public String errorMessage{get;set;}
    public Assignment__c assign{get;set;}
    public ResourcePageController(ApexPages.StandardController controller){
        currentRecord = apexpages.currentpage().getparameters().get('id');
        
        //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'This Resource doesnot have any Allocation');
        //ApexPages.addMessage(myMsg);
        
        ref = new List<wrapAssignment>(); 
        
        if(currentRecord != null) {
            
            for(Assignment__c assign: [select Start_Date__c, End_Date__c, Project__c,Project__r.Name,
                                       Project__r.Contractual_Start_Date__c, Project__r.Contractual_End_Date__c, 
                                       Project__r.Contact_Person__c, Project__r.EP_Project_Manager__c, Project__r.EP_Project_Manager__r.Name,
                                       Project__r.Contact_Person__r.Name, RecordType.Name, Budgeted_Hours__c, Burned_Hours__c,Approved_Hours__c,
                                       Billing_Type__c, Resource__r.User__c,EP_Shadow_Resource__r.User__c, Is_Primary_Resource_Contributing__c,Record_Type_Name__c
                                       from Assignment__c 
                                       WHERE Resource__c =: currentRecord OR EP_Shadow_Resource__c =: currentRecord ]){
                                           system.debug('---------2--------------'+assign);
                                           wrapAssignment wc = new wrapAssignment();
                                           
                                           if(assign.Record_Type_Name__c== 'Primary' && assign.EP_Shadow_Resource__c==Null){
                                               wc.projectName=assign.Project__r.Name;
                                               wc.assignmentType= assign.RecordType.Name;
                                           	   wc.budgetedHours= assign.Budgeted_Hours__c;
                                               wc.burnedHours= assign.Burned_Hours__c;
                                               wc.approvedHours= assign.Approved_Hours__c;
                                               wc.startDate= assign.Start_Date__c;
                                               wc.endDate= assign.End_Date__c;
                                               wc.billingType= assign.Billing_Type__c;
                                               if(wc.billingType == 'Billable'){
                                               wc.billingTypeCheck = true;
                                           }
                                           }
                                           
                                           else if(assign.Record_Type_Name__c== 'Shadow' && assign.EP_Shadow_Resource__c==currentRecord){
                                              wc.projectName=assign.Project__r.Name;
                                              wc.assignmentType= assign.RecordType.Name;
                                           	  wc.budgetedHours= assign.Budgeted_Hours__c;
                                          	  wc.burnedHours= assign.Burned_Hours__c;
                                              wc.approvedHours= assign.Approved_Hours__c;
                                              wc.startDate= assign.Start_Date__c;
                                              wc.endDate= assign.End_Date__c;
                                              wc.billingType= assign.Billing_Type__c;
                                              if(wc.billingType == 'Billable'){
                                               wc.billingTypeCheck = true;
                                              }
                                           }
                                           
                                          /* wc.assignmentType= assign.RecordType.Name;
                                           wc.budgetedHours= assign.Budgeted_Hours__c;
                                           wc.burnedHours= assign.Burned_Hours__c;
                                           wc.approvedHours= assign.Approved_Hours__c;
                                           wc.startDate= assign.Start_Date__c;
                                           wc.endDate= assign.End_Date__c;
                                           wc.billingType= assign.Billing_Type__c;
                                           if(wc.billingType == 'Billable'){
                                               wc.billingTypeCheck = true;
                                           }*/
                                           
                                           ref.add(wc);
                                           
                                       }
            if(ref.size()==0){
                errorMessage = 'This Resource doesnot have any Allocation';
            }
            
        }
        
    }
    public class wrapAssignment {
        public string projectName {get; set;}
        public string assignmentType {get; set;}
        public string billingType {get; set;}
        public decimal budgetedHours {get; set;} 
        public decimal burnedHours {get; set;} 
        public decimal approvedHours {get; set;} 
        public date startDate {get; set;} 
        public date endDate {get; set;} 
        public boolean billingTypeCheck {get; set;}
        
        public wrapAssignment() {
            budgetedHours=0;
            burnedHours=0;
            approvedHours=0;
        }  
    }
    
}