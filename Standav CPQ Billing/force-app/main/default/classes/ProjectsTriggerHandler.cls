Public class ProjectsTriggerHandler{
    public static void beforeInsertHandler(List<Project__c> relatedProjectsList){
        if(Trigger.isInsert && Trigger.isBefore){
            projectTriggerHelper.beforeInsertUpdateMethod(relatedProjectsList, null);
            projectTriggerHelper.linkAccountToProject(relatedProjectsList);
        }
    }
    public static void afterInsertHandler(List<Project__c> relatedProjectsList){
        if(Trigger.isInsert && Trigger.isAfter){
            projectTriggerHelper.afterInsertUpdatewMethod(relatedProjectsList, null);
            projectTriggerHelper.projectSharing(relatedProjectsList);
        }
    }
    public static void beforeUpdatHandler(List<Project__c> relatedProjectsList, Map<Id, Project__c> oldProjectDataMap){
        if(Trigger.isUpdate && Trigger.isBefore){
            projectTriggerHelper.beforeInsertUpdateMethod(relatedProjectsList, oldProjectDataMap);
            projectTriggerHelper.afterInsertUpdatewMethod(relatedProjectsList, null);
        }
    }
    public static void afterUpdateHandler(List<Project__c> relatedProjectsList, Map<Id, Project__c> oldProjectDataMap){
        if(Trigger.isUpdate && Trigger.isAfter){
            projectTriggerHelper.afterInsertUpdatewMethod(relatedProjectsList, oldProjectDataMap);
        }
    }
 /*   public static void beforeDeleteHandler(){
        if(Trigger.isDelete && Trigger.isBefore){
        
        }
    }
    public static void afterDeleteHandler(){
        if(Trigger.isDelete && Trigger.isAfter){
        
        }
    }
    public static void afterUndeleteHandler(){
        if(Trigger.isUndelete && Trigger.isAfter){
        
        }
    }*/
}