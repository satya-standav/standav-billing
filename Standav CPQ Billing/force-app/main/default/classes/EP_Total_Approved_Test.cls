@isTest
public class EP_Total_Approved_Test {
    public static testmethod void updateAccount(){
            User user = new User();
            user.LastName = 'Test User';
            user.Alias = 'tu';
            user.Email = 'test@gmail.com';
            user.Username = 'test.dev@stand.com';
            user.CommunityNickname = 'test.t';
            //user.UserRoleId = '00ei0000000rTfp';
            user.User_Time_Zone__c = 'Pst';
            user.ProfileId = '00e54000000HSHG';           
            user.TimeZoneSidKey    = 'America/Denver';
            user.LocaleSidKey      = 'en_US';
            user.EmailEncodingKey  = 'UTF-8';
            user.LanguageLocaleKey = 'en_US';
            insert user;
        	
        	Account acc = new Account();
        	acc.Name = 'Test Account';
        	insert acc;
        
        	Contact con = new Contact();
        	con.FirstName = 'test';
        	con.LastName = 'contact';
        	insert con;
        	
            
            Project__c project = new Project__c();
            project.Name = 'Test Project';
            project.EP_Project_Manager__c = user.Id;
            project.Contact_Person__c = con.Id;
            project.Start_Date__c = Date.newInstance(2016, 12, 9);
        	project.Account__c = acc.id;
        	project.Number__c = 2;
            insert project;        
            update project;
        
            Resource__c resource = new Resource__c();
            resource.User__c = user.Id;
            resource.Contact__c = con.Id;
            insert resource;        
            update resource;
        
            Assignment__c assignment = new Assignment__c();
            assignment.Project__c = project.id;
            assignment.Resource__c = resource.id;
            assignment.Status__c = 'Approved';
        	assignment.Budgeted_Hours__c = 100;
            assignment.Start_Date__c = Date.newInstance(2016, 12, 9);
            assignment.End_Date__c = Date.newInstance(2018, 12, 9);
            assignment.Role__c = 'Developer';
            assignment.Billing_Type__c = 'Billable';
            assignment.Assignment_Type__c = 'Long Term';   
            //assignment.Actual_Hours__c = 20;
            insert assignment;        
           // update assignment;
            
            Time_Card__c card = new Time_Card__c();
        	card.Assignment__c = assignment.id;
            card.Billable_Hours_Fri__c = 4;
            card.Billable_Hours_Mon__c =4;
            card.Billable_Hours_Sat__c = 0;
            card.Billable_Hours_Sun__c = 0;
            card.Billable_Hours_Thu__c = 4;
            card.Billable_Hours_Tue__c = 4;
            card.Billable_Hours_Wed__c =4;
            card.Status__c = 'Approved';            
            insert card;        
           // update card;
        
            Test.startTest();
               EP_Total_Approved_Hours_Weekly ua = new EP_Total_Approved_Hours_Weekly();
               Database.executebatch(ua);
               EP_Update_Total_No_Approved_Hours ua1 = new EP_Update_Total_No_Approved_Hours();
               Database.executebatch(ua1);
               EP_Total_Approved_Hours_Month ua2 = new EP_Total_Approved_Hours_Month();
               Database.executebatch(ua2);
            Test.stopTest();
        }
}