public class ProjectTriggerHelper{

    public static void beforeInsertUpdateMethod(List<Project__c> relatedProjectsList, Map<Id, Project__c> relatedProjectDataMap){
        Set<Id> relatedOpportunityIdSet = new Set<Id>();
         Map<Id, Opportunity> opportunityWithAccountMap = new Map<Id, Opportunity>();
        for(Project__c newProject : relatedProjectsList){
            if((Trigger.isInsert && newProject.Opportunity__c != null) ||
                (Trigger.isUpdate && newProject.Opportunity__c != relatedProjectDataMap.get(newProject.Id).Opportunity__c)){
                relatedOpportunityIdSet.add(newProject.Opportunity__c);
            }
        }
        if(relatedOpportunityIdSet.size() > 0){
            opportunityWithAccountMap = new Map<Id, Opportunity>([SELECT Id, AccountId FROM Opportunity WHERE Id IN: relatedOpportunityIdSet]);
        }
        if(!opportunityWithAccountMap.isEmpty()){
            for(Project__c newProject : relatedProjectsList){
                if(newProject.Opportunity__c != null && opportunityWithAccountMap.get(newProject.Opportunity__c) != null &&
                    opportunityWithAccountMap.get(newProject.Opportunity__c).AccountId != null){ 
                    newProject.Account__c = opportunityWithAccountMap.get(newProject.Opportunity__c).AccountId; 
                }
            }
        }
    }
    
    public static void afterInsertUpdatewMethod(List<Project__c> relatedProjectsList, Map<Id, Project__c> relatedProjectDataMap){
        List<Approvers_Flow__c> relatedAssignedApprovesList = new List<Approvers_Flow__c>();
        List<Approvers_Flow__c> relatedApprovedList;
        try{
            relatedApprovedList = [SELECT Id FROM Approvers_Flow__c WHERE Project__c IN: relatedProjectsList];
        }catch(Exception ex){
            relatedApprovedList = new List<Approvers_Flow__c>();
        }
        for(Project__c newProjectInserted : relatedProjectsList){
            if((newProjectInserted.Number__c != null || newProjectInserted.Number__c > 0) &&
                 relatedApprovedList.size() == 0){
                for(Integer approver = 1; approver <= newProjectInserted.Number__c; approver++){
                    Approvers_Flow__c newApproverAssigned = new Approvers_Flow__c();
                    newApproverAssigned.Name = 'Approver-'+approver;
                    newApproverAssigned.Order_of_Approval_Step__c = approver;
                    newApproverAssigned.Project__c = newProjectInserted.Id;
                    if(approver == 1)
                    newApproverAssigned.User_Approver__c = newProjectInserted.EP_Project_Manager__c;
                    if(approver == 2)
                        newApproverAssigned.Client_Approver__c = newProjectInserted.Contact_Person__c;
                    relatedAssignedApprovesList.add(newApproverAssigned);
                } 
                system.debug('--0-----relatedAssignedApprovesList--------------'+relatedAssignedApprovesList);
            }
            if(Trigger.isAfter && Trigger.isUpdate && 
                newProjectInserted.Number__c != relatedProjectDataMap.get(newProjectInserted.Id).Number__c){
                relatedAssignedApprovesList.clear();
                if(newProjectInserted.Number__c < relatedProjectDataMap.get(newProjectInserted.Id).Number__c &&
                    relatedApprovedList.size() != newProjectInserted.Number__c){
                    newProjectInserted.addError('Number of approvers has been reduced, please delete the appropriate approvers before reducing the number of approvers');     
                }else if(newProjectInserted.Number__c > relatedProjectDataMap.get(newProjectInserted.Id).Number__c){
                    for(Integer approver = 1; approver <= (newProjectInserted.Number__c - relatedProjectDataMap.get(newProjectInserted.Id).Number__c); approver++){
                        Approvers_Flow__c newApproverAssigned = new Approvers_Flow__c();
                        newApproverAssigned.Name = 'Approver-'+(relatedProjectDataMap.get(newProjectInserted.Id).Number__c + approver);
                        newApproverAssigned.Order_of_Approval_Step__c = relatedProjectDataMap.get(newProjectInserted.Id).Number__c + approver;
                        newApproverAssigned.Project__c = newProjectInserted.Id;
                        relatedAssignedApprovesList.add(newApproverAssigned);
                    }
                }
                system.debug('----1---relatedAssignedApprovesList--------------'+relatedAssignedApprovesList);
            }
        }
        system.debug('---2----relatedAssignedApprovesList--------------'+relatedAssignedApprovesList);
        if(relatedAssignedApprovesList.size() > 0){
            upsert relatedAssignedApprovesList;
        }
    }
    public static void projectSharing(List<Project__c> relatedProjectsList) {
       // Create a new list of sharing objects for Job
       List<Project__Share> projectShrs  = new List<Project__Share>();
       List<AccountShare> accShrs  = new List<AccountShare>();
       Set<Id> adminUserList = new Set<Id>();
       Set<Id> financeUserList = new Set<Id>();
       Map<Id, ID> clientAccountIdMap = new Map<Id, Id>();
       
       for(User portalUser : [SELECT Id, Name, Profile.Name 
                                FROM User 
                                WHERE (Profile.Name = 'Admin Team Profile - Community' OR
                                Profile.Name = 'Operation Team Profile - Community' OR
                                Profile.Name = 'ClientApprovalSite Profile')]){
           if(portalUser.Profile.Name == 'Admin Team Profile - Community'){
               adminUserList.add(portalUser.Id);
           }
           if(portalUser.Profile.Name == 'Operation Team Profile - Community'){
               financeUserList.add(portalUser.Id);
           }
       }
       // Declare variables for project manager sharing
       Project__Share projectmanagerShr;
       Project__Share adminProjShare;
       Project__Share financeProjshare;
       AccountShare accShare;
       
        for(Project__C project : relatedProjectsList){
            // Instantiate the sharing objects
            projectmanagerShr = new Project__Share();
            // Set the ID of record being shared
            projectmanagerShr.ParentId = project.Id;
            // Set the ID of user or group being granted access
            projectmanagerShr.UserOrGroupId = project.EP_Project_Manager__c;
            // Set the access level
            projectmanagerShr.AccessLevel = 'Edit';
            // Set the Apex sharing reason for hiring manager and recruiter
            projectmanagerShr.RowCause = Schema.Project__Share.RowCause.Project_Share__c;
            // Add objects to list for insert
            projectShrs.add(projectmanagerShr);
            
            if(project.Account__c != null){
                accShare = new AccountShare();
                accShare.accountid = project.Account__c;
                accShare.UserOrGroupId = project.EP_Project_Manager__c;
                accShare.accountaccesslevel = 'Edit';
                accShare.OpportunityAccessLevel = 'None';
                accShare.CaseAccessLevel = 'None';
                //accShare.ContactAccessLevel = 'Edit';
                accShrs.add(accShare);
            }
            //Admin share
            for(Id adminUserId : adminUserList){
                adminProjShare = new Project__Share();
                adminProjShare.ParentId = project.Id;
                adminProjShare.UserOrGroupId = adminUserId;
                adminProjShare.AccessLevel = 'edit';
                adminProjShare.RowCause = Schema.Project__Share.RowCause.Project_Share__c;
                // Add objects to list for insert
                projectShrs.add(adminProjShare);
            }
            //finance share
            for(Id financeUserId : financeUserList){
                financeProjshare = new Project__Share();
                financeProjshare.ParentId = project.Id;
                financeProjshare.UserOrGroupId = financeUserId ;
                financeProjshare.AccessLevel = 'read';
                financeProjshare.RowCause = Schema.Project__Share.RowCause.Project_Share__c;
                // Add objects to list for insert
                projectShrs.add(financeProjshare);
            }

            if(project.Contact_Person__c != null){
                clientAccountIdMap.put(project.Contact_Person__c, project.EP_Project_Manager__c);
            }
        }
        system.debug('------projectShrs---------'+projectShrs);
        system.debug('------projectShrs---size------'+projectShrs.size());

        system.debug('------accShrs---------'+accShrs);
        system.debug('------accShrs---size------'+accShrs.size());

        
        // Insert sharing records and capture save result 
        // The false parameter allows for partial processing if multiple records are passed 
        // into the operation 
        Database.SaveResult[] lsr = Database.insert(projectShrs,false);
        system.debug('------lsr ---------'+lsr);
        if(accShrs.size() > 0){
            Database.insert(accShrs,false);
        }
    }

    //Link opportunites account to project account
    public static void linkAccountToProject(List<Project__c> newProjectList){
        set<Id> opportunityIdSet = new Set<Id>();
        Map<Id, Id> oppWithAccIdsMap = new Map<Id, Id>();
        for(Project__c proj : newProjectList){
            if(proj.Opportunity__c != null){
                opportunityIdSet.add(proj.Opportunity__c);
            }
        }
        if(opportunityIdSet.size() > 0){
            for(Opportunity opp : [SELECT Id, AccountId 
                                      FROM Opportunity
                                      WHERE Id IN: opportunityIdSet]){
                oppWithAccIdsMap.put(opp.Id, opp.AccountId);
            }
        }
        if(!oppWithAccIdsMap.isEmpty()){
            for(Project__c proj : newProjectList){
                if(proj.Opportunity__c != null){
                    proj.Account__c = oppWithAccIdsMap.get(proj.Opportunity__c);
                }   
            }
        }
    }
}