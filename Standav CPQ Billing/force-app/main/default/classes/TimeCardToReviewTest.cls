@isTest
public class TimeCardToReviewTest {
        private static testMethod void test() {

            User user = new User();
            user.LastName = 'Test User';
            user.Alias = 'tu';
            user.Email = 'test@gmail.com';
            user.Username = 'test.dev@stand.com';
            user.CommunityNickname = 'test.t';
            user.User_Time_Zone__c = 'Pst';
            user.ProfileId = '00e54000000HSHG';           
            user.TimeZoneSidKey    = 'America/Denver';
            user.LocaleSidKey      = 'en_US';
            user.EmailEncodingKey  = 'UTF-8';
            user.LanguageLocaleKey = 'en_US';
            insert user;
        
            Account acc = new Account();
        	acc.Name = 'Test Account';
        	insert acc;
         
            Contact con = new Contact();
        	con.FirstName = 'test';
        	con.LastName = 'contact';
        	insert con;
        
            Project__c project = new Project__c();
            project.Name = 'Test Project';
            project.EP_Project_Manager__c = user.Id;
            project.Contact_Person__c = con.Id;
            project.Start_Date__c = Date.newInstance(2016, 12, 9);
        	project.Account__c = acc.id;
        	project.Number__c = 2;
            insert project;        
            update project;
        
            Resource__c resource = new Resource__c();
            resource.User__c = user.Id;
            resource.Contact__c = con.Id;
            insert resource;        
            update resource;
        
            Assignment__c assignment = new Assignment__c();
            assignment.Project__c = project.id;
            assignment.Resource__c = resource.id;
            assignment.Start_Date__c = Date.newInstance(2016, 12, 9);
            assignment.End_Date__c = Date.newInstance(2018, 12, 9);
            assignment.Role__c = 'Developer';
            assignment.Billing_Type__c = 'Billable';
            assignment.Assignment_Type__c = 'Long Term';  
            assignment.Budgeted_Hours__c = 40;
            insert assignment;        

            Time_Card__c card = new Time_Card__c();
        	card.Assignment__c = assignment.id;
            card.Billable_Hours_Fri__c = 4;
            card.Billable_Hours_Mon__c =4;
            card.Billable_Hours_Sat__c = 0;
            card.Billable_Hours_Sun__c = 0;
            card.Billable_Hours_Thu__c = 4;
            card.Billable_Hours_Tue__c = 4;
            card.Billable_Hours_Wed__c =4;
            card.Start_Date__c=Date.newInstance(2016, 12, 9);
            card.EP_End_Date__c=Date.newInstance(2018, 12, 9);
            //card.Status__c = 'Rejected';            
            insert card;
           
         Test.startTest();
         PageReference myVfPage = Page.Approval_Review_Page;
         myVfPage.getParameters().put('id',card.id);
         Test.setCurrentPage(myVfPage); 
         ApexPages.currentPage().setRedirect(true);

         TimeCardToReview f1 = new TimeCardToReview();  
              f1.cancel();
    
        Test.stopTest(); 
        }
    @isTest static void testreject() {
                  User user = new User();
            user.LastName = 'Test User';
            user.Alias = 'tu';
            user.Email = 'test@gmail.com';
            user.Username = 'test.dev@stand.com';
            user.CommunityNickname = 'test.t';
            user.User_Time_Zone__c = 'Pst';
            user.ProfileId = '00e54000000HSHG';           
            user.TimeZoneSidKey    = 'America/Denver';
            user.LocaleSidKey      = 'en_US';
            user.EmailEncodingKey  = 'UTF-8';
            user.LanguageLocaleKey = 'en_US';
            insert user;
        
            Account acc = new Account();
        	acc.Name = 'Test Account';
        	insert acc;
         
            Contact con = new Contact();
        	con.FirstName = 'test';
        	con.LastName = 'contact';
        	insert con;
        
            Project__c project = new Project__c();
            project.Name = 'Test Project';
            project.EP_Project_Manager__c = user.Id;
            project.Contact_Person__c = con.Id;
            project.Start_Date__c = Date.newInstance(2016, 12, 9);
        	project.Account__c = acc.id;
        	project.Number__c = 2;
            insert project;        
            update project;
        
            Resource__c resource = new Resource__c();
            resource.User__c = user.Id;
            resource.Contact__c = con.Id;
            insert resource;        
            update resource;
        
            Assignment__c assignment = new Assignment__c();
            assignment.Project__c = project.id;
            assignment.Resource__c = resource.id;
            assignment.Start_Date__c = Date.newInstance(2016, 12, 9);
            assignment.End_Date__c = Date.newInstance(2018, 12, 9);
            assignment.Role__c = 'Developer';
            assignment.Billing_Type__c = 'Billable';
            assignment.Assignment_Type__c = 'Long Term'; 
        	assignment.Budgeted_Hours__c = 40;
            insert assignment;        

            Time_Card__c card = new Time_Card__c();
        	card.Assignment__c = assignment.id;
            card.Billable_Hours_Fri__c = 4;
            card.Billable_Hours_Mon__c =4;
            card.Billable_Hours_Sat__c = 0;
            card.Billable_Hours_Sun__c = 0;
            card.Billable_Hours_Thu__c = 4;
            card.Billable_Hours_Tue__c = 4;
            card.Billable_Hours_Wed__c =4;
            card.Start_Date__c=Date.newInstance(2016, 12, 9);
            card.EP_End_Date__c=Date.newInstance(2018, 12, 9);
            card.Status__c = 'Rejected';            
            insert card;
        
         Test.startTest(); 
         PageReference myVfPage = Page.Approval_Review_Page;
         myVfPage.getParameters().put('id',card.id);
         Test.setCurrentPage(myVfPage); 
         ApexPages.currentPage().setRedirect(true);

        TimeCardToReview f1 = new TimeCardToReview();
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Approving request using Trigger');
        req1.setObjectId(card.id);
        //req1.setAction('Approve');

        // Submit on behalf of a specific submitter
        req1.setSubmitterId(user.Id); 
        
        // Submit the approval request for the account
        Approval.ProcessResult result = Approval.process(req1);
        
        // Verify the result
        System.assert(result.isSuccess());
        
        // Approve the submitted request
        // First, get the ID of the newly created item
        List<Id> newWorkItemIds = result.getNewWorkitemIds();
        
        // Instantiate the new ProcessWorkitemRequest object and populate it
        Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
        req1.setComments('Approving request using Trigger');
        //req1.setAction('Approve');
        req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        
        // Use the ID from the newly created item to specify the item to be worked
        req2.setWorkitemId(newWorkItemIds.get(0));
        
        // Submit the request for approval
        Approval.ProcessResult result2 =  Approval.process(req2);
        
        // Verify the results
        System.assert(result2.isSuccess(), 'Result Status:'+result2.isSuccess());
 
        f1.approveTimeCard();
         Test.stopTest(); 

    }
    
}