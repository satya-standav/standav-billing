@isTest
public class ClientApprovalTest {
          
    private static testMethod void test() {
            User user = new User();
            user.LastName = 'Test User';
            user.Alias = 'tu';
            user.Email = 'test@gmail.com';
            user.Username = 'test.dev@stand.com';
            user.CommunityNickname = 'test.t';
            user.User_Time_Zone__c = 'Pst';
            user.ProfileId = '00e54000000HSHG';           
            user.TimeZoneSidKey    = 'America/Denver';
            user.LocaleSidKey      = 'en_US';
            user.EmailEncodingKey  = 'UTF-8';
            user.LanguageLocaleKey = 'en_US';
            insert user;
        
            Account acc = new Account();
        	acc.Name = 'Test Account';
        	insert acc;
         
            Contact con = new Contact();
        	con.FirstName = 'test';
        	con.LastName = 'contact';
        	insert con;
        
            Project__c project = new Project__c();
            project.Name = 'Test Project';
            project.EP_Project_Manager__c = user.Id;
            project.Contact_Person__c = con.Id;
            project.Start_Date__c = Date.newInstance(2016, 12, 9);
        	project.Account__c = acc.id;
        	project.Number__c = 2;
            insert project;        
            update project;
        
            Resource__c resource = new Resource__c();
            resource.User__c = user.Id;
            resource.Contact__c = con.Id;
            insert resource;        
            update resource;
        
            Assignment__c assignment = new Assignment__c();
            assignment.Project__c = project.id;
            assignment.Resource__c = resource.id;
            assignment.Status__c = 'Approved';
            assignment.Start_Date__c = Date.newInstance(2016, 12, 9);
            assignment.End_Date__c = Date.newInstance(2018, 12, 9);
            assignment.Role__c = 'Developer';
            assignment.Billing_Type__c = 'Billable';
            assignment.Assignment_Type__c = 'Long Term'; 
            assignment.RecordType = new RecordType();
            assignment.RecordType.Name = 'Primary';
        
            insert assignment;        
            
            Time_Card__c card = new Time_Card__c();
        	card.Assignment__c = assignment.id;
            card.Billable_Hours_Fri__c = 4;
            card.Billable_Hours_Mon__c =4;
            card.Billable_Hours_Sat__c = 0;
            card.Billable_Hours_Sun__c = 0;
            card.Billable_Hours_Thu__c = 4;
            card.Billable_Hours_Tue__c = 4;
            card.Billable_Hours_Wed__c =4;
            card.Status__c = 'Approved';            
            insert card;        

        Test.startTest();
               PageReference myVfPage = Page.ClientApprovalPage;
               myVfPage.getParameters().put('id',card.id);
               Test.setCurrentPage(myVfPage); 
               String id = ApexPages.currentPage().getParameters().get('id');
        
               List<ClentApprovalProcessPageController.timeCardWrapperForApprovals> newTimeCardWrpper;
               ClentApprovalProcessPageController.timeCardWrapperForApprovals testWrap1=new ClentApprovalProcessPageController.timeCardWrapperForApprovals();
               ClentApprovalProcessPageController f1 = new ClentApprovalProcessPageController();
               f1.massApprove();
               f1.massReject();
               f1.reDirect();
               //f1.fetchApprovedTimeCards();
               f1.approveTimeCard();
               //f1.rejectRecord();
               testWrap1.relatedTimeCard= card; 
               f1.timeCardWrapperForApprovalsList.add(testWrap1);
        Test.stopTest();}
          
    private static testMethod void test1() {
            User user = new User();
            user.LastName = 'Test User';
            user.Alias = 'tu';
            user.Email = 'test@gmail.com';
            user.Username = 'test.dev@stand.com';
            user.CommunityNickname = 'test.t';
            user.User_Time_Zone__c = 'Pst';
            user.ProfileId = '00e54000000HSHG';           
            user.TimeZoneSidKey    = 'America/Denver';
            user.LocaleSidKey      = 'en_US';
            user.EmailEncodingKey  = 'UTF-8';
            user.LanguageLocaleKey = 'en_US';
            insert user;
        
            Account acc = new Account();
        	acc.Name = 'Test Account';
        	insert acc;
         
            Contact con = new Contact();
        	con.FirstName = 'test';
        	con.LastName = 'contact';
        	insert con;
        
            Project__c project = new Project__c();
            project.Name = 'Test Project';
            project.EP_Project_Manager__c = user.Id;
            project.Contact_Person__c = con.Id;
            project.Start_Date__c = Date.newInstance(2016, 12, 9);
        	project.Account__c = acc.id;
        	project.Number__c = 2;
            insert project;        
            update project;
        
            Resource__c resource = new Resource__c();
            resource.User__c = user.Id;
            resource.Contact__c = con.Id;
            insert resource;        
            update resource;
        
            Assignment__c assignment = new Assignment__c();
            assignment.Project__c = project.id;
            assignment.Resource__c = resource.id;
            assignment.Status__c = 'Approved';
            assignment.Start_Date__c = Date.newInstance(2016, 12, 9);
            assignment.End_Date__c = Date.newInstance(2018, 12, 9);
            assignment.Role__c = 'Developer';
            assignment.Billing_Type__c = 'Billable';
            assignment.Assignment_Type__c = 'Long Term'; 
            assignment.RecordType = new RecordType();
            assignment.RecordType.Name = 'Primary';
        
            insert assignment;        
            
            Time_Card__c card = new Time_Card__c();
        	card.Assignment__c = assignment.id;
            card.Billable_Hours_Fri__c = 4;
            card.Billable_Hours_Mon__c =4;
            card.Billable_Hours_Sat__c = 0;
            card.Billable_Hours_Sun__c = 0;
            card.Billable_Hours_Thu__c = 4;
            card.Billable_Hours_Tue__c = 4;
            card.Billable_Hours_Wed__c =4;
            card.Status__c = 'Approved';            
            insert card;        

        Test.startTest();
               PageReference myVfPage = Page.ClientApprovalPage;
               myVfPage.getParameters().put('id',card.id);
               Test.setCurrentPage(myVfPage); 
               String id = ApexPages.currentPage().getParameters().get('id');
        
               List<ClentApprovalProcessPageController.timeCardWrapperForApprovals> newTimeCardWrpper;
               ClentApprovalProcessPageController.timeCardWrapperForApprovals testWrap1=new ClentApprovalProcessPageController.timeCardWrapperForApprovals();
               ClentApprovalProcessPageController f1 = new ClentApprovalProcessPageController();

               f1.rejectRecord();

        Test.stopTest();}
    
          
    private static testMethod void test2() {
            User user = new User();
            user.LastName = 'Test User';
            user.Alias = 'tu';
            user.Email = 'test@gmail.com';
            user.Username = 'test.dev@stand.com';
            user.CommunityNickname = 'test.t';
            user.User_Time_Zone__c = 'Pst';
            user.ProfileId = '00e54000000HSHG';           
            user.TimeZoneSidKey    = 'America/Denver';
            user.LocaleSidKey      = 'en_US';
            user.EmailEncodingKey  = 'UTF-8';
            user.LanguageLocaleKey = 'en_US';
            insert user;
        
            Account acc = new Account();
        	acc.Name = 'Test Account';
        	insert acc;
         
            Contact con = new Contact();
        	con.FirstName = 'test';
        	con.LastName = 'contact';
        	insert con;
        
            Project__c project = new Project__c();
            project.Name = 'Test Project';
            project.EP_Project_Manager__c = user.Id;
            project.Contact_Person__c = con.Id;
            project.Start_Date__c = Date.newInstance(2016, 12, 9);
        	project.Account__c = acc.id;
        	project.Number__c = 2;
            insert project;        
            update project;
        
            Resource__c resource = new Resource__c();
            resource.User__c = user.Id;
            resource.Contact__c = con.Id;
            insert resource;        
            update resource;
        
            Assignment__c assignment = new Assignment__c();
            assignment.Project__c = project.id;
            assignment.Resource__c = resource.id;
            assignment.Status__c = 'Approved';
            assignment.Start_Date__c = Date.newInstance(2016, 12, 9);
            assignment.End_Date__c = Date.newInstance(2018, 12, 9);
            assignment.Role__c = 'Developer';
            assignment.Billing_Type__c = 'Billable';
            assignment.Assignment_Type__c = 'Long Term'; 
            assignment.RecordType = new RecordType();
            assignment.RecordType.Name = 'Primary';
        
            insert assignment;        
            
            Time_Card__c card = new Time_Card__c();
        	card.Assignment__c = assignment.id;
            card.Billable_Hours_Fri__c = 4;
            card.Billable_Hours_Mon__c =4;
            card.Billable_Hours_Sat__c = 0;
            card.Billable_Hours_Sun__c = 0;
            card.Billable_Hours_Thu__c = 4;
            card.Billable_Hours_Tue__c = 4;
            card.Billable_Hours_Wed__c =4;
            card.Status__c = 'Approved';            
            insert card;        

        Test.startTest();
               PageReference myVfPage = Page.ClientApprovalPage;
               myVfPage.getParameters().put('id',con.id);
               Test.setCurrentPage(myVfPage); 
               String id = ApexPages.currentPage().getParameters().get('id');
        
               List<ClentApprovalProcessPageController.timeCardWrapperForApprovals> newTimeCardWrpper;
               ClentApprovalProcessPageController.timeCardWrapperForApprovals testWrap1=new ClentApprovalProcessPageController.timeCardWrapperForApprovals();
               ClentApprovalProcessPageController f1 = new ClentApprovalProcessPageController();
               f1.fetchApprovedTimeCards();
               testWrap1.relatedTimeCard=card;
               f1.timeCardWrapperForApprovalsList.add(testWrap1);
        Test.stopTest();}
}