public class TimeCardReviewSample {
    public Time_Card__c timeCardToReview{get; set;}
    public List<Date> weekDatesList {get;set;}
    public String oppIdToWin {get; set;}
    public String relatedClientEmail {get; set;}
    public String clientId{get; set;}
    public Boolean showClientApprovalButton {get; set;}
    public string renderAs{get;set;}


    public TimeCardReviewSample(){
    
        relatedClientEmail = this.relatedClientEmail;
        showClientApprovalButton = false;
        weekDatesList = new List<Date>();
        oppIdToWin = System.currentPageReference().getParameters().get('id');
        system.debug('-------------------'+System.currentPageReference().getParameters().get('id'));
        timeCardToReview = [SELECT Id, Assignment__r.Project__c, Name, Assignment__r.EP_Shadow_Resource__c, Assignment__r.Resource__c,
                            Type__c, EP_End_Date__c, Billable_Hours_Fri__c, EP_Notes__c, EP_Approval_Date__c, Assignment__r.Project__r.Contact_Person__c,
                            EP_Total_Number_of_Billable_Hours__c,lastmodifieddate, EP_Total_Number_of_Unbillable_Hours__c, Assignment__r.Project__r.Contact_Person__r.Email,
                            EP_Rejected_Date__c, Submitted_On__c,Assignment__r.Project__r.EP_Project_Manager__r.Name,Assignment__r.Project__r.Contact_Person__r.Name, Project_Manager__c, Billable_Hours_Mon__c, Manager_Approval_Comments__c,
                            Billable_Hours_Sat__c, Assignment__c, Status__c, Start_Date__c, Billable_Hours_Sun__c, 
                            Billable_Hours_Thu__c, Billable_Hours_Tue__c, Billable_Hours_Wed__c, UnBillable_Hours_Fri__c, 
                            UnBillable_Hours_Mon__c, UnBillable_Hours_Sat__c, UnBillable_Hours_Sun__c, UnBillable_Hours_Thu__c, 
                            UnBillable_Hours_Tue__c, UnBillable_Hours_Wed__c, Assignment__r.Project__r.Name,
                            Assignment__r.Resource__r.User__r.Name ,Assignment__r.Resource__r.Name, Assignment__r.RecordType.Name, Assignment__r.Is_Primary_Resource_Contributing__c,
                            X1st_Approver__c, X2nd_Approver__c, X3rd_Approver__c, X4th_Approver__c, X5th_Approver__c, Approval_Status__c,
                            Assignment__r.EP_Shadow_Resource__r.User__r.Name,  Assignment__r.Role__c, 
                            Monday_Work_Details__c,Tuesday_Work_Details__c,Wednesday_Work_Details__c,Thursday_Work_Details__c,Friday_Work_Details__c, 
                            Saturday_Work_Details__c,Sunday_Work_Details__c,Show_Comments__c,
                            (SELECT Id, Name FROM attachments)
                            FROM Time_Card__c
                            WHERE Id =: System.currentPageReference().getParameters().get('id') Limit 1];
        timeCardToReview.UnBillable_Hours_Mon__c = timeCardToReview.UnBillable_Hours_Mon__c == null ? 0.0 : timeCardToReview.UnBillable_Hours_Mon__c;
        timeCardToReview.UnBillable_Hours_Tue__c = timeCardToReview.UnBillable_Hours_Tue__c == null ? 0.0 : timeCardToReview.UnBillable_Hours_Tue__c;
        timeCardToReview.UnBillable_Hours_Wed__c = timeCardToReview.UnBillable_Hours_Wed__c == null ? 0.0 : timeCardToReview.UnBillable_Hours_Wed__c;
        timeCardToReview.UnBillable_Hours_Thu__c = timeCardToReview.UnBillable_Hours_Thu__c == null ? 0.0 : timeCardToReview.UnBillable_Hours_Thu__c;
        timeCardToReview.UnBillable_Hours_Fri__c = timeCardToReview.UnBillable_Hours_Fri__c == null ? 0.0 : timeCardToReview.UnBillable_Hours_Fri__c;
        timeCardToReview.UnBillable_Hours_Sat__c = timeCardToReview.UnBillable_Hours_Sat__c == null ? 0.0 : timeCardToReview.UnBillable_Hours_Sat__c;
        timeCardToReview.UnBillable_Hours_Sun__c = timeCardToReview.UnBillable_Hours_Sun__c == null ? 0.0 : timeCardToReview.UnBillable_Hours_Sun__c;
        
        timeCardToReview.Billable_Hours_Mon__c = timeCardToReview.Billable_Hours_Mon__c == null ? 0.0 : timeCardToReview.Billable_Hours_Mon__c;
        timeCardToReview.Billable_Hours_Tue__c = timeCardToReview.Billable_Hours_Tue__c == null ? 0.0 : timeCardToReview.Billable_Hours_Tue__c;
        timeCardToReview.Billable_Hours_Wed__c = timeCardToReview.Billable_Hours_Wed__c == null ? 0.0 : timeCardToReview.Billable_Hours_Wed__c;
        timeCardToReview.Billable_Hours_Thu__c = timeCardToReview.Billable_Hours_Thu__c == null ? 0.0 : timeCardToReview.Billable_Hours_Thu__c;
        timeCardToReview.Billable_Hours_Fri__c = timeCardToReview.Billable_Hours_Fri__c == null ? 0.0 : timeCardToReview.Billable_Hours_Fri__c;
        timeCardToReview.Billable_Hours_Sat__c = timeCardToReview.Billable_Hours_Sat__c == null ? 0.0 : timeCardToReview.Billable_Hours_Sat__c;
        timeCardToReview.Billable_Hours_Sun__c = timeCardToReview.Billable_Hours_Sun__c == null ? 0.0 : timeCardToReview.Billable_Hours_Sun__c;
        clientId = timeCardToReview.Assignment__r.Project__r.Contact_Person__c;
        system.debug('------timeCardToReview----------'+timeCardToReview);
        system.debug('------timeCardToReview.Start_Date__c----------'+timeCardToReview.Start_Date__c);
        system.debug('------timeCardToReview.EP_End_Date__c----------'+timeCardToReview.EP_End_Date__c);
        Date dtvaries = timeCardToReview.Start_Date__c;
        system.debug('------dtvaries ----------'+dtvaries );
        while(dtvaries <= timeCardToReview.EP_End_Date__c){
            weekDatesList.add(dtvaries);
            dtvaries = dtvaries.addDays(1);
        } 
        system.debug('------1 client----------'+timeCardToReview.Assignment__r.Project__r.Contact_Person__c);
        if(timeCardToReview.Assignment__r.Project__r.Contact_Person__c == null){
            showClientApprovalButton = false;
        }else if(timeCardToReview.Approval_Status__c == null && timeCardToReview.X2nd_Approver__c != null && 
             String.valueOf(timeCardToReview.X2nd_Approver__c).substring(0,3) != String.valueOf(userInfo.getUserID()).substring(0,3)){
             system.debug('-----timeCardSubmitted.Approval_Status__c-------'+timeCardToReview.Approval_Status__c);
             system.debug('------timeCardSubmitted.X2nd_Approver__c------'+timeCardToReview.X2nd_Approver__c);
             showClientApprovalButton = True;
         }else if(timeCardToReview.Approval_Status__c == 'Approver 1 Approved' && (timeCardToReview.X3rd_Approver__c == null || (timeCardToReview.X3rd_Approver__c != null && 
             String.valueOf(timeCardToReview.X3rd_Approver__c).substring(0,3) != String.valueOf(userInfo.getUSerID()).substring(0,3)))){
             showClientApprovalButton = True;
         }else{
             showClientApprovalButton = false;
         }
    }
    
    //approve MEthod
    public PageReference approveTimeCard(){
        oppIdToWin = System.currentPageReference().getParameters().get('id');
        system.debug('----------oppIdToWin----------'+oppIdToWin); 
        
        List<Time_Card__c> approvedTimeCardList = new List<Time_Card__c>();
        Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
        req.setComments('Approving request using Trigger');
        req.setAction('Approve');     
        Id workItemId = getWorkItemId(oppIdToWin); 
        if(workItemId == null){
            //timeCardToApprove.addError('Error Occured in Trigger');
            Time_Card__c timeCardToApprove = [SELECT Id, Project_Manager__c, X1st_Approver__c, Manager_Approval_Comments__c,
                                              X2nd_Approver__c, X3rd_Approver__c, X4th_Approver__c,
                                              X5th_Approver__c, Approval_Status__c, Status__c
                                              FROM Time_Card__c WHERE Id =: oppIdToWin LIMIT 1];
            system.debug('----------timeCardToApprove --------'+timeCardToApprove );
            if((timeCardToApprove.Project_Manager__c == timeCardToApprove.X1st_Approver__c ||
               timeCardToApprove.Project_Manager__c == timeCardToApprove.X2nd_Approver__c||
               timeCardToApprove.Project_Manager__c == timeCardToApprove.X3rd_Approver__c ||
               timeCardToApprove.Project_Manager__c == timeCardToApprove.X4th_Approver__c ||
               timeCardToApprove.Project_Manager__c == timeCardToApprove.X5th_Approver__c) && 
               timeCardToApprove.Approval_Status__c != null){
               system.debug('----  timeCardToApprove Status__c---------'+timeCardToApprove.Status__c);
               timeCardToApprove.Status__c = 'Approved';
               timeCardToApprove.Manager_Approval_Comments__c = timeCardToReview.Manager_Approval_Comments__c;
               update timeCardToApprove;
               system.debug('----timeCardToApprove Status__c---1------'+timeCardToApprove.Status__c); 
           }
        }else{
            system.debug('-----------workItemId-------'+workItemId);
            req.setWorkitemId(workItemId);
            Approval.ProcessResult result =  Approval.process(req);
        }
        approvedTimeCardList.add(timeCardToReview);
        ApprovalProcessFlowController.calculateApprovalRule(approvedTimeCardList, null);
        PageReference pr = new PageReference('/apex/ApprovalProcessPage');
        pr.setRedirect(true);
        return pr;
    }
    
    //get approval history
    /**
    *Get ProcessInstanceWorkItemId using SOQL
    **/
    public static Id getWorkItemId(Id targetObjectId){
        system.debug('-----------targetObjectId-------'+targetObjectId);
        Id workItemId = null;
        system.debug('-------1----workItemId-------'+workItemId);
        system.debug('-------1----workItem-------'+[Select p.Id, p.ProcessInstance.TargetObjectId from ProcessInstanceWorkitem p]);
        for(ProcessInstanceWorkitem workItem  : [Select p.Id from ProcessInstanceWorkitem p where p.ProcessInstance.TargetObjectId =: targetObjectId]){
            system.debug('-----------workItem-------'+workItem);
            workItemId  =  workItem.Id;
        }
        return workItemId;
    }
    
    //Reject approval
    public PageReference rejectRecord()
    {
        oppIdToWin = System.currentPageReference().getParameters().get('id');
        system.debug('------oppIdToWin -------'+oppIdToWin );
        Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
        req.setComments('Rejected request using Trigger');
        req.setAction('Reject');
        Id workItemId = getWorkItemId(oppIdToWin); 
        if(workItemId == null){
            //oppIdToWin.addError('Error Occured in Trigger');
            Time_Card__c timeCardToApprove = [SELECT Id, Project_Manager__c, X1st_Approver__c, 
                                              X2nd_Approver__c, X3rd_Approver__c, X4th_Approver__c,
                                              X5th_Approver__c, Approval_Status__c, Status__c, Manager_Approval_Comments__c
                                              FROM Time_Card__c WHERE Id =: oppIdToWin LIMIT 1];
            system.debug('----------timeCardToApprove --------'+timeCardToApprove );
            if((timeCardToApprove.Project_Manager__c == timeCardToApprove.X1st_Approver__c ||
               timeCardToApprove.Project_Manager__c == timeCardToApprove.X2nd_Approver__c||
               timeCardToApprove.Project_Manager__c == timeCardToApprove.X3rd_Approver__c ||
               timeCardToApprove.Project_Manager__c == timeCardToApprove.X4th_Approver__c ||
               timeCardToApprove.Project_Manager__c == timeCardToApprove.X5th_Approver__c) && 
               timeCardToApprove.Approval_Status__c != null){
               system.debug('----  timeCardToApprove Status__c---------'+timeCardToApprove.Status__c);
               timeCardToApprove.Status__c = 'Draft';
               timeCardToApprove.Manager_Approval_Comments__c = timeCardToReview.Manager_Approval_Comments__c;
               update timeCardToApprove;
               system.debug('----timeCardToApprove Status__c---1------'+timeCardToApprove.Status__c);
           } 
        }
        else{
            req.setWorkitemId(workItemId);
            Approval.ProcessResult result =  Approval.process(req);
        }
        PageReference pr = new PageReference('/apex/ApprovalProcessPage');
        pr.setRedirect(true);
        return pr;
    }    
    

    //approve MEthod
    public PageReference approveAndSendToClient(){    
        
        oppIdToWin = System.currentPageReference().getParameters().get('id');   
        Set<Id> timeCardIdSet = new Set<Id>();
        timeCardIdSet.add(oppIdToWin);
             
        Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
        req.setComments('Approving request using Trigger');
        req.setAction('Approve');  
        Id workItemId = getWorkItemId(oppIdToWin);    
        Time_Card__c timeCardToApprove = [SELECT Id FROM Time_Card__c WHERE Id =: oppIdToWin] ;    
        req.setWorkitemId(workItemId);
        Approval.ProcessResult result =  Approval.process(req);
        List<Time_Card__c> approvedTimeCardsList = new List<Time_Card__c>();
        approvedTimeCardsList.add(timeCardToApprove);
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail =  new Messaging.SingleEmailMessage();
        List<String> sendTo = new List<String>{relatedClientEmail};
        mail.setToAddresses(sendTo);
        
         mail.setSubject('Time Card for Approval');
         String body = '<html lang="ja"><body>'+
           '<br><br>'+'Dear Approver'+
           '<br><br>'+'There are timesheets submitted by the team members pending your approval. Please review and take necessary action.'+
           '<br><br>'+'You can find the details in the link given below.'+
           '<br><br> <a href=https://devemp-standav.cs40.force.com/clientpage/ClientApprovalPage?clientId='+clientId+'&id='+oppIdToWin+'>Take Action Now.</a>'+
                            '<br><br>'+'Kind regards,'+
                            '<br>'+'Standav';
        mail.setHtmlBody(body);
        mails.add(mail);
        Messaging.sendEmail(mails);
        PageReference pr = new PageReference('/apex/ApprovalProcessPage');
        pr.setRedirect(true);
        return pr;
    }
    
    public PageReference cancel(){
        PageReference pr = new PageReference('/apex/ApprovalProcessPage');
        pr.setRedirect(true);
        return pr;
    }

}