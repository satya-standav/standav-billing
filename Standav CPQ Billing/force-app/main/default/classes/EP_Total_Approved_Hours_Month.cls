global class EP_Total_Approved_Hours_Month implements Database.Batchable<sObject>
{    
    global Database.QueryLocator start(Database.BatchableContext BC)
    { 
        //String approvedStatus = 'Approved';        
        return Database.getQueryLocator([Select Project__r.Name,Resource__r.User__r.Name,Name,(Select Status__c,
        Start_Date__c, EP_Total_Number_of_Billable_Hours__c, EP_Total_Number_of_Unbillable_Hours__c,Approved_Hours_Weekly__c from
        Time_Cards__r WHERE Status__c != 'Rejected' AND Start_Date__c = LAST_N_WEEKS:4),Approved_Hours__c from Assignment__c]);
    }
    global void execute(Database.BatchableContext BC, List<Assignment__c> assignmentList)
    {
        for(Assignment__c assignment : assignmentList){
            Double totalHoursapproved = 0;                   
            for(Time_Card__c tc : assignment.Time_Cards__r){  
               if(tc.Status__c == 'Approved'){
                totalHoursapproved+=tc.EP_Total_Number_of_Billable_Hours__c;
                }
            }
            assignment.Monthly_Approved_Hours__c = totalHoursapproved;
          
        }
              
        database.update(assignmentList,false);
    }
    global void finish(Database.BatchableContext BC) {        
            List<Assignment__c> asslist = [Select Id,Approved_Hours__c,Monthly_Approved_Hours__c,Project__r.Name,Resource__r.User__r.Name,Name,(Select Status__c,
             Start_Date__c, EP_Total_Number_of_Billable_Hours__c,Approved_Hours_Weekly__c, EP_Total_Number_of_Unbillable_Hours__c from
            Time_Cards__r Where Status__c != 'Rejected' AND Start_Date__c = LAST_N_WEEKS:4) from Assignment__c];            
            String csvContent = '';
            String csvHeader = 'Sr.No.,Project Name,Resource Name,Week Starting,Hours Submitted,Approved Hours Weekly,Status\n';
            String csvBody = '';
            Integer srNo = 1;           
            for(Assignment__c assign : asslist){
                List<Time_Card__c> timeCardList = assign.Time_Cards__r;
                if(timeCardList!=null && !timeCardList.isEmpty()) {                
                    for(Time_Card__c tc : timeCardList){
                        csvBody+= srNo + ',' + assign.Project__r.Name + ',' + assign.Resource__r.User__r.Name +',' + tc.Start_Date__c.format()+','+tc.EP_Total_Number_of_Billable_Hours__c + ','+tc.Approved_Hours_Weekly__c +',' +tc.Status__c + '\n';        
                         srNo++;
                    }
                   
                }
            }            
            csvContent += csvHeader;
            csvContent += csvBody;        
            Blob csvFile = Blob.valueOf(csvCOntent);
            String csvFileName = 'Monthly Submitted Hours - '+ Datetime.now().format('MMM') + Datetime.now().year() + '.csv';
            List<User> userList = new List<User>();
            userList = [SELECT Id,Email,IsActive FROM User WHERE Profile.Name = 'Operations(Community)' AND IsActive = True] ;
            List<String> toAddresses = new List<String>();
           
        	for(User u : userList)
            {           
               toAddresses.add(u.Email);
            }  

            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            //String[] toAddresses = new String[] {'rajesh.g@standav.com'};
            mail.setToAddresses(toAddresses);
            mail.setSubject('Monthly Approved Hours');
            mail.setPlainTextBody('Please find below the attached Report for Approved hours from the Last Month');        
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName(csvFileName);
            efa.setBody(csvFile);
            mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
  
}