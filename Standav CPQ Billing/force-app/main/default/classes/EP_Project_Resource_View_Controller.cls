public class EP_Project_Resource_View_Controller 
{
    public Project__c projectvar{get;set;}
    public list<Assignment__c> assignmentvar{get;set;}
   
    ID ProjectId;
    public List<wrapperView> ref{get;set;} 
    
    
    public EP_Project_Resource_View_Controller(ApexPages.StandardController sc)
    {
        ref= new List<wrapperView>();
        ProjectId = apexpages.currentpage().getparameters().get('id');
       	system.debug('Page Name'+apexpages.currentPage(). getParameters().get('id'));
        if(ProjectId!=null){
            projectvar=[SELECT ID,Name FROM Project__c WHERE ID=:ProjectId LIMIT 1];
            assignmentvar = [Select ID,Project__c,Resource__c,(SELECT Status__c, EP_Total_Number_of_Billable_Hours__c, EP_Total_Number_of_Unbillable_Hours__c from Time_Cards__r),resource__r.User__r.Name, Billing_Type__c,Recordtype.name,resource__r.Resource_Type__c,resource__r.Contact__c,resource__r.User__c FROM Assignment__c WHERE Project__c=:projectid];
            
            for(Assignment__c assign : assignmentvar)
            {
                wrapperView wc = new wrapperView();
                wc.burnedhoursbillable=0;
                wc.burnedhoursnonbillable=0;
                wc.approvedhoursbillable=0;
                wc.approvedhoursnonbillable=0;
                system.debug('=====wc==='+wc);
                wc.resourceName = assign.resource__r.User__r.Name;
                wc.recordtype=assign.Recordtype.name;
                wc.billingtype=assign.Billing_Type__c;
                 if(wc.billingType == 'Billable'){
                    wc.billingTypeCheck = true;
                }
                else{
                    wc.billingTypeCheck = false;
                }
                if(assign.Time_Cards__r.size() > 0){
                    for(Time_Card__c timeCard:assign.Time_Cards__r)
                    { 
                        if(assign.billing_type__c == 'Billable' && timeCard.Status__c=='Approved')
                        {
                            wc.approvedhoursbillable+=timecard.EP_Total_Number_of_Billable_Hours__c != null ? timecard.EP_Total_Number_of_Billable_Hours__c : 0.0;
                        }
                        else if(assign.billing_type__c == 'Non Billable' && timeCard.Status__c=='Approved')
                        {
                            wc.approvedhoursnonbillable+=timecard.EP_Total_Number_of_UnBillable_Hours__c != null ? timecard.EP_Total_Number_of_UnBillable_Hours__c : 0.0;
                        }
                        else if(assign.billing_type__c == 'Billable' && timeCard.Status__c!='Approved')
                        {
                            system.debug('burnedhoursbillable=' + timecard.EP_Total_Number_of_Billable_Hours__c);
                            system.debug(wc.burnedhoursbillable);
                            wc.burnedhoursbillable+=(timeCard.EP_Total_Number_of_Billable_Hours__c != null ? timecard.EP_Total_Number_of_Billable_Hours__c : 0.0);
                            
                        }
                        else if(assign.billing_type__c == 'Non Billable' && timeCard.Status__c!='Approved')
                        {
                            wc.burnedhoursnonbillable+=timecard.EP_Total_Number_of_UnBillable_Hours__c != null ? timecard.EP_Total_Number_of_UnBillable_Hours__c : 0.0;
                        }
                       // ref.add(wc);  
                    }
                    ref.add(wc); 
                }
            }
        }
    }
    
    public class wrapperView{ 
        public string resourceName{get;set;} 
        public string recordType{get;set;} 
        public string billingtype{get;set;}
        public decimal burnedhoursbillable{get;set;}
        public decimal burnedhoursnonbillable{get;set;}
        public decimal approvedhoursbillable{get;set;}
        public decimal approvedhoursnonbillable{get;set;}
        public boolean billingTypeCheck {get; set;}
        
       
           

         
    }
}