/**
*  @Class Name: TimeCardEntry
*  @Description: Handles all functionality required on ...... VF PAGE
*  @Company: Standav
*  @CreatedDate: 22/02/2019
*  Change Log
*  -----------------------------------------------------------
*  Developer           Modification Date           Comments
*  -----------------------------------------------------------
*
*/
public with sharing class TimeCardEntry
{    /* when work is started on this, remove top level comment and reset function level end comment tag
    /*Start Variables* /
    public List<wrapAssignments> lstwrapAssignments         {get;set;}
    public wrapTotalHrs wrapWeeklyHrs                       {get;set;}
    public Decimal weekTotalHrs                             {get;set;}
    public Date dtStartDate                                 {get;set;}
    public Date dtEndDate                                   {get;set;}
    public Timecard__c objTimCardForResource                {get;set;}
    public List<Date> lstWeekDates                          {get;set;}
    public Boolean isError                                  {get;set;}
    public Boolean showTotalHours                           {get;set;}
    public Boolean selectAll                                {get;set;}
    public Date dtSelected;
    /*End Variables* /

    /*Start Constructor* /
    public TimeCardEntry()
    {
        objTimCardForResource = new Timecard__c();
        showTotalHours = false;
        List<Resources__c> lstResource = [SELECT Id FROM Resources__c WHERE User__c =: UserInfo.getUserId() AND Active__c = true];
        if(lstResource.size()>0)
        {
            isError = false;
            objTimCardForResource.Resource__c = lstResource[0].Id;
            dtSelected = Date.today();
            fetchTimeCards();
        }
        else
        {
            isError = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'No Resource record found for current user.'));
        }

    }
    /*End Constructor*/


    /*
        Method Name         : fetchTimeCards
        param               : N/A
        Description         : method to fetch the timecard based on resource and selected week
    * /
    public void fetchTimeCards()
    {
        lstwrapAssignments = new List<wrapAssignments>();
        map<String, Timecard__c> mapUniqueToTimeCard = new map<String, Timecard__c>();
        lstWeekDates = new list<Date>();
        dtStartDate = dtSelected.toStartofWeek();
        dtEndDate = dtStartDate.addDays(6);
        Date dtvaries = dtStartDate;
        while(dtvaries <= dtEndDate)
        {
            lstWeekDates.add(dtvaries);
            dtvaries = dtvaries.addDays(1);
        }
        set<Id> setAssignmentIds = new set<Id>();
        system.debug(dtStartDate+'===========dtStartDate==='+dtEndDate);
        for(Timecard__c objTimeCard: [SELECT Id, Project__c , Assignment__c, Assignment__r.Date__c, Case__c, Project__r.RecordType.DeveloperName, Assignment__r.Hours__c,
                                             Billable__c, Date__c, Hours__c, Resource__c, Notes__c
                                        FROM Timecard__c
                                       WHERE Date__c >=: dtStartDate AND  Date__c <=: dtEndDate AND Resource__c  =: objTimCardForResource.Resource__c AND Assignment__c != null])
        {
            system.debug('===========objTimeCard==='+objTimeCard);
            setAssignmentIds.add(objTimeCard.Assignment__c);
            system.debug('============objTimeCard.Assignment__c objTimeCard.Date__c==='+objTimeCard.Assignment__c +''+objTimeCard.Date__c);
            mapUniqueToTimeCard.put(objTimeCard.Assignment__c +''+objTimeCard.Date__c,  objTimeCard);
        }
        List<Assignment__c> lstAssignments = [SELECT Id, Project__c, Project__r.RecordType.DeveloperName, Hours__c, Case__c, Date__c, Resource_Requirement_Details__c
                                                FROM Assignment__c WHERE (Id IN: setAssignmentIds OR (Date__c >=: dtStartDate AND  Date__c <=: dtEndDate))
                                                AND Resource__c  =: objTimCardForResource.Resource__c];
        for(Assignment__c objAssignment: lstAssignments)
        {
            wrapAssignments objwrapAssignments = new wrapAssignments();
            objwrapAssignments.strRequirement = objAssignment.Resource_Requirement_Details__c;
            objwrapAssignments.isSelected = false;
            Boolean isExistingCard = false;
            objwrapAssignments.objTimeCardAssignment.Case__c = objAssignment.Case__c ;
            objwrapAssignments.objTimeCardAssignment.Assignment__c = objAssignment.Id;
            objwrapAssignments.objTimeCardAssignment.Project__c = objAssignment.Project__c;
            objwrapAssignments.strSelectedProjectRecordTypeName = objAssignment.Project__r.RecordType.DeveloperName;
            objwrapAssignments.decExpectedHrs = objAssignment.Hours__c;
            objwrapAssignments.expectedDate = objAssignment.Date__c;
            for(Date dtWeek: lstWeekDates)
            {
                system.debug('============dtWeek==='+dtWeek);
                system.debug('============objAssignment.Id dtWeek==='+objAssignment.Id+''+dtWeek);
                if(mapUniqueToTimeCard.containsKey(objAssignment.Id+''+dtWeek))
                {
                    Timecard__c objTimeCard = mapUniqueToTimeCard.get(objAssignment.Id+''+dtWeek);
                    system.debug('============objTimeCard==='+objTimeCard);
                    objwrapAssignments.objTimeCardAssignment.Notes__c = objTimeCard.Notes__c;
                    // objwrapAssignments.objTimeCardAssignment.Case__c = objTimeCard.Case__c ;
                    objwrapAssignments.mapWrapTimeEntry.put(dtWeek,  objTimeCard);
                    objwrapAssignments.objTimeCardAssignment.Billable__c = objTimeCard.Billable__c;
                    isExistingCard = true;
                }
                else
                {
                    system.debug('============else===');
                    Timecard__c objTimeCard = new Timecard__c();
                    objTimeCard.Assignment__c = objAssignment.Id;
                    objTimeCard.Date__c = dtWeek;
                    objTimeCard.Resource__c = objTimCardForResource.Resource__c;
                    objTimeCard.Project__c = objAssignment.Project__c;
                    if(dtWeek == objAssignment.Date__c){
                        objTimeCard.Hours__c = objAssignment.Hours__c;
                    }
                    else
                    {
                        objTimeCard.Hours__c = 0;
                    }
                    // objwrapAssignments.objTimeCardAssignment.Case__c = objAssignment.Case__c ;

                    objwrapAssignments.mapWrapTimeEntry.put(dtWeek, objTimeCard);
                }
            }
            if(!isExistingCard) objwrapAssignments.objTimeCardAssignment.Billable__c = true;
            lstwrapAssignments.add(objwrapAssignments);
        }
        calculateTotalHrs();
    }

    private void calculateTotalHrs(){
        wrapWeeklyHrs = new wrapTotalHrs();
        weekTotalHrs = 0;
        for(wrapAssignments objWrapAssign: lstwrapAssignments)
        {
            if(objWrapAssign.objTimeCardAssignment.Assignment__c != null  && objWrapAssign.objTimeCardAssignment.Project__c != null)
            {
                List<Timecard__c> timeCardList = objWrapAssign.mapWrapTimeEntry.values();
                for (Integer i = 0; i <timeCardList.size(); i++)
                {

                    if(timeCardList[i].Hours__c == null)
                    {
                        timeCardList[i].Hours__c = 0;
                    }
                    if(wrapWeeklyHrs.mapDayTotalHrs.containsKey(timeCardList[i].Date__c))
                    {
                        Decimal totalHrs = wrapWeeklyHrs.mapDayTotalHrs.get(timeCardList[i].Date__c);
                        totalHrs += timeCardList[i].Hours__c;
                        wrapWeeklyHrs.mapDayTotalHrs.put(timeCardList[i].Date__c, totalHrs);
                    }
                    else
                    {
                        wrapWeeklyHrs.mapDayTotalHrs.put(timeCardList[i].Date__c, timeCardList[i].Hours__c);
                    }
                }
            }
        }
        for(Decimal hrs :wrapWeeklyHrs.mapDayTotalHrs.values())
        {
            weekTotalHrs += hrs;
        }

        for(Date wkDate :lstWeekDates)
        {
            if(!wrapWeeklyHrs.mapDayTotalHrs.containsKey(wkDate))
            {
                wrapWeeklyHrs.mapDayTotalHrs.put(wkDate, 0);
            }
        }
    }

    /*
        Method Name         : addNewTimeEntry
        param               : N/A
        Description         : method to add a new time card and assign the default date and resource
    * /
    public void addNewTimeEntry()
    {
        wrapAssignments objwrapAssignments = new wrapAssignments();
        objwrapAssignments.isSelected = false;
        objwrapAssignments.objTimeCardAssignment.Billable__c = true;

        for(Date dtWeek: lstWeekDates)
        {
            Timecard__c objTimeCard = new Timecard__c();
            objTimeCard.Date__c = dtWeek;
            objTimeCard.Resource__c = objTimCardForResource.Resource__c;
            objwrapAssignments.mapWrapTimeEntry.put(dtWeek, objTimeCard);
        }
        lstwrapAssignments.add(objwrapAssignments);
    }

    /*
        Method Name         : assignmentSelected
        param               : N/A
        Description         : method to the project id on select of agreement
    * /
    public void assignmentSelected()
    {
        set<Id> setAssignIds = new set<Id>();
        for(wrapAssignments objWrapAssign: lstwrapAssignments)
        {
            if(objWrapAssign.objTimeCardAssignment.Assignment__c != null)
            {
                setAssignIds.add(objWrapAssign.objTimeCardAssignment.Assignment__c);
            }
        }
        map<Id, Assignment__c> mapAssignment = new map<Id, Assignment__c>([SELECT Id, Project__c, Date__c, Project__r.RecordType.DeveloperName, Hours__c, Case__c, Case__r.CaseNumber, Resource_Requirement_Details__c
                                                                             FROM Assignment__c WHERE Id = :setAssignIds]);
        for(wrapAssignments objWrapAssign: lstwrapAssignments)
        {
            if(objWrapAssign.objTimeCardAssignment.Assignment__c != null && mapAssignment.containsKey(objWrapAssign.objTimeCardAssignment.Assignment__c))
            {
                objWrapAssign.decExpectedHrs = mapAssignment.get(objWrapAssign.objTimeCardAssignment.Assignment__c).Hours__c;
                objWrapAssign.expectedDate = mapAssignment.get(objWrapAssign.objTimeCardAssignment.Assignment__c).Date__c;
                objWrapAssign.objTimeCardAssignment.Project__c = mapAssignment.get(objWrapAssign.objTimeCardAssignment.Assignment__c).Project__c;
                objWrapAssign.objTimeCardAssignment.Case__c = mapAssignment.get(objWrapAssign.objTimeCardAssignment.Assignment__c).Case__c;
                objWrapAssign.strSelectedProjectRecordTypeName = mapAssignment.get(objWrapAssign.objTimeCardAssignment.Assignment__c).Project__r.RecordType.DeveloperName;
                objWrapAssign.strRequirement = mapAssignment.get(objWrapAssign.objTimeCardAssignment.Assignment__c).Resource_Requirement_Details__c;
                for(Timecard__c objTimeCard: objWrapAssign.mapWrapTimeEntry.values())
                {
                    objTimeCard.Assignment__c = objWrapAssign.objTimeCardAssignment.Assignment__c;
                    objTimeCard.Project__c =  mapAssignment.get(objWrapAssign.objTimeCardAssignment.Assignment__c).Project__c;
                    if(objTimeCard.Date__c == mapAssignment.get(objWrapAssign.objTimeCardAssignment.Assignment__c).Date__c)
                    {
                        objTimeCard.Hours__c = mapAssignment.get(objWrapAssign.objTimeCardAssignment.Assignment__c).Hours__c;
                    }
                }
            }
        }
    }


    /*
        Method Name         : deleteSelectedTimeEntry
        param               : N/A
        Description         : method to delete the selected Time Entry
    * /
    public void deleteSelectedTimeEntry()
    {
        try{
            List<Timecard__c> lstTimecard = new List<Timecard__c>();
            Integer sz = lstwrapAssignments.size() - 1;
            for (Integer i = sz; i >= 0; i--)
            {
                system.debug('=======lstwrapAssignments[i].isSelected======='+lstwrapAssignments[i].isSelected);
                if(lstwrapAssignments[i].isSelected)
                {
                    wrapAssignments objWrapAssign =  lstwrapAssignments[i];
                    system.debug('=======objWrapAssign======='+objWrapAssign);
                    for(Timecard__c objTimeCard: objWrapAssign.mapWrapTimeEntry.values())
                    {
                        system.debug('=======objTimeCard======='+objTimeCard);
                        if(objTimeCard.Id != null) lstTimecard.add(objTimeCard);
                    }
                    lstwrapAssignments.remove(i);
                }
            }
            system.debug('=======lstTimecard======='+lstTimecard);
            delete lstTimecard;
        }
        catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));
        }
    }


    /*
        Method Name         : saveTimeEntry
        param               : N/A
        Description         : method to save the  Time Entries
    * /
    public void saveTimeEntry()
    {
        showTotalHours = true;
        weekTotalHrs = 0;
        List<Timecard__c> lstTimecardUpsert = new List<Timecard__c>();
        List<Timecard__c> lstTimecardDelete = new List<Timecard__c>();
        List<Assignment__c> lstAssignmentUpdate = new List<Assignment__c>();
        wrapWeeklyHrs = new wrapTotalHrs();
        wrapWeeklyHrs.mapDayTotalHrs = new Map<Date, Decimal>();

        for(wrapAssignments objWrapAssign: lstwrapAssignments)
        {
            if(objWrapAssign.objTimeCardAssignment.Assignment__c != null  && objWrapAssign.objTimeCardAssignment.Project__c != null)
            {
                lstAssignmentUpdate.add(new Assignment__c(Id = objWrapAssign.objTimeCardAssignment.Assignment__c,
                                                            Case__c = objWrapAssign.objTimeCardAssignment.Case__c));
                List<Timecard__c> timeCardList = objWrapAssign.mapWrapTimeEntry.values();
                for (Integer i = 0; i <timeCardList.size(); i++)
                {

                    if(timeCardList[i].Hours__c == null)
                    {
                        timeCardList[i].Hours__c = 0;
                    }


                    if(wrapWeeklyHrs.mapDayTotalHrs.containsKey(timeCardList[i].Date__c))
                    {
                        Decimal totalHrs = wrapWeeklyHrs.mapDayTotalHrs.get(timeCardList[i].Date__c);
                        totalHrs += timeCardList[i].Hours__c;
                        wrapWeeklyHrs.mapDayTotalHrs.put(timeCardList[i].Date__c, totalHrs);
                    }
                    else
                    {
                        wrapWeeklyHrs.mapDayTotalHrs.put(timeCardList[i].Date__c, timeCardList[i].Hours__c);
                    }



                   // add timecard ONLY if hours > 0
                     if(timeCardList[i].Hours__c != null && timeCardList[i].Hours__c > 0)
                    {
                        if(String.isBlank(objWrapAssign.objTimeCardAssignment.Notes__c))
                        {
                            showTotalHours = false;
                            objWrapAssign.objTimeCardAssignment.Notes__c.addError('Required.');
                            return;
                        }
                        timeCardList[i].Notes__c = objWrapAssign.objTimeCardAssignment.Notes__c;
                        timeCardList[i].Billable__c = objWrapAssign.objTimeCardAssignment.Billable__c;
                        timeCardList[i].Case__c = objWrapAssign.objTimeCardAssignment.Case__c;

                        lstTimecardUpsert.add(timeCardList[i]);

                    }
                    else
                    {
                        if(timeCardList[i].id != null)
                        {   String timeCardId = timeCardList[i].id;
                            Timecard__c newTime = new Timecard__c(id = timeCardId);
                            lstTimecardDelete.add(newTime);
                            timeCardList[i].id = null;
                        }
                    }
                }
            }
        }
        for(Decimal hrs :wrapWeeklyHrs.mapDayTotalHrs.values())
        {
            weekTotalHrs += hrs;
        }

        for(Date wkDate :lstWeekDates)
        {
            if(!wrapWeeklyHrs.mapDayTotalHrs.containsKey(wkDate))
                {
                    wrapWeeklyHrs.mapDayTotalHrs.put(wkDate, 0);
                }
        }
        Savepoint sp = Database.setSavepoint();
        try{
            upsert lstTimecardUpsert;
            delete lstTimecardDelete;
            update lstAssignmentUpdate;
            wrapWeeklyHrs.weeklyTotalHrs = WeeklyTotalHrs();
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, 'TimeCards successfully submitted.'));
        }
        catch(Exception ex){
            Database.rollback(sp);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));
        }

    }


    /*
        Method Name         : prevWeekTimeEntry
        param               : N/A
        Description         : method to fetch previous week Time Entries
    * /
    public void prevWeekTimeEntry()
    {
        dtSelected = dtStartDate.addDays(-1);
        fetchTimeCards();
    }


    /*
        Method Name         : nextWeekTimeEntry
        param               : N/A
        Description         : method to fetch next week Time Entries
    * /
    public void nextWeekTimeEntry()
    {
        dtSelected = dtEndDate.addDays(1);
        fetchTimeCards();
    }


    /*
        Method Name         : WeeklyTotalHrs
        param               : N/A
        Description         : method to fetch weekly total hrs
    * /
    public decimal WeeklyTotalHrs()
    {
        Decimal decTotal = 0;
        if(!wrapWeeklyHrs.mapDayTotalHrs.values().isEmpty())
        {
            for(Decimal objTimeCardHrs: wrapWeeklyHrs.mapDayTotalHrs.values())
            {
                decTotal += (objTimeCardHrs != null ? objTimeCardHrs :0);
            }
        }
        return decTotal;
    }


    /*
        Class Name          : wrapTotalHrs
        Description         : Class to maintain the daily total hrs and weekly total hrs
    * /
    public class wrapTotalHrs
    {
        public Map<Date, Decimal> mapDayTotalHrs        {get;set;}
        public Decimal weeklyTotalHrs                   {get;set;}
        public wrapTotalHrs(){
            weeklyTotalHrs = 0;
            mapDayTotalHrs = new Map<Date, Decimal>();
        }
    }


    /*
        Class Name          : selectAll
        Description         : Class to select all the Time cards
    * /
    public void selectAll() {

        if (selectAll == true) {
            for (wrapAssignments wrapperlist: lstwrapAssignments) {
                wrapperlist.isSelected = true;
            }

        } else {
            for (wrapAssignments wrapperlist: lstwrapAssignments) {
                wrapperlist.isSelected = false;
            }
        }
    }


    /*
        Class Name          : wrapAssignments
        Description         : Class to maintain the Assignment and  its related time card entry
    * /
    public class wrapAssignments
    {
        public Timecard__c objTimeCardAssignment        {get;set;}
        public Boolean isSelected                       {get;set;}
        public Map<Date, Timecard__c> mapWrapTimeEntry  {get;set;}
        public String strSelectedProjectRecordTypeName  {get;set;}
        public Decimal decExpectedHrs                   {get;set;}
        public Date expectedDate                        {get;set;}
        public String strRequirement                    {get;set;}

        public wrapAssignments()
        {
            this.objTimeCardAssignment = new Timecard__c();
            this.mapWrapTimeEntry = new map<Date, Timecard__c>();
            decExpectedHrs = 0;
            strRequirement = '';
        }

        public decimal getTotalHrs()
        {
            Decimal decTotal = 0;
            for(Timecard__c objTimeCard: mapWrapTimeEntry.values())
            {
                decTotal += (objTimeCard.Hours__c != null ? objTimeCard.Hours__c :0);
            }
            return decTotal;
        }
    }
    */
}