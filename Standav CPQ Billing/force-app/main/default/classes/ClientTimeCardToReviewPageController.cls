public class ClientTimeCardToReviewPageController {

    //public String showClientApprovalButton { get; set; }
    public Time_Card__c timeCardToReview{get; set;}
    public List<Date> weekDatesList {get;set;}
    public String timeCardId{get; set;}
    public String clientId{get; set;}
    
    public ClientTimeCardToReviewPageController(){
        system.debug('-------------------'+System.currentPageReference().getParameters().get('id'));
        timeCardId = System.currentPageReference().getParameters().get('id');
        weekDatesList = new List<Date>();
        timeCardToReview = [SELECT Id, Assignment__r.Project__c, Name, Assignment__r.EP_Shadow_Resource__c, Assignment__r.Resource__c,
                            Type__c, EP_End_Date__c, Billable_Hours_Fri__c, EP_Notes__c, EP_Approval_Date__c, Assignment__r.Project__r.Contact_Person__c,
                            EP_Total_Number_of_Billable_Hours__c, EP_Total_Number_of_Unbillable_Hours__c, Assignment__r.Project__r.Contact_Person__r.Email,
                            EP_Rejected_Date__c, Submitted_On__c, Project_Manager__c, Billable_Hours_Mon__c, Show_Comments__c,Manager_Approval_Comments__c,
                            Billable_Hours_Sat__c, Assignment__c, Status__c, Start_Date__c, Billable_Hours_Sun__c, 
                            Billable_Hours_Thu__c, Billable_Hours_Tue__c, Billable_Hours_Wed__c, UnBillable_Hours_Fri__c, 
                            UnBillable_Hours_Mon__c, UnBillable_Hours_Sat__c, UnBillable_Hours_Sun__c, UnBillable_Hours_Thu__c, 
                            UnBillable_Hours_Tue__c, UnBillable_Hours_Wed__c, Assignment__r.Project__r.Name,
                            Assignment__r.Resource__r.User__r.Name , Assignment__r.RecordType.Name, Assignment__r.Is_Primary_Resource_Contributing__c,
                            X1st_Approver__c, X2nd_Approver__c, X3rd_Approver__c, X4th_Approver__c, X5th_Approver__c, Approval_Status__c,
                            Assignment__r.EP_Shadow_Resource__r.User__r.Name,  Assignment__r.Role__c, 
                            Monday_Work_Details__c,Tuesday_Work_Details__c,Wednesday_Work_Details__c,Thursday_Work_Details__c,Friday_Work_Details__c, 
                            Saturday_Work_Details__c,Sunday_Work_Details__c,
                            (SELECT Id, Name FROM attachments)
                            FROM Time_Card__c
                            WHERE Id =: System.currentPageReference().getParameters().get('id') Limit 1];
        timeCardToReview.Billable_Hours_Mon__c = timeCardToReview.Billable_Hours_Mon__c == null ? 0.0 : timeCardToReview.Billable_Hours_Mon__c;
        timeCardToReview.Billable_Hours_Tue__c = timeCardToReview.Billable_Hours_Tue__c == null ? 0.0 : timeCardToReview.Billable_Hours_Tue__c;
        timeCardToReview.Billable_Hours_Wed__c = timeCardToReview.Billable_Hours_Wed__c == null ? 0.0 : timeCardToReview.Billable_Hours_Wed__c;
        timeCardToReview.Billable_Hours_Thu__c = timeCardToReview.Billable_Hours_Thu__c == null ? 0.0 : timeCardToReview.Billable_Hours_Thu__c;
        timeCardToReview.Billable_Hours_Fri__c = timeCardToReview.Billable_Hours_Fri__c == null ? 0.0 : timeCardToReview.Billable_Hours_Fri__c;
        timeCardToReview.Billable_Hours_Sat__c = timeCardToReview.Billable_Hours_Sat__c == null ? 0.0 : timeCardToReview.Billable_Hours_Sat__c;
        timeCardToReview.Billable_Hours_Sun__c = timeCardToReview.Billable_Hours_Sun__c == null ? 0.0 : timeCardToReview.Billable_Hours_Sun__c;
        system.debug('------timeCardToReview----------'+timeCardToReview);
        system.debug('------timeCardToReview.Start_Date__c----------'+timeCardToReview.Start_Date__c);
        system.debug('------timeCardToReview.EP_End_Date__c----------'+timeCardToReview.EP_End_Date__c);
        Date dtvaries = timeCardToReview.Start_Date__c;
        system.debug('------dtvaries ----------'+dtvaries );
        while(dtvaries <= timeCardToReview.EP_End_Date__c){
            weekDatesList.add(dtvaries);
            dtvaries = dtvaries.addDays(1);
        }
        clientId = timeCardToReview.Assignment__r.Project__r.Contact_Person__c;
    }
    
    //Approve method
    public PageReference approveTimeCard(){
        timeCardUpdate('Client Approved', timeCardToReview.EP_Notes__c);
        String pageURL = System.Label.Client_Page_URL+'ClientApprovalPage?clientId='+clientId;
        PageReference pr = new PageReference(pageURL);
        //PageReference pr = new PageReference('/clientpage/ClientApprovalPage');
        pr.getParameters().put('clientId',clientId);
        //pr.setRedirect(false);
        return pr;
    }
    //Reject method
    public PageReference rejectRecord(){
        timeCardUpdate('Draft', timeCardToReview.EP_Notes__c);
        String pageURL = System.Label.Client_Page_URL+'ClientApprovalPage?clientId='+clientId;
        PageReference pr = new PageReference(pageURL);
        //PageReference pr = new PageReference('/clientpage/ClientApprovalPage?clientId='+clientId);
        //pr.setRedirect(false); // If you want a redirect. Do not set anything if you want a forward.
        return pr;
    }
    public pageReference reDirect(){
        String pageURL = System.Label.Client_Page_URL+'ClientApprovalPage?clientId='+clientId;
        PageReference pr = new PageReference(pageURL);
        //PageReference pr = new PageReference('/clientpage/ClientApprovalPage?clientId='+clientId);
        //pr.setRedirect(false); // If you want a redirect. Do not set anything if you want a forward.
        return pr;
    }

    public Time_Card__c timeCardUpdate(String status, String comments){

        Time_Card__c timeCardUpdated = [SELECT Id, Assignment__r.Project__c, Name, Assignment__r.EP_Shadow_Resource__c, Assignment__r.Resource__c,
                                                Type__c, EP_End_Date__c, Billable_Hours_Fri__c, EP_Notes__c, EP_Approval_Date__c, Manager_Approval_Comments__c,
                                                EP_Total_Number_of_Billable_Hours__c, EP_Total_Number_of_Unbillable_Hours__c, Finally_Approved__c,
                                                EP_Rejected_Date__c, Submitted_On__c, Project_Manager__c, Billable_Hours_Mon__c, 
                                                Billable_Hours_Sat__c, Assignment__c, Status__c, Start_Date__c, Billable_Hours_Sun__c, 
                                                Billable_Hours_Thu__c, Billable_Hours_Tue__c, Billable_Hours_Wed__c, UnBillable_Hours_Fri__c, 
                                                UnBillable_Hours_Mon__c, UnBillable_Hours_Sat__c, UnBillable_Hours_Sun__c, UnBillable_Hours_Thu__c, 
                                                UnBillable_Hours_Tue__c, UnBillable_Hours_Wed__c, Assignment__r.Project__r.Name,
                                                Assignment__r.Resource__r.User__r.Name , Assignment__r.RecordType.Name, Assignment__r.Is_Primary_Resource_Contributing__c,
                                                X1st_Approver__c, X2nd_Approver__c, X3rd_Approver__c, X4th_Approver__c, X5th_Approver__c, Approval_Status__c,
                                                Assignment__r.EP_Shadow_Resource__r.User__r.Name,  Assignment__r.Role__c, Show_Comments__c
                                                FROM Time_Card__c 
                                                WHERE Status__c = 'Approved' AND Assignment__r.RecordType.Name = 'Primary' AND 
                                                Id =: System.currentPageReference().getParameters().get('id') Limit 1];
        timeCardUpdated.Status__c = status;  
        timeCardUpdated.EP_Notes__c = comments;
        if(status == 'Draft'){
            timeCardUpdated.Client_Rejected__c = True; 
            timeCardUpdated.Approval_Status__c = '';
        }else{
            timeCardUpdated.Finally_Approved__c = True;
        }
        update timeCardUpdated;
        return timeCardUpdated;
    }
}