@isTest
private class EP_Project_Resource_Test{
    @testSetUp static void createData(){
       
 User user = new User();
            user.LastName = 'Test User';
            user.Alias = 'tu';
            user.Email = 'test@gmail.com';
            user.Username = 'test.dev@stand.com';
            user.CommunityNickname = 'test.t';
            //user.UserRoleId = '00ei0000000rTfp';
            user.User_Time_Zone__c = 'Pst';
            user.ProfileId = '00e54000000HSHG';           
            user.TimeZoneSidKey    = 'America/Denver';
            user.LocaleSidKey      = 'en_US';
            user.EmailEncodingKey  = 'UTF-8';
            user.LanguageLocaleKey = 'en_US';
            insert user;
        
            Account acc = new Account();
        	acc.Name = 'Test Account';
        	insert acc;
         
            Contact con = new Contact();
        	con.FirstName = 'test';
        	con.LastName = 'contact';
        	insert con;
     
     
        Project__c project = new Project__c();
            project.Name = 'Test Project';
            project.EP_Project_Manager__c = user.Id;
            project.Contact_Person__c = con.Id;
            project.Start_Date__c = Date.newInstance(2016, 12, 9);
        	project.Account__c = acc.id;
        	project.Number__c = 2;
            insert project;        
            update project;
        
       
       Resource__c resource = new Resource__c();
            resource.User__c = '0051Q00000GoFQn';
            //resource.Contact__c = '0035400000Neo7d';
            insert resource;        
            update resource;
              Resource__c resource1 = new Resource__c();
            resource1.User__c = '00554000004e6gkAAA';
            //resource.Contact__c = '0035400000Neo7d';
            insert resource1;        
            update resource1;
        
            Assignment__c assignment = new Assignment__c();
            assignment.Project__c = project.id;
            assignment.Resource__c = resource.id;
            assignment.Status__c = 'Approved';
            assignment.Start_Date__c = Date.newInstance(2016, 12, 9);
            assignment.End_Date__c = Date.newInstance(2018, 12, 9);
            assignment.Role__c = 'Developer';
            assignment.Billing_Type__c = 'Billable';
        	assignment.Budgeted_Hours__c = 50;
            assignment.Assignment_Type__c = 'Long Term';   
            //assignment.Actual_Hours__c = 20;
            insert assignment;        
           // update assignment;
            Assignment__c assignment1 = new Assignment__c();
            assignment1.Project__c = project.id;
            assignment1.Resource__c = resource1.id;
            assignment1.Status__c = 'Approved';
            assignment1.Start_Date__c = Date.newInstance(2016, 12, 9);
            assignment1.End_Date__c = Date.newInstance(2018, 12, 9);
            assignment1.Role__c = 'Developer';
            assignment1.Billing_Type__c = 'Non Billable';
            assignment1.Assignment_Type__c = 'Long Term';  
        	assignment1.Budgeted_Hours__c = 50;
            //assignment.Actual_Hours__c = 20;
            insert assignment1;   
       
        
  Time_Card__c card1 = new Time_Card__c();
        card1.Assignment__c = assignment.id;
            card1.EP_End_Date__c = Date.newInstance(2019, 12, 9);
            card1.Billable_Hours_Fri__c = 1;
            card1.Billable_Hours_Mon__c = 1;
            card1.Billable_Hours_Sat__c = 1;
            card1.Billable_Hours_Sun__c = 1;
            card1.Billable_Hours_Thu__c = 0;
            card1.Billable_Hours_Tue__c = 0;
            card1.Billable_Hours_Wed__c = 0;
            card1.Status__c = 'Approved';    
            card1.EP_Notes__c = 'test';
            card1.Start_Date__c =Date.newInstance(2019, 12, 2);
            card1.EP_Approval_Date__c = Date.newInstance(2019, 12, 9) ;
            card1.EP_Rejected_Date__c = Date.newInstance(2019, 12, 9) ;
            card1.Project_Manager__c = user.id;
            card1.Submitted_On__c = Date.newInstance(2019, 12, 9) ;
            card1.X1st_Approver__c = 'Test';
            card1.X2nd_Approver__c = 'Test';
            card1.X3rd_Approver__c = 'Test';
            card1.X4th_Approver__c = 'Test';
            card1.X5th_Approver__c = 'Test';
            card1.Approval_Status__c = 'Approver 1 Approved';
            insert card1;
        
        Time_Card__c card2 = new Time_Card__c();
        card2.Assignment__c = assignment.id;
            card2.EP_End_Date__c = Date.newInstance(2019, 12, 9);
            card2.Unbillable_Hours_Fri__c = 1;
            card2.Unbillable_Hours_Mon__c = 1;
            card2.Unbillable_Hours_Sat__c = 1;
            card2.Unbillable_Hours_Sun__c = 1;
            card2.Unbillable_Hours_Thu__c = 0;
            card2.Unbillable_Hours_Tue__c = 0;
            card2.Unbillable_Hours_Wed__c = 0;
            card2.Status__c = 'Approved';
            card2.EP_Notes__c = 'test';
            card2.Start_Date__c =Date.newInstance(2019, 12, 2);
            card2.EP_Approval_Date__c = Date.newInstance(2019, 12, 9) ;
            card2.EP_Rejected_Date__c = Date.newInstance(2019, 12, 9) ;
            card2.Project_Manager__c = user.id;
            card2.Submitted_On__c = Date.newInstance(2019, 12, 9) ;
            card2.X1st_Approver__c = 'Test';
            card2.X2nd_Approver__c = 'Test';
            card2.X3rd_Approver__c = 'Test';
            card2.X4th_Approver__c = 'Test';
            card2.X5th_Approver__c = 'Test';
            card2.Approval_Status__c = 'Approver 1 Approved';
            insert card2;
       
        Time_Card__c card3 = new Time_Card__c();
        card3.Assignment__c = assignment.id;
            card3.EP_End_Date__c = Date.newInstance(2019, 12, 9);
            card3.Billable_Hours_Fri__c = 1;
            card3.Billable_Hours_Mon__c = 1;
            card3.Billable_Hours_Sat__c = 1;
            card3.Billable_Hours_Sun__c = 1;
            card3.Billable_Hours_Thu__c = 0;
            card3.Billable_Hours_Tue__c = 0;
            card3.Billable_Hours_Wed__c = 0;
            card3.Status__c = 'Assigned';
            card3.EP_Notes__c = 'test';
            card3.Start_Date__c =Date.newInstance(2019, 12, 2);
            card3.EP_Approval_Date__c = Date.newInstance(2019, 12, 9) ;
            card3.EP_Rejected_Date__c = Date.newInstance(2019, 12, 9) ;
            card3.Project_Manager__c = user.id;
            card3.Submitted_On__c = Date.newInstance(2019, 12, 9) ;
            card3.X1st_Approver__c = 'Test';
            card3.X2nd_Approver__c = 'Test';
            card3.X3rd_Approver__c = 'Test';
            card3.X4th_Approver__c = 'Test';
            card3.X5th_Approver__c = 'Test';
            card3.Approval_Status__c = 'Approver 1 Approved';
            insert card3;
      
      Time_Card__c card4 = new Time_Card__c();
        card4.Assignment__c = assignment.id;
            card4.EP_End_Date__c = Date.newInstance(2019, 12, 9);
            card4.Unbillable_Hours_Fri__c = 1;
            card4.Unbillable_Hours_Mon__c = 1;
            card4.Unbillable_Hours_Sat__c = 1;
            card4.Unbillable_Hours_Sun__c = 1;
            card4.Unbillable_Hours_Thu__c = 0;
            card4.Unbillable_Hours_Tue__c = 0;
            card4.Unbillable_Hours_Wed__c = 0;
            card4.Status__c = 'Assigned'; 
            card4.EP_Notes__c = 'test';
            card4.Start_Date__c =Date.newInstance(2019, 12, 2);
            card4.EP_Approval_Date__c = Date.newInstance(2019, 12, 9) ;
            card4.EP_Rejected_Date__c = Date.newInstance(2019, 12, 9) ;
            card4.Project_Manager__c = user.id;
            card4.Submitted_On__c = Date.newInstance(2019, 12, 9) ;
            card4.X1st_Approver__c = 'Test';
            card4.X2nd_Approver__c = 'Test';
            card4.X3rd_Approver__c = 'Test';
            card4.X4th_Approver__c = 'Test';
            card4.X5th_Approver__c = 'Test';
            card4.Approval_Status__c = 'Approver 1 Approved';
            insert card4;
                 Time_Card__c card = new Time_Card__c();
        	card.Assignment__c = assignment.id;
            card.Billable_Hours_Fri__c = 4;
            card.Billable_Hours_Mon__c =4;
            card.Billable_Hours_Sat__c = 0;
            card.Billable_Hours_Sun__c = 0;
            card.Billable_Hours_Thu__c = 4;
            card.Billable_Hours_Tue__c = 4;
            card.Billable_Hours_Wed__c =4;
            card.Status__c = 'Approved';            
            insert card;        
     
     }
    
      private static testMethod void test() {
      List<project__c> projectlist = [Select Id,Name from Project__c];
      List<resource__c> resourcelist = [Select Id,Name from Resource__c];
        system.debug('projectlist Id'+projectlist[0].id);  
          Test.startTest();
           
           PageReference pageRef = Page.EP_Project_Resource_View;
        pageRef.getParameters().put('id', projectlist[0].id);
          Test.setCurrentPage(pageRef);
         // PageReference objPageRef = EP_Project_Resource_View_Controller.EP_Project_Resource_View_Controller();

        //ApexPages.StandardController sc = new ApexPages.StandardController(projectlist[0]);
        //EP_Project_Resource_View_Controller testPlan = new EP_Project_Resource_View_Controller(sc);
          
          Pagereference pageref2 = Page.EP_PageToGetResourcePage;
          pageRef2.getParameters().put('id',resourcelist[0].id);
          Test.setCurrentPage(pageRef2);
          ApexPages.StandardController sc1 = new ApexPages.StandardController(resourcelist[0]);
          List<ResourcePageController.wrapAssignment> newTimeCardWrpper2;
               ResourcePageController.wrapAssignment testWrap3=new ResourcePageController.wrapAssignment(); 
          ResourcePageController testPlan1 = new ResourcePageController(sc1);

       
       
        Test.setCurrentPage(pageRef);
                 
      Test.stopTest();

  }
  
   
}