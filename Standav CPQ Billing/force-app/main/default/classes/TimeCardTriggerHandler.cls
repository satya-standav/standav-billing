Public class TimeCardTriggerHandler{
    /*public static void beforeInsertHandler(List<Time_Card__c> relatedTimeCardList){
        if(Trigger.isInsert && Trigger.isBefore){
            //TimeCardTriggerHelper.beforeInsertUpdateMethod(relatedTimeCardList, null);
        }
    }
    public static void afterInsertHandler(List<Time_Card__c> relatedTimeCardList){
        if(Trigger.isInsert && Trigger.isAfter){
            //TimeCardTriggerHelper.afterInsertMethod(relatedTimeCardList, null);
        }
    }
    public static void beforeUpdatHandler(List<Time_Card__c> relatedTimeCardList, Map<Id, Time_Card__c> oldTimeCardDataMap){
        if(Trigger.isUpdate && Trigger.isBefore){
            //TimeCardTriggerHelper.beforeInsertUpdateMethod(relatedTimeCardList, oldTimeCardDataMap);
        }
    }*/
    public static void afterInsertHandler(List<Time_Card__c> relatedTimeCardList){
        if(Trigger.isInsert && Trigger.isAfter){
            TimeCardTriggerHelper.attachPrimaryAndShadowTimeCard(relatedTimeCardList, null);
        }
    }
    public static void afterUpdateHandler(List<Time_Card__c> relatedTimeCardList, Map<Id, Time_Card__c> oldTimeCardDataMap){
        if(Trigger.isUpdate && Trigger.isAfter){
            TimeCardTriggerHelper.updatePrimaryToShadow(relatedTimeCardList, oldTimeCardDataMap);
        }
    }
    
    /*public static void beforeDeleteHandler(){
        if(Trigger.isDelete && Trigger.isBefore){
        
        }
    }
    public static void afterDeleteHandler(){
        if(Trigger.isDelete && Trigger.isAfter){
        
        }
    }
    public static void afterUndeleteHandler(){
        if(Trigger.isUndelete && Trigger.isAfter){
        
        }*/
    
}