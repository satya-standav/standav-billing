public class BurnedChartPageController {
    
    public String errorMessage{get;set;}
    Public Map<String, List<assignmentWithTimeCardWrapper>> projectWithWrapperDataList{get; set;}
    
    public BurnedChartPageController(){
        
        //List<assignmentWithTimeCardWrapper> wrapperList = new List<assignmentWithTimeCardWrapper>();
        projectWithWrapperDataList = new Map<String, List<assignmentWithTimeCardWrapper>>();
        
        
        for(Assignment__c assignmentwithTC : [SELECT Id,Name,Project__r.Name,Project__r.Start_Date__c,Project__r.End_Date__c,
                                             Start_Date__c,Project__r.EP_Project_Manager__r.Name,Budgeted_Hours__c, Role__c,
                                             Invoiced_Hours__c,Project__r.Contact_Person__c,Remaining_Hours__c,RecordType.Name,
                                             Resource__c, End_Date__c, Resource__r.User__r.Name,Burned_Hours__c,Approved_Hours__c,  
                                             Billing_Type__c
                                             FROM Assignment__c
                                             WHERE Project__r.Contact_Person__c =: System.currentPageReference().getParameters().get('clientId') AND
                                             RecordType.Name = 'Primary' AND 
                                             Billing_Type__c = 'Billable']){
            List<assignmentWithTimeCardWrapper> wrapperList = new List<assignmentWithTimeCardWrapper>();
            assignmentWithTimeCardWrapper wrapperData = new assignmentWithTimeCardWrapper();
            
            if(projectWithWrapperDataList.containsKey(assignmentwithTC.Project__r.Name)){
                wrapperData.ResourceName = assignmentwithTC.Resource__r.User__r.Name;
                wrapperData.budgetedHours = assignmentwithTC.Budgeted_Hours__c;
                wrapperData.ApprovedHours = assignmentwithTC.Approved_Hours__c; 
                wrapperData.BurnedHours = assignmentwithTC.Burned_Hours__c;
                wrapperData.InvoicedHours = assignmentwithTC.Invoiced_Hours__c;
                wrapperData.RemainingHours = assignmentwithTC.Remaining_Hours__c;
                wrapperData.endDate = assignmentwithTC.End_Date__c;
                wrapperData.Role = assignmentwithTC.Role__c;
                projectWithWrapperDataList.get(assignmentwithTC.Project__r.Name).add(wrapperData);
            }
            else{
                wrapperData.ResourceName = assignmentwithTC.Resource__r.User__r.Name;
                wrapperData.budgetedHours = assignmentwithTC.Budgeted_Hours__c;
                wrapperData.ApprovedHours = assignmentwithTC.Approved_Hours__c; 
                wrapperData.BurnedHours = assignmentwithTC.Burned_Hours__c;
                wrapperData.InvoicedHours = assignmentwithTC.Invoiced_Hours__c;
                wrapperData.RemainingHours = assignmentwithTC.Remaining_Hours__c;
                wrapperData.Role = assignmentwithTC.Role__c;
                wrapperData.endDate = assignmentwithTC.End_Date__c;
                wrapperList.add(wrapperData);
                projectWithWrapperDataList.put(assignmentwithTC.Project__r.Name, wrapperList); 
            }                              
        } 
    }
    public class assignmentWithTimeCardWrapper{
        public String ResourceName {get; set;}
        public Decimal budgetedHours{get; set;}
        public Decimal BurnedHours {get; set;}
        public Decimal ApprovedHours {get; set;}
        public Decimal InvoicedHours {get; set;}
        public Decimal RemainingHours {get; set;}
        public String Role{get;set;}
        public Date endDate{get;set;}
        public assignmentWithTimeCardWrapper(){
            
        }
    }
}