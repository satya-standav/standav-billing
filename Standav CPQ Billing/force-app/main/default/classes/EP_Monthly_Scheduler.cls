global class EP_Monthly_Scheduler implements Schedulable
{
    global void execute(SchedulableContext sc)
    {
    
        MonthEmailBatch ep = new MonthEmailBatch();
        database.executebatch(ep);
    }
   
}