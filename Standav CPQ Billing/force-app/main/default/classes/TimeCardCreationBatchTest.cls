@isTest
public class TimeCardCreationBatchTest

{

    public TestMethod static void TimeCardCreationBatchTest_Method()

    {
         User user = new User();
            user.LastName = 'Test';
            user.Alias = 'tester';
            user.Email = 'test@gmail.com';
            user.Username = 'test@stand.com';
            user.CommunityNickname = 'te';
            user.User_Time_Zone__c = 'Pst';
            user.ProfileId = '00e54000000HSHG';           
            user.TimeZoneSidKey    = 'America/Denver';
            user.LocaleSidKey      = 'en_US';
            user.EmailEncodingKey  = 'UTF-8';
            user.LanguageLocaleKey = 'en_US';
            insert user;
        
        User user1 = new User();
            user1.LastName = 'Test1';
            user1.Alias = 'tester1';
            user1.Email = 'test1@gmail.com';
            user1.Username = 'test1@stand.com';
            user1.CommunityNickname = 'te1';
            user1.User_Time_Zone__c = 'Pst';
            user1.ProfileId = '00e54000000HSHG';           
            user1.TimeZoneSidKey    = 'America/Denver';
            user1.LocaleSidKey      = 'en_US';
            user1.EmailEncodingKey  = 'UTF-8';
            user1.LanguageLocaleKey = 'en_US';
            insert user1;
        
        Contact cont = new Contact();
            cont.LastName = 'Konikkara';
            insert cont;
        
        Account acc = new Account();
        	acc.Name = 'Test Account';
        	insert acc;
     
     
        Project__c project = new Project__c();
            project.Name = 'TestProject';
            project.EP_Project_Manager__c = user.ID;
            project.Start_Date__c = Date.newInstance(2018, 12, 9);
            project.End_Date__c = Date.newInstance(2020, 12, 9);
            project.Contact_Person__c = cont.Id;
        	project.Account__c = acc.id;
        	project.Number__c = 2;
            insert project; 
      
      Resource__c resource = new Resource__c();
            resource.User__c = user.ID;
            insert resource;  
        
      Resource__c resource1 = new Resource__c();
            resource.User__c = user1.ID;
            insert resource1;
        
      Assignment__c assignment = new Assignment__c();
            assignment.Project__c = project.id;
            assignment.Resource__c = resource.id;
            assignment.Status__c = 'Approved';
            assignment.Start_Date__c = Date.newInstance(2018, 12, 9);
            assignment.End_Date__c = Date.newInstance(2020, 12, 9);
            assignment.Role__c = 'Developer';
            assignment.Billing_Type__c = 'Billable';
            assignment.Assignment_Type__c = 'Long Term';  
            assignment.RecordTypeId ='012540000018VJv';
        	assignment.Budgeted_Hours__c = 50;
            //assignment.Actual_Hours__c = 20;
            insert assignment;
        
       Assignment__c assignment1 = new Assignment__c();
            assignment1.Project__c = project.id;
            assignment1.Resource__c = resource1.id;
            assignment1.Status__c = 'Approved';
        	
            assignment1.Start_Date__c = Date.newInstance(2019, 12, 9);
            assignment1.End_Date__c = Date.newInstance(2020, 12, 9);
            assignment1.Role__c = 'Developer';
            assignment1.Billing_Type__c = 'Non Billable';
            assignment1.Assignment_Type__c = 'Long Term';  
            assignment1.RecordTypeId ='012540000018VJv';
        	assignment1.Budgeted_Hours__c = 50;
            //assignment1.Actual_Hours__c = 20;
            insert assignment1;
        
         TimeCardCreationBatchController myBatchObject = new TimeCardCreationBatchController(); 
          Id batchId = Database.executeBatch(myBatchObject);
    
    }
    
    
}