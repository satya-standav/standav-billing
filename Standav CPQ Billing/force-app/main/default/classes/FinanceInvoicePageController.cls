public class FinanceInvoicePageController {

    public String oppIdToWin {get; set;}
    public static Decimal totalBillableHours; 
    public string projectCriteraString{get; set;}
    public string resourceCriteraString{get; set;}
    public String errorMessage{get;set;}
    public Boolean ishide{get;set;}
    public Date startDateFilter{get;set;}
    public Date endDateFilter{get;set;}
    public List<SelectOption> resourceNameOptionsList{get; set;} 
    public List<SelectOption> projectNameOptionsList{get; set;}
    public List < timeCardWrapper > timeCardWrapperList {
        get {
            if (timeCardWrapperList != null) {
                return timeCardWrapperList;
            }
            return fetchTimeCards();
        }
        public set;
    }
    Map<Id, String> userIdWithNameMap;

    public FinanceInvoicePageController(){
        ishide = true;
        userIdWithNameMap = new Map<Id, String>();
        resourceNameOptionsList = new List<SelectOption>();
        Set<SelectOption> resourceNameSet = new Set<SelectOption>();
        resourceNameSet.add(new SelectOption('', 'Please Select'));

        for(User UserRec : UserDataCache.getMap().values()){
            userIdWithNameMap.put(UserRec.Id, UserRec.FirstName+' '+UserRec.LastName);
            resourceNameSet.add(new SelectOption(UserRec.FirstName+' '+UserRec.LastName, UserRec.FirstName+' '+UserRec.LastName)); 
        } 
        resourceNameOptionsList.addAll(resourceNameSet); 
    }
    
    public PageReference resetFiltersMethod(){
        fetchTimeCards();
        errorMessage = null;
        projectCriteraString = null;
        resourceCriteraString = null;
        startDateFilter = null;
        endDateFilter = null;
        return null;
    }
    public PageReference filterMethod(){
        system.debug('------resourceCriteraString--------'+resourceCriteraString);
        totalBillableHours = 0.0;
        Date filterStartDate = startDateFilter;
        Date filterEndDate = endDateFilter;
        string queryString;

        if(startDateFilter != null){
            Date dToday1 = filterStartDate;
            Datetime dt1 = datetime.newInstance(dToday1.year(), dToday1.month(),dToday1.day());
            String dayOfCurrentDate = dt1.format('E');
            filterStartDate = (dayOfCurrentDate == 'Sun' ? filterStartDate.addDays(1):
                             dayOfCurrentDate == 'Mon' ? filterStartDate.addDays(0):
                             dayOfCurrentDate == 'Tue' ? filterStartDate.addDays(-1):
                             dayOfCurrentDate == 'Wed' ? filterStartDate.addDays(-2):
                             dayOfCurrentDate == 'Thu' ? filterStartDate.addDays(-3):
                             dayOfCurrentDate == 'Fri' ? filterStartDate.addDays(-4):
                             dayOfCurrentDate == 'Sat' ? filterStartDate.addDays(-5): 
                             filterStartDate);     
        }
        if(endDateFilter != null){
            Date dToday2 = filterEndDate;
            Datetime dt2 = datetime.newInstance(dToday2.year(), dToday2.month(),dToday2.day());
            String dayOfCurrentDate = dt2.format('E');
            filterEndDate = (dayOfCurrentDate == 'Sun' ? filterEndDate.addDays(0):
                             dayOfCurrentDate == 'Mon' ? filterEndDate.addDays(6):
                             dayOfCurrentDate == 'Tue' ? filterEndDate.addDays(5):
                             dayOfCurrentDate == 'Wed' ? filterEndDate.addDays(4):
                             dayOfCurrentDate == 'Thu' ? filterEndDate.addDays(3):
                             dayOfCurrentDate == 'Fri' ? filterEndDate.addDays(2):
                             dayOfCurrentDate == 'Sat' ? filterEndDate.addDays(1): 
                             filterEndDate);     
        }
        String approvedString = 'Approved';
        String clientapproed = 'Client Approved';
        if(resourceCriteraString != null)
            queryString = 'SELECT Id, Name, Start_Date__c, End_Date__c,Budgeted_Hours__c,Project__r.Name,EP_Shadow_Resource__r.User__r.Name, Resource__r.User__r.Name,Resource__r.User__r.Name,RecordType.Name,Billing_Type__c,'+
                '(SELECT Id, Client_Details__c,Name,Assignment__r.Resource__r.User__c, Assignment__c, EP_Total_Number_of_Billable_Hours__c, Start_Date__c, EP_End_Date__c, Status__c,Invoiced__c FROM Time_Cards__r WHERE Invoiced__c = False AND ((Client_Details__c = false AND Status__c =: approvedString) OR (Client_Details__c = true AND Status__c =: clientapproed)))'+
                ' FROM Assignment__c WHERE Resource__r.User__r.Name =: resourceCriteraString';
        if(projectCriteraString != null)
            queryString = 'SELECT Id, Name, Start_Date__c, End_Date__c,Budgeted_Hours__c,Project__r.Name,EP_Shadow_Resource__r.User__r.Name, Resource__r.User__r.Name,Resource__r.User__r.Name,RecordType.Name,Billing_Type__c,'+
                '(SELECT Id, Client_Details__c,Name,Assignment__r.Resource__r.User__c, Assignment__c, EP_Total_Number_of_Billable_Hours__c, Start_Date__c, EP_End_Date__c, Status__c,Invoiced__c FROM Time_Cards__r WHERE Invoiced__c = False AND ((Client_Details__c = false AND Status__c =: approvedString) OR (Client_Details__c = true AND Status__c =: clientapproed)))'+
                ' FROM Assignment__c WHERE Project__r.Name =: projectCriteraString';
        if(filterStartDate != null && filterEndDate != null)
            queryString = 'SELECT Id, Name, Start_Date__c, End_Date__c,Budgeted_Hours__c,Project__r.Name,EP_Shadow_Resource__r.User__r.Name, Resource__r.User__r.Name,Resource__r.User__r.Name,RecordType.Name,Billing_Type__c,'+
                '(SELECT Id,Client_Details__c,Assignment__r.Resource__r.User__c, Name, Assignment__c, EP_Total_Number_of_Billable_Hours__c, Start_Date__c, EP_End_Date__c, Status__c,Invoiced__c FROM Time_Cards__r WHERE Invoiced__c = False AND ((Client_Details__c = false AND Status__c =: approvedString) OR (Client_Details__c = true AND Status__c =: clientapproed)) AND (Start_Date__c >=: filterStartDate AND EP_End_Date__c <=: filterEndDate))'+
                ' FROM Assignment__c';
        if(resourceCriteraString != null && projectCriteraString != null)
            queryString = 'SELECT Id, Name, Start_Date__c, End_Date__c,Budgeted_Hours__c,Project__r.Name,EP_Shadow_Resource__r.User__r.Name, Resource__r.User__r.Name,Resource__r.User__r.Name,RecordType.Name,Billing_Type__c,'+
                '(SELECT Id, Client_Details__c,Assignment__r.Resource__r.User__c,Name, Assignment__c, EP_Total_Number_of_Billable_Hours__c, Start_Date__c, EP_End_Date__c, Status__c,Invoiced__c FROM Time_Cards__r WHERE Invoiced__c = False AND ((Client_Details__c = false AND Status__c =: approvedString) OR (Client_Details__c = true AND Status__c =: clientapproed)))'+
                ' FROM Assignment__c WHERE Resource__r.User__r.Name =: resourceCriteraString AND Project__r.Name =: projectCriteraString';
        if(filterStartDate != null && filterEndDate != null && projectCriteraString != null)
            queryString = 'SELECT Id, Name, Start_Date__c, End_Date__c,Budgeted_Hours__c,Project__r.Name,EP_Shadow_Resource__r.User__r.Name, Resource__r.User__r.Name,Resource__r.User__r.Name,RecordType.Name,Billing_Type__c,'+
                '(SELECT Id, Client_Details__c,Assignment__r.Resource__r.User__c,Assignment__r.Resource__r.User__c,Assignment__r.Resource__r.User__c,Name, Assignment__c, EP_Total_Number_of_Billable_Hours__c, Start_Date__c, EP_End_Date__c, Status__c,Invoiced__c FROM Time_Cards__r WHERE Invoiced__c = True AND ((Client_Details__c = false AND Status__c =: approvedString) OR (Client_Details__c = true AND Status__c =: clientapproed)) AND ((Client_Details__c = false AND Status__c =: approvedString) OR (Client_Details__c = true AND Status__c =: clientapproed)) AND (Start_Date__c >=: filterStartDate AND EP_End_Date__c <=: filterEndDate))'+
                ' FROM Assignment__c WHERE Project__r.Name =: projectCriteraString';
        if(filterStartDate != null && filterEndDate != null&& resourceCriteraString != null)
            queryString = 'SELECT Id, Name, Start_Date__c, End_Date__c,Budgeted_Hours__c,Project__r.Name,EP_Shadow_Resource__r.User__r.Name, Resource__r.User__r.Name,Resource__r.User__r.Name,RecordType.Name,Billing_Type__c,'+
                '(SELECT Id, Client_Details__c,Assignment__r.Resource__r.User__c,Name, Assignment__c, EP_Total_Number_of_Billable_Hours__c, Start_Date__c, EP_End_Date__c, Status__c,Invoiced__c FROM Time_Cards__r WHERE Invoiced__c = False AND ((Client_Details__c = false AND Status__c =: approvedString) OR (Client_Details__c = true AND Status__c =: clientapproed)) AND (Start_Date__c >=: filterStartDate AND EP_End_Date__c <=: filterEndDate))'+
                ' FROM Assignment__c WHERE Resource__r.User__r.Name =: resourceCriteraString';
        if(resourceCriteraString != null && projectCriteraString != null && filterStartDate != null && filterEndDate != null)
            queryString = 'SELECT Id, Name, Start_Date__c, End_Date__c,Budgeted_Hours__c,Project__r.Name,EP_Shadow_Resource__r.User__r.Name, Resource__r.User__r.Name,Resource__r.User__r.Name,RecordType.Name,Billing_Type__c,'+
                '(SELECT Id, Client_Details__c,Assignment__r.Resource__r.User__c,Name, Assignment__c, EP_Total_Number_of_Billable_Hours__c, Start_Date__c, EP_End_Date__c, Status__c,Invoiced__c FROM Time_Cards__r WHERE Invoiced__c = False AND ((Client_Details__c = false AND Status__c =: approvedString) OR (Client_Details__c = true AND Status__c =: clientapproed)) AND (Start_Date__c >=: filterStartDate AND EP_End_Date__c <=: filterEndDate))'+
                ' FROM Assignment__c WHERE Resource__r.User__r.Name =: resourceCriteraString AND Project__r.Name =: projectCriteraString';
        system.debug('------queryString--------'+queryString);
        if(queryString == null){
            timeCardWrapperList = new List < timeCardWrapper > ();
            timeCardWrapperList = fetchTimeCards();
            if(timeCardWrapperList.size() == 0){
                errorMessage = 'No Pending Timesheets for the selected filter';
            }
        }
        if(queryString != null){
            timeCardWrapperList = new List < timeCardWrapper > ();
            for (Assignment__c relatedAssignment : database.query(queryString)){
                if(relatedAssignment.RecordType.Name == 'Primary' && relatedAssignment.Billing_Type__c == 'Billable'){
                    timeCardWrapper newTimeCardWrpper = new timeCardWrapper();
                    newTimeCardWrpper.relatedAssignment = relatedAssignment;
                    for (Time_Card__c relatedTimeCard: relatedAssignment.Time_Cards__r) {
                        newTimeCardWrpper.resourceNameString = userIdWithNameMap.get(relatedTimeCard.Assignment__r.Resource__r.User__c);
                    
                        totalBillableHours = totalBillableHours+relatedTimeCard.EP_Total_Number_of_Billable_Hours__c;
                        if (newTimeCardWrpper.assignmentWithTimeCardsMap.get(relatedTimeCard.Assignment__c) != null &&
                            !newTimeCardWrpper.assignmentWithTimeCardsMap.get(relatedTimeCard.Assignment__c).contains(relatedTimeCard)) {
                            newTimeCardWrpper.assignmentWithTimeCardsMap.get(relatedTimeCard.Assignment__c).add(relatedTimeCard);
                        } else {
                            List < Time_Card__c > releatedTimeCardsList = new List < Time_Card__c > ();
                            releatedTimeCardsList.add(relatedTimeCard);
                            newTimeCardWrpper.assignmentWithTimeCardsMap.put(relatedTimeCard.Assignment__c, releatedTimeCardsList);
                        }
                    }
                    if(!newTimeCardWrpper.assignmentWithTimeCardsMap.isEmpty()){
                        timeCardWrapperList.add(newTimeCardWrpper);
                    }
                }
            }
            if(timeCardWrapperList.size() == 0){
                errorMessage = 'No Pending Timesheets for the selected filter';
            }else{
                errorMessage = null;
            }
        }
        system.debug('----totalBillableHours------'+totalBillableHours);
        return null;
    }

    public List < timeCardWrapper > fetchTimeCards() {
        system.debug('--------wntered if ');
        totalBillableHours = 0.0;
        timeCardWrapperList = new List < timeCardWrapper > ();
        projectNameOptionsList = new List<SelectOption>();
        Set<SelectOption> projectNameSet = new Set<SelectOption>();
        projectNameSet.add(new SelectOption('', 'Please Select'));
        timeCardWrapperList = new List < timeCardWrapper > ();
        
        //Collect the assignments which are active on day the page loads
        for (Assignment__c relatedAssignment: [SELECT Id, Name, Start_Date__c, End_Date__c,
                Budgeted_Hours__c,Project__r.Name,EP_Shadow_Resource__r.User__r.Name, 
                Resource__r.User__r.Name,RecordType.Name,Billing_Type__c,
                (SELECT Id, Client_Details__c,Name,Assignment__r.Resource__r.User__c, Assignment__c, 
                EP_Total_Number_of_Billable_Hours__c, Start_Date__c, EP_End_Date__c, Status__c,Invoiced__c 
                FROM Time_Cards__r 
                WHERE Invoiced__c = False AND 
                    ((Client_Details__c = false AND Status__c = 'Approved') OR 
                    (Client_Details__c = true AND Status__c = 'Client Approved')))
                FROM Assignment__c 
                WHERE RecordType.Name = 'Primary' AND Billing_Type__c = 'Billable']){
            timeCardWrapper newTimeCardWrpper = new timeCardWrapper();
            newTimeCardWrpper.relatedAssignment = relatedAssignment;
            for (Time_Card__c relatedTimeCard: relatedAssignment.Time_Cards__r) {
                totalBillableHours = totalBillableHours+relatedTimeCard.EP_Total_Number_of_Billable_Hours__c;
                newTimeCardWrpper.resourceNameString = userIdWithNameMap.get(relatedTimeCard.Assignment__r.Resource__r.User__c);
                projectNameSet.add(new SelectOption(relatedAssignment.Project__r.Name, relatedAssignment.Project__r.Name));  
                if (newTimeCardWrpper.assignmentWithTimeCardsMap.get(relatedTimeCard.Assignment__c) != null &&
                    !newTimeCardWrpper.assignmentWithTimeCardsMap.get(relatedTimeCard.Assignment__c).contains(relatedTimeCard)) {
                    newTimeCardWrpper.assignmentWithTimeCardsMap.get(relatedTimeCard.Assignment__c).add(relatedTimeCard);
                } else {
                    List < Time_Card__c > releatedTimeCardsList = new List < Time_Card__c > ();
                    releatedTimeCardsList.add(relatedTimeCard);
                    newTimeCardWrpper.assignmentWithTimeCardsMap.put(relatedTimeCard.Assignment__c, releatedTimeCardsList);
                }
            }
            if(!newTimeCardWrpper.assignmentWithTimeCardsMap.isEmpty()){
                timeCardWrapperList.add(newTimeCardWrpper);
            }
        }
        projectNameOptionsList.addAll(projectNameSet);
        if(timeCardWrapperList.size() == 0){
            errorMessage = 'No Pending Timesheets to Invoice';
        }
        
        return timeCardWrapperList; 
    }
    //Fetch the related data method -end
        
    //Wrapper class - start
    public class timeCardWrapper {
        
        public Assignment__c relatedAssignment {
            get;
            set;
        }
        public Map < Id, List < Time_Card__c >> assignmentWithTimeCardsMap {
            get;
            set;
        }
        public String resourceNameString{get;set;}
        public timeCardWrapper() {
            this.relatedAssignment = new Assignment__c();
            this.assignmentWithTimeCardsMap = new Map < Id, List < Time_Card__c >> ();
        }
    
    }
    //Wrapper class -End
	public pageReference reDirect(){
        PageReference pageref = new PageReference('/apex/PDFCSSPage');
        pageref.getParameters().put('id',oppIdToWin);
        pageref.setRedirect(false);
        return pageref;  
    }
    
    public pageReference reZip(){
       Time_Card__c timecardtoreview = [Select Id,Name,Assignment__r.Resource__r.Name,Start_Date__c From Time_Card__C Where Id=:oppIdToWin];
      
    //String objectId = oppIdToWin; // your object's Id
    String docName = timecardtoreview.Assignment__r.Resource__r.Name+' Attachments '+timecardtoreview.Start_Date__c+'.zip';
    List<Attachment> attachments = [SELECT Id,Name,ParentId, Body FROM Attachment WHERE ParentId = :oppIdToWin];
    if(attachments!=NULL){
        ishide = true;  
    }
 
    System.debug('>>> attachments ' + attachments.size());
    Zippex sampleZip = new Zippex();
    for(Attachment file : attachments) {
      sampleZip.addFile( file.Name, file.Body, null);
    }
    try{
      
      Document doc = new Document();
      doc.FolderId = [SELECT Id From Folder WHERE Name='AttachmentsZipFolder' LIMIT 1].id;
      doc.Name = docName;
      doc.Body = sampleZip.getZipArchive();
      insert doc;
      System.debug('>>> doc ' + doc.Id);
      return new PageReference('https://qa-standavglobal.cs92.force.com/StandavPortal/servlet/servlet.FileDownload?file=' + doc.Id);
    } catch ( Exception ex ) {
      System.debug('>>> ERROR ' + ex);
    }
       
    return null;   
    }
    //Save invoiced time cards 
    public void saveInvoicedTimeCards(){

        List<Time_Card__c> timeCardsToInvoicedList = new List<Time_Card__c>();
        for(timeCardWrapper timeCardToSave : timeCardWrapperList){
            for(String assignmentId : timeCardToSave.assignmentWithTimeCardsMap.keySet()){
                for(Time_Card__c timeCard : timeCardToSave.assignmentWithTimeCardsMap.get(assignmentId)){
                    if(timeCard.Invoiced__c){
                        timeCardsToInvoicedList.add(timeCard);
                    }
                }
            }
        }
        if(timeCardsToInvoicedList.size() > 0){
            update timeCardsToInvoicedList;
        }
        fetchTimeCards();
    }
}