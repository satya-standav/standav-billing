global class updateShadowTimeCard{
    @InvocableMethod(label='Update Shadow Time Card Status' description='Update the Shadow Time Card Status')
    public static void updateStatus(List<Time_Card__c> primaryTimeCardsIdSet){
        
        Set<Id> relatedAssignmentIdSet = new Set<Id>();
        List<Time_Card__c> relatedShadowTimeCardsListToUpdate = new List<Time_Card__c>();
        List<Time_Card__c> relatedShadowTimeCardsList;
        Map<String, String> projectWithResourceMap = new Map<string, String>();
        Map<String, String> primaryAssignmentIsWithstatusMap = new Map<string, String>();
        Map<String, String> datesWithResourceMap = new Map<string, String>();
        
        for(Time_Card__c primaryTimeCard : primaryTimeCardsIdSet){
            if(primaryTimeCard.Assignment__c != null){
                relatedAssignmentIdSet.add(primaryTimeCard.Assignment__c);
                primaryAssignmentIsWithstatusMap.put(primaryTimeCard.Id, primaryTimeCard.Status__c );
            }
            datesWithResourceMap.put(primaryTimeCard.EP_Primary_Resource_Email_Id__c, primaryTimeCard.Start_Date__c.format()+primaryTimeCard.EP_End_Date__c.format());
        }
        if(relatedAssignmentIdSet.size() > 0){
            for(Assignment__c assignment : [SELECT Id, Project__c, Resource__c,RecordType.Name FROM Assignment__c WHERE Id IN: relatedAssignmentIdSet]){
                if(assignment.Project__c != null && assignment.Resource__c != null){
                    projectWithResourceMap.put(assignment.Project__c, assignment.Resource__c);
                }
            }
        }
        if(!projectWithResourceMap.isEmpty()){
            relatedShadowTimeCardsList = [SELECT Id, Status__c, Assignment__r.Resource__c, Assignment__r.Project__c, Assignment__c, 
                                         Assignment__r.RecordType.Name, Assignment__r.RecordTypeId, Start_Date__c, EP_End_Date__c,
                                         EP_Primary_Resource_Email_Id__c 
                                         FROM Time_Card__c 
                                         WHERE Assignment__r.Project__c IN: projectWithResourceMap.keySet() AND
                                         Assignment__r.Resource__c IN: projectWithResourceMap.values() AND 
                                         Assignment__r.RecordType.Name = 'Shadow'];
        }
        if(relatedShadowTimeCardsList.size() > 0){
            for(Time_Card__c primaryTimeCard : primaryTimeCardsIdSet){
                for(Time_Card__c shadowTimeCard : relatedShadowTimeCardsList){
                         string resourceName = shadowTimeCard.Assignment__r.Resource__c;
                             if(shadowTimeCard.Assignment__r.Resource__c == projectWithResourceMap.get(shadowTimeCard.Assignment__r.Project__c) &&
                                 !primaryAssignmentIsWithstatusMap.isEmpty() &&
                                 shadowTimeCard.Start_Date__c.format()+shadowTimeCard.EP_End_Date__c.format() == datesWithResourceMap.get(shadowTimeCard.EP_Primary_Resource_Email_Id__c)){
                                 shadowTimeCard.Status__c = primaryAssignmentIsWithstatusMap.get(primaryTimeCard.Id);
                                     relatedShadowTimeCardsListToUpdate.add(shadowTimeCard);
                             }
                     }
             }
             if(relatedShadowTimeCardsListToUpdate.size() > 0){
                 update relatedShadowTimeCardsListToUpdate;
             }
         }
     }
}