@isTest
public class InvoiceforFinanceTest {
    private static testMethod void test() {
            User user = new User();
            user.LastName = 'Test User';
            user.Alias = 'tu';
            user.Email = 'test@gmail.com';
            user.Username = 'test.dev@stand.com';
            user.CommunityNickname = 'test.t';
            //user.UserRoleId = '00ei0000000rTfp';
            user.User_Time_Zone__c = 'Pst';
            user.ProfileId = '00e54000000HSHG';           
            user.TimeZoneSidKey    = 'America/Denver';
            user.LocaleSidKey      = 'en_US';
            user.EmailEncodingKey  = 'UTF-8';
            user.LanguageLocaleKey = 'en_US';
            insert user;
        
            Account acc = new Account();
        	acc.Name = 'Test Account';
        	insert acc;
         
            Contact con = new Contact();
        	con.FirstName = 'test';
        	con.LastName = 'contact';
        	insert con;
        
            Project__c project = new Project__c();
            project.Name = 'Test Project';
            project.EP_Project_Manager__c = user.Id;
            project.Contact_Person__c = con.Id;
            project.Start_Date__c = Date.newInstance(2016, 12, 9);
        	project.Account__c = acc.id;
        	project.Number__c = 2;
            insert project;        
            update project;
        
            Resource__c resource = new Resource__c();
            resource.User__c = user.Id;
            resource.Contact__c = con.Id;
            insert resource;        
            update resource;
        
            Assignment__c assignment = new Assignment__c();
            assignment.Project__c = project.id;
            assignment.Resource__c = resource.id;
            assignment.Status__c = 'Approved';
            assignment.Start_Date__c = Date.newInstance(2016, 12, 9);
            assignment.End_Date__c = Date.newInstance(2018, 12, 9);
            assignment.Role__c = 'Developer';
            assignment.Billing_Type__c = 'Billable';
            assignment.Assignment_Type__c = 'Long Term';  
        	assignment.Budgeted_Hours__c = 30;
            //assignment.RecordType.Name = 'Primary';
            //assignment. RecordType.Name = 'Primary';
            insert assignment;        
           // update assignment;
            
            Time_Card__c card = new Time_Card__c();
        	card.Assignment__c = assignment.id;
            card.Billable_Hours_Fri__c = 4;
            card.Billable_Hours_Mon__c =4;
            card.Billable_Hours_Sat__c = 5;
            card.Billable_Hours_Sun__c = 0;
            card.Billable_Hours_Thu__c = 4;
            card.Billable_Hours_Tue__c = 4;
            card.Billable_Hours_Wed__c =4;
            card.Status__c = 'Approved';  
            card.Invoiced__c = False;
            //card.Assignment__r.Budgeted_Hours__c = 200;          
            insert card; 
          

            Test.startTest(); 
        
                PageReference myVfPage = Page.FinanceInvoicePage;
                myVfPage.getParameters().put('id',card.id);
                Test.setCurrentPage(myVfPage); 
                String id = ApexPages.currentPage().getParameters().get('id');
        
               List<FinanceInvoicePageController.timeCardWrapper> newTimeCardWrpper;
               FinanceInvoicePageController.timeCardWrapper testWrap1=new FinanceInvoicePageController.timeCardWrapper();  
               FinanceInvoicePageController f1 = new FinanceInvoicePageController();
               f1.fetchTimeCards();
               f1.saveInvoicedTimeCards();
               //timeCardsToInvoicedList.add(card);

        
               List<InvoicedCardsPageController.timeCardWrapper> newTimeCardWrpper1;
               InvoicedCardsPageController.timeCardWrapper testWrap2=new InvoicedCardsPageController.timeCardWrapper(); 
               InvoicedCardsPageController f2 = new InvoicedCardsPageController();
               f2.fetchTimeCards();
               //f2.saveInvoicedTimeCards();
               //System.assertEquals(card.Status__c, 'Approved');  

               List<PendingApprovalforFinanceController.timeCardWrapper> newTimeCardWrpper2;
               PendingApprovalforFinanceController.timeCardWrapper testWrap3=new PendingApprovalforFinanceController.timeCardWrapper(); 
               PendingApprovalforFinanceController f3 = new PendingApprovalforFinanceController();
               f3.fetchTimeCards();
               //f3.saveInvoicedTimeCards();
              
        Test.stopTest();
    }
private static testMethod void test1() {
            User user = new User();
            user.LastName = 'Test User';
            user.Alias = 'tu';
            user.Email = 'test@gmail.com';
            user.Username = 'test.dev@stand.com';
            user.CommunityNickname = 'test.t';
            //user.UserRoleId = '00ei0000000rTfp';
            user.User_Time_Zone__c = 'Pst';
            user.ProfileId = '00e54000000HSHG';           
            user.TimeZoneSidKey    = 'America/Denver';
            user.LocaleSidKey      = 'en_US';
            user.EmailEncodingKey  = 'UTF-8';
            user.LanguageLocaleKey = 'en_US';
            insert user;
        
            Account acc = new Account();
        	acc.Name = 'Test Account';
        	insert acc;
         
            Contact con = new Contact();
        	con.FirstName = 'test';
        	con.LastName = 'contact';
        	insert con;
        
            Project__c project = new Project__c();
            project.Name = 'Test Project';
            project.EP_Project_Manager__c = user.Id;
            project.Contact_Person__c = con.Id;
            project.Start_Date__c = Date.newInstance(2016, 12, 9);
        	project.Account__c = acc.id;
        	project.Number__c = 2;
            insert project;        
            update project;
        
            Resource__c resource = new Resource__c();
            resource.User__c = user.Id;
            resource.Contact__c = con.Id;
            insert resource;        
            update resource;
        
            Assignment__c assignment = new Assignment__c();
            assignment.Project__c = project.id;
            assignment.Resource__c = resource.id;
            assignment.Status__c = 'Approved';
            assignment.Start_Date__c = Date.newInstance(2016, 12, 9);
            assignment.End_Date__c = Date.newInstance(2018, 12, 9);
            assignment.Role__c = 'Developer';
            assignment.Billing_Type__c = 'Billable';
            assignment.Assignment_Type__c = 'Long Term';  
    		assignment.Budgeted_Hours__c = 30;
            //assignment.RecordType.Name = 'Primary';
            //assignment. RecordType.Name = 'Primary';
            insert assignment;        
           // update assignment;
            
            Time_Card__c card = new Time_Card__c();
        	card.Assignment__c = assignment.id;
            card.Billable_Hours_Fri__c = 4;
            card.Billable_Hours_Mon__c =4;
            card.Billable_Hours_Sat__c = 5;
            card.Billable_Hours_Sun__c = 0;
            card.Billable_Hours_Thu__c = 4;
            card.Billable_Hours_Tue__c = 4;
            card.Billable_Hours_Wed__c =4;
            card.Status__c = 'Approved';  
            card.Invoiced__c = True;
            //card.Assignment__r.Budgeted_Hours__c = 200;          
            insert card; 
          

            Test.startTest(); 
        
                PageReference myVfPage = Page.FinanceInvoicePage;
                myVfPage.getParameters().put('id',card.id);
                Test.setCurrentPage(myVfPage); 
                String id = ApexPages.currentPage().getParameters().get('id');
        
               List<FinanceInvoicePageController.timeCardWrapper> newTimeCardWrpper;
               FinanceInvoicePageController.timeCardWrapper testWrap1=new FinanceInvoicePageController.timeCardWrapper();  
               FinanceInvoicePageController f1 = new FinanceInvoicePageController();
               f1.fetchTimeCards();
               f1.saveInvoicedTimeCards();
               //timeCardsToInvoicedList.add(card);

        
               List<InvoicedCardsPageController.timeCardWrapper> newTimeCardWrpper1;
               InvoicedCardsPageController.timeCardWrapper testWrap2=new InvoicedCardsPageController.timeCardWrapper(); 
               InvoicedCardsPageController f2 = new InvoicedCardsPageController();
               f2.fetchTimeCards();
               //f2.saveInvoicedTimeCards();
               //System.assertEquals(card.Status__c, 'Approved');  

               List<PendingApprovalforFinanceController.timeCardWrapper> newTimeCardWrpper2;
               PendingApprovalforFinanceController.timeCardWrapper testWrap3=new PendingApprovalforFinanceController.timeCardWrapper(); 
               PendingApprovalforFinanceController f3 = new PendingApprovalforFinanceController();
               f3.fetchTimeCards();
               //f3.saveInvoicedTimeCards();
              
        Test.stopTest();
    }

}