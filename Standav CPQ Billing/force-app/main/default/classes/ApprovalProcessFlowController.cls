/**
*  @Class Name: ApprovalProcessFlowController
*  @Description: Handles all functionality required for approval process of time cards.
*  @Company: Standav
*  @CreatedDate: 11/29/2019
*  Change Log
*  -----------------------------------------------------------
*  Developer: Shruti MP           
Modification Date : 03/05/2020      
Comments
*  -----------------------------------------------------------
*
*/
public class ApprovalProcessFlowController{
    /*
    *  Method Name         : calculateApprovalRule
    *  param               : List<Time_Card__C>
    *  Description         : Method to automate the approval process for time sheets which are submitted.
    */
    public static void calculateApprovalRule(List<Time_Card__C> relatedTimeCardsToSubmittedList, Set<Id> timeCardIdSet){
        
        if(relatedTimeCardsToSubmittedList == null){
            relatedTimeCardsToSubmittedList = [SELECT Id, Assignment__r.Project__c, Name, Assignment__r.EP_Shadow_Resource__c, Assignment__r.Resource__c,
                                                Type__c, EP_End_Date__c, Billable_Hours_Fri__c, EP_Notes__c, EP_Approval_Date__c, 
                                                EP_Total_Number_of_Billable_Hours__c, EP_Total_Number_of_Unbillable_Hours__c, 
                                                EP_Rejected_Date__c, Submitted_On__c, Project_Manager__c, Billable_Hours_Mon__c, 
                                                Billable_Hours_Sat__c, Assignment__c, Status__c, Start_Date__c, Billable_Hours_Sun__c, 
                                                Billable_Hours_Thu__c, Billable_Hours_Tue__c, Billable_Hours_Wed__c, UnBillable_Hours_Fri__c, 
                                                UnBillable_Hours_Mon__c, UnBillable_Hours_Sat__c, UnBillable_Hours_Sun__c, UnBillable_Hours_Thu__c, 
                                                UnBillable_Hours_Tue__c, UnBillable_Hours_Wed__c, Assignment__r.Project__r.Name,
                                                Assignment__r.Resource__r.User__r.Name , Assignment__r.RecordType.Name, Assignment__r.Is_Primary_Resource_Contributing__c,
                                                X1st_Approver__c, X2nd_Approver__c, X3rd_Approver__c, X4th_Approver__c, X5th_Approver__c, Approval_Status__c
                                                FROM Time_Card__c
                                                WHERE Id IN: timeCardIdSet];
        }
        Set<Id> relatedProjectIdSet = new Set<Id>();
        Set<Id> relatedAssignmentIdSet = new Set<Id>();
        Map<String, Id> approvalStepWithApproverMap = new Map<String, Id>{};
        List<Time_Card__c> timeCardsWithApproversList = new List<Time_Card__c>();
        List<Time_Card__c> relatedShadowTimeCardsToUpdateList = new List<Time_Card__c>();
        Map<Id, String> userWithEmailMap = new Map<Id, String>();
        
        system.debug('----------------relatedTimeCardsToSubmittedList----------'+relatedTimeCardsToSubmittedList);
        system.debug('----------------relatedTimeCardsToSubmittedList----size------'+relatedTimeCardsToSubmittedList.size());
        for(Time_Card__c timeCard : relatedTimeCardsToSubmittedList){
            system.debug('------------timeCard.Assignment__r.Project__c---------'+timeCard.Assignment__r.Project__c);
            if(timeCard.Assignment__r.Project__c != null){
                relatedProjectIdSet.add(timeCard.Assignment__r.Project__c);
            }else{
                relatedAssignmentIdSet.add(timeCard.Assignment__c);
            }    
        }
        if(relatedAssignmentIdSet.size() > 0){
            for(Assignment__c assignment : [SELECT Id, Project__c
                                            FROM Assignment__c WHERE 
                                            Id IN: relatedAssignmentIdSet]){
                if(assignment.Project__c != null){
                    relatedProjectIdSet.add(assignment.Project__c);
                }
            }
        }
        system.debug('-------relatedProjectIdSet-------------'+relatedProjectIdSet);
        if(relatedProjectIdSet.size() > 0){
            for(Approvers_Flow__c approverFlow : [SELECT Id, Order_of_Approval_Step__c, Client_Approver__c, Project__c, User_Approver__c, User_Approver__r.Email 
                                                 FROM Approvers_Flow__c WHERE Project__c IN: relatedProjectIdSet]){
                system.debug('----------approverFlow.Order_of_Approval_Step__c--------------'+approverFlow.Order_of_Approval_Step__c);
                if(approverFlow.User_Approver__c != null){
                    approvalStepWithApproverMap.put(approverFlow.Project__c+'-'+approverFlow.Order_of_Approval_Step__c, approverFlow.User_Approver__c);
                    userWithEmailMap.put(approverFlow.User_Approver__c, approverFlow.User_Approver__r.Email);
                }else if(approverFlow.Client_Approver__c != null){
                    approvalStepWithApproverMap.put(approverFlow.Project__c+'-'+approverFlow.Order_of_Approval_Step__c, approverFlow.Client_Approver__c);
                }
            }
        }
        system.debug('-------approvalStepWithApproverMap-------------'+approvalStepWithApproverMap);
        if(!approvalStepWithApproverMap.isEmpty()){
            for(Time_Card__c timeCard : relatedTimeCardsToSubmittedList){
                //if(timeCard.Assignment__r.Project__c != null){
                    system.debug('-----timeCard.Approval_Status__c----------'+timeCard.Approval_Status__c);

                    timeCard.X1st_Approver__c = approvalStepWithApproverMap.get(timeCard.Assignment__r.Project__c+'-1') != null ?
                                                approvalStepWithApproverMap.get(timeCard.Assignment__r.Project__c+'-1') : timeCard.X1st_Approver__c;
                    timeCard.X2nd_Approver__c = approvalStepWithApproverMap.get(timeCard.Assignment__r.Project__c+'-2') != null ?
                                                approvalStepWithApproverMap.get(timeCard.Assignment__r.Project__c+'-2') : timeCard.X2nd_Approver__c;
                    timeCard.X3rd_Approver__c = approvalStepWithApproverMap.get(timeCard.Assignment__r.Project__c+'-3') != null ?
                                                approvalStepWithApproverMap.get(timeCard.Assignment__r.Project__c+'-3') : timeCard.X3rd_Approver__c;
                    timeCard.X4th_Approver__c = approvalStepWithApproverMap.get(timeCard.Assignment__r.Project__c+'-4') != null ?
                                                approvalStepWithApproverMap.get(timeCard.Assignment__r.Project__c+'-4') : timeCard.X4th_Approver__c;
                    timeCard.X5th_Approver__c = approvalStepWithApproverMap.get(timeCard.Assignment__r.Project__c+'-5') != null ?
                                                approvalStepWithApproverMap.get(timeCard.Assignment__r.Project__c+'-5') : timeCard.X5th_Approver__c;
                    
                    timeCard.Current_Approver_Email__c = userWithEmailMap.get(timeCard.X1st_Approver__c) != null ? userWithEmailMap.get(timeCard.X1st_Approver__c) : null;
                    timeCard.X2nd_Approver_Email__c = userWithEmailMap.get(timeCard.X2nd_Approver__c) != null ? userWithEmailMap.get(timeCard.X2nd_Approver__c) : null;
                    timeCard.X3rd_Approver_Email__c = userWithEmailMap.get(timeCard.X3rd_Approver__c) != null ? userWithEmailMap.get(timeCard.X3rd_Approver__c) : null;
                    timeCard.X4th_Approver_Email__c = userWithEmailMap.get(timeCard.X4th_Approver__c) != null ? userWithEmailMap.get(timeCard.X4th_Approver__c) : null;
                    timeCard.X5th_Approver_Email__c = userWithEmailMap.get(timeCard.X5th_Approver__c) != null ? userWithEmailMap.get(timeCard.X5th_Approver__c) : null;
                    
                    if(timeCard.Approval_Status__c == null && timeCard.X1st_Approver__c != null && 
                        String.valueOf(timeCard.X1st_Approver__c).substring(0,3) == String.valueOf(userInfo.getUSerID()).substring(0,3)){
                        timeCard.Project_Manager__c = timeCard.X1st_Approver__c;
                        timeCard.Current_Approver_Email__c = userWithEmailMap.get(timeCard.X1st_Approver__c);
                    }
                    if(timeCard.Approval_Status__c == 'Approver 1 Approved' && timeCard.X2nd_Approver__c != null && 
                        String.valueOf(timeCard.X2nd_Approver__c).substring(0,3) == String.valueOf(userInfo.getUSerID()).substring(0,3)){
                        timeCard.Project_Manager__c = timeCard.X2nd_Approver__c;
                        timeCard.X2nd_Approver_Email__c = userWithEmailMap.get(timeCard.X2nd_Approver__c);
                    }
                    if(timeCard.Approval_Status__c == 'Approver 2 Approved' && timeCard.X3rd_Approver__c  != null && 
                        String.valueOf(timeCard.X3rd_Approver__c).substring(0,3) == String.valueOf(userInfo.getUSerID()).substring(0,3)){
                        timeCard.Project_Manager__c = timeCard.X3rd_Approver__c ;
                        timeCard.X3rd_Approver_Email__c = userWithEmailMap.get(timeCard.X3rd_Approver__c);
                    }
                    if(timeCard.Approval_Status__c == 'Approver 3 Approved' && approvalStepWithApproverMap.get(timeCard.Assignment__r.Project__c+'-4') != null && 
                        String.valueOf(timeCard.X4th_Approver__c).substring(0,3) == String.valueOf(userInfo.getUSerID()).substring(0,3)){
                        timeCard.Project_Manager__c = approvalStepWithApproverMap.get(timeCard.Assignment__r.Project__c+'-4');
                        timeCard.X4th_Approver_Email__c = userWithEmailMap.get(timeCard.X4th_Approver__c);
                    }
                    if(timeCard.Approval_Status__c == 'Approver 4 Approved' && approvalStepWithApproverMap.get(timeCard.Assignment__r.Project__c+'-5') != null && 
                        String.valueOf(timeCard.X5th_Approver__c).substring(0,3) == String.valueOf(userInfo.getUSerID()).substring(0,3)){
                        timeCard.Project_Manager__c = approvalStepWithApproverMap.get(timeCard.Assignment__r.Project__c+'-5');
                        timeCard.X5th_Approver_Email__c = userWithEmailMap.get(timeCard.X5th_Approver__c);
                    }
                            
                    system.debug('-----timeCard.Project_Manager__c----------'+timeCard.Project_Manager__c);
                    timeCardsWithApproversList.add(timeCard);                
            }
        }
        if(timeCardsWithApproversList.size() > 0){
            update timeCardsWithApproversList;
        }
        system.debug('--------timeCardsWithApproversList----------'+timeCardsWithApproversList);
        system.debug('--------timeCardsWithApproversList------size----'+timeCardsWithApproversList.size());
        //Start automatic submission to approval process
         for(Time_Card__c timeCard : timeCardsWithApproversList){
             system.debug('--------timeCard----------'+timeCard);
             system.debug('--------timeCard-Approval_Status__c---------'+timeCard.Approval_Status__c);
             system.debug('--------timeCard----X1st_Approver__c------'+timeCard.X1st_Approver__c);
             system.debug('--------timeCard-Status__c---------'+timeCard.Status__c);
             system.debug('--------timeCard-Assignment__r.RecordType.Name---------'+timeCard.Assignment__r.RecordType.Name);
             if(timeCard.Approval_Status__c == null && timeCard.X1st_Approver__c != null && 
                 (String.valueOf(timeCard.X1st_Approver__c).substring(0,3) == String.valueOf(userInfo.getUSerID()).substring(0,3))){
                 if(!Approval.isLocked(timeCard.Id) && timeCard.Status__c == 'Submitted'){
                     // Create an approval request for the Opportunity
                     system.debug('-------approval history------------'+Approval.isLocked(timeCard.Id));
                    // ApprovalProcessFlowController.createapprovalProcess(timeCard.id, timeCard.X1st_Approver__c);
                 }
             }
             system.debug('--------timeCard----X2nd_Approver__c------'+timeCard.X2nd_Approver__c);
             if(timeCard.Approval_Status__c == 'Approver 1 Approved' && timeCard.X2nd_Approver__c != null && 
                 (String.valueOf(timeCard.X2nd_Approver__c).substring(0,3) == String.valueOf(userInfo.getUSerID()).substring(0,3))
                 && !Approval.isLocked(timeCard.Id) && timeCard.Status__c == 'Submitted'){
                 //ApprovalProcessFlowController.createapprovalProcess(timeCard.id, timeCard.X2nd_Approver__c);
             }
             system.debug('--------timeCard----X3rd_Approver__c------'+timeCard.X3rd_Approver__c);
            if(timeCard.Approval_Status__c == 'Approver 2 Approved' && timeCard.X3rd_Approver__c != null && 
                (String.valueOf(timeCard.X3rd_Approver__c).substring(0,3) == String.valueOf(userInfo.getUSerID()).substring(0,3))
                && !Approval.isLocked(timeCard.Id) && timeCard.Status__c == 'Submitted'){
                //ApprovalProcessFlowController.createapprovalProcess(timeCard.id, timeCard.X3rd_Approver__c);
            }
            if(timeCard.Approval_Status__c == 'Approver 3 Approved' && timeCard.X4th_Approver__c != null && 
                (String.valueOf(timeCard.X4th_Approver__c).substring(0,3) == String.valueOf(userInfo.getUSerID()).substring(0,3))
                && !Approval.isLocked(timeCard.Id) && timeCard.Status__c == 'Submitted'){
                //ApprovalProcessFlowController.createapprovalProcess(timeCard.id, timeCard.X4th_Approver__c);
            }
            if(timeCard.Approval_Status__c == 'Approver 4 Approved' && timeCard.X3rd_Approver__c != null && 
                (String.valueOf(timeCard.X5th_Approver__c).substring(0,3) == String.valueOf(userInfo.getUSerID()).substring(0,3))
                && !Approval.isLocked(timeCard.Id) && timeCard.Status__c == 'Submitted'){
                //ApprovalProcessFlowController.createapprovalProcess(timeCard.id, timeCard.X5th_Approver__c);
            }
         }
    }
    //Create approval process 
    public static Approval.ProcessResult createapprovalProcess(Id objectId, Id approverId){
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        system.debug('-------------req1-------'+req1);
        system.debug('-------------approverId-------'+approverId);
        system.debug('-------------objectId-------'+objectId);
        req1.setComments('Submitting request for approval automatically using Trigger');
        req1.setObjectId(objectId);
        req1.setNextApproverIds(new Id[] {approverId});
        // Submit the approval request for the Opportunity
        Approval.ProcessResult result = Approval.process(req1); 
        system.debug('----1---------req1-------'+req1);
        system.debug('----1---------result-------'+result); 
        return result;  
    }
}