/**
*  @Class Name: UsageCreationTriggerHandler  
*  @Description: Handler to create usage from TimeCardTrigger
*  @Company: Standav
*  @CreatedDate: 9/07/2020
*  Change Log
*  -----------------------------------------------------------
*  Developer           Modification Date           Comments
*  -----------------------------------------------------------
*  Avinash V             09/07/2020                 Original Version
*/
public class UsageCreationTriggerHandler {
    public static void CreateUsage(List<Time_Card__c> timecardlist){
           
        Time_Card__c tc = timecardlist[0];
        if(tc.Status__c == 'Approved' || tc.Status__c == 'Client Approved'){
        //get the recent time card
        //Project__c prj = tc.Project__c;
        id projid = tc.Project__c;
        //get project
        //List<Order> ordlist = [SELECT Id FROM Order WHERE Project__r.Id =: projid ];
        List<Order> ordlist = [SELECT Id FROM Order WHERE Project__c =: projid ];
        //Get the order
        
        //Fetch time_card - time and quantity
        Date tc_startdate = tc.Start_Date__c;
        Date tc_enddate = tc.EP_End_Date__c;
        Decimal quantity = tc.EP_Total_Number_of_Billable_Hours__c;
        
        //Get Usage Summary for time card start date
        Order ord = ordlist[0];
        List<blng__UsageSummary__c> usgsummlist = [SELECT id,blng__MatchingId__c,blng__SummaryEndDate__c,blng__SummaryStartDate__c FROM blng__UsageSummary__c WHERE blng__Order__c =: ord.id ];
        
          blng__UsageSummary__c tc_us = new blng__UsageSummary__c();
        //Find the usage summary associated with time card
            for(blng__UsageSummary__c us : usgsummlist ){
                Date sum_start_date = us.blng__SummaryStartDate__c;
                Date sum_end_date = us.blng__SummaryEndDate__c;
                if(tc_startdate >= sum_start_date && tc_startdate <= sum_end_date){
                    tc_us = us;  
                }
            }
            
            Datetime tc_start_datetime = (Datetime)tc_startdate;
            Datetime tc_end_datetime = (Datetime)tc_enddate;
            //now we have usage summary
            //
            //now create a usage
            system.debug('Usage being created');
            blng__Usage__c usage1 = new blng__Usage__c();
            usage1.blng__MatchingId__c = tc_us.blng__MatchingId__c;
            usage1.blng__UsageSummary__c = tc_us.Id; 
            usage1.blng__StartDateTime__c = tc_start_datetime;
            usage1.blng__EndDateTime__c = tc_end_datetime;
            usage1.blng__Quantity__c = quantity;
            //insert usage1;
            Database.SaveResult srusage = Database.insert(usage1, false);
			if (srusage.isSuccess()) {
       		 // Operation was successful, so get the ID of the record that was processed
     		   System.debug('Successfully inserted Usage. Id: ' + srusage.getId());
   			 }
   			 else {
     		   // Operation failed, so get all errors                
       		for(Database.Error err : srusage.getErrors()) {
            	System.debug('The following error has occurred.');                    
           	 	System.debug(err.getStatusCode() + ': ' + err.getMessage());
            	System.debug('Usage fields that affected this error: ' + err.getFields());
        	}
   		 	}
            system.debug('usage created');
          //insert usage
    }
    }

}