public class UserDataCache {
    /***********************************************************************************************
    *
    * Public Methods
    *
    ***********************************************************************************************/

    /**
     * @description:    This method returns a map of UserId to proposals
     * @param:          setIds - a set of Userids to get data for
     * @return:         Map<Id, List<User>> - map of UserId to proposals
     */
    public static Map<Id, User> getMap() {
        return loadMap();
    }

    /***********************************************************************************************
    *
    * Private Methods
    *
    ***********************************************************************************************/

    /**
     * @description:    Lazy-loads a mapping from id to proposal.
     *                  Stores the mapping in a static variable so that
     *                  it doesn't have to query every time.
     * @param:          setIds - a set of Userids to get data for
     * @return:         Map<Id, User> - map of UserId to proposals
     */
    private static Map<Id, User> cachedMap;
    private static Map<Id, User> loadMap() {
        cachedMap = new Map<Id, User>();
        for( User mappingEntry : [ SELECT Id, IsActive, LastName, FirstName, Name, ProfileId, Profile.Name 
                                FROM User 
                                WHERE (Profile.Name LIKE '%Community' OR Profile.Name LIKE '%(Community)') AND 
                                Profile.Name != 'Operation Team Profile - Community' AND 
                                Profile.Name != 'Operations(Community)' AND
                                IsActive = True] ) {

                cachedMap.put( mappingEntry.Id, mappingEntry );
        }
        return cachedMap;
    }
}