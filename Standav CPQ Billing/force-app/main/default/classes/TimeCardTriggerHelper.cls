public class TimeCardTriggerHelper{
    public static void afterInsertUpdatewMethod(List<Time_Card__c> relatedTimeCardList, Map<Id, Time_Card__c> oldTimeCardDataMap){
    	
        /*Map<String, String> primaryTimeCardMap = new Map<String, String>();
    	Map<Id, String> recordIdWithNameMap = new Map<Id, String>();
    	Set<Id> newAssignmentsSet = new Set<Id>();
    	Map<String, String> assignWithResourceMap = new Map<String, String>();
    	Map<String, String> assignWithProjectMap = new Map<String, String>();
    	Map<String, String> assignWithRecordTypetMap = new Map<String, String>();
    	List<Time_Card__c> timeCardToUpdate = new List<Time_Card__c>();
        Map<Date, Date> startWithEndDateMap = new Map<Date, Date>();
    	Map<String, Boolean> rejectedMap = new Map<String, Boolean>();

        for(Time_Card__c newTimeCard : relatedTimeCardList){
    		newAssignmentsSet.add(newTimeCard.Assignment__c);
    	}

    	for(Assignment__c assignment : [SELECT Id, Project__c, Resource__c, RecordType.Name, 
                                    	Role__c, Is_Primary_Resource_Contributing__c, 
                                        Start_Date__c, End_Date__c 
                                        FROM Assignment__c
                                        WHERE Id IN: newAssignmentsSet]){
			assignWithResourceMap.put(assignment.Id, assignment.Resource__c);
			assignWithProjectMap.put(assignment.Id, assignment.Project__c);
			assignWithRecordTypetMap.put(assignment.Id, assignment.RecordType.Name);
		}

		for(Time_Card__c newTimeCard : relatedTimeCardList){
			if(assignWithRecordTypetMap.containsKey(newTimeCard.Assignment__c) &&
				assignWithRecordTypetMap.get(newTimeCard.Assignment__c) == 'Primary'){
				primaryTimeCardMap.put(assignWithProjectMap.get(newTimeCard.Assignment__c)+'-'+newTimeCard.Start_Date__c+'-'+newTimeCard.EP_End_Date__c+'-'+assignWithResourceMap.get(newTimeCard.Assignment__c), newTimeCard.Status__c);
    		    startWithEndDateMap.put(newTimeCard.Start_Date__c, newTimeCard.EP_End_Date__c);
                rejectedMap.put(assignWithProjectMap.get(newTimeCard.Assignment__c)+'-'+newTimeCard.Start_Date__c+'-'+newTimeCard.EP_End_Date__c+'-'+assignWithResourceMap.get(newTimeCard.Assignment__c), newTimeCard.Rejected_By_Salesforce_Approvers__c);
            }
    	}
        for(Time_Card__c shadowTimeCard : [SELECT Id, Assignment__c, Assignment__r.Project__c,
                                            Assignment__r.Resource__c, Start_Date__c, EP_End_Date__c,
                                            Status__c
                                            FROM Time_Card__c
                                            WHERE Assignment__r.Project__c IN: assignWithProjectMap.values()
                                            AND Assignment__r.Resource__c IN: assignWithResourceMap.values()
                                            AND Start_Date__c IN: startWithEndDateMap.keyset()
                                            AND EP_End_Date__c IN: startWithEndDateMap.values()
                                            AND Assignment__r.RecordType.Name = 'Shadow']){
            if(primaryTimeCardMap.containsKey(shadowTimeCard.Assignment__r.Project__c+'-'+shadowTimeCard.Start_Date__c+'-'+shadowTimeCard.EP_End_Date__c+'-'+shadowTimeCard.Assignment__r.Resource__c)){
                shadowTimeCard.Status__c = primaryTimeCardMap.get(shadowTimeCard.Assignment__r.Project__c+'-'+shadowTimeCard.Start_Date__c+'-'+shadowTimeCard.EP_End_Date__c+'-'+shadowTimeCard.Assignment__r.Resource__c) == 'Approved' ||
                    primaryTimeCardMap.get(shadowTimeCard.Assignment__r.Project__c+'-'+shadowTimeCard.Start_Date__c+'-'+shadowTimeCard.EP_End_Date__c+'-'+shadowTimeCard.Assignment__r.Resource__c) == 'Client Approved' ?
                    primaryTimeCardMap.get(shadowTimeCard.Assignment__r.Project__c+'-'+shadowTimeCard.Start_Date__c+'-'+shadowTimeCard.EP_End_Date__c+'-'+shadowTimeCard.Assignment__r.Resource__c) :
                    shadowTimeCard.Status__c;
            if(!rejectedMap.isEmpty() &&
                rejectedMap.get(shadowTimeCard.Assignment__r.Project__c+'-'+shadowTimeCard.Start_Date__c+'-'+shadowTimeCard.EP_End_Date__c+'-'+shadowTimeCard.Assignment__r.Resource__c) == True){
               // shadowTimeCard.Status__c = 'Draft';
            }
            timeCardToUpdate.add(shadowTimeCard);
            }
        }
        if(timeCardToUpdate.size() > 0){
            update timeCardToUpdate;
        }*/
    }

    //After insert attach shadow time card with primary time card.
    public static void attachPrimaryAndShadowTimeCard(List<Time_Card__c> primaryTimeCardsList, Map<ID, Time_Card__c> oldTimeCardMap){
        try{
        set<Id> newTimeCardAssignmentIdSet = new Set<Id>();
        set<Id> newPriTimeCardAssignmentIdSet = new Set<Id>();
        List<Date> startDateList = new List<Date>();
        List<Date> endDateList = new List<Date>();
        List<Time_Card__c> timeCardToUpdate = new List<Time_Card__c>();
        List<Time_Card__c> shadowTimeCardToUpdate = new List<Time_Card__c>();
        Map<String, Time_Card__c> initialTimeCardMap = new Map<String, Time_Card__c>();
        
        for(Time_Card__c newTimeCard : primaryTimeCardsList){
            if(newTimeCard.Assignment_Type__c == 'Shadow'){
                newTimeCardAssignmentIdSet.add(newTimeCard.Assignment__c);
                startDateList.add(newTimeCard.Start_Date__c);
                endDateList.add(newTimeCard.EP_End_Date__c);
                initialTimeCardMap.put(newTimeCard.Start_Date__c+'-'+newTimeCard.EP_End_Date__c, newTimeCard);
            }
        }
        system.debug('-----newTimeCardAssignmentIdSet---------'+newTimeCardAssignmentIdSet);
        system.debug('--------startDateList------'+startDateList);
        system.debug('------endDateList--------'+endDateList);
        system.debug('-----initialTimeCardMap---------'+initialTimeCardMap);
        for(Assignment__c relatedAssignment : [SELECT Id, Related_Resource_Allocation__c
                                                FROM Assignment__c
                                                WHERE Id IN: newTimeCardAssignmentIdSet]){
            newPriTimeCardAssignmentIdSet.add(relatedAssignment.Related_Resource_Allocation__c);
        }
        if(newPriTimeCardAssignmentIdSet.size() > 0){
            system.debug('--------newTimeCardAssignmentIdSet.size()----'+newTimeCardAssignmentIdSet.size());
            for(Assignment__c relatedAssignment : [SELECT Id, Related_Resource_Allocation__c,
                                                    (Select Id, Start_Date__c, EP_End_Date__c, 
                                                    Related_Time_Card__c, Assignment_Type__c
                                                    FROM Time_Cards__r 
                                                    WHERE Start_Date__c IN: startDateList AND
                                                    EP_End_Date__c IN : endDateList AND
                                                    Assignment_Type__c = 'Primary')
                                                    FROM Assignment__c
                                                    WHERE Id IN : newPriTimeCardAssignmentIdSet]){
                system.debug('-----relatedAssignment---------'+relatedAssignment);
                for(Time_Card__c relatedTimeCard : relatedAssignment.Time_Cards__r){
                    system.debug('-----relatedTimeCard---------'+relatedTimeCard);
                    if(initialTimeCardMap.containsKey(relatedTimeCard.Start_Date__c+'-'+relatedTimeCard.EP_End_Date__c)){
                        relatedTimeCard.Related_Time_Card__c = initialTimeCardMap.get(relatedTimeCard.Start_Date__c+'-'+relatedTimeCard.EP_End_Date__c).Id;
                        system.debug('-----relatedTimeCard---1------'+relatedTimeCard);
                        timeCardToUpdate.add(relatedTimeCard);
                    }
                }
            }      
        }
        system.debug('-------timeCardToUpdate-------'+timeCardToUpdate);
        system.debug('-------timeCardToUpdate-size------'+timeCardToUpdate.size());
        if(timeCardToUpdate.size() > 0){
            update timeCardToUpdate;
        }

        //Update shadow
        for(Time_Card__c newTimeCard : primaryTimeCardsList){
            if(newTimeCard.Assignment_Type__c == 'Shadow'){
                for(Time_Card__c primaryTimeCard : timeCardToUpdate){
                    if(primaryTimeCard.Related_Time_Card__c == newTimeCard.Id){
                        Time_Card__c shadowTimeCard = new Time_Card__c();
                        shadowTimeCard.Id = newTimeCard.Id;
                        shadowTimeCard.Related_Time_Card__c = primaryTimeCard.Id;
                        shadowTimeCardToUpdate.add(shadowTimeCard);
                    }
                }
            }
        }
        if(shadowTimeCardToUpdate.size() > 0){
            update shadowTimeCardToUpdate;
        }

        }catch(Exception ex){
            system.debug('----------'+ex.getCause()+'-'+ex.getMessage()+'-'+ex.getLineNumber()+'-'+ex.getTypeName());
        }
    }

    //Reflect the primary status on shadow
    public static void updatePrimaryToShadow(List<Time_Card__c> newTimeCardMap, Map<Id, Time_Card__c> oldTimeCardMap){
        Set<Id> shadowTimeCardSet = new Set<Id>();
        Map<Id, String> priamryTimeCardWithStatusMap = new Map<Id, String>();
        List<Time_Card__c> timeCardToUpdateList = new List<Time_Card__c>();

        for(Time_Card__c primaryTimeCard : newTimeCardMap){
            if(primaryTimeCard != null && oldTimeCardMap.get(primaryTimeCard.Id) != null &&
                primaryTimeCard.Status__c != oldTimeCardMap.get(primaryTimeCard.Id).Status__c &&
                primaryTimeCard.Assignment_Type__c == 'Primary' && 
                ((primaryTimeCard.Rejected_By_Salesforce_Approvers__c == True &&
                primaryTimeCard.Status__c == 'Draft') || 
                    primaryTimeCard.Status__c == 'Approved' || 
                    primaryTimeCard.Status__c == 'Client Approved')){
                shadowTimeCardSet.add(primaryTimeCard.Related_Time_Card__c);
                priamryTimeCardWithStatusMap.put(primaryTimeCard.Related_Time_Card__c, primaryTimeCard.Status__c);
            }
        }
        if(shadowTimeCardSet.size() > 0){
            for(Time_Card__c shadowTimeCard : [SELECT Id, Status__c, Assignment_Type__c
                                                FROM Time_Card__c
                                                WHERE Id IN: shadowTimeCardSet
                                                AND Assignment_Type__c = 'Shadow']){
                shadowTimeCard.Status__c = priamryTimeCardWithStatusMap.get(shadowTimeCard.Id);
                timeCardToUpdateList.add(shadowTimeCard);
            }
        }
        if(timeCardToUpdateList.size() > 0){
            update timeCardToUpdateList;
        }
    }
}