@isTest
private class AssignmentTriggerTest{
   // @testSetUp 
   @isTest
    static void createData(){
      
        User user = new User();
            user.LastName = 'Test';
            user.Alias = 'tester';
            user.Email = 'test@gmail.com';
            user.Username = 'test@stand.com';
            user.CommunityNickname = 'te';
            user.User_Time_Zone__c = 'Pst';
            user.ProfileId = '00e54000000HSHG';           
            user.TimeZoneSidKey    = 'America/Denver';
            user.LocaleSidKey      = 'en_US';
            user.EmailEncodingKey  = 'UTF-8';
            user.LanguageLocaleKey = 'en_US';
            insert user;
        
        User user1 = new User();
            user1.LastName = 'Test1';
            user1.Alias = 'tester1';
            user1.Email = 'test1@gmail.com';
            user1.Username = 'test1@stand.com';
            user1.CommunityNickname = 'te1';
            user1.User_Time_Zone__c = 'Pst';
            user1.ProfileId = '00e54000000HSHG';           
            user1.TimeZoneSidKey    = 'America/Denver';
            user1.LocaleSidKey      = 'en_US';
            user1.EmailEncodingKey  = 'UTF-8';
            user1.LanguageLocaleKey = 'en_US';
            insert user1;
        
        User user2 = new User();
            user2.LastName = 'Test2';
            user2.Alias = 'tester2';
            user2.Email = 'test2@gmail.com';
            user2.Username = 'test2@stand.com';
            user2.CommunityNickname = 'te2';
            user2.User_Time_Zone__c = 'Pst';
            user2.ProfileId = '00e54000000HSHG';           
            user2.TimeZoneSidKey    = 'America/Denver';
            user2.LocaleSidKey      = 'en_US';
            user2.EmailEncodingKey  = 'UTF-8';
            user2.LanguageLocaleKey = 'en_US';
            insert user2;
        
        User user3 = new User();
            user3.LastName = 'Test3';
            user3.Alias = 'tester23';
            user3.Email = 'test23@gmail.com';
            user3.Username = 'test23@stand.com';
            user3.CommunityNickname = 'te23';
            user3.User_Time_Zone__c = 'Pst';
            user3.ProfileId = '00e54000000HSHG';           
            user3.TimeZoneSidKey    = 'America/Denver';
            user3.LocaleSidKey      = 'en_US';
            user3.EmailEncodingKey  = 'UTF-8';
            user3.LanguageLocaleKey = 'en_US';
            insert user3;

        
        Account Acc = new Account();
            Acc.Name = 'TestBoi';
            insert Acc;
        
        Contact cont = new Contact();
            cont.LastName = 'Konikkara';
            insert cont;
     
        Project__c project = new Project__c();
            project.Name = 'TestProject';
            project.EP_Project_Manager__c = user.ID;
            project.Start_Date__c = Date.newInstance(2018, 12, 9);
            project.Contact_Person__c = cont.Id;
           // project.Opportunity__c = NULL;
            project.Number__c = 2;
            insert project;
        
      Resource__c resource = new Resource__c();
            resource.User__c = user.ID;
            insert resource;  
        
      Resource__c resource1 = new Resource__c();
            resource1.User__c = user1.ID;
            insert resource1;
      
       Resource__c resource2 = new Resource__c();
            resource2.User__c = user2.ID;
            insert resource2;
        
       Resource__c resource3 = new Resource__c();
            resource2.User__c = user3.ID;
            insert resource3;
        
      Assignment__c assignment = new Assignment__c();
            assignment.Project__c = project.id;
            assignment.Resource__c = resource.id;
            assignment.Status__c = 'Approved';
            assignment.Start_Date__c = Date.newInstance(2018, 12, 9);
            assignment.End_Date__c = Date.newInstance(2019, 12, 9);
            assignment.Role__c = 'Developer';
            assignment.Billing_Type__c = 'Billable';
            assignment.Assignment_Type__c = 'Long Term';  
            assignment.RecordTypeId ='012540000018VJv';
        assignment.Budgeted_Hours__c= 40;
            //assignment.Actual_Hours__c = 20;
            insert assignment;
        
       Assignment__c assignment1 = new Assignment__c();
            assignment1.Project__c = project.id;
            assignment1.Resource__c = resource.id;
            assignment1.Status__c = 'Approved';
            assignment1.EP_Shadow_Resource__c = resource1.id;
            assignment1.Start_Date__c = Date.newInstance(2018, 12, 9);
            assignment1.End_Date__c = Date.newInstance(2019, 12, 9);
            assignment1.Role__c = 'Developer';
            assignment1.Billing_Type__c = 'Non Billable';
            assignment1.Assignment_Type__c = 'Long Term';  
            assignment1.RecordTypeId ='012540000018VLmAAM';
        assignment1.Budgeted_Hours__c= 40;
           // assignment1.Actual_Hours__c = 20;
            insert assignment1;
        	
        Assignment__c assignment3 = new Assignment__c();
            assignment3.Project__c = project.id;
            assignment3.Resource__c = resource2.id;
            assignment3.Status__c = 'Approved';
            assignment3.Start_Date__c = Date.newInstance(2018, 12, 9);
            assignment3.End_Date__c = Date.newInstance(2019, 12, 9);
            assignment3.Role__c = 'Developer';
            assignment3.Billing_Type__c = 'Billable';
            assignment3.Assignment_Type__c = 'Long Term';  
            //assignment3.RecordTypeId ='012540000018VJv';
            assignment3.Budgeted_Hours__c= 40;
            assignment3.RecordType = new RecordType();
            assignment3.RecordType.Name = 'Shadow';
            //insert assignment3;
        
        	Assignment__c assignment2 = new Assignment__c();
            assignment2.Project__c = project.id;
            //assignment2.Resource__c = resource2.id;
            assignment2.Status__c = 'Approved';
            assignment2.EP_Shadow_Resource__c = resource3.id;
            assignment2.Start_Date__c = Date.newInstance(2019, 12, 9);
            assignment2.End_Date__c = Date.newInstance(2020, 12, 9);
            assignment2.Role__c = 'Developer';
            assignment2.Billing_Type__c = 'Non Billable';
            assignment2.Assignment_Type__c = 'Long Term';  
            assignment2.RecordTypeId ='012540000018VLmAAM';
            assignment2.Budgeted_Hours__c= 40;
            assignment2.RecordType = new RecordType();
            assignment2.RecordType.Name = 'Primary';
           // insert assignment2;
    
    
        project.Name = 'RetestProject';
        assignment.Role__c = 'Consultant';
        update assignment;
        update project;
        
    try{
       System.Test.startTest();
 insert assignment3;
 insert assignment2;
 insert assignment1;
 insert assignment;
    System.Test.stopTest();
    }
        catch (Exception e) {
        
    Boolean exceptionThrown = false;
        exceptionThrown = true;
        Boolean expectedExceptionThrown =  e.getMessage().contains('We can not create a duplicate assignment, we already have an existing assignment for the selected project and selected resource') ? true : false;
System.assertEquals(expectedExceptionThrown, false);
        Boolean expectedExceptionThrown1 =  e.getMessage().contains('We can not create shadow resource allocation without the primary resource allocation') ? true : false;
System.assertEquals(expectedExceptionThrown1, false);
        Boolean expectedExceptionThrown2 =  e.getMessage().contains('We can not create shadow resource allocation which are not matching with primary allocation paramerts such as date and project and resource') ? true : false;
System.assertEquals(expectedExceptionThrown2, false);
}
    }
}